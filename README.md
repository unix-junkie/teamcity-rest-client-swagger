TeamCity REST API Client, Swagger flavour
=========================================

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Build Status](https://travis-ci.org/unix-junkie/teamcity-rest-client-swagger.svg?branch=master)](https://travis-ci.org/unix-junkie/teamcity-rest-client-swagger)
[![Build Status](https://gitlab.com/unix-junkie/teamcity-rest-client-swagger/badges/master/pipeline.svg)](https://gitlab.com/unix-junkie/teamcity-rest-client-swagger)
[![Download](https://api.bintray.com/packages/unix-junkie/maven/teamcity-rest-client-swagger/images/download.svg)](https://bintray.com/unix-junkie/maven/teamcity-rest-client-swagger)
[![JitPack](https://jitpack.io/v/unix-junkie/teamcity-rest-client-swagger.svg)](https://jitpack.io/#unix-junkie/teamcity-rest-client-swagger)

Usage example
-------------

```java
final boolean useHttpBasicAuth = ...;

final ApiClient client = new ApiClient();
client.setBasePath(System.getProperty("teamcity.url", "http://localhost:8112/bs"));

if (useHttpBasicAuth) {
        client.setUsername(System.getProperty("teamcity.user"));
        client.setPassword(System.getProperty("teamcity.password"));
} else {
        client.setAccessToken("...");
}

/*
 * List users:
 */
new UserApi(client).serveUsers(null, null).getUser().forEach(System.out::println);

/*
 * List build types:
 */
new BuildTypeApi(client).getBuildTypes("paused:false", null).getBuildType().forEach(System.out::println);
```

Optional dependencies
---------------------

1. If you're receiving `java.lang.IllegalStateException: InjectionManagerFactory not found`
   at run time (which will happen unless you're running within a container):
   ```xml
   <dependency>
   	<groupId>org.glassfish.jersey.inject</groupId>
   	<artifactId>jersey-hk2</artifactId>
   	<version>${jersey.version}</version>
   	<scope>runtime</scope>
   </dependency>
   ```

JAXB and Java 1.9+
------------------

The library includes [JAXB](https://github.com/eclipse-ee4j/jaxb-ri) for Java 1.9+
using a [_Maven profile_](https://maven.apache.org/guides/introduction/introduction-to-profiles.html)
with conditional activation. This is fine as long as clients use _Maven_, too.
For non-_Maven_ clients, you'll need to include:

 * [`javax.xml.bind:jaxb-api`](https://search.maven.org/search?q=g:javax.xml.bind%20AND%20a:jaxb-api&core=gav)
 * [`com.sun.xml.bind:jaxb-core`](https://search.maven.org/search?q=g:com.sun.xml.bind%20AND%20a:jaxb-core&core=gav)
 * [`com.sun.xml.bind:jaxb-impl`](https://search.maven.org/search?q=g:com.sun.xml.bind%20AND%20a:jaxb-impl&core=gav)
 * [`javax.activation:activation`](https://search.maven.org/search?q=g:javax.activation%20AND%20a:activation&core=gav)

For _Gradle_, this can be implemented as follows:

```kotlin
dependencies {
    if (JavaVersion.current() > JavaVersion.VERSION_1_8) {
        implementation("javax.xml.bind:jaxb-api:2.3.1")
        implementation("com.sun.xml.bind:jaxb-core:2.3.0.1")
        implementation("com.sun.xml.bind:jaxb-impl:2.3.2")
        implementation("javax.activation:activation:1.1.1")
    }
}
```
