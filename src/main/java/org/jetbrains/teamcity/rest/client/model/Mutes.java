/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.jetbrains.teamcity.rest.client.model.Mute;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import javax.xml.bind.annotation.*;

/**
 * Mutes
 */

@XmlRootElement(name = "mutes")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "mutes")
public class Mutes {
  @JsonProperty("count")
  @JacksonXmlProperty(isAttribute = true, localName = "count")
  @XmlAttribute(name = "count")
  private Integer count = null;

  @JsonProperty("nextHref")
  @JacksonXmlProperty(isAttribute = true, localName = "nextHref")
  @XmlAttribute(name = "nextHref")
  private String nextHref = null;

  @JsonProperty("prevHref")
  @JacksonXmlProperty(isAttribute = true, localName = "prevHref")
  @XmlAttribute(name = "prevHref")
  private String prevHref = null;

  @JsonProperty("default")
  @JacksonXmlProperty(localName = "default")
  @XmlElement(name = "default")
  private Boolean _default = false;

  @JsonProperty("href")
  @JacksonXmlProperty(isAttribute = true, localName = "href")
  @XmlAttribute(name = "href")
  private String href = null;

  @JsonProperty("mute")
  // Is a container wrapped=false
  // items.name=mute items.baseName=mute items.xmlName= items.xmlNamespace=
  // items.example= items.type=Mute
  @XmlElement(name = "mute")
  private java.util.List<Mute> mute = null;

  public Mutes count(Integer count) {
    this.count = count;
    return this;
  }

   /**
   * Get count
   * @return count
  **/
  @ApiModelProperty(value = "")
  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Mutes nextHref(String nextHref) {
    this.nextHref = nextHref;
    return this;
  }

   /**
   * Get nextHref
   * @return nextHref
  **/
  @ApiModelProperty(value = "")
  public String getNextHref() {
    return nextHref;
  }

  public void setNextHref(String nextHref) {
    this.nextHref = nextHref;
  }

  public Mutes prevHref(String prevHref) {
    this.prevHref = prevHref;
    return this;
  }

   /**
   * Get prevHref
   * @return prevHref
  **/
  @ApiModelProperty(value = "")
  public String getPrevHref() {
    return prevHref;
  }

  public void setPrevHref(String prevHref) {
    this.prevHref = prevHref;
  }

  public Mutes _default(Boolean _default) {
    this._default = _default;
    return this;
  }

   /**
   * Get _default
   * @return _default
  **/
  @ApiModelProperty(value = "")
  public Boolean isDefault() {
    return _default;
  }

  public void setDefault(Boolean _default) {
    this._default = _default;
  }

  public Mutes href(String href) {
    this.href = href;
    return this;
  }

   /**
   * Get href
   * @return href
  **/
  @ApiModelProperty(value = "")
  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Mutes mute(java.util.List<Mute> mute) {
    this.mute = mute;
    return this;
  }

  public Mutes addMuteItem(Mute muteItem) {
    if (this.mute == null) {
      this.mute = new java.util.ArrayList<>();
    }
    this.mute.add(muteItem);
    return this;
  }

   /**
   * Get mute
   * @return mute
  **/
  @ApiModelProperty(value = "")
  public java.util.List<Mute> getMute() {
    return mute;
  }

  public void setMute(java.util.List<Mute> mute) {
    this.mute = mute;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Mutes mutes = (Mutes) o;
    return Objects.equals(this.count, mutes.count) &&
        Objects.equals(this.nextHref, mutes.nextHref) &&
        Objects.equals(this.prevHref, mutes.prevHref) &&
        Objects.equals(this._default, mutes._default) &&
        Objects.equals(this.href, mutes.href) &&
        Objects.equals(this.mute, mutes.mute);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, nextHref, prevHref, _default, href, mute);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Mutes {\n");
    
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    nextHref: ").append(toIndentedString(nextHref)).append("\n");
    sb.append("    prevHref: ").append(toIndentedString(prevHref)).append("\n");
    sb.append("    _default: ").append(toIndentedString(_default)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    mute: ").append(toIndentedString(mute)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

