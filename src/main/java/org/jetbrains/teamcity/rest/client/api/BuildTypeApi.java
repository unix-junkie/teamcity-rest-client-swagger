package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.AgentRequirement;
import org.jetbrains.teamcity.rest.client.model.AgentRequirements;
import org.jetbrains.teamcity.rest.client.model.ArtifactDependencies;
import org.jetbrains.teamcity.rest.client.model.ArtifactDependency;
import org.jetbrains.teamcity.rest.client.model.Branches;
import org.jetbrains.teamcity.rest.client.model.Build;
import org.jetbrains.teamcity.rest.client.model.BuildType;
import org.jetbrains.teamcity.rest.client.model.BuildTypes;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.Feature;
import org.jetbrains.teamcity.rest.client.model.Features;
import java.io.File;
import org.jetbrains.teamcity.rest.client.model.Files;
import org.jetbrains.teamcity.rest.client.model.Investigations;
import org.jetbrains.teamcity.rest.client.model.Items;
import org.jetbrains.teamcity.rest.client.model.NewBuildTypeDescription;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Property;
import org.jetbrains.teamcity.rest.client.model.SnapshotDependencies;
import org.jetbrains.teamcity.rest.client.model.SnapshotDependency;
import org.jetbrains.teamcity.rest.client.model.Step;
import org.jetbrains.teamcity.rest.client.model.Steps;
import org.jetbrains.teamcity.rest.client.model.Tags;
import org.jetbrains.teamcity.rest.client.model.Trigger;
import org.jetbrains.teamcity.rest.client.model.Triggers;
import org.jetbrains.teamcity.rest.client.model.Type;
import org.jetbrains.teamcity.rest.client.model.VcsLabeling;
import org.jetbrains.teamcity.rest.client.model.VcsRootEntries;
import org.jetbrains.teamcity.rest.client.model.VcsRootEntry;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;



public class BuildTypeApi {
  private ApiClient apiClient;

  public BuildTypeApi() {
    this(Configuration.getDefaultApiClient());
  }

  public BuildTypeApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return AgentRequirement
   * @throws ApiException if fails to make API call
   */
  public AgentRequirement addAgentRequirement(String btLocator, String fields, AgentRequirement body) throws ApiException {
    return addAgentRequirementWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;AgentRequirement&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentRequirement> addAgentRequirementWithHttpInfo(String btLocator, String fields, AgentRequirement body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addAgentRequirement");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentRequirement> __localVarReturnType = new GenericType<AgentRequirement>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ArtifactDependency
   * @throws ApiException if fails to make API call
   */
  public ArtifactDependency addArtifactDep(String btLocator, String fields, ArtifactDependency body) throws ApiException {
    return addArtifactDepWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;ArtifactDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ArtifactDependency> addArtifactDepWithHttpInfo(String btLocator, String fields, ArtifactDependency body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addArtifactDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ArtifactDependency> __localVarReturnType = new GenericType<ArtifactDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType addBuildType(BuildType body, String fields) throws ApiException {
    return addBuildTypeWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> addBuildTypeWithHttpInfo(BuildType body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Feature
   * @throws ApiException if fails to make API call
   */
  public Feature addFeature(String btLocator, String fields, Feature body) throws ApiException {
    return addFeatureWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Feature&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Feature> addFeatureWithHttpInfo(String btLocator, String fields, Feature body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addFeature");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Feature> __localVarReturnType = new GenericType<Feature>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param parameterName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String addFeatureParameter(String btLocator, String featureId, String parameterName, String body) throws ApiException {
    return addFeatureParameterWithHttpInfo(btLocator, featureId, parameterName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param parameterName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> addFeatureParameterWithHttpInfo(String btLocator, String featureId, String parameterName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addFeatureParameter");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling addFeatureParameter");
    }
    
    // verify the required parameter 'parameterName' is set
    if (parameterName == null) {
      throw new ApiException(400, "Missing the required parameter 'parameterName' when calling addFeatureParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/parameters/{parameterName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()))
      .replaceAll("\\{" + "parameterName" + "\\}", apiClient.escapeString(parameterName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return SnapshotDependency
   * @throws ApiException if fails to make API call
   */
  public SnapshotDependency addSnapshotDep(String btLocator, String fields, SnapshotDependency body) throws ApiException {
    return addSnapshotDepWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;SnapshotDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<SnapshotDependency> addSnapshotDepWithHttpInfo(String btLocator, String fields, SnapshotDependency body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addSnapshotDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<SnapshotDependency> __localVarReturnType = new GenericType<SnapshotDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Step
   * @throws ApiException if fails to make API call
   */
  public Step addStep(String btLocator, String fields, Step body) throws ApiException {
    return addStepWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Step&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Step> addStepWithHttpInfo(String btLocator, String fields, Step body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addStep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Step> __localVarReturnType = new GenericType<Step>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param parameterName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String addStepParameter(String btLocator, String stepId, String parameterName, String body) throws ApiException {
    return addStepParameterWithHttpInfo(btLocator, stepId, parameterName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param parameterName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> addStepParameterWithHttpInfo(String btLocator, String stepId, String parameterName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addStepParameter");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling addStepParameter");
    }
    
    // verify the required parameter 'parameterName' is set
    if (parameterName == null) {
      throw new ApiException(400, "Missing the required parameter 'parameterName' when calling addStepParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters/{parameterName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()))
      .replaceAll("\\{" + "parameterName" + "\\}", apiClient.escapeString(parameterName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param optimizeSettings  (optional)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType addTemplate(String btLocator, BuildType body, Boolean optimizeSettings, String fields) throws ApiException {
    return addTemplateWithHttpInfo(btLocator, body, optimizeSettings, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param optimizeSettings  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> addTemplateWithHttpInfo(String btLocator, BuildType body, Boolean optimizeSettings, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "optimizeSettings", optimizeSettings));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Trigger
   * @throws ApiException if fails to make API call
   */
  public Trigger addTrigger(String btLocator, String fields, Trigger body) throws ApiException {
    return addTriggerWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Trigger&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Trigger> addTriggerWithHttpInfo(String btLocator, String fields, Trigger body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addTrigger");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Trigger> __localVarReturnType = new GenericType<Trigger>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return VcsRootEntry
   * @throws ApiException if fails to make API call
   */
  public VcsRootEntry addVcsRootEntry(String btLocator, VcsRootEntry body, String fields) throws ApiException {
    return addVcsRootEntryWithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootEntry&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootEntry> addVcsRootEntryWithHttpInfo(String btLocator, VcsRootEntry body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling addVcsRootEntry");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootEntry> __localVarReturnType = new GenericType<VcsRootEntry>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String changeArtifactDepSetting(String btLocator, String artifactDepLocator, String fieldName, String body) throws ApiException {
    return changeArtifactDepSettingWithHttpInfo(btLocator, artifactDepLocator, fieldName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> changeArtifactDepSettingWithHttpInfo(String btLocator, String artifactDepLocator, String fieldName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling changeArtifactDepSetting");
    }
    
    // verify the required parameter 'artifactDepLocator' is set
    if (artifactDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'artifactDepLocator' when calling changeArtifactDepSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling changeArtifactDepSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "artifactDepLocator" + "\\}", apiClient.escapeString(artifactDepLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String changeFeatureSetting(String btLocator, String featureId, String name, String body) throws ApiException {
    return changeFeatureSettingWithHttpInfo(btLocator, featureId, name, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> changeFeatureSettingWithHttpInfo(String btLocator, String featureId, String name, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling changeFeatureSetting");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling changeFeatureSetting");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling changeFeatureSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/{name}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String changeRequirementSetting(String btLocator, String agentRequirementLocator, String fieldName, String body) throws ApiException {
    return changeRequirementSettingWithHttpInfo(btLocator, agentRequirementLocator, fieldName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> changeRequirementSettingWithHttpInfo(String btLocator, String agentRequirementLocator, String fieldName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling changeRequirementSetting");
    }
    
    // verify the required parameter 'agentRequirementLocator' is set
    if (agentRequirementLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentRequirementLocator' when calling changeRequirementSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling changeRequirementSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "agentRequirementLocator" + "\\}", apiClient.escapeString(agentRequirementLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String changeStepSetting(String btLocator, String stepId, String fieldName, String body) throws ApiException {
    return changeStepSettingWithHttpInfo(btLocator, stepId, fieldName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> changeStepSettingWithHttpInfo(String btLocator, String stepId, String fieldName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling changeStepSetting");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling changeStepSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling changeStepSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String changeTriggerSetting(String btLocator, String triggerLocator, String fieldName, String body) throws ApiException {
    return changeTriggerSettingWithHttpInfo(btLocator, triggerLocator, fieldName, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fieldName  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> changeTriggerSettingWithHttpInfo(String btLocator, String triggerLocator, String fieldName, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling changeTriggerSetting");
    }
    
    // verify the required parameter 'triggerLocator' is set
    if (triggerLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'triggerLocator' when calling changeTriggerSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling changeTriggerSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "triggerLocator" + "\\}", apiClient.escapeString(triggerLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAgentRequirement(String btLocator, String agentRequirementLocator) throws ApiException {

    deleteAgentRequirementWithHttpInfo(btLocator, agentRequirementLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAgentRequirementWithHttpInfo(String btLocator, String agentRequirementLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteAgentRequirement");
    }
    
    // verify the required parameter 'agentRequirementLocator' is set
    if (agentRequirementLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentRequirementLocator' when calling deleteAgentRequirement");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "agentRequirementLocator" + "\\}", apiClient.escapeString(agentRequirementLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllParameters(String btLocator) throws ApiException {

    deleteAllParametersWithHttpInfo(btLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllParametersWithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteAllParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllParameters_0(String btLocator) throws ApiException {

    deleteAllParameters_0WithHttpInfo(btLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllParameters_0WithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteAllParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteArtifactDep(String btLocator, String artifactDepLocator) throws ApiException {

    deleteArtifactDepWithHttpInfo(btLocator, artifactDepLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteArtifactDepWithHttpInfo(String btLocator, String artifactDepLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteArtifactDep");
    }
    
    // verify the required parameter 'artifactDepLocator' is set
    if (artifactDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'artifactDepLocator' when calling deleteArtifactDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "artifactDepLocator" + "\\}", apiClient.escapeString(artifactDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteBuildType(String btLocator) throws ApiException {

    deleteBuildTypeWithHttpInfo(btLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteBuildTypeWithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteBuildType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteFeature(String btLocator, String featureId) throws ApiException {

    deleteFeatureWithHttpInfo(btLocator, featureId);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteFeatureWithHttpInfo(String btLocator, String featureId) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteFeature");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling deleteFeature");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter(String name, String btLocator) throws ApiException {

    deleteParameterWithHttpInfo(name, btLocator);
  }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameterWithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter_0(String name, String btLocator) throws ApiException {

    deleteParameter_0WithHttpInfo(name, btLocator);
  }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameter_0WithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter_0");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteSnapshotDep(String btLocator, String snapshotDepLocator) throws ApiException {

    deleteSnapshotDepWithHttpInfo(btLocator, snapshotDepLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteSnapshotDepWithHttpInfo(String btLocator, String snapshotDepLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteSnapshotDep");
    }
    
    // verify the required parameter 'snapshotDepLocator' is set
    if (snapshotDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'snapshotDepLocator' when calling deleteSnapshotDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "snapshotDepLocator" + "\\}", apiClient.escapeString(snapshotDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteStep(String btLocator, String stepId) throws ApiException {

    deleteStepWithHttpInfo(btLocator, stepId);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteStepWithHttpInfo(String btLocator, String stepId) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteStep");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling deleteStep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteTrigger(String btLocator, String triggerLocator) throws ApiException {

    deleteTriggerWithHttpInfo(btLocator, triggerLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteTriggerWithHttpInfo(String btLocator, String triggerLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteTrigger");
    }
    
    // verify the required parameter 'triggerLocator' is set
    if (triggerLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'triggerLocator' when calling deleteTrigger");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "triggerLocator" + "\\}", apiClient.escapeString(triggerLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteVcsRootEntry(String btLocator, String vcsRootLocator) throws ApiException {

    deleteVcsRootEntryWithHttpInfo(btLocator, vcsRootLocator);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteVcsRootEntryWithHttpInfo(String btLocator, String vcsRootLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling deleteVcsRootEntry");
    }
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling deleteVcsRootEntry");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fields  (optional)
   * @return AgentRequirement
   * @throws ApiException if fails to make API call
   */
  public AgentRequirement getAgentRequirement(String btLocator, String agentRequirementLocator, String fields) throws ApiException {
    return getAgentRequirementWithHttpInfo(btLocator, agentRequirementLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentRequirement&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentRequirement> getAgentRequirementWithHttpInfo(String btLocator, String agentRequirementLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getAgentRequirement");
    }
    
    // verify the required parameter 'agentRequirementLocator' is set
    if (agentRequirementLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentRequirementLocator' when calling getAgentRequirement");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "agentRequirementLocator" + "\\}", apiClient.escapeString(agentRequirementLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentRequirement> __localVarReturnType = new GenericType<AgentRequirement>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return AgentRequirements
   * @throws ApiException if fails to make API call
   */
  public AgentRequirements getAgentRequirements(String btLocator, String fields) throws ApiException {
    return getAgentRequirementsWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentRequirements&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentRequirements> getAgentRequirementsWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getAgentRequirements");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentRequirements> __localVarReturnType = new GenericType<AgentRequirements>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return Items
   * @throws ApiException if fails to make API call
   */
  public Items getAliases(String btLocator, String field) throws ApiException {
    return getAliasesWithHttpInfo(btLocator, field).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;Items&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Items> getAliasesWithHttpInfo(String btLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getAliases");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling getAliases");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/aliases"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Items> __localVarReturnType = new GenericType<Items>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fields  (optional)
   * @return ArtifactDependency
   * @throws ApiException if fails to make API call
   */
  public ArtifactDependency getArtifactDep(String btLocator, String artifactDepLocator, String fields) throws ApiException {
    return getArtifactDepWithHttpInfo(btLocator, artifactDepLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;ArtifactDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ArtifactDependency> getArtifactDepWithHttpInfo(String btLocator, String artifactDepLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getArtifactDep");
    }
    
    // verify the required parameter 'artifactDepLocator' is set
    if (artifactDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'artifactDepLocator' when calling getArtifactDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "artifactDepLocator" + "\\}", apiClient.escapeString(artifactDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ArtifactDependency> __localVarReturnType = new GenericType<ArtifactDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fieldName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getArtifactDepSetting(String btLocator, String artifactDepLocator, String fieldName) throws ApiException {
    return getArtifactDepSettingWithHttpInfo(btLocator, artifactDepLocator, fieldName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fieldName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getArtifactDepSettingWithHttpInfo(String btLocator, String artifactDepLocator, String fieldName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getArtifactDepSetting");
    }
    
    // verify the required parameter 'artifactDepLocator' is set
    if (artifactDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'artifactDepLocator' when calling getArtifactDepSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling getArtifactDepSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "artifactDepLocator" + "\\}", apiClient.escapeString(artifactDepLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ArtifactDependencies
   * @throws ApiException if fails to make API call
   */
  public ArtifactDependencies getArtifactDeps(String btLocator, String fields) throws ApiException {
    return getArtifactDepsWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;ArtifactDependencies&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ArtifactDependencies> getArtifactDepsWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getArtifactDeps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ArtifactDependencies> __localVarReturnType = new GenericType<ArtifactDependencies>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes getBuildTypes(String locator, String fields) throws ApiException {
    return getBuildTypesWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> getBuildTypesWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildren(String path, String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    return getChildrenWithHttpInfo(path, btLocator, basePath, locator, fields, resolveParameters).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenWithHttpInfo(String path, String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildren");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getChildren");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/children{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildrenAlias(String path, String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    return getChildrenAliasWithHttpInfo(path, btLocator, basePath, locator, fields, resolveParameters).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenAliasWithHttpInfo(String path, String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildrenAlias");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getChildrenAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param responseBuilder  (optional)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContent(String path, String btLocator, String responseBuilder, Boolean resolveParameters) throws ApiException {

    getContentWithHttpInfo(path, btLocator, responseBuilder, resolveParameters);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param responseBuilder  (optional)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentWithHttpInfo(String path, String btLocator, String responseBuilder, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContent");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getContent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/content{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "responseBuilder", responseBuilder));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContentAlias(String path, String btLocator, Boolean resolveParameters) throws ApiException {

    getContentAliasWithHttpInfo(path, btLocator, resolveParameters);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentAliasWithHttpInfo(String path, String btLocator, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContentAlias");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getContentAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/files{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstances getCurrentVcsInstances(String btLocator, String fields) throws ApiException {
    return getCurrentVcsInstancesWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstances> getCurrentVcsInstancesWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getCurrentVcsInstances");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcsRootInstances"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   * @deprecated
   */
  @Deprecated
  public VcsRootInstances getCurrentVcsInstancesObsolete(String btLocator, String fields) throws ApiException {
    return getCurrentVcsInstancesObsoleteWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   * @deprecated
   */
  @Deprecated
  public ApiResponse<VcsRootInstances> getCurrentVcsInstancesObsoleteWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getCurrentVcsInstancesObsolete");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-instances"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @return NewBuildTypeDescription
   * @throws ApiException if fails to make API call
   */
  public NewBuildTypeDescription getExampleNewProjectDescription(String btLocator) throws ApiException {
    return getExampleNewProjectDescriptionWithHttpInfo(btLocator).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @return ApiResponse&lt;NewBuildTypeDescription&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<NewBuildTypeDescription> getExampleNewProjectDescriptionWithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getExampleNewProjectDescription");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/example/newBuildTypeDescription"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<NewBuildTypeDescription> __localVarReturnType = new GenericType<NewBuildTypeDescription>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @return NewBuildTypeDescription
   * @throws ApiException if fails to make API call
   */
  public NewBuildTypeDescription getExampleNewProjectDescriptionCompatibilityVersion1(String btLocator) throws ApiException {
    return getExampleNewProjectDescriptionCompatibilityVersion1WithHttpInfo(btLocator).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @return ApiResponse&lt;NewBuildTypeDescription&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<NewBuildTypeDescription> getExampleNewProjectDescriptionCompatibilityVersion1WithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getExampleNewProjectDescriptionCompatibilityVersion1");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/newBuildTypeDescription"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<NewBuildTypeDescription> __localVarReturnType = new GenericType<NewBuildTypeDescription>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @return Feature
   * @throws ApiException if fails to make API call
   */
  public Feature getFeature(String btLocator, String featureId, String fields) throws ApiException {
    return getFeatureWithHttpInfo(btLocator, featureId, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Feature&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Feature> getFeatureWithHttpInfo(String btLocator, String featureId, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getFeature");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling getFeature");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Feature> __localVarReturnType = new GenericType<Feature>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param parameterName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getFeatureParameter(String btLocator, String featureId, String parameterName) throws ApiException {
    return getFeatureParameterWithHttpInfo(btLocator, featureId, parameterName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param parameterName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getFeatureParameterWithHttpInfo(String btLocator, String featureId, String parameterName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getFeatureParameter");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling getFeatureParameter");
    }
    
    // verify the required parameter 'parameterName' is set
    if (parameterName == null) {
      throw new ApiException(400, "Missing the required parameter 'parameterName' when calling getFeatureParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/parameters/{parameterName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()))
      .replaceAll("\\{" + "parameterName" + "\\}", apiClient.escapeString(parameterName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getFeatureParameters(String btLocator, String featureId, String fields) throws ApiException {
    return getFeatureParametersWithHttpInfo(btLocator, featureId, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getFeatureParametersWithHttpInfo(String btLocator, String featureId, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getFeatureParameters");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling getFeatureParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param name  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getFeatureSetting(String btLocator, String featureId, String name) throws ApiException {
    return getFeatureSettingWithHttpInfo(btLocator, featureId, name).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param name  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getFeatureSettingWithHttpInfo(String btLocator, String featureId, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getFeatureSetting");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling getFeatureSetting");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getFeatureSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/{name}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Features
   * @throws ApiException if fails to make API call
   */
  public Features getFeatures(String btLocator, String fields) throws ApiException {
    return getFeaturesWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Features&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Features> getFeaturesWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getFeatures");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Features> __localVarReturnType = new GenericType<Features>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Investigations
   * @throws ApiException if fails to make API call
   */
  public Investigations getInvestigations(String btLocator, String fields) throws ApiException {
    return getInvestigationsWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigations&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigations> getInvestigationsWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getInvestigations");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/investigations"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigations> __localVarReturnType = new GenericType<Investigations>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getMetadata(String path, String btLocator, String fields, Boolean resolveParameters) throws ApiException {
    return getMetadataWithHttpInfo(path, btLocator, fields, resolveParameters).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getMetadataWithHttpInfo(String path, String btLocator, String fields, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getMetadata");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getMetadata");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/metadata{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<File> __localVarReturnType = new GenericType<File>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property getParameter(String name, String btLocator, String fields) throws ApiException {
    return getParameterWithHttpInfo(name, btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> getParameterWithHttpInfo(String name, String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameter");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return Type
   * @throws ApiException if fails to make API call
   */
  public Type getParameterType(String name, String btLocator) throws ApiException {
    return getParameterTypeWithHttpInfo(name, btLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return ApiResponse&lt;Type&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Type> getParameterTypeWithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterType");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameterType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/type"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Type> __localVarReturnType = new GenericType<Type>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterTypeRawValue(String name, String btLocator) throws ApiException {
    return getParameterTypeRawValueWithHttpInfo(name, btLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterTypeRawValueWithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterTypeRawValue");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameterTypeRawValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/type/rawValue"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterValueLong(String name, String btLocator) throws ApiException {
    return getParameterValueLongWithHttpInfo(name, btLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterValueLongWithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterValueLong");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterValueLong_0(String name, String btLocator) throws ApiException {
    return getParameterValueLong_0WithHttpInfo(name, btLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterValueLong_0WithHttpInfo(String name, String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterValueLong_0");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameterValueLong_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property getParameter_0(String name, String btLocator, String fields) throws ApiException {
    return getParameter_0WithHttpInfo(name, btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> getParameter_0WithHttpInfo(String name, String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameter_0");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getParameters(String btLocator, String locator, String fields) throws ApiException {
    return getParametersWithHttpInfo(btLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getParametersWithHttpInfo(String btLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getParameters_0(String btLocator, String locator, String fields) throws ApiException {
    return getParameters_0WithHttpInfo(btLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getParameters_0WithHttpInfo(String btLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fieldName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getRequirementSetting(String btLocator, String agentRequirementLocator, String fieldName) throws ApiException {
    return getRequirementSettingWithHttpInfo(btLocator, agentRequirementLocator, fieldName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fieldName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getRequirementSettingWithHttpInfo(String btLocator, String agentRequirementLocator, String fieldName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getRequirementSetting");
    }
    
    // verify the required parameter 'agentRequirementLocator' is set
    if (agentRequirementLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentRequirementLocator' when calling getRequirementSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling getRequirementSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "agentRequirementLocator" + "\\}", apiClient.escapeString(agentRequirementLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getRoot(String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    return getRootWithHttpInfo(btLocator, basePath, locator, fields, resolveParameters).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param resolveParameters  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getRootWithHttpInfo(String btLocator, String basePath, String locator, String fields, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getSettingsFile(String btLocator) throws ApiException {
    return getSettingsFileWithHttpInfo(btLocator).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getSettingsFileWithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getSettingsFile");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settingsFile"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @param fields  (optional)
   * @return SnapshotDependency
   * @throws ApiException if fails to make API call
   */
  public SnapshotDependency getSnapshotDep(String btLocator, String snapshotDepLocator, String fields) throws ApiException {
    return getSnapshotDepWithHttpInfo(btLocator, snapshotDepLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;SnapshotDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<SnapshotDependency> getSnapshotDepWithHttpInfo(String btLocator, String snapshotDepLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getSnapshotDep");
    }
    
    // verify the required parameter 'snapshotDepLocator' is set
    if (snapshotDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'snapshotDepLocator' when calling getSnapshotDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "snapshotDepLocator" + "\\}", apiClient.escapeString(snapshotDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<SnapshotDependency> __localVarReturnType = new GenericType<SnapshotDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return SnapshotDependencies
   * @throws ApiException if fails to make API call
   */
  public SnapshotDependencies getSnapshotDeps(String btLocator, String fields) throws ApiException {
    return getSnapshotDepsWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;SnapshotDependencies&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<SnapshotDependencies> getSnapshotDepsWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getSnapshotDeps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<SnapshotDependencies> __localVarReturnType = new GenericType<SnapshotDependencies>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @return Step
   * @throws ApiException if fails to make API call
   */
  public Step getStep(String btLocator, String stepId, String fields) throws ApiException {
    return getStepWithHttpInfo(btLocator, stepId, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Step&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Step> getStepWithHttpInfo(String btLocator, String stepId, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getStep");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling getStep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Step> __localVarReturnType = new GenericType<Step>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param parameterName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getStepParameter(String btLocator, String stepId, String parameterName) throws ApiException {
    return getStepParameterWithHttpInfo(btLocator, stepId, parameterName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param parameterName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getStepParameterWithHttpInfo(String btLocator, String stepId, String parameterName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getStepParameter");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling getStepParameter");
    }
    
    // verify the required parameter 'parameterName' is set
    if (parameterName == null) {
      throw new ApiException(400, "Missing the required parameter 'parameterName' when calling getStepParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters/{parameterName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()))
      .replaceAll("\\{" + "parameterName" + "\\}", apiClient.escapeString(parameterName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getStepParameters(String btLocator, String stepId, String fields) throws ApiException {
    return getStepParametersWithHttpInfo(btLocator, stepId, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getStepParametersWithHttpInfo(String btLocator, String stepId, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getStepParameters");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling getStepParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fieldName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getStepSetting(String btLocator, String stepId, String fieldName) throws ApiException {
    return getStepSettingWithHttpInfo(btLocator, stepId, fieldName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fieldName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getStepSettingWithHttpInfo(String btLocator, String stepId, String fieldName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getStepSetting");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling getStepSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling getStepSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Steps
   * @throws ApiException if fails to make API call
   */
  public Steps getSteps(String btLocator, String fields) throws ApiException {
    return getStepsWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Steps&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Steps> getStepsWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getSteps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Steps> __localVarReturnType = new GenericType<Steps>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param templateLocator  (required)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType getTemplate(String btLocator, String templateLocator, String fields) throws ApiException {
    return getTemplateWithHttpInfo(btLocator, templateLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param templateLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> getTemplateWithHttpInfo(String btLocator, String templateLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getTemplate");
    }
    
    // verify the required parameter 'templateLocator' is set
    if (templateLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'templateLocator' when calling getTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates/{templateLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "templateLocator" + "\\}", apiClient.escapeString(templateLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes getTemplates(String btLocator, String fields) throws ApiException {
    return getTemplatesWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> getTemplatesWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getTemplates");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fields  (optional)
   * @return Trigger
   * @throws ApiException if fails to make API call
   */
  public Trigger getTrigger(String btLocator, String triggerLocator, String fields) throws ApiException {
    return getTriggerWithHttpInfo(btLocator, triggerLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Trigger&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Trigger> getTriggerWithHttpInfo(String btLocator, String triggerLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getTrigger");
    }
    
    // verify the required parameter 'triggerLocator' is set
    if (triggerLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'triggerLocator' when calling getTrigger");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "triggerLocator" + "\\}", apiClient.escapeString(triggerLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Trigger> __localVarReturnType = new GenericType<Trigger>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fieldName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getTriggerSetting(String btLocator, String triggerLocator, String fieldName) throws ApiException {
    return getTriggerSettingWithHttpInfo(btLocator, triggerLocator, fieldName).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fieldName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getTriggerSettingWithHttpInfo(String btLocator, String triggerLocator, String fieldName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getTriggerSetting");
    }
    
    // verify the required parameter 'triggerLocator' is set
    if (triggerLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'triggerLocator' when calling getTriggerSetting");
    }
    
    // verify the required parameter 'fieldName' is set
    if (fieldName == null) {
      throw new ApiException(400, "Missing the required parameter 'fieldName' when calling getTriggerSetting");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}/{fieldName}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "triggerLocator" + "\\}", apiClient.escapeString(triggerLocator.toString()))
      .replaceAll("\\{" + "fieldName" + "\\}", apiClient.escapeString(fieldName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return Triggers
   * @throws ApiException if fails to make API call
   */
  public Triggers getTriggers(String btLocator, String fields) throws ApiException {
    return getTriggersWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Triggers&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Triggers> getTriggersWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getTriggers");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Triggers> __localVarReturnType = new GenericType<Triggers>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @return VcsLabeling
   * @throws ApiException if fails to make API call
   */
  public VcsLabeling getVCSLabelingOptions(String btLocator) throws ApiException {
    return getVCSLabelingOptionsWithHttpInfo(btLocator).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @return ApiResponse&lt;VcsLabeling&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsLabeling> getVCSLabelingOptionsWithHttpInfo(String btLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getVCSLabelingOptions");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcsLabeling"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsLabeling> __localVarReturnType = new GenericType<VcsLabeling>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return VcsRootEntries
   * @throws ApiException if fails to make API call
   */
  public VcsRootEntries getVcsRootEntries(String btLocator, String fields) throws ApiException {
    return getVcsRootEntriesWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootEntries&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootEntries> getVcsRootEntriesWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getVcsRootEntries");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootEntries> __localVarReturnType = new GenericType<VcsRootEntries>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return VcsRootEntry
   * @throws ApiException if fails to make API call
   */
  public VcsRootEntry getVcsRootEntry(String btLocator, String vcsRootLocator, String fields) throws ApiException {
    return getVcsRootEntryWithHttpInfo(btLocator, vcsRootLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootEntry&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootEntry> getVcsRootEntryWithHttpInfo(String btLocator, String vcsRootLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getVcsRootEntry");
    }
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling getVcsRootEntry");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootEntry> __localVarReturnType = new GenericType<VcsRootEntry>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getVcsRootEntryCheckoutRules(String btLocator, String vcsRootLocator) throws ApiException {
    return getVcsRootEntryCheckoutRulesWithHttpInfo(btLocator, vcsRootLocator).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getVcsRootEntryCheckoutRulesWithHttpInfo(String btLocator, String vcsRootLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getVcsRootEntryCheckoutRules");
    }
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling getVcsRootEntryCheckoutRules");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}/checkout-rules"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getZipped(String path, String btLocator, String basePath, String locator, String name, Boolean resolveParameters) throws ApiException {

    getZippedWithHttpInfo(path, btLocator, basePath, locator, name, resolveParameters);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param btLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @param resolveParameters  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getZippedWithHttpInfo(String path, String btLocator, String basePath, String locator, String name, Boolean resolveParameters) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getZipped");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling getZipped");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs/files/latest/archived{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "resolveParameters", resolveParameters));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param inlineSettings  (optional)
   * @throws ApiException if fails to make API call
   */
  public void removeAllTemplates(String btLocator, Boolean inlineSettings) throws ApiException {

    removeAllTemplatesWithHttpInfo(btLocator, inlineSettings);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param inlineSettings  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeAllTemplatesWithHttpInfo(String btLocator, Boolean inlineSettings) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling removeAllTemplates");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "inlineSettings", inlineSettings));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param templateLocator  (required)
   * @param inlineSettings  (optional)
   * @throws ApiException if fails to make API call
   */
  public void removeTemplate(String btLocator, String templateLocator, Boolean inlineSettings) throws ApiException {

    removeTemplateWithHttpInfo(btLocator, templateLocator, inlineSettings);
  }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param templateLocator  (required)
   * @param inlineSettings  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeTemplateWithHttpInfo(String btLocator, String templateLocator, Boolean inlineSettings) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling removeTemplate");
    }
    
    // verify the required parameter 'templateLocator' is set
    if (templateLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'templateLocator' when calling removeTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates/{templateLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "templateLocator" + "\\}", apiClient.escapeString(templateLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "inlineSettings", inlineSettings));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return AgentRequirement
   * @throws ApiException if fails to make API call
   */
  public AgentRequirement replaceAgentRequirement(String btLocator, String agentRequirementLocator, String fields, AgentRequirement body) throws ApiException {
    return replaceAgentRequirementWithHttpInfo(btLocator, agentRequirementLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param agentRequirementLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;AgentRequirement&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentRequirement> replaceAgentRequirementWithHttpInfo(String btLocator, String agentRequirementLocator, String fields, AgentRequirement body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceAgentRequirement");
    }
    
    // verify the required parameter 'agentRequirementLocator' is set
    if (agentRequirementLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentRequirementLocator' when calling replaceAgentRequirement");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "agentRequirementLocator" + "\\}", apiClient.escapeString(agentRequirementLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentRequirement> __localVarReturnType = new GenericType<AgentRequirement>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return AgentRequirements
   * @throws ApiException if fails to make API call
   */
  public AgentRequirements replaceAgentRequirements(String btLocator, String fields, AgentRequirements body) throws ApiException {
    return replaceAgentRequirementsWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;AgentRequirements&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentRequirements> replaceAgentRequirementsWithHttpInfo(String btLocator, String fields, AgentRequirements body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceAgentRequirements");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/agent-requirements"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentRequirements> __localVarReturnType = new GenericType<AgentRequirements>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ArtifactDependency
   * @throws ApiException if fails to make API call
   */
  public ArtifactDependency replaceArtifactDep(String btLocator, String artifactDepLocator, String fields, ArtifactDependency body) throws ApiException {
    return replaceArtifactDepWithHttpInfo(btLocator, artifactDepLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param artifactDepLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;ArtifactDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ArtifactDependency> replaceArtifactDepWithHttpInfo(String btLocator, String artifactDepLocator, String fields, ArtifactDependency body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceArtifactDep");
    }
    
    // verify the required parameter 'artifactDepLocator' is set
    if (artifactDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'artifactDepLocator' when calling replaceArtifactDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "artifactDepLocator" + "\\}", apiClient.escapeString(artifactDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ArtifactDependency> __localVarReturnType = new GenericType<ArtifactDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ArtifactDependencies
   * @throws ApiException if fails to make API call
   */
  public ArtifactDependencies replaceArtifactDeps(String btLocator, String fields, ArtifactDependencies body) throws ApiException {
    return replaceArtifactDepsWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;ArtifactDependencies&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ArtifactDependencies> replaceArtifactDepsWithHttpInfo(String btLocator, String fields, ArtifactDependencies body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceArtifactDeps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/artifact-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ArtifactDependencies> __localVarReturnType = new GenericType<ArtifactDependencies>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Feature
   * @throws ApiException if fails to make API call
   */
  public Feature replaceFeature(String btLocator, String featureId, String fields, Feature body) throws ApiException {
    return replaceFeatureWithHttpInfo(btLocator, featureId, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Feature&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Feature> replaceFeatureWithHttpInfo(String btLocator, String featureId, String fields, Feature body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceFeature");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling replaceFeature");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Feature> __localVarReturnType = new GenericType<Feature>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties replaceFeatureParameters(String btLocator, String featureId, Properties body, String fields) throws ApiException {
    return replaceFeatureParametersWithHttpInfo(btLocator, featureId, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param featureId  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> replaceFeatureParametersWithHttpInfo(String btLocator, String featureId, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceFeatureParameters");
    }
    
    // verify the required parameter 'featureId' is set
    if (featureId == null) {
      throw new ApiException(400, "Missing the required parameter 'featureId' when calling replaceFeatureParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features/{featureId}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "featureId" + "\\}", apiClient.escapeString(featureId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Features
   * @throws ApiException if fails to make API call
   */
  public Features replaceFeatures(String btLocator, String fields, Features body) throws ApiException {
    return replaceFeaturesWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Features&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Features> replaceFeaturesWithHttpInfo(String btLocator, String fields, Features body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceFeatures");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/features"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Features> __localVarReturnType = new GenericType<Features>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return SnapshotDependency
   * @throws ApiException if fails to make API call
   */
  public SnapshotDependency replaceSnapshotDep(String btLocator, String snapshotDepLocator, String fields, SnapshotDependency body) throws ApiException {
    return replaceSnapshotDepWithHttpInfo(btLocator, snapshotDepLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param snapshotDepLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;SnapshotDependency&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<SnapshotDependency> replaceSnapshotDepWithHttpInfo(String btLocator, String snapshotDepLocator, String fields, SnapshotDependency body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceSnapshotDep");
    }
    
    // verify the required parameter 'snapshotDepLocator' is set
    if (snapshotDepLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'snapshotDepLocator' when calling replaceSnapshotDep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "snapshotDepLocator" + "\\}", apiClient.escapeString(snapshotDepLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<SnapshotDependency> __localVarReturnType = new GenericType<SnapshotDependency>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return SnapshotDependencies
   * @throws ApiException if fails to make API call
   */
  public SnapshotDependencies replaceSnapshotDeps(String btLocator, String fields, SnapshotDependencies body) throws ApiException {
    return replaceSnapshotDepsWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;SnapshotDependencies&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<SnapshotDependencies> replaceSnapshotDepsWithHttpInfo(String btLocator, String fields, SnapshotDependencies body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceSnapshotDeps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/snapshot-dependencies"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<SnapshotDependencies> __localVarReturnType = new GenericType<SnapshotDependencies>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Step
   * @throws ApiException if fails to make API call
   */
  public Step replaceStep(String btLocator, String stepId, String fields, Step body) throws ApiException {
    return replaceStepWithHttpInfo(btLocator, stepId, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Step&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Step> replaceStepWithHttpInfo(String btLocator, String stepId, String fields, Step body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceStep");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling replaceStep");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Step> __localVarReturnType = new GenericType<Step>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties replaceStepParameters(String btLocator, String stepId, Properties body, String fields) throws ApiException {
    return replaceStepParametersWithHttpInfo(btLocator, stepId, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param stepId  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> replaceStepParametersWithHttpInfo(String btLocator, String stepId, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceStepParameters");
    }
    
    // verify the required parameter 'stepId' is set
    if (stepId == null) {
      throw new ApiException(400, "Missing the required parameter 'stepId' when calling replaceStepParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "stepId" + "\\}", apiClient.escapeString(stepId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Steps
   * @throws ApiException if fails to make API call
   */
  public Steps replaceSteps(String btLocator, String fields, Steps body) throws ApiException {
    return replaceStepsWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Steps&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Steps> replaceStepsWithHttpInfo(String btLocator, String fields, Steps body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceSteps");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/steps"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Steps> __localVarReturnType = new GenericType<Steps>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Trigger
   * @throws ApiException if fails to make API call
   */
  public Trigger replaceTrigger(String btLocator, String triggerLocator, String fields, Trigger body) throws ApiException {
    return replaceTriggerWithHttpInfo(btLocator, triggerLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param triggerLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Trigger&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Trigger> replaceTriggerWithHttpInfo(String btLocator, String triggerLocator, String fields, Trigger body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceTrigger");
    }
    
    // verify the required parameter 'triggerLocator' is set
    if (triggerLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'triggerLocator' when calling replaceTrigger");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "triggerLocator" + "\\}", apiClient.escapeString(triggerLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Trigger> __localVarReturnType = new GenericType<Trigger>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return Triggers
   * @throws ApiException if fails to make API call
   */
  public Triggers replaceTriggers(String btLocator, String fields, Triggers body) throws ApiException {
    return replaceTriggersWithHttpInfo(btLocator, fields, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;Triggers&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Triggers> replaceTriggersWithHttpInfo(String btLocator, String fields, Triggers body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceTriggers");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/triggers"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Triggers> __localVarReturnType = new GenericType<Triggers>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return VcsRootEntries
   * @throws ApiException if fails to make API call
   */
  public VcsRootEntries replaceVcsRootEntries(String btLocator, VcsRootEntries body, String fields) throws ApiException {
    return replaceVcsRootEntriesWithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootEntries&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootEntries> replaceVcsRootEntriesWithHttpInfo(String btLocator, VcsRootEntries body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling replaceVcsRootEntries");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootEntries> __localVarReturnType = new GenericType<VcsRootEntries>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Branches
   * @throws ApiException if fails to make API call
   */
  public Branches serveBranches(String btLocator, String locator, String fields) throws ApiException {
    return serveBranchesWithHttpInfo(btLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Branches&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Branches> serveBranchesWithHttpInfo(String btLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBranches");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/branches"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Branches> __localVarReturnType = new GenericType<Branches>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildField(String btLocator, String buildLocator, String field) throws ApiException {
    return serveBuildFieldWithHttpInfo(btLocator, buildLocator, field).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildFieldWithHttpInfo(String btLocator, String buildLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildField");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/builds/{buildLocator}/{field}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags serveBuildTypeBuildsTags(String btLocator, String field) throws ApiException {
    return serveBuildTypeBuildsTagsWithHttpInfo(btLocator, field).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> serveBuildTypeBuildsTagsWithHttpInfo(String btLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildTypeBuildsTags");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildTypeBuildsTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/buildTags"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildTypeField(String btLocator, String field) throws ApiException {
    return serveBuildTypeFieldWithHttpInfo(btLocator, field).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildTypeFieldWithHttpInfo(String btLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildTypeField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildTypeField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/{field}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType serveBuildTypeXML(String btLocator, String fields) throws ApiException {
    return serveBuildTypeXMLWithHttpInfo(btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> serveBuildTypeXMLWithHttpInfo(String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildTypeXML");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build serveBuildWithProject(String btLocator, String buildLocator, String fields) throws ApiException {
    return serveBuildWithProjectWithHttpInfo(btLocator, buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> serveBuildWithProjectWithHttpInfo(String btLocator, String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildWithProject");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildWithProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/builds/{buildLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds serveBuilds(String btLocator, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    return serveBuildsWithHttpInfo(btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> serveBuildsWithHttpInfo(String btLocator, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuilds");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/builds"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "status", status));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "triggeredByUser", triggeredByUser));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includePersonal", includePersonal));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeCanceled", includeCanceled));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "onlyPinned", onlyPinned));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("multi", "tag", tag));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "agentName", agentName));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceBuild", sinceBuild));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceDate", sinceDate));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "start", start));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "count", count));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setBuildTypeField(String btLocator, String field, String body) throws ApiException {
    return setBuildTypeFieldWithHttpInfo(btLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setBuildTypeFieldWithHttpInfo(String btLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setBuildTypeField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setBuildTypeField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/{field}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter(String btLocator, Property body, String fields) throws ApiException {
    return setParameterWithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameterWithHttpInfo(String btLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return Type
   * @throws ApiException if fails to make API call
   */
  public Type setParameterType(String name, String btLocator, Type body) throws ApiException {
    return setParameterTypeWithHttpInfo(name, btLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Type&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Type> setParameterTypeWithHttpInfo(String name, String btLocator, Type body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterType");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameterType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/type"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Type> __localVarReturnType = new GenericType<Type>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterTypeRawValue(String name, String btLocator, String body) throws ApiException {
    return setParameterTypeRawValueWithHttpInfo(name, btLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterTypeRawValueWithHttpInfo(String name, String btLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterTypeRawValue");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameterTypeRawValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/type/rawValue"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterValueLong(String name, String btLocator, String body) throws ApiException {
    return setParameterValueLongWithHttpInfo(name, btLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterValueLongWithHttpInfo(String name, String btLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterValueLong");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterValueLong_0(String name, String btLocator, String body) throws ApiException {
    return setParameterValueLong_0WithHttpInfo(name, btLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterValueLong_0WithHttpInfo(String name, String btLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterValueLong_0");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameterValueLong_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_0(String name, String btLocator, Property body, String fields) throws ApiException {
    return setParameter_0WithHttpInfo(name, btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_0WithHttpInfo(String name, String btLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameter_0");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_1(String btLocator, Property body, String fields) throws ApiException {
    return setParameter_1WithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_1WithHttpInfo(String btLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameter_1");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_2(String name, String btLocator, Property body, String fields) throws ApiException {
    return setParameter_2WithHttpInfo(name, btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_2WithHttpInfo(String name, String btLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameter_2");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameter_2");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties setParameters(String btLocator, Properties body, String fields) throws ApiException {
    return setParametersWithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> setParametersWithHttpInfo(String btLocator, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/parameters"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties setParameters_0(String btLocator, Properties body, String fields) throws ApiException {
    return setParameters_0WithHttpInfo(btLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> setParameters_0WithHttpInfo(String btLocator, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/settings"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param optimizeSettings  (optional)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes setTemplates(String btLocator, BuildTypes body, Boolean optimizeSettings, String fields) throws ApiException {
    return setTemplatesWithHttpInfo(btLocator, body, optimizeSettings, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @param optimizeSettings  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> setTemplatesWithHttpInfo(String btLocator, BuildTypes body, Boolean optimizeSettings, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setTemplates");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/templates"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "optimizeSettings", optimizeSettings));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @return VcsLabeling
   * @throws ApiException if fails to make API call
   */
  public VcsLabeling setVCSLabelingOptions(String btLocator, VcsLabeling body) throws ApiException {
    return setVCSLabelingOptionsWithHttpInfo(btLocator, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;VcsLabeling&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsLabeling> setVCSLabelingOptionsWithHttpInfo(String btLocator, VcsLabeling body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling setVCSLabelingOptions");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcsLabeling"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsLabeling> __localVarReturnType = new GenericType<VcsLabeling>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return VcsRootEntry
   * @throws ApiException if fails to make API call
   */
  public VcsRootEntry updateVcsRootEntry(String btLocator, String vcsRootLocator, VcsRootEntry body, String fields) throws ApiException {
    return updateVcsRootEntryWithHttpInfo(btLocator, vcsRootLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootEntry&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootEntry> updateVcsRootEntryWithHttpInfo(String btLocator, String vcsRootLocator, VcsRootEntry body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling updateVcsRootEntry");
    }
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling updateVcsRootEntry");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootEntry> __localVarReturnType = new GenericType<VcsRootEntry>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String updateVcsRootEntryCheckoutRules(String btLocator, String vcsRootLocator, String body) throws ApiException {
    return updateVcsRootEntryCheckoutRulesWithHttpInfo(btLocator, vcsRootLocator, body).getData();
      }

  /**
   * 
   * 
   * @param btLocator  (required)
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> updateVcsRootEntryCheckoutRulesWithHttpInfo(String btLocator, String vcsRootLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling updateVcsRootEntryCheckoutRules");
    }
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling updateVcsRootEntryCheckoutRules");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}/checkout-rules"
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
