package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.AgentPool;
import org.jetbrains.teamcity.rest.client.model.AgentPools;
import org.jetbrains.teamcity.rest.client.model.Branches;
import org.jetbrains.teamcity.rest.client.model.Build;
import org.jetbrains.teamcity.rest.client.model.BuildType;
import org.jetbrains.teamcity.rest.client.model.BuildTypes;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.NewBuildTypeDescription;
import org.jetbrains.teamcity.rest.client.model.NewProjectDescription;
import org.jetbrains.teamcity.rest.client.model.Project;
import org.jetbrains.teamcity.rest.client.model.ProjectFeature;
import org.jetbrains.teamcity.rest.client.model.ProjectFeatures;
import org.jetbrains.teamcity.rest.client.model.Projects;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Property;
import org.jetbrains.teamcity.rest.client.model.Type;



public class ProjectApi {
  private ApiClient apiClient;

  public ProjectApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ProjectApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object add(String projectLocator, ProjectFeature body, String fields) throws ApiException {
    return addWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Object&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Object> addWithHttpInfo(String projectLocator, ProjectFeature body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling add");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Object> __localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType createBuildType(String projectLocator, NewBuildTypeDescription body, String fields) throws ApiException {
    return createBuildTypeWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> createBuildTypeWithHttpInfo(String projectLocator, NewBuildTypeDescription body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling createBuildType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType createBuildTypeTemplate(String projectLocator, NewBuildTypeDescription body, String fields) throws ApiException {
    return createBuildTypeTemplateWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> createBuildTypeTemplateWithHttpInfo(String projectLocator, NewBuildTypeDescription body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling createBuildTypeTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/templates"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project createProject(NewProjectDescription body) throws ApiException {
    return createProjectWithHttpInfo(body).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> createProjectWithHttpInfo(NewProjectDescription body) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String createSecureToken(String projectLocator, String body) throws ApiException {
    return createSecureTokenWithHttpInfo(projectLocator, body).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> createSecureTokenWithHttpInfo(String projectLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling createSecureToken");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/secure/tokens"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void delete(String featureLocator, String projectLocator) throws ApiException {

    deleteWithHttpInfo(featureLocator, projectLocator);
  }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteWithHttpInfo(String featureLocator, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling delete");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling delete");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllParameters(String projectLocator) throws ApiException {

    deleteAllParametersWithHttpInfo(projectLocator);
  }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllParametersWithHttpInfo(String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteAllParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllParameters_0(String featureLocator, String projectLocator, String fields) throws ApiException {

    deleteAllParameters_0WithHttpInfo(featureLocator, projectLocator, fields);
  }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllParameters_0WithHttpInfo(String featureLocator, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling deleteAllParameters_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteAllParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter(String name, String projectLocator) throws ApiException {

    deleteParameterWithHttpInfo(name, projectLocator);
  }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameterWithHttpInfo(String name, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter_0(String name, String featureLocator, String projectLocator, String fields) throws ApiException {

    deleteParameter_0WithHttpInfo(name, featureLocator, projectLocator, fields);
  }

  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameter_0WithHttpInfo(String name, String featureLocator, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter_0");
    }
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling deleteParameter_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteProject(String projectLocator) throws ApiException {

    deleteProjectWithHttpInfo(projectLocator);
  }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteProjectWithHttpInfo(String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteProjectAgentPools(String projectLocator, String agentPoolLocator) throws ApiException {

    deleteProjectAgentPoolsWithHttpInfo(projectLocator, agentPoolLocator);
  }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteProjectAgentPoolsWithHttpInfo(String projectLocator, String agentPoolLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deleteProjectAgentPools");
    }
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling deleteProjectAgentPools");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/agentPools/{agentPoolLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object get(String projectLocator, String locator, String fields) throws ApiException {
    return getWithHttpInfo(projectLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Object&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Object> getWithHttpInfo(String projectLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling get");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Object> __localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Branches
   * @throws ApiException if fails to make API call
   */
  public Branches getBranches(String projectLocator, String locator, String fields) throws ApiException {
    return getBranchesWithHttpInfo(projectLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Branches&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Branches> getBranchesWithHttpInfo(String projectLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getBranches");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/branches"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Branches> __localVarReturnType = new GenericType<Branches>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes getBuildTypesOrder(String projectLocator, String field) throws ApiException {
    return getBuildTypesOrderWithHttpInfo(projectLocator, field).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> getBuildTypesOrderWithHttpInfo(String projectLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getBuildTypesOrder");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling getBuildTypesOrder");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/order/buildTypes"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType getDefaultTemplate(String projectLocator, String fields) throws ApiException {
    return getDefaultTemplateWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> getDefaultTemplateWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getDefaultTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/defaultTemplate"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param id  (optional)
   * @return NewProjectDescription
   * @throws ApiException if fails to make API call
   */
  public NewProjectDescription getExampleNewProjectDescription(String projectLocator, String id) throws ApiException {
    return getExampleNewProjectDescriptionWithHttpInfo(projectLocator, id).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param id  (optional)
   * @return ApiResponse&lt;NewProjectDescription&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<NewProjectDescription> getExampleNewProjectDescriptionWithHttpInfo(String projectLocator, String id) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getExampleNewProjectDescription");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/example/newProjectDescription"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "id", id));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<NewProjectDescription> __localVarReturnType = new GenericType<NewProjectDescription>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param id  (optional)
   * @return NewProjectDescription
   * @throws ApiException if fails to make API call
   */
  public NewProjectDescription getExampleNewProjectDescriptionCompatibilityVersion1(String projectLocator, String id) throws ApiException {
    return getExampleNewProjectDescriptionCompatibilityVersion1WithHttpInfo(projectLocator, id).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param id  (optional)
   * @return ApiResponse&lt;NewProjectDescription&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<NewProjectDescription> getExampleNewProjectDescriptionCompatibilityVersion1WithHttpInfo(String projectLocator, String id) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getExampleNewProjectDescriptionCompatibilityVersion1");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/newProjectDescription"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "id", id));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<NewProjectDescription> __localVarReturnType = new GenericType<NewProjectDescription>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property getParameter(String name, String projectLocator, String fields) throws ApiException {
    return getParameterWithHttpInfo(name, projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> getParameterWithHttpInfo(String name, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameter");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return Type
   * @throws ApiException if fails to make API call
   */
  public Type getParameterType(String name, String projectLocator) throws ApiException {
    return getParameterTypeWithHttpInfo(name, projectLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return ApiResponse&lt;Type&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Type> getParameterTypeWithHttpInfo(String name, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterType");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameterType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/type"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Type> __localVarReturnType = new GenericType<Type>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterTypeRawValue(String name, String projectLocator) throws ApiException {
    return getParameterTypeRawValueWithHttpInfo(name, projectLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterTypeRawValueWithHttpInfo(String name, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterTypeRawValue");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameterTypeRawValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/type/rawValue"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterValueLong(String name, String projectLocator) throws ApiException {
    return getParameterValueLongWithHttpInfo(name, projectLocator).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterValueLongWithHttpInfo(String name, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterValueLong");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterValueLong_0(String name, String featureLocator, String projectLocator, String fields) throws ApiException {
    return getParameterValueLong_0WithHttpInfo(name, featureLocator, projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterValueLong_0WithHttpInfo(String name, String featureLocator, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterValueLong_0");
    }
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling getParameterValueLong_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameterValueLong_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property getParameter_0(String name, String featureLocator, String projectLocator, String fields, String fields2) throws ApiException {
    return getParameter_0WithHttpInfo(name, featureLocator, projectLocator, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> getParameter_0WithHttpInfo(String name, String featureLocator, String projectLocator, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameter_0");
    }
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling getParameter_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getParameters(String projectLocator, String locator, String fields) throws ApiException {
    return getParametersWithHttpInfo(projectLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getParametersWithHttpInfo(String projectLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getParameters_0(String featureLocator, String projectLocator, String locator, String fields, String fields2) throws ApiException {
    return getParameters_0WithHttpInfo(featureLocator, projectLocator, locator, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getParameters_0WithHttpInfo(String featureLocator, String projectLocator, String locator, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling getParameters_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project getParentProject(String projectLocator, String fields) throws ApiException {
    return getParentProjectWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> getParentProjectWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getParentProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parentProject"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return AgentPools
   * @throws ApiException if fails to make API call
   */
  public AgentPools getProjectAgentPools(String projectLocator, String fields) throws ApiException {
    return getProjectAgentPoolsWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPools&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPools> getProjectAgentPoolsWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getProjectAgentPools");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/agentPools"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPools> __localVarReturnType = new GenericType<AgentPools>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return Projects
   * @throws ApiException if fails to make API call
   */
  public Projects getProjectsOrder(String projectLocator, String field) throws ApiException {
    return getProjectsOrderWithHttpInfo(projectLocator, field).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;Projects&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Projects> getProjectsOrderWithHttpInfo(String projectLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getProjectsOrder");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling getProjectsOrder");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/order/projects"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Projects> __localVarReturnType = new GenericType<Projects>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param token  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getSecureValue(String projectLocator, String token) throws ApiException {
    return getSecureValueWithHttpInfo(projectLocator, token).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param token  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getSecureValueWithHttpInfo(String projectLocator, String token) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getSecureValue");
    }
    
    // verify the required parameter 'token' is set
    if (token == null) {
      throw new ApiException(400, "Missing the required parameter 'token' when calling getSecureValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/secure/values/{token}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "token" + "\\}", apiClient.escapeString(token.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getSettingsFile(String projectLocator) throws ApiException {
    return getSettingsFileWithHttpInfo(projectLocator).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getSettingsFileWithHttpInfo(String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getSettingsFile");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/settingsFile"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object getSingle(String featureLocator, String projectLocator, String fields) throws ApiException {
    return getSingleWithHttpInfo(featureLocator, projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Object&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Object> getSingleWithHttpInfo(String featureLocator, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling getSingle");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getSingle");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Object> __localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project reloadSettingsFile(String projectLocator, String fields) throws ApiException {
    return reloadSettingsFileWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> reloadSettingsFileWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling reloadSettingsFile");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/latest"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void removeDefaultTemplate(String projectLocator, String fields) throws ApiException {

    removeDefaultTemplateWithHttpInfo(projectLocator, fields);
  }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeDefaultTemplateWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling removeDefaultTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/defaultTemplate"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object replace(String featureLocator, String projectLocator, ProjectFeature body, String fields) throws ApiException {
    return replaceWithHttpInfo(featureLocator, projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Object&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Object> replaceWithHttpInfo(String featureLocator, String projectLocator, ProjectFeature body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling replace");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling replace");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Object> __localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Object
   * @throws ApiException if fails to make API call
   */
  public Object replaceAll(String projectLocator, ProjectFeatures body, String fields) throws ApiException {
    return replaceAllWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Object&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Object> replaceAllWithHttpInfo(String projectLocator, ProjectFeatures body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling replaceAll");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Object> __localVarReturnType = new GenericType<Object>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildFieldWithProject(String projectLocator, String btLocator, String buildLocator, String field) throws ApiException {
    return serveBuildFieldWithProjectWithHttpInfo(projectLocator, btLocator, buildLocator, field).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildFieldWithProjectWithHttpInfo(String projectLocator, String btLocator, String buildLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildFieldWithProject");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildFieldWithProject");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildFieldWithProject");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildFieldWithProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds/{buildLocator}/{field}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType serveBuildType(String projectLocator, String btLocator, String fields) throws ApiException {
    return serveBuildTypeWithHttpInfo(projectLocator, btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> serveBuildTypeWithHttpInfo(String projectLocator, String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildType");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes/{btLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildTypeFieldWithProject(String projectLocator, String btLocator, String field) throws ApiException {
    return serveBuildTypeFieldWithProjectWithHttpInfo(projectLocator, btLocator, field).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildTypeFieldWithProjectWithHttpInfo(String projectLocator, String btLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildTypeFieldWithProject");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildTypeFieldWithProject");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildTypeFieldWithProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes/{btLocator}/{field}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType serveBuildTypeTemplates(String projectLocator, String btLocator, String fields) throws ApiException {
    return serveBuildTypeTemplatesWithHttpInfo(projectLocator, btLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> serveBuildTypeTemplatesWithHttpInfo(String projectLocator, String btLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildTypeTemplates");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildTypeTemplates");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/templates/{btLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes serveBuildTypesInProject(String projectLocator, String fields) throws ApiException {
    return serveBuildTypesInProjectWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> serveBuildTypesInProjectWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildTypesInProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build serveBuildWithProject(String projectLocator, String btLocator, String buildLocator, String fields) throws ApiException {
    return serveBuildWithProjectWithHttpInfo(projectLocator, btLocator, buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> serveBuildWithProjectWithHttpInfo(String projectLocator, String btLocator, String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuildWithProject");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuildWithProject");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildWithProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds/{buildLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds serveBuilds(String projectLocator, String btLocator, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    return serveBuildsWithHttpInfo(projectLocator, btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param btLocator  (required)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> serveBuildsWithHttpInfo(String projectLocator, String btLocator, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveBuilds");
    }
    
    // verify the required parameter 'btLocator' is set
    if (btLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'btLocator' when calling serveBuilds");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "btLocator" + "\\}", apiClient.escapeString(btLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "status", status));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "triggeredByUser", triggeredByUser));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includePersonal", includePersonal));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeCanceled", includeCanceled));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "onlyPinned", onlyPinned));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("multi", "tag", tag));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "agentName", agentName));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceBuild", sinceBuild));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceDate", sinceDate));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "start", start));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "count", count));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project serveProject(String projectLocator, String fields) throws ApiException {
    return serveProjectWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> serveProjectWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveProjectField(String projectLocator, String field) throws ApiException {
    return serveProjectFieldWithHttpInfo(projectLocator, field).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveProjectFieldWithHttpInfo(String projectLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveProjectField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveProjectField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/{field}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Projects
   * @throws ApiException if fails to make API call
   */
  public Projects serveProjects(String locator, String fields) throws ApiException {
    return serveProjectsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Projects&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Projects> serveProjectsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Projects> __localVarReturnType = new GenericType<Projects>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes serveTemplatesInProject(String projectLocator, String fields) throws ApiException {
    return serveTemplatesInProjectWithHttpInfo(projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> serveTemplatesInProjectWithHttpInfo(String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling serveTemplatesInProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/templates"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes setBuildTypesOrder(String projectLocator, String field, BuildTypes body) throws ApiException {
    return setBuildTypesOrderWithHttpInfo(projectLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> setBuildTypesOrderWithHttpInfo(String projectLocator, String field, BuildTypes body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setBuildTypesOrder");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setBuildTypesOrder");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/order/buildTypes"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return BuildType
   * @throws ApiException if fails to make API call
   */
  public BuildType setDefaultTemplate(String projectLocator, BuildType body, String fields) throws ApiException {
    return setDefaultTemplateWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildType&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildType> setDefaultTemplateWithHttpInfo(String projectLocator, BuildType body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setDefaultTemplate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/defaultTemplate"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildType> __localVarReturnType = new GenericType<BuildType>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter(String projectLocator, Property body, String fields) throws ApiException {
    return setParameterWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameterWithHttpInfo(String projectLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return Type
   * @throws ApiException if fails to make API call
   */
  public Type setParameterType(String name, String projectLocator, Type body) throws ApiException {
    return setParameterTypeWithHttpInfo(name, projectLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Type&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Type> setParameterTypeWithHttpInfo(String name, String projectLocator, Type body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterType");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameterType");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/type"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Type> __localVarReturnType = new GenericType<Type>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterTypeRawValue(String name, String projectLocator, String body) throws ApiException {
    return setParameterTypeRawValueWithHttpInfo(name, projectLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterTypeRawValueWithHttpInfo(String name, String projectLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterTypeRawValue");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameterTypeRawValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/type/rawValue"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterValueLong(String name, String projectLocator, String body) throws ApiException {
    return setParameterValueLongWithHttpInfo(name, projectLocator, body).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterValueLongWithHttpInfo(String name, String projectLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterValueLong");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterValueLong_0(String name, String featureLocator, String projectLocator, String body, String fields) throws ApiException {
    return setParameterValueLong_0WithHttpInfo(name, featureLocator, projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterValueLong_0WithHttpInfo(String name, String featureLocator, String projectLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterValueLong_0");
    }
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling setParameterValueLong_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameterValueLong_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_0(String name, String projectLocator, Property body, String fields) throws ApiException {
    return setParameter_0WithHttpInfo(name, projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_0WithHttpInfo(String name, String projectLocator, Property body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameter_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_1(String featureLocator, String projectLocator, Property body, String fields, String fields2) throws ApiException {
    return setParameter_1WithHttpInfo(featureLocator, projectLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_1WithHttpInfo(String featureLocator, String projectLocator, Property body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling setParameter_1");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameter_1");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_2(String name, String featureLocator, String projectLocator, Property body, String fields, String fields2) throws ApiException {
    return setParameter_2WithHttpInfo(name, featureLocator, projectLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_2WithHttpInfo(String name, String featureLocator, String projectLocator, Property body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameter_2");
    }
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling setParameter_2");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameter_2");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties setParameters(String projectLocator, Properties body, String fields) throws ApiException {
    return setParametersWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> setParametersWithHttpInfo(String projectLocator, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parameters"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties setParameters_0(String featureLocator, String projectLocator, Properties body, String fields, String fields2) throws ApiException {
    return setParameters_0WithHttpInfo(featureLocator, projectLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param featureLocator  (required)
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> setParameters_0WithHttpInfo(String featureLocator, String projectLocator, Properties body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'featureLocator' is set
    if (featureLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'featureLocator' when calling setParameters_0");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParameters_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties"
      .replaceAll("\\{" + "featureLocator" + "\\}", apiClient.escapeString(featureLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project setParentProject(String projectLocator, Project body, String fields) throws ApiException {
    return setParentProjectWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> setParentProjectWithHttpInfo(String projectLocator, Project body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setParentProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/parentProject"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return AgentPools
   * @throws ApiException if fails to make API call
   */
  public AgentPools setProjectAgentPools(String projectLocator, AgentPools body, String fields) throws ApiException {
    return setProjectAgentPoolsWithHttpInfo(projectLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPools&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPools> setProjectAgentPoolsWithHttpInfo(String projectLocator, AgentPools body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setProjectAgentPools");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/agentPools"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPools> __localVarReturnType = new GenericType<AgentPools>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return AgentPool
   * @throws ApiException if fails to make API call
   */
  public AgentPool setProjectAgentPools_0(String projectLocator, AgentPool body) throws ApiException {
    return setProjectAgentPools_0WithHttpInfo(projectLocator, body).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;AgentPool&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPool> setProjectAgentPools_0WithHttpInfo(String projectLocator, AgentPool body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setProjectAgentPools_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/agentPools"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPool> __localVarReturnType = new GenericType<AgentPool>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setProjectField(String projectLocator, String field, String body) throws ApiException {
    return setProjectFieldWithHttpInfo(projectLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setProjectFieldWithHttpInfo(String projectLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setProjectField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setProjectField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/{field}"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return Projects
   * @throws ApiException if fails to make API call
   */
  public Projects setProjectsOrder(String projectLocator, String field, Projects body) throws ApiException {
    return setProjectsOrderWithHttpInfo(projectLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param projectLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Projects&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Projects> setProjectsOrderWithHttpInfo(String projectLocator, String field, Projects body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling setProjectsOrder");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setProjectsOrder");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/projects/{projectLocator}/order/projects"
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Projects> __localVarReturnType = new GenericType<Projects>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
