package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.AuditEvent;
import org.jetbrains.teamcity.rest.client.model.AuditEvents;



public class AuditApi {
  private ApiClient apiClient;

  public AuditApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AuditApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return AuditEvents
   * @throws ApiException if fails to make API call
   */
  public AuditEvents get(String locator, String fields) throws ApiException {
    return getWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;AuditEvents&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AuditEvents> getWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/audit";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AuditEvents> __localVarReturnType = new GenericType<AuditEvents>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param auditEventLocator  (required)
   * @param fields  (optional)
   * @return AuditEvent
   * @throws ApiException if fails to make API call
   */
  public AuditEvent getSingle(String auditEventLocator, String fields) throws ApiException {
    return getSingleWithHttpInfo(auditEventLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param auditEventLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AuditEvent&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AuditEvent> getSingleWithHttpInfo(String auditEventLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'auditEventLocator' is set
    if (auditEventLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'auditEventLocator' when calling getSingle");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/audit/{auditEventLocator}"
      .replaceAll("\\{" + "auditEventLocator" + "\\}", apiClient.escapeString(auditEventLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AuditEvent> __localVarReturnType = new GenericType<AuditEvent>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
