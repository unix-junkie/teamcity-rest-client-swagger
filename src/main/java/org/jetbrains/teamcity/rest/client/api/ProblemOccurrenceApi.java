package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.ProblemOccurrence;
import org.jetbrains.teamcity.rest.client.model.ProblemOccurrences;



public class ProblemOccurrenceApi {
  private ApiClient apiClient;

  public ProblemOccurrenceApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ProblemOccurrenceApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ProblemOccurrences
   * @throws ApiException if fails to make API call
   */
  public ProblemOccurrences getProblems(String locator, String fields) throws ApiException {
    return getProblemsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;ProblemOccurrences&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ProblemOccurrences> getProblemsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/problemOccurrences";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ProblemOccurrences> __localVarReturnType = new GenericType<ProblemOccurrences>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param problemLocator  (required)
   * @param fields  (optional)
   * @return ProblemOccurrence
   * @throws ApiException if fails to make API call
   */
  public ProblemOccurrence serveInstance(String problemLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(problemLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param problemLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;ProblemOccurrence&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ProblemOccurrence> serveInstanceWithHttpInfo(String problemLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'problemLocator' is set
    if (problemLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'problemLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/problemOccurrences/{problemLocator}"
      .replaceAll("\\{" + "problemLocator" + "\\}", apiClient.escapeString(problemLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ProblemOccurrence> __localVarReturnType = new GenericType<ProblemOccurrence>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
