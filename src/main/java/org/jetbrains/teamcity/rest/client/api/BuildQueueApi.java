package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Agents;
import org.jetbrains.teamcity.rest.client.model.Build;
import org.jetbrains.teamcity.rest.client.model.BuildCancelRequest;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.Tags;



public class BuildQueueApi {
  private ApiClient apiClient;

  public BuildQueueApi() {
    this(Configuration.getDefaultApiClient());
  }

  public BuildQueueApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public void addTags(String buildLocator, Tags body) throws ApiException {

    addTagsWithHttpInfo(buildLocator, body);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> addTagsWithHttpInfo(String buildLocator, Tags body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling addTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return BuildCancelRequest
   * @throws ApiException if fails to make API call
   */
  public BuildCancelRequest cancelBuild(String buildLocator) throws ApiException {
    return cancelBuildWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;BuildCancelRequest&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildCancelRequest> cancelBuildWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling cancelBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{buildLocator}/example/buildCancelRequest"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildCancelRequest> __localVarReturnType = new GenericType<BuildCancelRequest>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param body  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build cancelBuild_0(String queuedBuildLocator, BuildCancelRequest body) throws ApiException {
    return cancelBuild_0WithHttpInfo(queuedBuildLocator, body).getData();
      }

  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> cancelBuild_0WithHttpInfo(String queuedBuildLocator, BuildCancelRequest body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'queuedBuildLocator' is set
    if (queuedBuildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'queuedBuildLocator' when calling cancelBuild_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{queuedBuildLocator}"
      .replaceAll("\\{" + "queuedBuildLocator" + "\\}", apiClient.escapeString(queuedBuildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteBuild(String queuedBuildLocator) throws ApiException {

    deleteBuildWithHttpInfo(queuedBuildLocator);
  }

  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteBuildWithHttpInfo(String queuedBuildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'queuedBuildLocator' is set
    if (queuedBuildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'queuedBuildLocator' when calling deleteBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{queuedBuildLocator}"
      .replaceAll("\\{" + "queuedBuildLocator" + "\\}", apiClient.escapeString(queuedBuildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteBuildsExperimental(String locator, String fields) throws ApiException {

    deleteBuildsExperimentalWithHttpInfo(locator, fields);
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteBuildsExperimentalWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build getBuild(String queuedBuildLocator, String fields) throws ApiException {
    return getBuildWithHttpInfo(queuedBuildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> getBuildWithHttpInfo(String queuedBuildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'queuedBuildLocator' is set
    if (queuedBuildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'queuedBuildLocator' when calling getBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{queuedBuildLocator}"
      .replaceAll("\\{" + "queuedBuildLocator" + "\\}", apiClient.escapeString(queuedBuildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds getBuilds(String locator, String fields) throws ApiException {
    return getBuildsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> getBuildsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param moveToTop  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build queueNewBuild(Build body, Boolean moveToTop) throws ApiException {
    return queueNewBuildWithHttpInfo(body, moveToTop).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param moveToTop  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> queueNewBuildWithHttpInfo(Build body, Boolean moveToTop) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "moveToTop", moveToTop));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds replaceBuilds(Builds body, String fields) throws ApiException {
    return replaceBuildsWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> replaceBuildsWithHttpInfo(Builds body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags replaceTags(String buildLocator, String locator, Tags body, String fields) throws ApiException {
    return replaceTagsWithHttpInfo(buildLocator, locator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> replaceTagsWithHttpInfo(String buildLocator, String locator, Tags body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling replaceTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildFieldByBuildOnly(String buildLocator, String field) throws ApiException {
    return serveBuildFieldByBuildOnlyWithHttpInfo(buildLocator, field).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildFieldByBuildOnlyWithHttpInfo(String buildLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildFieldByBuildOnly");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildFieldByBuildOnly");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{buildLocator}/{field}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param fields  (optional)
   * @return Agents
   * @throws ApiException if fails to make API call
   */
  public Agents serveCompatibleAgents(String queuedBuildLocator, String fields) throws ApiException {
    return serveCompatibleAgentsWithHttpInfo(queuedBuildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param queuedBuildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Agents&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Agents> serveCompatibleAgentsWithHttpInfo(String queuedBuildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'queuedBuildLocator' is set
    if (queuedBuildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'queuedBuildLocator' when calling serveCompatibleAgents");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{queuedBuildLocator}/compatibleAgents"
      .replaceAll("\\{" + "queuedBuildLocator" + "\\}", apiClient.escapeString(queuedBuildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Agents> __localVarReturnType = new GenericType<Agents>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags serveTags(String buildLocator, String locator, String fields) throws ApiException {
    return serveTagsWithHttpInfo(buildLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> serveTagsWithHttpInfo(String buildLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds setBuildQueueOrder(Builds body, String fields) throws ApiException {
    return setBuildQueueOrderWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> setBuildQueueOrderWithHttpInfo(Builds body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/order";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param queuePosition  (required)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build setBuildQueuePosition(String queuePosition, String fields) throws ApiException {
    return setBuildQueuePositionWithHttpInfo(queuePosition, fields).getData();
      }

  /**
   * 
   * 
   * @param queuePosition  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> setBuildQueuePositionWithHttpInfo(String queuePosition, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'queuePosition' is set
    if (queuePosition == null) {
      throw new ApiException(400, "Missing the required parameter 'queuePosition' when calling setBuildQueuePosition");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/order/{queuePosition}"
      .replaceAll("\\{" + "queuePosition" + "\\}", apiClient.escapeString(queuePosition.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param queuePosition  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build setBuildQueuePosition_0(String queuePosition, Build body, String fields) throws ApiException {
    return setBuildQueuePosition_0WithHttpInfo(queuePosition, body, fields).getData();
      }

  /**
   * 
   * 
   * @param queuePosition  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> setBuildQueuePosition_0WithHttpInfo(String queuePosition, Build body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'queuePosition' is set
    if (queuePosition == null) {
      throw new ApiException(400, "Missing the required parameter 'queuePosition' when calling setBuildQueuePosition_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/buildQueue/order/{queuePosition}"
      .replaceAll("\\{" + "queuePosition" + "\\}", apiClient.escapeString(queuePosition.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
