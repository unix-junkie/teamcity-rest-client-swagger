package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Entries;
import java.io.File;
import org.jetbrains.teamcity.rest.client.model.Files;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstance;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;



public class VcsRootInstanceApi {
  private ApiClient apiClient;

  public VcsRootInstanceApi() {
    this(Configuration.getDefaultApiClient());
  }

  public VcsRootInstanceApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteInstanceField(String vcsRootInstanceLocator, String field) throws ApiException {

    deleteInstanceFieldWithHttpInfo(vcsRootInstanceLocator, field);
  }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteInstanceFieldWithHttpInfo(String vcsRootInstanceLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling deleteInstanceField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling deleteInstanceField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field}"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteRepositoryState(String vcsRootInstanceLocator) throws ApiException {

    deleteRepositoryStateWithHttpInfo(vcsRootInstanceLocator);
  }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteRepositoryStateWithHttpInfo(String vcsRootInstanceLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling deleteRepositoryState");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildren(String path, String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    return getChildrenWithHttpInfo(path, vcsRootInstanceLocator, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenWithHttpInfo(String path, String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildren");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getChildren");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/children{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildrenAlias(String path, String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    return getChildrenAliasWithHttpInfo(path, vcsRootInstanceLocator, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenAliasWithHttpInfo(String path, String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildrenAlias");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getChildrenAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param responseBuilder  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContent(String path, String vcsRootInstanceLocator, String responseBuilder) throws ApiException {

    getContentWithHttpInfo(path, vcsRootInstanceLocator, responseBuilder);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param responseBuilder  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentWithHttpInfo(String path, String vcsRootInstanceLocator, String responseBuilder) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContent");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getContent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/content{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "responseBuilder", responseBuilder));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void getContentAlias(String path, String vcsRootInstanceLocator) throws ApiException {

    getContentAliasWithHttpInfo(path, vcsRootInstanceLocator);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentAliasWithHttpInfo(String path, String vcsRootInstanceLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContentAlias");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getContentAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/files{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getMetadata(String path, String vcsRootInstanceLocator, String fields) throws ApiException {
    return getMetadataWithHttpInfo(path, vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getMetadataWithHttpInfo(String path, String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getMetadata");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getMetadata");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/metadata{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<File> __localVarReturnType = new GenericType<File>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return Entries
   * @throws ApiException if fails to make API call
   */
  public Entries getRepositoryState(String vcsRootInstanceLocator, String fields) throws ApiException {
    return getRepositoryStateWithHttpInfo(vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Entries&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Entries> getRepositoryStateWithHttpInfo(String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getRepositoryState");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Entries> __localVarReturnType = new GenericType<Entries>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getRepositoryStateCreationDate(String vcsRootInstanceLocator) throws ApiException {
    return getRepositoryStateCreationDateWithHttpInfo(vcsRootInstanceLocator).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getRepositoryStateCreationDateWithHttpInfo(String vcsRootInstanceLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getRepositoryStateCreationDate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState/creationDate"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getRoot(String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    return getRootWithHttpInfo(vcsRootInstanceLocator, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getRootWithHttpInfo(String vcsRootInstanceLocator, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getZipped(String path, String vcsRootInstanceLocator, String basePath, String locator, String name) throws ApiException {

    getZippedWithHttpInfo(path, vcsRootInstanceLocator, basePath, locator, name);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getZippedWithHttpInfo(String path, String vcsRootInstanceLocator, String basePath, String locator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getZipped");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling getZipped");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/archived{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param requestor  (optional)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstances scheduleCheckingForChanges(String locator, String requestor, String fields) throws ApiException {
    return scheduleCheckingForChangesWithHttpInfo(locator, requestor, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param requestor  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstances> scheduleCheckingForChangesWithHttpInfo(String locator, String requestor, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/checkingForChangesQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestor", requestor));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param okOnNothingFound  (optional)
   * @throws ApiException if fails to make API call
   */
  public void scheduleCheckingForChanges_0(String locator, Boolean okOnNothingFound) throws ApiException {

    scheduleCheckingForChanges_0WithHttpInfo(locator, okOnNothingFound);
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param okOnNothingFound  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> scheduleCheckingForChanges_0WithHttpInfo(String locator, Boolean okOnNothingFound) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/commitHookNotification";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "okOnNothingFound", okOnNothingFound));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstance
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstance serveInstance(String vcsRootInstanceLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstance&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstance> serveInstanceWithHttpInfo(String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstance> __localVarReturnType = new GenericType<VcsRootInstance>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveInstanceField(String vcsRootInstanceLocator, String field) throws ApiException {
    return serveInstanceFieldWithHttpInfo(vcsRootInstanceLocator, field).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveInstanceFieldWithHttpInfo(String vcsRootInstanceLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveInstanceField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveInstanceField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field}"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstances serveInstances(String locator, String fields) throws ApiException {
    return serveInstancesWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstances> serveInstancesWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveRootInstanceProperties(String vcsRootInstanceLocator, String fields) throws ApiException {
    return serveRootInstancePropertiesWithHttpInfo(vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> serveRootInstancePropertiesWithHttpInfo(String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveRootInstanceProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/properties"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setInstanceField(String vcsRootInstanceLocator, String field, String body) throws ApiException {
    return setInstanceFieldWithHttpInfo(vcsRootInstanceLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setInstanceFieldWithHttpInfo(String vcsRootInstanceLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling setInstanceField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setInstanceField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field}"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Entries
   * @throws ApiException if fails to make API call
   */
  public Entries setRepositoryState(String vcsRootInstanceLocator, Entries body, String fields) throws ApiException {
    return setRepositoryStateWithHttpInfo(vcsRootInstanceLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootInstanceLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Entries&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Entries> setRepositoryStateWithHttpInfo(String vcsRootInstanceLocator, Entries body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling setRepositoryState");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState"
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Entries> __localVarReturnType = new GenericType<Entries>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
