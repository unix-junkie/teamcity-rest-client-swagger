package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Build;
import org.jetbrains.teamcity.rest.client.model.BuildCancelRequest;
import org.jetbrains.teamcity.rest.client.model.BuildChanges;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.Comment;
import java.io.File;
import org.jetbrains.teamcity.rest.client.model.Files;
import org.jetbrains.teamcity.rest.client.model.IssuesUsages;
import org.jetbrains.teamcity.rest.client.model.MultipleOperationResult;
import org.jetbrains.teamcity.rest.client.model.PinInfo;
import org.jetbrains.teamcity.rest.client.model.ProblemOccurrences;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Property;
import org.jetbrains.teamcity.rest.client.model.Tags;
import org.jetbrains.teamcity.rest.client.model.TestOccurrences;



public class BuildApi {
  private ApiClient apiClient;

  public BuildApi() {
    this(Configuration.getDefaultApiClient());
  }

  public BuildApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags addTags(String buildLocator, Tags body, String fields) throws ApiException {
    return addTagsWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> addTagsWithHttpInfo(String buildLocator, Tags body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling addTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult addTagsMultiple(String buildLocator, Tags body, String fields) throws ApiException {
    return addTagsMultipleWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> addTagsMultipleWithHttpInfo(String buildLocator, Tags body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling addTagsMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build cancelBuild(String buildLocator, BuildCancelRequest body, String fields) throws ApiException {
    return cancelBuildWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> cancelBuildWithHttpInfo(String buildLocator, BuildCancelRequest body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling cancelBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return BuildCancelRequest
   * @throws ApiException if fails to make API call
   */
  public BuildCancelRequest cancelBuild_0(String buildLocator) throws ApiException {
    return cancelBuild_0WithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;BuildCancelRequest&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildCancelRequest> cancelBuild_0WithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling cancelBuild_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/example/buildCancelRequest"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildCancelRequest> __localVarReturnType = new GenericType<BuildCancelRequest>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult cancelMultiple(String buildLocator, BuildCancelRequest body, String fields) throws ApiException {
    return cancelMultipleWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> cancelMultipleWithHttpInfo(String buildLocator, BuildCancelRequest body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling cancelMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllParameters(String buildLocator, String fields) throws ApiException {

    deleteAllParametersWithHttpInfo(buildLocator, fields);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllParametersWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteAllParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteBuild(String buildLocator) throws ApiException {

    deleteBuildWithHttpInfo(buildLocator);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteBuildWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param locator  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteBuilds(String locator) throws ApiException {

    deleteBuildsWithHttpInfo(locator);
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteBuildsWithHttpInfo(String locator) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteComment(String buildLocator) throws ApiException {

    deleteCommentWithHttpInfo(buildLocator);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteCommentWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteComment");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/comment"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult deleteCommentMultiple(String buildLocator, String fields) throws ApiException {
    return deleteCommentMultipleWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> deleteCommentMultipleWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteCommentMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}/comment"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult deleteMultiple(String buildLocator, String fields) throws ApiException {
    return deleteMultipleWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> deleteMultipleWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter(String name, String buildLocator, String fields) throws ApiException {

    deleteParameterWithHttpInfo(name, buildLocator, fields);
  }

  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameterWithHttpInfo(String name, String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling deleteParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return BuildChanges
   * @throws ApiException if fails to make API call
   */
  public BuildChanges getArtifactDependencyChanges(String buildLocator, String fields) throws ApiException {
    return getArtifactDependencyChangesWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildChanges&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildChanges> getArtifactDependencyChangesWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getArtifactDependencyChanges");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/artifactDependencyChanges"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildChanges> __localVarReturnType = new GenericType<BuildChanges>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getArtifactsDirectory(String buildLocator) throws ApiException {
    return getArtifactsDirectoryWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getArtifactsDirectoryWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getArtifactsDirectory");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/artifactsDirectory"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getBuildNumber(String buildLocator) throws ApiException {
    return getBuildNumberWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getBuildNumberWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getBuildNumber");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/number"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getBuildStatusText(String buildLocator) throws ApiException {
    return getBuildStatusTextWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getBuildStatusTextWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getBuildStatusText");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/statusText"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Comment
   * @throws ApiException if fails to make API call
   */
  public Comment getCanceledInfo(String buildLocator, String fields) throws ApiException {
    return getCanceledInfoWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Comment&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Comment> getCanceledInfoWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getCanceledInfo");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/canceledInfo"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Comment> __localVarReturnType = new GenericType<Comment>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildren(String path, String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    return getChildrenWithHttpInfo(path, buildLocator, basePath, locator, fields, logBuildUsage).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenWithHttpInfo(String path, String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildren");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getChildren");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/children{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildrenAlias(String path, String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    return getChildrenAliasWithHttpInfo(path, buildLocator, basePath, locator, fields, logBuildUsage).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenAliasWithHttpInfo(String path, String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildrenAlias");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getChildrenAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param responseBuilder  (optional)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContent(String path, String buildLocator, String responseBuilder, Boolean logBuildUsage) throws ApiException {

    getContentWithHttpInfo(path, buildLocator, responseBuilder, logBuildUsage);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param responseBuilder  (optional)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentWithHttpInfo(String path, String buildLocator, String responseBuilder, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContent");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getContent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/content{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "responseBuilder", responseBuilder));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContentAlias(String path, String buildLocator, Boolean logBuildUsage) throws ApiException {

    getContentAliasWithHttpInfo(path, buildLocator, logBuildUsage);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentAliasWithHttpInfo(String path, String buildLocator, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContentAlias");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getContentAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/files{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getMetadata(String path, String buildLocator, String fields, Boolean logBuildUsage) throws ApiException {
    return getMetadataWithHttpInfo(path, buildLocator, fields, logBuildUsage).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getMetadataWithHttpInfo(String path, String buildLocator, String fields, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getMetadata");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getMetadata");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/metadata{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<File> __localVarReturnType = new GenericType<File>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds getMultiple(String buildLocator, String fields) throws ApiException {
    return getMultipleWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> getMultipleWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property getParameter(String name, String buildLocator, String fields, String fields2) throws ApiException {
    return getParameterWithHttpInfo(name, buildLocator, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> getParameterWithHttpInfo(String name, String buildLocator, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameter");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameterValueLong(String name, String buildLocator, String fields) throws ApiException {
    return getParameterValueLongWithHttpInfo(name, buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameterValueLongWithHttpInfo(String name, String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling getParameterValueLong");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param propertyName  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getParameter_0(String buildLocator, String propertyName) throws ApiException {
    return getParameter_0WithHttpInfo(buildLocator, propertyName).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param propertyName  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getParameter_0WithHttpInfo(String buildLocator, String propertyName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getParameter_0");
    }
    
    // verify the required parameter 'propertyName' is set
    if (propertyName == null) {
      throw new ApiException(400, "Missing the required parameter 'propertyName' when calling getParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/resulting-properties/{propertyName}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "propertyName" + "\\}", apiClient.escapeString(propertyName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getParameters(String buildLocator, String locator, String fields, String fields2) throws ApiException {
    return getParametersWithHttpInfo(buildLocator, locator, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getParametersWithHttpInfo(String buildLocator, String locator, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return PinInfo
   * @throws ApiException if fails to make API call
   */
  public PinInfo getPinData(String buildLocator, String fields) throws ApiException {
    return getPinDataWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;PinInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<PinInfo> getPinDataWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getPinData");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/pinInfo"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<PinInfo> __localVarReturnType = new GenericType<PinInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getPinned(String buildLocator) throws ApiException {
    return getPinnedWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getPinnedWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getPinned");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/pin"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ProblemOccurrences
   * @throws ApiException if fails to make API call
   */
  public ProblemOccurrences getProblems(String buildLocator, String fields) throws ApiException {
    return getProblemsWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;ProblemOccurrences&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<ProblemOccurrences> getProblemsWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getProblems");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/problemOccurrences"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<ProblemOccurrences> __localVarReturnType = new GenericType<ProblemOccurrences>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param value  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getResolvedParameter(String buildLocator, String value) throws ApiException {
    return getResolvedParameterWithHttpInfo(buildLocator, value).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param value  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getResolvedParameterWithHttpInfo(String buildLocator, String value) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getResolvedParameter");
    }
    
    // verify the required parameter 'value' is set
    if (value == null) {
      throw new ApiException(400, "Missing the required parameter 'value' when calling getResolvedParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/resolved/{value}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "value" + "\\}", apiClient.escapeString(value.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getRoot(String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    return getRootWithHttpInfo(buildLocator, basePath, locator, fields, logBuildUsage).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @param logBuildUsage  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getRootWithHttpInfo(String buildLocator, String basePath, String locator, String fields, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return TestOccurrences
   * @throws ApiException if fails to make API call
   */
  public TestOccurrences getTests(String buildLocator, String fields) throws ApiException {
    return getTestsWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;TestOccurrences&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<TestOccurrences> getTestsWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getTests");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/testOccurrences"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<TestOccurrences> __localVarReturnType = new GenericType<TestOccurrences>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getZipped(String path, String buildLocator, String basePath, String locator, String name, Boolean logBuildUsage) throws ApiException {

    getZippedWithHttpInfo(path, buildLocator, basePath, locator, name, logBuildUsage);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param buildLocator  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @param logBuildUsage  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getZippedWithHttpInfo(String path, String buildLocator, String basePath, String locator, String name, Boolean logBuildUsage) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getZipped");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getZipped");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/artifacts/archived{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "logBuildUsage", logBuildUsage));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public void pinBuild(String buildLocator, String body) throws ApiException {

    pinBuildWithHttpInfo(buildLocator, body);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> pinBuildWithHttpInfo(String buildLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling pinBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/pin"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult pinMultiple(String buildLocator, PinInfo body, String fields) throws ApiException {
    return pinMultipleWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> pinMultipleWithHttpInfo(String buildLocator, PinInfo body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling pinMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}/pinInfo"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult removeTagsMultiple(String buildLocator, Tags body, String fields) throws ApiException {
    return removeTagsMultipleWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> removeTagsMultipleWithHttpInfo(String buildLocator, Tags body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling removeTagsMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public void replaceComment(String buildLocator, String body) throws ApiException {

    replaceCommentWithHttpInfo(buildLocator, body);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> replaceCommentWithHttpInfo(String buildLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling replaceComment");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/comment"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return MultipleOperationResult
   * @throws ApiException if fails to make API call
   */
  public MultipleOperationResult replaceCommentMultiple(String buildLocator, String body, String fields) throws ApiException {
    return replaceCommentMultipleWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;MultipleOperationResult&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<MultipleOperationResult> replaceCommentMultipleWithHttpInfo(String buildLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling replaceCommentMultiple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/multiple/{buildLocator}/comment"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<MultipleOperationResult> __localVarReturnType = new GenericType<MultipleOperationResult>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags replaceTags(String buildLocator, String locator, Tags body, String fields) throws ApiException {
    return replaceTagsWithHttpInfo(buildLocator, locator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> replaceTagsWithHttpInfo(String buildLocator, String locator, Tags body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling replaceTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void resetBuildFinishParameters(String buildLocator) throws ApiException {

    resetBuildFinishParametersWithHttpInfo(buildLocator);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> resetBuildFinishParametersWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling resetBuildFinishParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/caches/finishProperties"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveAggregatedBuildStatus(String buildLocator) throws ApiException {
    return serveAggregatedBuildStatusWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveAggregatedBuildStatusWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveAggregatedBuildStatus");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/status"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param suffix  (required)
   * @throws ApiException if fails to make API call
   */
  public void serveAggregatedBuildStatusIcon(String buildLocator, String suffix) throws ApiException {

    serveAggregatedBuildStatusIconWithHttpInfo(buildLocator, suffix);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param suffix  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> serveAggregatedBuildStatusIconWithHttpInfo(String buildLocator, String suffix) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveAggregatedBuildStatusIcon");
    }
    
    // verify the required parameter 'suffix' is set
    if (suffix == null) {
      throw new ApiException(400, "Missing the required parameter 'suffix' when calling serveAggregatedBuildStatusIcon");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/aggregated/{buildLocator}/statusIcon{suffix}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "suffix" + "\\}", apiClient.escapeString(suffix.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildType  (optional)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds serveAllBuilds(String buildType, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    return serveAllBuildsWithHttpInfo(buildType, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildType  (optional)
   * @param status  (optional)
   * @param triggeredByUser  (optional)
   * @param includePersonal  (optional)
   * @param includeCanceled  (optional)
   * @param onlyPinned  (optional)
   * @param tag  (optional)
   * @param agentName  (optional)
   * @param sinceBuild  (optional)
   * @param sinceDate  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> serveAllBuildsWithHttpInfo(String buildType, String status, String triggeredByUser, Boolean includePersonal, Boolean includeCanceled, Boolean onlyPinned, java.util.List<String> tag, String agentName, String sinceBuild, String sinceDate, Long start, Integer count, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "buildType", buildType));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "status", status));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "triggeredByUser", triggeredByUser));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includePersonal", includePersonal));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeCanceled", includeCanceled));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "onlyPinned", onlyPinned));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("multi", "tag", tag));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "agentName", agentName));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceBuild", sinceBuild));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceDate", sinceDate));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "start", start));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "count", count));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build serveBuild(String buildLocator, String fields) throws ApiException {
    return serveBuildWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> serveBuildWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveBuildActualParameters(String buildLocator, String fields) throws ApiException {
    return serveBuildActualParametersWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> serveBuildActualParametersWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildActualParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/resulting-properties"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildFieldByBuildOnly(String buildLocator, String field) throws ApiException {
    return serveBuildFieldByBuildOnlyWithHttpInfo(buildLocator, field).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildFieldByBuildOnlyWithHttpInfo(String buildLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildFieldByBuildOnly");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveBuildFieldByBuildOnly");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/{field}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return IssuesUsages
   * @throws ApiException if fails to make API call
   */
  public IssuesUsages serveBuildRelatedIssues(String buildLocator, String fields) throws ApiException {
    return serveBuildRelatedIssuesWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;IssuesUsages&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<IssuesUsages> serveBuildRelatedIssuesWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildRelatedIssues");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/relatedIssues"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<IssuesUsages> __localVarReturnType = new GenericType<IssuesUsages>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return IssuesUsages
   * @throws ApiException if fails to make API call
   */
  public IssuesUsages serveBuildRelatedIssuesOld(String buildLocator, String fields) throws ApiException {
    return serveBuildRelatedIssuesOldWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;IssuesUsages&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<IssuesUsages> serveBuildRelatedIssuesOldWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildRelatedIssuesOld");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/related-issues"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<IssuesUsages> __localVarReturnType = new GenericType<IssuesUsages>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param name  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveBuildStatisticValue(String buildLocator, String name) throws ApiException {
    return serveBuildStatisticValueWithHttpInfo(buildLocator, name).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param name  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveBuildStatisticValueWithHttpInfo(String buildLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildStatisticValue");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling serveBuildStatisticValue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/statistics/{name}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveBuildStatisticValues(String buildLocator, String fields) throws ApiException {
    return serveBuildStatisticValuesWithHttpInfo(buildLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> serveBuildStatisticValuesWithHttpInfo(String buildLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildStatisticValues");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/statistics"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param suffix  (required)
   * @throws ApiException if fails to make API call
   */
  public void serveBuildStatusIcon(String buildLocator, String suffix) throws ApiException {

    serveBuildStatusIconWithHttpInfo(buildLocator, suffix);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param suffix  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> serveBuildStatusIconWithHttpInfo(String buildLocator, String suffix) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveBuildStatusIcon");
    }
    
    // verify the required parameter 'suffix' is set
    if (suffix == null) {
      throw new ApiException(400, "Missing the required parameter 'suffix' when calling serveBuildStatusIcon");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/statusIcon{suffix}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "suffix" + "\\}", apiClient.escapeString(suffix.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fileName  (required)
   * @throws ApiException if fails to make API call
   */
  public void serveSourceFile(String buildLocator, String fileName) throws ApiException {

    serveSourceFileWithHttpInfo(buildLocator, fileName);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param fileName  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> serveSourceFileWithHttpInfo(String buildLocator, String fileName) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveSourceFile");
    }
    
    // verify the required parameter 'fileName' is set
    if (fileName == null) {
      throw new ApiException(400, "Missing the required parameter 'fileName' when calling serveSourceFile");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/sources/files/{fileName}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()))
      .replaceAll("\\{" + "fileName" + "\\}", apiClient.escapeString(fileName.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Tags
   * @throws ApiException if fails to make API call
   */
  public Tags serveTags(String buildLocator, String locator, String fields) throws ApiException {
    return serveTagsWithHttpInfo(buildLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tags&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tags> serveTagsWithHttpInfo(String buildLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling serveTags");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/tags"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tags> __localVarReturnType = new GenericType<Tags>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setBuildNumber(String buildLocator, String body) throws ApiException {
    return setBuildNumberWithHttpInfo(buildLocator, body).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setBuildNumberWithHttpInfo(String buildLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setBuildNumber");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/number"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return PinInfo
   * @throws ApiException if fails to make API call
   */
  public PinInfo setBuildPinData(String buildLocator, PinInfo body, String fields) throws ApiException {
    return setBuildPinDataWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;PinInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<PinInfo> setBuildPinDataWithHttpInfo(String buildLocator, PinInfo body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setBuildPinData");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/pinInfo"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<PinInfo> __localVarReturnType = new GenericType<PinInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setBuildStatusText(String buildLocator, String body) throws ApiException {
    return setBuildStatusTextWithHttpInfo(buildLocator, body).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setBuildStatusTextWithHttpInfo(String buildLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setBuildStatusText");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/statusText"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter(String buildLocator, Property body, String fields, String fields2) throws ApiException {
    return setParameterWithHttpInfo(buildLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameterWithHttpInfo(String buildLocator, Property body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setParameterValueLong(String name, String buildLocator, String body, String fields) throws ApiException {
    return setParameterValueLongWithHttpInfo(name, buildLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setParameterValueLongWithHttpInfo(String name, String buildLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameterValueLong");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setParameterValueLong");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes/{name}/value"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Property
   * @throws ApiException if fails to make API call
   */
  public Property setParameter_0(String name, String buildLocator, Property body, String fields, String fields2) throws ApiException {
    return setParameter_0WithHttpInfo(name, buildLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param name  (required)
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Property&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Property> setParameter_0WithHttpInfo(String name, String buildLocator, Property body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling setParameter_0");
    }
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setParameter_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes/{name}"
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()))
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Property> __localVarReturnType = new GenericType<Property>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties setParameters(String buildLocator, Properties body, String fields, String fields2) throws ApiException {
    return setParametersWithHttpInfo(buildLocator, body, fields, fields2).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @param fields2  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> setParametersWithHttpInfo(String buildLocator, Properties body, String fields, String fields2) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setParameters");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/attributes"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields2));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public void unpinBuild(String buildLocator, String body) throws ApiException {

    unpinBuildWithHttpInfo(buildLocator, body);
  }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> unpinBuildWithHttpInfo(String buildLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling unpinBuild");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/builds/{buildLocator}/pin"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
}
