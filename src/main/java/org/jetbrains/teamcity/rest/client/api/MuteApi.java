package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Mute;
import org.jetbrains.teamcity.rest.client.model.Mutes;



public class MuteApi {
  private ApiClient apiClient;

  public MuteApi() {
    this(Configuration.getDefaultApiClient());
  }

  public MuteApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Mute
   * @throws ApiException if fails to make API call
   */
  public Mute createInstance(Mute body, String fields) throws ApiException {
    return createInstanceWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Mute&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Mute> createInstanceWithHttpInfo(Mute body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/mutes";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Mute> __localVarReturnType = new GenericType<Mute>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Mutes
   * @throws ApiException if fails to make API call
   */
  public Mutes createInstances(Mutes body, String fields) throws ApiException {
    return createInstancesWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Mutes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Mutes> createInstancesWithHttpInfo(Mutes body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/mutes/multiple";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Mutes> __localVarReturnType = new GenericType<Mutes>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param muteLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public void deleteInstance(String muteLocator, String body) throws ApiException {

    deleteInstanceWithHttpInfo(muteLocator, body);
  }

  /**
   * 
   * 
   * @param muteLocator  (required)
   * @param body  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteInstanceWithHttpInfo(String muteLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'muteLocator' is set
    if (muteLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'muteLocator' when calling deleteInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/mutes/{muteLocator}"
      .replaceAll("\\{" + "muteLocator" + "\\}", apiClient.escapeString(muteLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Mutes
   * @throws ApiException if fails to make API call
   */
  public Mutes getMutes(String locator, String fields) throws ApiException {
    return getMutesWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Mutes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Mutes> getMutesWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/mutes";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Mutes> __localVarReturnType = new GenericType<Mutes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param muteLocator  (required)
   * @param fields  (optional)
   * @return Mute
   * @throws ApiException if fails to make API call
   */
  public Mute serveInstance(String muteLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(muteLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param muteLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Mute&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Mute> serveInstanceWithHttpInfo(String muteLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'muteLocator' is set
    if (muteLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'muteLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/mutes/{muteLocator}"
      .replaceAll("\\{" + "muteLocator" + "\\}", apiClient.escapeString(muteLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Mute> __localVarReturnType = new GenericType<Mute>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
