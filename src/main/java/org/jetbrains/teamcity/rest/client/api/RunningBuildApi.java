package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Build;



public class RunningBuildApi {
  private ApiClient apiClient;

  public RunningBuildApi() {
    this(Configuration.getDefaultApiClient());
  }

  public RunningBuildApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * Adds a message to the build log. Service messages are accepted.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void addLogMessage(String buildLocator, String body, String fields) throws ApiException {

    addLogMessageWithHttpInfo(buildLocator, body, fields);
  }

  /**
   * Adds a message to the build log. Service messages are accepted.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> addLogMessageWithHttpInfo(String buildLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling addLogMessage");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/runningBuilds/{buildLocator}/log"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      "text/plain"
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * Marks the previously queued (agent-less) build which matches buildLocator as running and returns this build.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Build
   * @throws ApiException if fails to make API call
   */
  public Build markBuildAsRunning(String buildLocator, String body, String fields) throws ApiException {
    return markBuildAsRunningWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * Marks the previously queued (agent-less) build which matches buildLocator as running and returns this build.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Build&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Build> markBuildAsRunningWithHttpInfo(String buildLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling markBuildAsRunning");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/runningBuilds/{buildLocator}/runningData"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      "application/xml", "application/json"
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      "text/plain"
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Build> __localVarReturnType = new GenericType<Build>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * Marks the running agent-less build as finished. An empty finish date means \&quot;now\&quot;.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setFinishedTime(String buildLocator, String body, String fields) throws ApiException {
    return setFinishedTimeWithHttpInfo(buildLocator, body, fields).getData();
      }

  /**
   * Marks the running agent-less build as finished. An empty finish date means \&quot;now\&quot;.
   * 
   * @param buildLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setFinishedTimeWithHttpInfo(String buildLocator, String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling setFinishedTime");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/runningBuilds/{buildLocator}/finishDate"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      "text/plain"
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      "text/plain"
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
