package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Investigation;
import org.jetbrains.teamcity.rest.client.model.Investigations;



public class InvestigationApi {
  private ApiClient apiClient;

  public InvestigationApi() {
    this(Configuration.getDefaultApiClient());
  }

  public InvestigationApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Investigation
   * @throws ApiException if fails to make API call
   */
  public Investigation createInstance(Investigation body, String fields) throws ApiException {
    return createInstanceWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigation&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigation> createInstanceWithHttpInfo(Investigation body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigation> __localVarReturnType = new GenericType<Investigation>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Investigations
   * @throws ApiException if fails to make API call
   */
  public Investigations createInstances(Investigations body, String fields) throws ApiException {
    return createInstancesWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigations&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigations> createInstancesWithHttpInfo(Investigations body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations/multiple";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigations> __localVarReturnType = new GenericType<Investigations>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteInstance(String investigationLocator) throws ApiException {

    deleteInstanceWithHttpInfo(investigationLocator);
  }

  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteInstanceWithHttpInfo(String investigationLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'investigationLocator' is set
    if (investigationLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'investigationLocator' when calling deleteInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations/{investigationLocator}"
      .replaceAll("\\{" + "investigationLocator" + "\\}", apiClient.escapeString(investigationLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Investigations
   * @throws ApiException if fails to make API call
   */
  public Investigations getInvestigations(String locator, String fields) throws ApiException {
    return getInvestigationsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigations&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigations> getInvestigationsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigations> __localVarReturnType = new GenericType<Investigations>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Investigation
   * @throws ApiException if fails to make API call
   */
  public Investigation replaceInstance(String investigationLocator, Investigation body, String fields) throws ApiException {
    return replaceInstanceWithHttpInfo(investigationLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigation&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigation> replaceInstanceWithHttpInfo(String investigationLocator, Investigation body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'investigationLocator' is set
    if (investigationLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'investigationLocator' when calling replaceInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations/{investigationLocator}"
      .replaceAll("\\{" + "investigationLocator" + "\\}", apiClient.escapeString(investigationLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigation> __localVarReturnType = new GenericType<Investigation>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @param fields  (optional)
   * @return Investigation
   * @throws ApiException if fails to make API call
   */
  public Investigation serveInstance(String investigationLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(investigationLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param investigationLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigation&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigation> serveInstanceWithHttpInfo(String investigationLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'investigationLocator' is set
    if (investigationLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'investigationLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/investigations/{investigationLocator}"
      .replaceAll("\\{" + "investigationLocator" + "\\}", apiClient.escapeString(investigationLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigation> __localVarReturnType = new GenericType<Investigation>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
