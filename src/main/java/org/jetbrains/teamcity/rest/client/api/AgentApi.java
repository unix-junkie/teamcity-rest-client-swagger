package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Agent;
import org.jetbrains.teamcity.rest.client.model.AgentPool;
import org.jetbrains.teamcity.rest.client.model.Agents;
import org.jetbrains.teamcity.rest.client.model.AuthorizedInfo;
import org.jetbrains.teamcity.rest.client.model.BuildTypes;
import org.jetbrains.teamcity.rest.client.model.Compatibilities;
import org.jetbrains.teamcity.rest.client.model.CompatibilityPolicy;
import org.jetbrains.teamcity.rest.client.model.EnabledInfo;



public class AgentApi {
  private ApiClient apiClient;

  public AgentApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AgentApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAgent(String agentLocator) throws ApiException {

    deleteAgentWithHttpInfo(agentLocator);
  }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAgentWithHttpInfo(String agentLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling deleteAgent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return Compatibilities
   * @throws ApiException if fails to make API call
   */
  public Compatibilities geIncompatibleBuildTypes(String agentLocator, String fields) throws ApiException {
    return geIncompatibleBuildTypesWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Compatibilities&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Compatibilities> geIncompatibleBuildTypesWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling geIncompatibleBuildTypes");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/incompatibleBuildTypes"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Compatibilities> __localVarReturnType = new GenericType<Compatibilities>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return AgentPool
   * @throws ApiException if fails to make API call
   */
  public AgentPool getAgentPool(String agentLocator, String fields) throws ApiException {
    return getAgentPoolWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPool&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPool> getAgentPoolWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling getAgentPool");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/pool"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPool> __localVarReturnType = new GenericType<AgentPool>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return CompatibilityPolicy
   * @throws ApiException if fails to make API call
   */
  public CompatibilityPolicy getAllowedRunConfigurations(String agentLocator, String fields) throws ApiException {
    return getAllowedRunConfigurationsWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;CompatibilityPolicy&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<CompatibilityPolicy> getAllowedRunConfigurationsWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling getAllowedRunConfigurations");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/compatibilityPolicy"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<CompatibilityPolicy> __localVarReturnType = new GenericType<CompatibilityPolicy>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return AuthorizedInfo
   * @throws ApiException if fails to make API call
   */
  public AuthorizedInfo getAuthorizedInfo(String agentLocator, String fields) throws ApiException {
    return getAuthorizedInfoWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AuthorizedInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AuthorizedInfo> getAuthorizedInfoWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling getAuthorizedInfo");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/authorizedInfo"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AuthorizedInfo> __localVarReturnType = new GenericType<AuthorizedInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes getCompatibleBuildTypes(String agentLocator, String fields) throws ApiException {
    return getCompatibleBuildTypesWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> getCompatibleBuildTypesWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling getCompatibleBuildTypes");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/compatibleBuildTypes"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return EnabledInfo
   * @throws ApiException if fails to make API call
   */
  public EnabledInfo getEnabledInfo(String agentLocator, String fields) throws ApiException {
    return getEnabledInfoWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;EnabledInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<EnabledInfo> getEnabledInfoWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling getEnabledInfo");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/enabledInfo"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<EnabledInfo> __localVarReturnType = new GenericType<EnabledInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return Agent
   * @throws ApiException if fails to make API call
   */
  public Agent serveAgent(String agentLocator, String fields) throws ApiException {
    return serveAgentWithHttpInfo(agentLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Agent&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Agent> serveAgentWithHttpInfo(String agentLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling serveAgent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Agent> __localVarReturnType = new GenericType<Agent>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveAgentField(String agentLocator, String field) throws ApiException {
    return serveAgentFieldWithHttpInfo(agentLocator, field).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveAgentFieldWithHttpInfo(String agentLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling serveAgentField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveAgentField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/{field}"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param includeDisconnected  (optional)
   * @param includeUnauthorized  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Agents
   * @throws ApiException if fails to make API call
   */
  public Agents serveAgents(Boolean includeDisconnected, Boolean includeUnauthorized, String locator, String fields) throws ApiException {
    return serveAgentsWithHttpInfo(includeDisconnected, includeUnauthorized, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param includeDisconnected  (optional)
   * @param includeUnauthorized  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Agents&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Agents> serveAgentsWithHttpInfo(Boolean includeDisconnected, Boolean includeUnauthorized, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeDisconnected", includeDisconnected));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeUnauthorized", includeUnauthorized));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Agents> __localVarReturnType = new GenericType<Agents>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setAgentField(String agentLocator, String field, String body) throws ApiException {
    return setAgentFieldWithHttpInfo(agentLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setAgentFieldWithHttpInfo(String agentLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling setAgentField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setAgentField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/{field}"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return AgentPool
   * @throws ApiException if fails to make API call
   */
  public AgentPool setAgentPool(String agentLocator, AgentPool body, String fields) throws ApiException {
    return setAgentPoolWithHttpInfo(agentLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPool&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPool> setAgentPoolWithHttpInfo(String agentLocator, AgentPool body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling setAgentPool");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/pool"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPool> __localVarReturnType = new GenericType<AgentPool>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return CompatibilityPolicy
   * @throws ApiException if fails to make API call
   */
  public CompatibilityPolicy setAllowedRunConfigurations(String agentLocator, CompatibilityPolicy body, String fields) throws ApiException {
    return setAllowedRunConfigurationsWithHttpInfo(agentLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;CompatibilityPolicy&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<CompatibilityPolicy> setAllowedRunConfigurationsWithHttpInfo(String agentLocator, CompatibilityPolicy body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling setAllowedRunConfigurations");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/compatibilityPolicy"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<CompatibilityPolicy> __localVarReturnType = new GenericType<CompatibilityPolicy>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return AuthorizedInfo
   * @throws ApiException if fails to make API call
   */
  public AuthorizedInfo setAuthorizedInfo(String agentLocator, AuthorizedInfo body, String fields) throws ApiException {
    return setAuthorizedInfoWithHttpInfo(agentLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;AuthorizedInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AuthorizedInfo> setAuthorizedInfoWithHttpInfo(String agentLocator, AuthorizedInfo body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling setAuthorizedInfo");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/authorizedInfo"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AuthorizedInfo> __localVarReturnType = new GenericType<AuthorizedInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return EnabledInfo
   * @throws ApiException if fails to make API call
   */
  public EnabledInfo setEnabledInfo(String agentLocator, EnabledInfo body, String fields) throws ApiException {
    return setEnabledInfoWithHttpInfo(agentLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param agentLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;EnabledInfo&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<EnabledInfo> setEnabledInfoWithHttpInfo(String agentLocator, EnabledInfo body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentLocator' is set
    if (agentLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentLocator' when calling setEnabledInfo");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agents/{agentLocator}/enabledInfo"
      .replaceAll("\\{" + "agentLocator" + "\\}", apiClient.escapeString(agentLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<EnabledInfo> __localVarReturnType = new GenericType<EnabledInfo>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
