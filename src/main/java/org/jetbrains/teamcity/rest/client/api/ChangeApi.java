package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.BuildTypes;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.Change;
import org.jetbrains.teamcity.rest.client.model.Changes;
import org.jetbrains.teamcity.rest.client.model.Entries;
import org.jetbrains.teamcity.rest.client.model.Issues;
import org.jetbrains.teamcity.rest.client.model.Items;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstance;



public class ChangeApi {
  private ApiClient apiClient;

  public ChangeApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ChangeApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return Entries
   * @throws ApiException if fails to make API call
   */
  public Entries getChangeAttributes(String changeLocator, String fields) throws ApiException {
    return getChangeAttributesWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Entries&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Entries> getChangeAttributesWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeAttributes");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/attributes"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Entries> __localVarReturnType = new GenericType<Entries>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return Changes
   * @throws ApiException if fails to make API call
   */
  public Changes getChangeDuplicates(String changeLocator, String fields) throws ApiException {
    return getChangeDuplicatesWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Changes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Changes> getChangeDuplicatesWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeDuplicates");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/duplicates"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Changes> __localVarReturnType = new GenericType<Changes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getChangeField(String changeLocator, String field) throws ApiException {
    return getChangeFieldWithHttpInfo(changeLocator, field).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getChangeFieldWithHttpInfo(String changeLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling getChangeField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/{field}"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds getChangeFirstBuilds(String changeLocator, String fields) throws ApiException {
    return getChangeFirstBuildsWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> getChangeFirstBuildsWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeFirstBuilds");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/firstBuilds"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @return Issues
   * @throws ApiException if fails to make API call
   */
  public Issues getChangeIssue(String changeLocator) throws ApiException {
    return getChangeIssueWithHttpInfo(changeLocator).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @return ApiResponse&lt;Issues&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Issues> getChangeIssueWithHttpInfo(String changeLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeIssue");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/issues"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Issues> __localVarReturnType = new GenericType<Issues>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @return Items
   * @throws ApiException if fails to make API call
   */
  public Items getChangeParentRevisions(String changeLocator) throws ApiException {
    return getChangeParentRevisionsWithHttpInfo(changeLocator).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @return ApiResponse&lt;Items&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Items> getChangeParentRevisionsWithHttpInfo(String changeLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeParentRevisions");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/parentRevisions"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Items> __localVarReturnType = new GenericType<Items>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstance
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstance getChangeVCSRoot(String changeLocator, String fields) throws ApiException {
    return getChangeVCSRootWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstance&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstance> getChangeVCSRootWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeVCSRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/vcsRoot"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstance> __localVarReturnType = new GenericType<VcsRootInstance>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstance
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstance getChangeVCSRootInstance(String changeLocator, String fields) throws ApiException {
    return getChangeVCSRootInstanceWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstance&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstance> getChangeVCSRootInstanceWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getChangeVCSRootInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/vcsRootInstance"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstance> __localVarReturnType = new GenericType<VcsRootInstance>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return Changes
   * @throws ApiException if fails to make API call
   */
  public Changes getParentChanges(String changeLocator, String fields) throws ApiException {
    return getParentChangesWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Changes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Changes> getParentChangesWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getParentChanges");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/parentChanges"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Changes> __localVarReturnType = new GenericType<Changes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return BuildTypes
   * @throws ApiException if fails to make API call
   */
  public BuildTypes getRelatedBuildTypes(String changeLocator, String fields) throws ApiException {
    return getRelatedBuildTypesWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;BuildTypes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<BuildTypes> getRelatedBuildTypesWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling getRelatedBuildTypes");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}/buildTypes"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<BuildTypes> __localVarReturnType = new GenericType<BuildTypes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return Change
   * @throws ApiException if fails to make API call
   */
  public Change serveChange(String changeLocator, String fields) throws ApiException {
    return serveChangeWithHttpInfo(changeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param changeLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Change&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Change> serveChangeWithHttpInfo(String changeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'changeLocator' is set
    if (changeLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'changeLocator' when calling serveChange");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes/{changeLocator}"
      .replaceAll("\\{" + "changeLocator" + "\\}", apiClient.escapeString(changeLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Change> __localVarReturnType = new GenericType<Change>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param project  (optional)
   * @param buildType  (optional)
   * @param build  (optional)
   * @param vcsRoot  (optional)
   * @param sinceChange  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Changes
   * @throws ApiException if fails to make API call
   */
  public Changes serveChanges(String project, String buildType, String build, String vcsRoot, String sinceChange, Long start, Integer count, String locator, String fields) throws ApiException {
    return serveChangesWithHttpInfo(project, buildType, build, vcsRoot, sinceChange, start, count, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param project  (optional)
   * @param buildType  (optional)
   * @param build  (optional)
   * @param vcsRoot  (optional)
   * @param sinceChange  (optional)
   * @param start  (optional)
   * @param count  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Changes&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Changes> serveChangesWithHttpInfo(String project, String buildType, String build, String vcsRoot, String sinceChange, Long start, Integer count, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/changes";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "project", project));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "buildType", buildType));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "build", build));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "vcsRoot", vcsRoot));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "sinceChange", sinceChange));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "start", start));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "count", count));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Changes> __localVarReturnType = new GenericType<Changes>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
