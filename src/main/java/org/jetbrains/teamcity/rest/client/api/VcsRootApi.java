package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.VcsRoot;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstance;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;
import org.jetbrains.teamcity.rest.client.model.VcsRoots;



public class VcsRootApi {
  private ApiClient apiClient;

  public VcsRootApi() {
    this(Configuration.getDefaultApiClient());
  }

  public VcsRootApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return VcsRoot
   * @throws ApiException if fails to make API call
   */
  public VcsRoot addRoot(VcsRoot body, String fields) throws ApiException {
    return addRootWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRoot&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRoot> addRootWithHttpInfo(VcsRoot body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRoot> __localVarReturnType = new GenericType<VcsRoot>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties changeProperties(String vcsRootLocator, Properties body, String fields) throws ApiException {
    return changePropertiesWithHttpInfo(vcsRootLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> changePropertiesWithHttpInfo(String vcsRootLocator, Properties body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling changeProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteAllProperties(String vcsRootLocator) throws ApiException {

    deleteAllPropertiesWithHttpInfo(vcsRootLocator);
  }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteAllPropertiesWithHttpInfo(String vcsRootLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling deleteAllProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteParameter(String vcsRootLocator, String name) throws ApiException {

    deleteParameterWithHttpInfo(vcsRootLocator, name);
  }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteParameterWithHttpInfo(String vcsRootLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling deleteParameter");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties/{name}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteRoot(String vcsRootLocator) throws ApiException {

    deleteRootWithHttpInfo(vcsRootLocator);
  }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteRootWithHttpInfo(String vcsRootLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling deleteRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getSettingsFile(String vcsRootLocator) throws ApiException {
    return getSettingsFileWithHttpInfo(vcsRootLocator).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getSettingsFileWithHttpInfo(String vcsRootLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling getSettingsFile");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/settingsFile"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String putParameter(String vcsRootLocator, String name, String body) throws ApiException {
    return putParameterWithHttpInfo(vcsRootLocator, name, body).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> putParameterWithHttpInfo(String vcsRootLocator, String name, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling putParameter");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling putParameter");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties/{name}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveField(String vcsRootLocator, String field) throws ApiException {
    return serveFieldWithHttpInfo(vcsRootLocator, field).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveFieldWithHttpInfo(String vcsRootLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/{field}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveInstanceField(String vcsRootLocator, String vcsRootInstanceLocator, String field) throws ApiException {
    return serveInstanceFieldWithHttpInfo(vcsRootLocator, vcsRootInstanceLocator, field).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveInstanceFieldWithHttpInfo(String vcsRootLocator, String vcsRootInstanceLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveInstanceField");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveInstanceField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveInstanceField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/{field}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveProperties(String vcsRootLocator, String fields) throws ApiException {
    return servePropertiesWithHttpInfo(vcsRootLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> servePropertiesWithHttpInfo(String vcsRootLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveProperty(String vcsRootLocator, String name) throws ApiException {
    return servePropertyWithHttpInfo(vcsRootLocator, name).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param name  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> servePropertyWithHttpInfo(String vcsRootLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling serveProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/properties/{name}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return VcsRoot
   * @throws ApiException if fails to make API call
   */
  public VcsRoot serveRoot(String vcsRootLocator, String fields) throws ApiException {
    return serveRootWithHttpInfo(vcsRootLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRoot&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRoot> serveRootWithHttpInfo(String vcsRootLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRoot> __localVarReturnType = new GenericType<VcsRoot>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstance
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstance serveRootInstance(String vcsRootLocator, String vcsRootInstanceLocator, String fields) throws ApiException {
    return serveRootInstanceWithHttpInfo(vcsRootLocator, vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstance&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstance> serveRootInstanceWithHttpInfo(String vcsRootLocator, String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveRootInstance");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveRootInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstance> __localVarReturnType = new GenericType<VcsRootInstance>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveRootInstanceProperties(String vcsRootLocator, String vcsRootInstanceLocator, String fields) throws ApiException {
    return serveRootInstancePropertiesWithHttpInfo(vcsRootLocator, vcsRootInstanceLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> serveRootInstancePropertiesWithHttpInfo(String vcsRootLocator, String vcsRootInstanceLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveRootInstanceProperties");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling serveRootInstanceProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/properties"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstances serveRootInstances(String vcsRootLocator, String fields) throws ApiException {
    return serveRootInstancesWithHttpInfo(vcsRootLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstances> serveRootInstancesWithHttpInfo(String vcsRootLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling serveRootInstances");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/instances"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return VcsRoots
   * @throws ApiException if fails to make API call
   */
  public VcsRoots serveRoots(String locator, String fields) throws ApiException {
    return serveRootsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRoots&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRoots> serveRootsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRoots> __localVarReturnType = new GenericType<VcsRoots>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setField(String vcsRootLocator, String field, String body) throws ApiException {
    return setFieldWithHttpInfo(vcsRootLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setFieldWithHttpInfo(String vcsRootLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling setField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/{field}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setInstanceField(String vcsRootLocator, String vcsRootInstanceLocator, String field, String body) throws ApiException {
    return setInstanceFieldWithHttpInfo(vcsRootLocator, vcsRootInstanceLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param vcsRootLocator  (required)
   * @param vcsRootInstanceLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setInstanceFieldWithHttpInfo(String vcsRootLocator, String vcsRootInstanceLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'vcsRootLocator' is set
    if (vcsRootLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootLocator' when calling setInstanceField");
    }
    
    // verify the required parameter 'vcsRootInstanceLocator' is set
    if (vcsRootInstanceLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'vcsRootInstanceLocator' when calling setInstanceField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setInstanceField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/{field}"
      .replaceAll("\\{" + "vcsRootLocator" + "\\}", apiClient.escapeString(vcsRootLocator.toString()))
      .replaceAll("\\{" + "vcsRootInstanceLocator" + "\\}", apiClient.escapeString(vcsRootInstanceLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
