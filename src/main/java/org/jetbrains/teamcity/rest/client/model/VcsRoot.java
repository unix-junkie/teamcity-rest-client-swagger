/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.jetbrains.teamcity.rest.client.model.Items;
import org.jetbrains.teamcity.rest.client.model.Project;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import javax.xml.bind.annotation.*;

/**
 * VcsRoot
 */

@XmlRootElement(name = "vcs-root")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "vcs-root")
public class VcsRoot {
  @JsonProperty("id")
  @JacksonXmlProperty(isAttribute = true, localName = "id")
  @XmlAttribute(name = "id")
  private String id = null;

  @JsonProperty("internalId")
  @JacksonXmlProperty(isAttribute = true, localName = "internalId")
  @XmlAttribute(name = "internalId")
  private String internalId = null;

  @JsonProperty("uuid")
  @JacksonXmlProperty(isAttribute = true, localName = "uuid")
  @XmlAttribute(name = "uuid")
  private String uuid = null;

  @JsonProperty("name")
  @JacksonXmlProperty(isAttribute = true, localName = "name")
  @XmlAttribute(name = "name")
  private String name = null;

  @JsonProperty("vcsName")
  @JacksonXmlProperty(isAttribute = true, localName = "vcsName")
  @XmlAttribute(name = "vcsName")
  private String vcsName = null;

  @JsonProperty("modificationCheckInterval")
  @JacksonXmlProperty(isAttribute = true, localName = "modificationCheckInterval")
  @XmlAttribute(name = "modificationCheckInterval")
  private Integer modificationCheckInterval = null;

  @JsonProperty("href")
  @JacksonXmlProperty(isAttribute = true, localName = "href")
  @XmlAttribute(name = "href")
  private String href = null;

  @JsonProperty("project")
  @JacksonXmlProperty(localName = "project")
  @XmlElement(name = "project")
  private Project project = null;

  @JsonProperty("properties")
  @JacksonXmlProperty(localName = "properties")
  @XmlElement(name = "properties")
  private Properties properties = null;

  @JsonProperty("vcsRootInstances")
  @JacksonXmlProperty(localName = "vcsRootInstances")
  @XmlElement(name = "vcsRootInstances")
  private VcsRootInstances vcsRootInstances = null;

  @JsonProperty("repositoryIdStrings")
  @JacksonXmlProperty(localName = "repositoryIdStrings")
  @XmlElement(name = "repositoryIdStrings")
  private Items repositoryIdStrings = null;

  @JsonProperty("projectLocator")
  @JacksonXmlProperty(isAttribute = true, localName = "projectLocator")
  @XmlAttribute(name = "projectLocator")
  private String projectLocator = null;

  @JsonProperty("locator")
  @JacksonXmlProperty(isAttribute = true, localName = "locator")
  @XmlAttribute(name = "locator")
  private String locator = null;

  public VcsRoot id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public VcsRoot internalId(String internalId) {
    this.internalId = internalId;
    return this;
  }

   /**
   * Get internalId
   * @return internalId
  **/
  @ApiModelProperty(value = "")
  public String getInternalId() {
    return internalId;
  }

  public void setInternalId(String internalId) {
    this.internalId = internalId;
  }

  public VcsRoot uuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

   /**
   * Get uuid
   * @return uuid
  **/
  @ApiModelProperty(value = "")
  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public VcsRoot name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public VcsRoot vcsName(String vcsName) {
    this.vcsName = vcsName;
    return this;
  }

   /**
   * Get vcsName
   * @return vcsName
  **/
  @ApiModelProperty(value = "")
  public String getVcsName() {
    return vcsName;
  }

  public void setVcsName(String vcsName) {
    this.vcsName = vcsName;
  }

  public VcsRoot modificationCheckInterval(Integer modificationCheckInterval) {
    this.modificationCheckInterval = modificationCheckInterval;
    return this;
  }

   /**
   * Get modificationCheckInterval
   * @return modificationCheckInterval
  **/
  @ApiModelProperty(value = "")
  public Integer getModificationCheckInterval() {
    return modificationCheckInterval;
  }

  public void setModificationCheckInterval(Integer modificationCheckInterval) {
    this.modificationCheckInterval = modificationCheckInterval;
  }

  public VcsRoot href(String href) {
    this.href = href;
    return this;
  }

   /**
   * Get href
   * @return href
  **/
  @ApiModelProperty(value = "")
  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public VcsRoot project(Project project) {
    this.project = project;
    return this;
  }

   /**
   * Get project
   * @return project
  **/
  @ApiModelProperty(value = "")
  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public VcsRoot properties(Properties properties) {
    this.properties = properties;
    return this;
  }

   /**
   * Get properties
   * @return properties
  **/
  @ApiModelProperty(value = "")
  public Properties getProperties() {
    return properties;
  }

  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  public VcsRoot vcsRootInstances(VcsRootInstances vcsRootInstances) {
    this.vcsRootInstances = vcsRootInstances;
    return this;
  }

   /**
   * Get vcsRootInstances
   * @return vcsRootInstances
  **/
  @ApiModelProperty(value = "")
  public VcsRootInstances getVcsRootInstances() {
    return vcsRootInstances;
  }

  public void setVcsRootInstances(VcsRootInstances vcsRootInstances) {
    this.vcsRootInstances = vcsRootInstances;
  }

  public VcsRoot repositoryIdStrings(Items repositoryIdStrings) {
    this.repositoryIdStrings = repositoryIdStrings;
    return this;
  }

   /**
   * Get repositoryIdStrings
   * @return repositoryIdStrings
  **/
  @ApiModelProperty(value = "")
  public Items getRepositoryIdStrings() {
    return repositoryIdStrings;
  }

  public void setRepositoryIdStrings(Items repositoryIdStrings) {
    this.repositoryIdStrings = repositoryIdStrings;
  }

  public VcsRoot projectLocator(String projectLocator) {
    this.projectLocator = projectLocator;
    return this;
  }

   /**
   * Get projectLocator
   * @return projectLocator
  **/
  @ApiModelProperty(value = "")
  public String getProjectLocator() {
    return projectLocator;
  }

  public void setProjectLocator(String projectLocator) {
    this.projectLocator = projectLocator;
  }

  public VcsRoot locator(String locator) {
    this.locator = locator;
    return this;
  }

   /**
   * Get locator
   * @return locator
  **/
  @ApiModelProperty(value = "")
  public String getLocator() {
    return locator;
  }

  public void setLocator(String locator) {
    this.locator = locator;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VcsRoot vcsRoot = (VcsRoot) o;
    return Objects.equals(this.id, vcsRoot.id) &&
        Objects.equals(this.internalId, vcsRoot.internalId) &&
        Objects.equals(this.uuid, vcsRoot.uuid) &&
        Objects.equals(this.name, vcsRoot.name) &&
        Objects.equals(this.vcsName, vcsRoot.vcsName) &&
        Objects.equals(this.modificationCheckInterval, vcsRoot.modificationCheckInterval) &&
        Objects.equals(this.href, vcsRoot.href) &&
        Objects.equals(this.project, vcsRoot.project) &&
        Objects.equals(this.properties, vcsRoot.properties) &&
        Objects.equals(this.vcsRootInstances, vcsRoot.vcsRootInstances) &&
        Objects.equals(this.repositoryIdStrings, vcsRoot.repositoryIdStrings) &&
        Objects.equals(this.projectLocator, vcsRoot.projectLocator) &&
        Objects.equals(this.locator, vcsRoot.locator);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, internalId, uuid, name, vcsName, modificationCheckInterval, href, project, properties, vcsRootInstances, repositoryIdStrings, projectLocator, locator);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VcsRoot {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    internalId: ").append(toIndentedString(internalId)).append("\n");
    sb.append("    uuid: ").append(toIndentedString(uuid)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    vcsName: ").append(toIndentedString(vcsName)).append("\n");
    sb.append("    modificationCheckInterval: ").append(toIndentedString(modificationCheckInterval)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    project: ").append(toIndentedString(project)).append("\n");
    sb.append("    properties: ").append(toIndentedString(properties)).append("\n");
    sb.append("    vcsRootInstances: ").append(toIndentedString(vcsRootInstances)).append("\n");
    sb.append("    repositoryIdStrings: ").append(toIndentedString(repositoryIdStrings)).append("\n");
    sb.append("    projectLocator: ").append(toIndentedString(projectLocator)).append("\n");
    sb.append("    locator: ").append(toIndentedString(locator)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

