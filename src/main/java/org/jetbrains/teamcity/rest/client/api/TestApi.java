package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Test;
import org.jetbrains.teamcity.rest.client.model.Tests;



public class TestApi {
  private ApiClient apiClient;

  public TestApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TestApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Tests
   * @throws ApiException if fails to make API call
   */
  public Tests getTests(String locator, String fields) throws ApiException {
    return getTestsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tests&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tests> getTestsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/tests";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tests> __localVarReturnType = new GenericType<Tests>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param testLocator  (required)
   * @param fields  (optional)
   * @return Test
   * @throws ApiException if fails to make API call
   */
  public Test serveInstance(String testLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(testLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param testLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Test&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Test> serveInstanceWithHttpInfo(String testLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'testLocator' is set
    if (testLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'testLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/tests/{testLocator}"
      .replaceAll("\\{" + "testLocator" + "\\}", apiClient.escapeString(testLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Test> __localVarReturnType = new GenericType<Test>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
