/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.jetbrains.teamcity.rest.client.model.StackTraceElement;
import org.jetbrains.teamcity.rest.client.model.Throwable;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import javax.xml.bind.annotation.*;

/**
 * Exception
 */

@XmlRootElement(name = "Exception")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "Exception")
public class Exception {
  @JsonProperty("cause")
  @JacksonXmlProperty(localName = "cause")
  @XmlElement(name = "cause")
  private Throwable cause = null;

  @JsonProperty("stackTrace")
  // Is a container wrapped=false
  // items.name=stackTrace items.baseName=stackTrace items.xmlName= items.xmlNamespace=
  // items.example= items.type=StackTraceElement
  @XmlElement(name = "stackTrace")
  private java.util.List<StackTraceElement> stackTrace = null;

  @JsonProperty("message")
  @JacksonXmlProperty(localName = "message")
  @XmlElement(name = "message")
  private String message = null;

  @JsonProperty("localizedMessage")
  @JacksonXmlProperty(localName = "localizedMessage")
  @XmlElement(name = "localizedMessage")
  private String localizedMessage = null;

  @JsonProperty("suppressed")
  // Is a container wrapped=false
  // items.name=suppressed items.baseName=suppressed items.xmlName= items.xmlNamespace=
  // items.example= items.type=Throwable
  @XmlElement(name = "suppressed")
  private java.util.List<Throwable> suppressed = null;

  public Exception cause(Throwable cause) {
    this.cause = cause;
    return this;
  }

   /**
   * Get cause
   * @return cause
  **/
  @ApiModelProperty(value = "")
  public Throwable getCause() {
    return cause;
  }

  public void setCause(Throwable cause) {
    this.cause = cause;
  }

  public Exception stackTrace(java.util.List<StackTraceElement> stackTrace) {
    this.stackTrace = stackTrace;
    return this;
  }

  public Exception addStackTraceItem(StackTraceElement stackTraceItem) {
    if (this.stackTrace == null) {
      this.stackTrace = new java.util.ArrayList<>();
    }
    this.stackTrace.add(stackTraceItem);
    return this;
  }

   /**
   * Get stackTrace
   * @return stackTrace
  **/
  @ApiModelProperty(value = "")
  public java.util.List<StackTraceElement> getStackTrace() {
    return stackTrace;
  }

  public void setStackTrace(java.util.List<StackTraceElement> stackTrace) {
    this.stackTrace = stackTrace;
  }

  public Exception message(String message) {
    this.message = message;
    return this;
  }

   /**
   * Get message
   * @return message
  **/
  @ApiModelProperty(value = "")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Exception localizedMessage(String localizedMessage) {
    this.localizedMessage = localizedMessage;
    return this;
  }

   /**
   * Get localizedMessage
   * @return localizedMessage
  **/
  @ApiModelProperty(value = "")
  public String getLocalizedMessage() {
    return localizedMessage;
  }

  public void setLocalizedMessage(String localizedMessage) {
    this.localizedMessage = localizedMessage;
  }

  public Exception suppressed(java.util.List<Throwable> suppressed) {
    this.suppressed = suppressed;
    return this;
  }

  public Exception addSuppressedItem(Throwable suppressedItem) {
    if (this.suppressed == null) {
      this.suppressed = new java.util.ArrayList<>();
    }
    this.suppressed.add(suppressedItem);
    return this;
  }

   /**
   * Get suppressed
   * @return suppressed
  **/
  @ApiModelProperty(value = "")
  public java.util.List<Throwable> getSuppressed() {
    return suppressed;
  }

  public void setSuppressed(java.util.List<Throwable> suppressed) {
    this.suppressed = suppressed;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Exception exception = (Exception) o;
    return Objects.equals(this.cause, exception.cause) &&
        Objects.equals(this.stackTrace, exception.stackTrace) &&
        Objects.equals(this.message, exception.message) &&
        Objects.equals(this.localizedMessage, exception.localizedMessage) &&
        Objects.equals(this.suppressed, exception.suppressed);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cause, stackTrace, message, localizedMessage, suppressed);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Exception {\n");
    
    sb.append("    cause: ").append(toIndentedString(cause)).append("\n");
    sb.append("    stackTrace: ").append(toIndentedString(stackTrace)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    localizedMessage: ").append(toIndentedString(localizedMessage)).append("\n");
    sb.append("    suppressed: ").append(toIndentedString(suppressed)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

