package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Group;
import org.jetbrains.teamcity.rest.client.model.Groups;
import org.jetbrains.teamcity.rest.client.model.PermissionAssignments;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Role;
import org.jetbrains.teamcity.rest.client.model.Roles;
import org.jetbrains.teamcity.rest.client.model.Token;
import org.jetbrains.teamcity.rest.client.model.Tokens;
import org.jetbrains.teamcity.rest.client.model.User;
import org.jetbrains.teamcity.rest.client.model.Users;



public class UserApi {
  private ApiClient apiClient;

  public UserApi() {
    this(Configuration.getDefaultApiClient());
  }

  public UserApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Group
   * @throws ApiException if fails to make API call
   */
  public Group addGroup(String userLocator, Group body, String fields) throws ApiException {
    return addGroupWithHttpInfo(userLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Group&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Group> addGroupWithHttpInfo(String userLocator, Group body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling addGroup");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/groups"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Group> __localVarReturnType = new GenericType<Group>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role addRole(String userLocator, Role body) throws ApiException {
    return addRoleWithHttpInfo(userLocator, body).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> addRoleWithHttpInfo(String userLocator, Role body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling addRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role addRoleSimple(String userLocator, String roleId, String scope) throws ApiException {
    return addRoleSimpleWithHttpInfo(userLocator, roleId, scope).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> addRoleSimpleWithHttpInfo(String userLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling addRoleSimple");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling addRoleSimple");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling addRoleSimple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   * @deprecated
   */
  @Deprecated
  public void addRoleSimplePost(String userLocator, String roleId, String scope) throws ApiException {

    addRoleSimplePostWithHttpInfo(userLocator, roleId, scope);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   * @deprecated
   */
  @Deprecated
  public ApiResponse<Void> addRoleSimplePostWithHttpInfo(String userLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling addRoleSimplePost");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling addRoleSimplePost");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling addRoleSimplePost");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @param fields  (optional)
   * @return Token
   * @throws ApiException if fails to make API call
   */
  public Token createToken(String userLocator, String name, String fields) throws ApiException {
    return createTokenWithHttpInfo(userLocator, name, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Token&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Token> createTokenWithHttpInfo(String userLocator, String name, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling createToken");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling createToken");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/tokens/{name}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Token> __localVarReturnType = new GenericType<Token>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return User
   * @throws ApiException if fails to make API call
   */
  public User createUser(User body, String fields) throws ApiException {
    return createUserWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;User&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<User> createUserWithHttpInfo(User body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/users";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<User> __localVarReturnType = new GenericType<User>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteRememberMe(String userLocator) throws ApiException {

    deleteRememberMeWithHttpInfo(userLocator);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteRememberMeWithHttpInfo(String userLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling deleteRememberMe");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/debug/rememberMe"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteRole(String userLocator, String roleId, String scope) throws ApiException {

    deleteRoleWithHttpInfo(userLocator, roleId, scope);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteRoleWithHttpInfo(String userLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling deleteRole");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling deleteRole");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling deleteRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteToken(String userLocator, String name) throws ApiException {

    deleteTokenWithHttpInfo(userLocator, name);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteTokenWithHttpInfo(String userLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling deleteToken");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling deleteToken");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/tokens/{name}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteUser(String userLocator) throws ApiException {

    deleteUserWithHttpInfo(userLocator);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteUserWithHttpInfo(String userLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling deleteUser");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteUserField(String userLocator, String field) throws ApiException {

    deleteUserFieldWithHttpInfo(userLocator, field);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteUserFieldWithHttpInfo(String userLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling deleteUserField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling deleteUserField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/{field}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return Group
   * @throws ApiException if fails to make API call
   */
  public Group getGroup(String userLocator, String groupLocator, String fields) throws ApiException {
    return getGroupWithHttpInfo(userLocator, groupLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Group&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Group> getGroupWithHttpInfo(String userLocator, String groupLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling getGroup");
    }
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling getGroup");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/groups/{groupLocator}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Group> __localVarReturnType = new GenericType<Group>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return Groups
   * @throws ApiException if fails to make API call
   */
  public Groups getGroups(String userLocator, String fields) throws ApiException {
    return getGroupsWithHttpInfo(userLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Groups&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Groups> getGroupsWithHttpInfo(String userLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling getGroups");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/groups"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Groups> __localVarReturnType = new GenericType<Groups>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getPermissions(String userLocator) throws ApiException {
    return getPermissionsWithHttpInfo(userLocator).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getPermissionsWithHttpInfo(String userLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling getPermissions");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/debug/permissions"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return PermissionAssignments
   * @throws ApiException if fails to make API call
   */
  public PermissionAssignments getPermissions_0(String userLocator, String locator, String fields) throws ApiException {
    return getPermissions_0WithHttpInfo(userLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;PermissionAssignments&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<PermissionAssignments> getPermissions_0WithHttpInfo(String userLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling getPermissions_0");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/permissions"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<PermissionAssignments> __localVarReturnType = new GenericType<PermissionAssignments>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return Tokens
   * @throws ApiException if fails to make API call
   */
  public Tokens getTokens(String userLocator, String fields) throws ApiException {
    return getTokensWithHttpInfo(userLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Tokens&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Tokens> getTokensWithHttpInfo(String userLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling getTokens");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/tokens"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Tokens> __localVarReturnType = new GenericType<Tokens>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role listRole(String userLocator, String roleId, String scope) throws ApiException {
    return listRoleWithHttpInfo(userLocator, roleId, scope).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> listRoleWithHttpInfo(String userLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling listRole");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling listRole");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling listRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @return Roles
   * @throws ApiException if fails to make API call
   */
  public Roles listRoles(String userLocator) throws ApiException {
    return listRolesWithHttpInfo(userLocator).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @return ApiResponse&lt;Roles&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Roles> listRolesWithHttpInfo(String userLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling listRoles");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Roles> __localVarReturnType = new GenericType<Roles>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String putUserProperty(String userLocator, String name, String body) throws ApiException {
    return putUserPropertyWithHttpInfo(userLocator, name, body).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> putUserPropertyWithHttpInfo(String userLocator, String name, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling putUserProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling putUserProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/properties/{name}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public void removeGroup(String userLocator, String groupLocator, String fields) throws ApiException {

    removeGroupWithHttpInfo(userLocator, groupLocator, fields);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeGroupWithHttpInfo(String userLocator, String groupLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling removeGroup");
    }
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling removeGroup");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/groups/{groupLocator}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public void removeUserProperty(String userLocator, String name) throws ApiException {

    removeUserPropertyWithHttpInfo(userLocator, name);
  }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeUserPropertyWithHttpInfo(String userLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling removeUserProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling removeUserProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/properties/{name}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Groups
   * @throws ApiException if fails to make API call
   */
  public Groups replaceGroups(String userLocator, Groups body, String fields) throws ApiException {
    return replaceGroupsWithHttpInfo(userLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Groups&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Groups> replaceGroupsWithHttpInfo(String userLocator, Groups body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling replaceGroups");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/groups"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Groups> __localVarReturnType = new GenericType<Groups>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @return Roles
   * @throws ApiException if fails to make API call
   */
  public Roles replaceRoles(String userLocator, Roles body) throws ApiException {
    return replaceRolesWithHttpInfo(userLocator, body).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Roles&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Roles> replaceRolesWithHttpInfo(String userLocator, Roles body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling replaceRoles");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/roles"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Roles> __localVarReturnType = new GenericType<Roles>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return User
   * @throws ApiException if fails to make API call
   */
  public User serveUser(String userLocator, String fields) throws ApiException {
    return serveUserWithHttpInfo(userLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;User&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<User> serveUserWithHttpInfo(String userLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling serveUser");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<User> __localVarReturnType = new GenericType<User>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveUserField(String userLocator, String field) throws ApiException {
    return serveUserFieldWithHttpInfo(userLocator, field).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveUserFieldWithHttpInfo(String userLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling serveUserField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveUserField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/{field}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties serveUserProperties(String userLocator, String fields) throws ApiException {
    return serveUserPropertiesWithHttpInfo(userLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> serveUserPropertiesWithHttpInfo(String userLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling serveUserProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/properties"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveUserProperty(String userLocator, String name) throws ApiException {
    return serveUserPropertyWithHttpInfo(userLocator, name).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param name  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveUserPropertyWithHttpInfo(String userLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling serveUserProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling serveUserProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/properties/{name}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Users
   * @throws ApiException if fails to make API call
   */
  public Users serveUsers(String locator, String fields) throws ApiException {
    return serveUsersWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Users&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Users> serveUsersWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/users";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Users> __localVarReturnType = new GenericType<Users>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setUserField(String userLocator, String field, String body) throws ApiException {
    return setUserFieldWithHttpInfo(userLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setUserFieldWithHttpInfo(String userLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling setUserField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setUserField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}/{field}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return User
   * @throws ApiException if fails to make API call
   */
  public User updateUser(String userLocator, User body, String fields) throws ApiException {
    return updateUserWithHttpInfo(userLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param userLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;User&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<User> updateUserWithHttpInfo(String userLocator, User body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'userLocator' is set
    if (userLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'userLocator' when calling updateUser");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/users/{userLocator}"
      .replaceAll("\\{" + "userLocator" + "\\}", apiClient.escapeString(userLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<User> __localVarReturnType = new GenericType<User>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
