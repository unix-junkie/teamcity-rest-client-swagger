package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Agent;
import org.jetbrains.teamcity.rest.client.model.AgentPool;
import org.jetbrains.teamcity.rest.client.model.AgentPools;
import org.jetbrains.teamcity.rest.client.model.Agents;
import org.jetbrains.teamcity.rest.client.model.Project;
import org.jetbrains.teamcity.rest.client.model.Projects;



public class AgentPoolApi {
  private ApiClient apiClient;

  public AgentPoolApi() {
    this(Configuration.getDefaultApiClient());
  }

  public AgentPoolApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Agent
   * @throws ApiException if fails to make API call
   */
  public Agent addAgent(String agentPoolLocator, Agent body, String fields) throws ApiException {
    return addAgentWithHttpInfo(agentPoolLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Agent&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Agent> addAgentWithHttpInfo(String agentPoolLocator, Agent body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling addAgent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/agents"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Agent> __localVarReturnType = new GenericType<Agent>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project addProject(String agentPoolLocator, Project body) throws ApiException {
    return addProjectWithHttpInfo(agentPoolLocator, body).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> addProjectWithHttpInfo(String agentPoolLocator, Project body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling addProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @return AgentPool
   * @throws ApiException if fails to make API call
   */
  public AgentPool createPool(AgentPool body) throws ApiException {
    return createPoolWithHttpInfo(body).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @return ApiResponse&lt;AgentPool&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPool> createPoolWithHttpInfo(AgentPool body) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPool> __localVarReturnType = new GenericType<AgentPool>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deletePool(String agentPoolLocator) throws ApiException {

    deletePoolWithHttpInfo(agentPoolLocator);
  }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deletePoolWithHttpInfo(String agentPoolLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling deletePool");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deletePoolProject(String agentPoolLocator, String projectLocator) throws ApiException {

    deletePoolProjectWithHttpInfo(agentPoolLocator, projectLocator);
  }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param projectLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deletePoolProjectWithHttpInfo(String agentPoolLocator, String projectLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling deletePoolProject");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling deletePoolProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects/{projectLocator}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteProjects(String agentPoolLocator) throws ApiException {

    deleteProjectsWithHttpInfo(agentPoolLocator);
  }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteProjectsWithHttpInfo(String agentPoolLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling deleteProjects");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getField(String agentPoolLocator, String field) throws ApiException {
    return getFieldWithHttpInfo(agentPoolLocator, field).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getFieldWithHttpInfo(String agentPoolLocator, String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling getField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling getField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/{field}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param fields  (optional)
   * @return AgentPool
   * @throws ApiException if fails to make API call
   */
  public AgentPool getPool(String agentPoolLocator, String fields) throws ApiException {
    return getPoolWithHttpInfo(agentPoolLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPool&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPool> getPoolWithHttpInfo(String agentPoolLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling getPool");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPool> __localVarReturnType = new GenericType<AgentPool>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Agents
   * @throws ApiException if fails to make API call
   */
  public Agents getPoolAgents(String agentPoolLocator, String locator, String fields) throws ApiException {
    return getPoolAgentsWithHttpInfo(agentPoolLocator, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Agents&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Agents> getPoolAgentsWithHttpInfo(String agentPoolLocator, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling getPoolAgents");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/agents"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Agents> __localVarReturnType = new GenericType<Agents>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return Project
   * @throws ApiException if fails to make API call
   */
  public Project getPoolProject(String agentPoolLocator, String projectLocator, String fields) throws ApiException {
    return getPoolProjectWithHttpInfo(agentPoolLocator, projectLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param projectLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Project&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Project> getPoolProjectWithHttpInfo(String agentPoolLocator, String projectLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling getPoolProject");
    }
    
    // verify the required parameter 'projectLocator' is set
    if (projectLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'projectLocator' when calling getPoolProject");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects/{projectLocator}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()))
      .replaceAll("\\{" + "projectLocator" + "\\}", apiClient.escapeString(projectLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Project> __localVarReturnType = new GenericType<Project>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param fields  (optional)
   * @return Projects
   * @throws ApiException if fails to make API call
   */
  public Projects getPoolProjects(String agentPoolLocator, String fields) throws ApiException {
    return getPoolProjectsWithHttpInfo(agentPoolLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Projects&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Projects> getPoolProjectsWithHttpInfo(String agentPoolLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling getPoolProjects");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Projects> __localVarReturnType = new GenericType<Projects>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return AgentPools
   * @throws ApiException if fails to make API call
   */
  public AgentPools getPools(String locator, String fields) throws ApiException {
    return getPoolsWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;AgentPools&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<AgentPools> getPoolsWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<AgentPools> __localVarReturnType = new GenericType<AgentPools>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @return Projects
   * @throws ApiException if fails to make API call
   */
  public Projects replaceProjects(String agentPoolLocator, Projects body) throws ApiException {
    return replaceProjectsWithHttpInfo(agentPoolLocator, body).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Projects&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Projects> replaceProjectsWithHttpInfo(String agentPoolLocator, Projects body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling replaceProjects");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/projects"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Projects> __localVarReturnType = new GenericType<Projects>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setField(String agentPoolLocator, String field, String body) throws ApiException {
    return setFieldWithHttpInfo(agentPoolLocator, field, body).getData();
      }

  /**
   * 
   * 
   * @param agentPoolLocator  (required)
   * @param field  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setFieldWithHttpInfo(String agentPoolLocator, String field, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'agentPoolLocator' is set
    if (agentPoolLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'agentPoolLocator' when calling setField");
    }
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling setField");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/agentPools/{agentPoolLocator}/{field}"
      .replaceAll("\\{" + "agentPoolLocator" + "\\}", apiClient.escapeString(agentPoolLocator.toString()))
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
