package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Group;
import org.jetbrains.teamcity.rest.client.model.Groups;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Role;
import org.jetbrains.teamcity.rest.client.model.Roles;



public class GroupApi {
  private ApiClient apiClient;

  public GroupApi() {
    this(Configuration.getDefaultApiClient());
  }

  public GroupApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return Group
   * @throws ApiException if fails to make API call
   */
  public Group addGroup(Group body, String fields) throws ApiException {
    return addGroupWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Group&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Group> addGroupWithHttpInfo(Group body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Group> __localVarReturnType = new GenericType<Group>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role addRole(String groupLocator, Role body) throws ApiException {
    return addRoleWithHttpInfo(groupLocator, body).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> addRoleWithHttpInfo(String groupLocator, Role body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling addRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role addRoleSimple(String groupLocator, String roleId, String scope) throws ApiException {
    return addRoleSimpleWithHttpInfo(groupLocator, roleId, scope).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> addRoleSimpleWithHttpInfo(String groupLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling addRoleSimple");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling addRoleSimple");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling addRoleSimple");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteGroup(String groupLocator) throws ApiException {

    deleteGroupWithHttpInfo(groupLocator);
  }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteGroupWithHttpInfo(String groupLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling deleteGroup");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteRole(String groupLocator, String roleId, String scope) throws ApiException {

    deleteRoleWithHttpInfo(groupLocator, roleId, scope);
  }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteRoleWithHttpInfo(String groupLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling deleteRole");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling deleteRole");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling deleteRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return Groups
   * @throws ApiException if fails to make API call
   */
  public Groups getParentGroups(String groupLocator, String fields) throws ApiException {
    return getParentGroupsWithHttpInfo(groupLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Groups&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Groups> getParentGroupsWithHttpInfo(String groupLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling getParentGroups");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/parent-groups"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Groups> __localVarReturnType = new GenericType<Groups>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getPermissions(String groupLocator) throws ApiException {
    return getPermissionsWithHttpInfo(groupLocator).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getPermissionsWithHttpInfo(String groupLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling getPermissions");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/debug/permissions"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getProperties(String groupLocator, String fields) throws ApiException {
    return getPropertiesWithHttpInfo(groupLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getPropertiesWithHttpInfo(String groupLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling getProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/properties"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return Role
   * @throws ApiException if fails to make API call
   */
  public Role listRole(String groupLocator, String roleId, String scope) throws ApiException {
    return listRoleWithHttpInfo(groupLocator, roleId, scope).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param roleId  (required)
   * @param scope  (required)
   * @return ApiResponse&lt;Role&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Role> listRoleWithHttpInfo(String groupLocator, String roleId, String scope) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling listRole");
    }
    
    // verify the required parameter 'roleId' is set
    if (roleId == null) {
      throw new ApiException(400, "Missing the required parameter 'roleId' when calling listRole");
    }
    
    // verify the required parameter 'scope' is set
    if (scope == null) {
      throw new ApiException(400, "Missing the required parameter 'scope' when calling listRole");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "roleId" + "\\}", apiClient.escapeString(roleId.toString()))
      .replaceAll("\\{" + "scope" + "\\}", apiClient.escapeString(scope.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Role> __localVarReturnType = new GenericType<Role>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @return Roles
   * @throws ApiException if fails to make API call
   */
  public Roles listRoles(String groupLocator) throws ApiException {
    return listRolesWithHttpInfo(groupLocator).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @return ApiResponse&lt;Roles&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Roles> listRolesWithHttpInfo(String groupLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling listRoles");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Roles> __localVarReturnType = new GenericType<Roles>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String putUserProperty(String groupLocator, String name, String body) throws ApiException {
    return putUserPropertyWithHttpInfo(groupLocator, name, body).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> putUserPropertyWithHttpInfo(String groupLocator, String name, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling putUserProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling putUserProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/properties/{name}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public void removeUserProperty(String groupLocator, String name) throws ApiException {

    removeUserPropertyWithHttpInfo(groupLocator, name);
  }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> removeUserPropertyWithHttpInfo(String groupLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling removeUserProperty");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling removeUserProperty");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/properties/{name}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return Group
   * @throws ApiException if fails to make API call
   */
  public Group serveGroup(String groupLocator, String fields) throws ApiException {
    return serveGroupWithHttpInfo(groupLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;Group&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Group> serveGroupWithHttpInfo(String groupLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling serveGroup");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Group> __localVarReturnType = new GenericType<Group>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Groups
   * @throws ApiException if fails to make API call
   */
  public Groups serveGroups(String fields) throws ApiException {
    return serveGroupsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Groups&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Groups> serveGroupsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Groups> __localVarReturnType = new GenericType<Groups>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveUserProperties(String groupLocator, String name) throws ApiException {
    return serveUserPropertiesWithHttpInfo(groupLocator, name).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param name  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveUserPropertiesWithHttpInfo(String groupLocator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling serveUserProperties");
    }
    
    // verify the required parameter 'name' is set
    if (name == null) {
      throw new ApiException(400, "Missing the required parameter 'name' when calling serveUserProperties");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/properties/{name}"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()))
      .replaceAll("\\{" + "name" + "\\}", apiClient.escapeString(name.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return Groups
   * @throws ApiException if fails to make API call
   */
  public Groups setParentGroups(String groupLocator, Groups body, String fields) throws ApiException {
    return setParentGroupsWithHttpInfo(groupLocator, body, fields).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Groups&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Groups> setParentGroupsWithHttpInfo(String groupLocator, Groups body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling setParentGroups");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/parent-groups"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Groups> __localVarReturnType = new GenericType<Groups>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @return Roles
   * @throws ApiException if fails to make API call
   */
  public Roles setRoles(String groupLocator, Roles body) throws ApiException {
    return setRolesWithHttpInfo(groupLocator, body).getData();
      }

  /**
   * 
   * 
   * @param groupLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;Roles&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Roles> setRolesWithHttpInfo(String groupLocator, Roles body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'groupLocator' is set
    if (groupLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'groupLocator' when calling setRoles");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/userGroups/{groupLocator}/roles"
      .replaceAll("\\{" + "groupLocator" + "\\}", apiClient.escapeString(groupLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Roles> __localVarReturnType = new GenericType<Roles>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
