package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.TestOccurrence;
import org.jetbrains.teamcity.rest.client.model.TestOccurrences;



public class TestOccurrenceApi {
  private ApiClient apiClient;

  public TestOccurrenceApi() {
    this(Configuration.getDefaultApiClient());
  }

  public TestOccurrenceApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return TestOccurrences
   * @throws ApiException if fails to make API call
   */
  public TestOccurrences getTestOccurrences(String locator, String fields) throws ApiException {
    return getTestOccurrencesWithHttpInfo(locator, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;TestOccurrences&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<TestOccurrences> getTestOccurrencesWithHttpInfo(String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/testOccurrences";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<TestOccurrences> __localVarReturnType = new GenericType<TestOccurrences>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param testLocator  (required)
   * @param fields  (optional)
   * @return TestOccurrence
   * @throws ApiException if fails to make API call
   */
  public TestOccurrence serveInstance(String testLocator, String fields) throws ApiException {
    return serveInstanceWithHttpInfo(testLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param testLocator  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;TestOccurrence&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<TestOccurrence> serveInstanceWithHttpInfo(String testLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'testLocator' is set
    if (testLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'testLocator' when calling serveInstance");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/testOccurrences/{testLocator}"
      .replaceAll("\\{" + "testLocator" + "\\}", apiClient.escapeString(testLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<TestOccurrence> __localVarReturnType = new GenericType<TestOccurrence>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
