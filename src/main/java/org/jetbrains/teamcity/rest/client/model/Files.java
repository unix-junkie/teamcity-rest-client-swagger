/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.File;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import javax.xml.bind.annotation.*;

/**
 * Files
 */

@XmlRootElement(name = "files")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "files")
public class Files {
  @JsonProperty("count")
  @JacksonXmlProperty(isAttribute = true, localName = "count")
  @XmlAttribute(name = "count")
  private Integer count = null;

  @JsonProperty("href")
  @JacksonXmlProperty(isAttribute = true, localName = "href")
  @XmlAttribute(name = "href")
  private String href = null;

  @JsonProperty("file")
  // Is a container wrapped=false
  // items.name=file items.baseName=file items.xmlName= items.xmlNamespace=
  // items.example= items.type=File
  @XmlElement(name = "file")
  private java.util.List<File> file = null;

  public Files count(Integer count) {
    this.count = count;
    return this;
  }

   /**
   * Get count
   * @return count
  **/
  @ApiModelProperty(value = "")
  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Files href(String href) {
    this.href = href;
    return this;
  }

   /**
   * Get href
   * @return href
  **/
  @ApiModelProperty(value = "")
  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Files file(java.util.List<File> file) {
    this.file = file;
    return this;
  }

  public Files addFileItem(File fileItem) {
    if (this.file == null) {
      this.file = new java.util.ArrayList<>();
    }
    this.file.add(fileItem);
    return this;
  }

   /**
   * Get file
   * @return file
  **/
  @ApiModelProperty(value = "")
  public java.util.List<File> getFile() {
    return file;
  }

  public void setFile(java.util.List<File> file) {
    this.file = file;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Files files = (Files) o;
    return Objects.equals(this.count, files.count) &&
        Objects.equals(this.href, files.href) &&
        Objects.equals(this.file, files.file);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, href, file);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Files {\n");
    
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    file: ").append(toIndentedString(file)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

