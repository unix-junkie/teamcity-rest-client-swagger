package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.Investigations;
import org.jetbrains.teamcity.rest.client.model.Items;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Session;
import org.jetbrains.teamcity.rest.client.model.Sessions;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;



public class DebugApi {
  private ApiClient apiClient;

  public DebugApi() {
    this(Configuration.getDefaultApiClient());
  }

  public DebugApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public void deleteCurrentRememberMe() throws ApiException {

    deleteCurrentRememberMeWithHttpInfo();
  }

  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteCurrentRememberMeWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/rememberMe";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param time  (optional)
   * @param load  (optional)
   * @param memory  (optional)
   * @param memoryChunks  (optional, default to 1)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String emptyTask(String time, Integer load, Integer memory, Integer memoryChunks) throws ApiException {
    return emptyTaskWithHttpInfo(time, load, memory, memoryChunks).getData();
      }

  /**
   * 
   * 
   * @param time  (optional)
   * @param load  (optional)
   * @param memory  (optional)
   * @param memoryChunks  (optional, default to 1)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> emptyTaskWithHttpInfo(String time, Integer load, Integer memory, Integer memoryChunks) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/emptyTask";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "time", time));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "load", load));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "memory", memory));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "memoryChunks", memoryChunks));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param query  (required)
   * @param fieldDelimiter  (optional, default to , )
   * @param dataRetrieveQuery  (optional)
   * @param count  (optional, default to 1000)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String executeDBQuery(String query, String fieldDelimiter, String dataRetrieveQuery, Integer count) throws ApiException {
    return executeDBQueryWithHttpInfo(query, fieldDelimiter, dataRetrieveQuery, count).getData();
      }

  /**
   * 
   * 
   * @param query  (required)
   * @param fieldDelimiter  (optional, default to , )
   * @param dataRetrieveQuery  (optional)
   * @param count  (optional, default to 1000)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> executeDBQueryWithHttpInfo(String query, String fieldDelimiter, String dataRetrieveQuery, Integer count) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'query' is set
    if (query == null) {
      throw new ApiException(400, "Missing the required parameter 'query' when calling executeDBQuery");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/database/query/{query}"
      .replaceAll("\\{" + "query" + "\\}", apiClient.escapeString(query.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fieldDelimiter", fieldDelimiter));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "dataRetrieveQuery", dataRetrieveQuery));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "count", count));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getBuildChainOptimizationLog(String buildLocator) throws ApiException {
    return getBuildChainOptimizationLogWithHttpInfo(buildLocator).getData();
      }

  /**
   * 
   * 
   * @param buildLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getBuildChainOptimizationLogWithHttpInfo(String buildLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'buildLocator' is set
    if (buildLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'buildLocator' when calling getBuildChainOptimizationLog");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/buildChainOptimizationLog/{buildLocator}"
      .replaceAll("\\{" + "buildLocator" + "\\}", apiClient.escapeString(buildLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param buildTypeLocator  (optional)
   * @param fields  (optional)
   * @return Builds
   * @throws ApiException if fails to make API call
   */
  public Builds getCachedBuildPromotions(String buildTypeLocator, String fields) throws ApiException {
    return getCachedBuildPromotionsWithHttpInfo(buildTypeLocator, fields).getData();
      }

  /**
   * 
   * 
   * @param buildTypeLocator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Builds&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Builds> getCachedBuildPromotionsWithHttpInfo(String buildTypeLocator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/caches/buildPromotions/content";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "buildTypeLocator", buildTypeLocator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Builds> __localVarReturnType = new GenericType<Builds>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getCachedBuildPromotionsStats(String fields) throws ApiException {
    return getCachedBuildPromotionsStatsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getCachedBuildPromotionsStatsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/caches/buildPromotions/stats";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getCachedBuildsStat(String fields) throws ApiException {
    return getCachedBuildsStatWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getCachedBuildsStatWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/caches/builds/stats";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Session
   * @throws ApiException if fails to make API call
   */
  public Session getCurrentSession(String fields) throws ApiException {
    return getCurrentSessionWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Session&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Session> getCurrentSessionWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/session";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Session> __localVarReturnType = new GenericType<Session>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getCurrentSessionMaxInactiveInterval() throws ApiException {
    return getCurrentSessionMaxInactiveIntervalWithHttpInfo().getData();
      }

  /**
   * 
   * 
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getCurrentSessionMaxInactiveIntervalWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/session/maxInactiveSeconds";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getCurrentUserPermissions() throws ApiException {
    return getCurrentUserPermissionsWithHttpInfo().getData();
      }

  /**
   * 
   * 
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getCurrentUserPermissionsWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentUserPermissions";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param dateLocator  (required)
   * @param format  (optional)
   * @param timezone  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getDate(String dateLocator, String format, String timezone) throws ApiException {
    return getDateWithHttpInfo(dateLocator, format, timezone).getData();
      }

  /**
   * 
   * 
   * @param dateLocator  (required)
   * @param format  (optional)
   * @param timezone  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getDateWithHttpInfo(String dateLocator, String format, String timezone) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'dateLocator' is set
    if (dateLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'dateLocator' when calling getDate");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/date/{dateLocator}"
      .replaceAll("\\{" + "dateLocator" + "\\}", apiClient.escapeString(dateLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "format", format));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "timezone", timezone));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getDiagnosticsPerfStats(String fields) throws ApiException {
    return getDiagnosticsPerfStatsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getDiagnosticsPerfStatsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/diagnostics/threadPerfStat/stats";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getEnvironmentVariables(String fields) throws ApiException {
    return getEnvironmentVariablesWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getEnvironmentVariablesWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/jvm/environmentVariables";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param method  (required)
   * @param value  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getHashed(String method, String value) throws ApiException {
    return getHashedWithHttpInfo(method, value).getData();
      }

  /**
   * 
   * 
   * @param method  (required)
   * @param value  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getHashedWithHttpInfo(String method, String value) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'method' is set
    if (method == null) {
      throw new ApiException(400, "Missing the required parameter 'method' when calling getHashed");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/values/transform/{method}"
      .replaceAll("\\{" + "method" + "\\}", apiClient.escapeString(method.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "value", value));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param host  (required)
   * @return Items
   * @throws ApiException if fails to make API call
   */
  public Items getIpAddress(String host) throws ApiException {
    return getIpAddressWithHttpInfo(host).getData();
      }

  /**
   * 
   * 
   * @param host  (required)
   * @return ApiResponse&lt;Items&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Items> getIpAddressWithHttpInfo(String host) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'host' is set
    if (host == null) {
      throw new ApiException(400, "Missing the required parameter 'host' when calling getIpAddress");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/dns/lookup/{host}"
      .replaceAll("\\{" + "host" + "\\}", apiClient.escapeString(host.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Items> __localVarReturnType = new GenericType<Items>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Investigations
   * @throws ApiException if fails to make API call
   */
  public Investigations getRawInvestigations(String fields) throws ApiException {
    return getRawInvestigationsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Investigations&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Investigations> getRawInvestigationsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/investigations";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Investigations> __localVarReturnType = new GenericType<Investigations>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param extra  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getRequestDetails(String extra) throws ApiException {
    return getRequestDetailsWithHttpInfo(extra).getData();
      }

  /**
   * 
   * 
   * @param extra  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getRequestDetailsWithHttpInfo(String extra) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'extra' is set
    if (extra == null) {
      throw new ApiException(400, "Missing the required parameter 'extra' when calling getRequestDetails");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/details{extra}"
      .replaceAll("\\{" + "extra" + "\\}", apiClient.escapeString(extra.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param value  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getScrambled(String value) throws ApiException {
    return getScrambledWithHttpInfo(value).getData();
      }

  /**
   * 
   * 
   * @param value  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getScrambledWithHttpInfo(String value) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/values/password/scrambled";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "value", value));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param manager  (optional)
   * @param fields  (optional)
   * @return Sessions
   * @throws ApiException if fails to make API call
   */
  public Sessions getSessions(Long manager, String fields) throws ApiException {
    return getSessionsWithHttpInfo(manager, fields).getData();
      }

  /**
   * 
   * 
   * @param manager  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Sessions&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Sessions> getSessionsWithHttpInfo(Long manager, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/sessions";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "manager", manager));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Sessions> __localVarReturnType = new GenericType<Sessions>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Properties
   * @throws ApiException if fails to make API call
   */
  public Properties getSystemProperties(String fields) throws ApiException {
    return getSystemPropertiesWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Properties&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Properties> getSystemPropertiesWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/jvm/systemProperties";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Properties> __localVarReturnType = new GenericType<Properties>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param lockedMonitors  (optional)
   * @param lockedSynchronizers  (optional)
   * @param detectLocks  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getThreadDump(String lockedMonitors, String lockedSynchronizers, String detectLocks) throws ApiException {
    return getThreadDumpWithHttpInfo(lockedMonitors, lockedSynchronizers, detectLocks).getData();
      }

  /**
   * 
   * 
   * @param lockedMonitors  (optional)
   * @param lockedSynchronizers  (optional)
   * @param detectLocks  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getThreadDumpWithHttpInfo(String lockedMonitors, String lockedSynchronizers, String detectLocks) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/threadDump";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "lockedMonitors", lockedMonitors));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "lockedSynchronizers", lockedSynchronizers));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "detectLocks", detectLocks));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param threadLocator  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getThreadInterrupted(String threadLocator) throws ApiException {
    return getThreadInterruptedWithHttpInfo(threadLocator).getData();
      }

  /**
   * 
   * 
   * @param threadLocator  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getThreadInterruptedWithHttpInfo(String threadLocator) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'threadLocator' is set
    if (threadLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'threadLocator' when calling getThreadInterrupted");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/threads/{threadLocator}/interrupted"
      .replaceAll("\\{" + "threadLocator" + "\\}", apiClient.escapeString(threadLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param value  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getUnscrambled(String value) throws ApiException {
    return getUnscrambledWithHttpInfo(value).getData();
      }

  /**
   * 
   * 
   * @param value  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getUnscrambledWithHttpInfo(String value) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/values/password/unscrambled";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "value", value));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param threadLocator  (required)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String interruptThread(String threadLocator, String body) throws ApiException {
    return interruptThreadWithHttpInfo(threadLocator, body).getData();
      }

  /**
   * 
   * 
   * @param threadLocator  (required)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> interruptThreadWithHttpInfo(String threadLocator, String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // verify the required parameter 'threadLocator' is set
    if (threadLocator == null) {
      throw new ApiException(400, "Missing the required parameter 'threadLocator' when calling interruptThread");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/threads/{threadLocator}/interrupted"
      .replaceAll("\\{" + "threadLocator" + "\\}", apiClient.escapeString(threadLocator.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public void invalidateCurrentSession() throws ApiException {

    invalidateCurrentSessionWithHttpInfo();
  }

  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> invalidateCurrentSessionWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/session";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String listDBTables() throws ApiException {
    return listDBTablesWithHttpInfo().getData();
      }

  /**
   * 
   * 
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> listDBTablesWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/database/tables";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String newRememberMe() throws ApiException {
    return newRememberMeWithHttpInfo().getData();
      }

  /**
   * 
   * 
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> newRememberMeWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/rememberMe";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param extra  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String postRequestDetails(String extra) throws ApiException {
    return postRequestDetailsWithHttpInfo(extra).getData();
      }

  /**
   * 
   * 
   * @param extra  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> postRequestDetailsWithHttpInfo(String extra) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'extra' is set
    if (extra == null) {
      throw new ApiException(400, "Missing the required parameter 'extra' when calling postRequestDetails");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/details{extra}"
      .replaceAll("\\{" + "extra" + "\\}", apiClient.escapeString(extra.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param extra  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String putRequestDetails(String extra) throws ApiException {
    return putRequestDetailsWithHttpInfo(extra).getData();
      }

  /**
   * 
   * 
   * @param extra  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> putRequestDetailsWithHttpInfo(String extra) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'extra' is set
    if (extra == null) {
      throw new ApiException(400, "Missing the required parameter 'extra' when calling putRequestDetails");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/details{extra}"
      .replaceAll("\\{" + "extra" + "\\}", apiClient.escapeString(extra.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public void requestFinalization() throws ApiException {

    requestFinalizationWithHttpInfo();
  }

  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> requestFinalizationWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/jvm/finalization";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public void requestGc() throws ApiException {

    requestGcWithHttpInfo();
  }

  /**
   * 
   * 
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> requestGcWithHttpInfo() throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/jvm/gc";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param archived  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String saveMemoryDump(Boolean archived) throws ApiException {
    return saveMemoryDumpWithHttpInfo(archived).getData();
      }

  /**
   * 
   * 
   * @param archived  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> saveMemoryDumpWithHttpInfo(Boolean archived) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/memory/dumps";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "archived", archived));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param locator  (optional)
   * @param requestor  (optional)
   * @param fields  (optional)
   * @return VcsRootInstances
   * @throws ApiException if fails to make API call
   */
  public VcsRootInstances scheduleCheckingForChanges(String locator, String requestor, String fields) throws ApiException {
    return scheduleCheckingForChangesWithHttpInfo(locator, requestor, fields).getData();
      }

  /**
   * 
   * 
   * @param locator  (optional)
   * @param requestor  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;VcsRootInstances&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<VcsRootInstances> scheduleCheckingForChangesWithHttpInfo(String locator, String requestor, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/vcsCheckingForChangesQueue";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "requestor", requestor));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<VcsRootInstances> __localVarReturnType = new GenericType<VcsRootInstances>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String setCurrentSessionMaxInactiveInterval(String body) throws ApiException {
    return setCurrentSessionMaxInactiveIntervalWithHttpInfo(body).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> setCurrentSessionMaxInactiveIntervalWithHttpInfo(String body) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/debug/currentRequest/session/maxInactiveSeconds";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "PUT", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
