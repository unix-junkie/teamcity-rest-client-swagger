package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.swagger.ApiClient;
import org.jetbrains.teamcity.rest.client.swagger.ApiResponse;
import org.jetbrains.teamcity.rest.client.swagger.Configuration;
import org.jetbrains.teamcity.rest.client.swagger.Pair;

import javax.ws.rs.core.GenericType;

import org.jetbrains.teamcity.rest.client.model.BackupProcessManager;
import java.io.File;
import org.jetbrains.teamcity.rest.client.model.Files;
import org.jetbrains.teamcity.rest.client.model.LicenseKey;
import org.jetbrains.teamcity.rest.client.model.LicenseKeys;
import org.jetbrains.teamcity.rest.client.model.LicensingData;
import org.jetbrains.teamcity.rest.client.model.Metrics;
import org.jetbrains.teamcity.rest.client.model.Plugins;
import org.jetbrains.teamcity.rest.client.model.Server;



public class ServerApi {
  private ApiClient apiClient;

  public ServerApi() {
    this(Configuration.getDefaultApiClient());
  }

  public ServerApi(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  public ApiClient getApiClient() {
    return apiClient;
  }

  public void setApiClient(ApiClient apiClient) {
    this.apiClient = apiClient;
  }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return LicenseKeys
   * @throws ApiException if fails to make API call
   */
  public LicenseKeys addLicenseKeys(String body, String fields) throws ApiException {
    return addLicenseKeysWithHttpInfo(body, fields).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;LicenseKeys&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<LicenseKeys> addLicenseKeysWithHttpInfo(String body, String fields) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/licensingData/licenseKeys";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<LicenseKeys> __localVarReturnType = new GenericType<LicenseKeys>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param licenseKey  (required)
   * @throws ApiException if fails to make API call
   */
  public void deleteLicenseKey(String licenseKey) throws ApiException {

    deleteLicenseKeyWithHttpInfo(licenseKey);
  }

  /**
   * 
   * 
   * @param licenseKey  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> deleteLicenseKeyWithHttpInfo(String licenseKey) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'licenseKey' is set
    if (licenseKey == null) {
      throw new ApiException(400, "Missing the required parameter 'licenseKey' when calling deleteLicenseKey");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/licensingData/licenseKeys/{licenseKey}"
      .replaceAll("\\{" + "licenseKey" + "\\}", apiClient.escapeString(licenseKey.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "DELETE", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String getBackupStatus(BackupProcessManager body) throws ApiException {
    return getBackupStatusWithHttpInfo(body).getData();
      }

  /**
   * 
   * 
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> getBackupStatusWithHttpInfo(BackupProcessManager body) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/backup";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildren(String path, String areaId, String basePath, String locator, String fields) throws ApiException {
    return getChildrenWithHttpInfo(path, areaId, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenWithHttpInfo(String path, String areaId, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildren");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getChildren");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/children{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getChildrenAlias(String path, String areaId, String basePath, String locator, String fields) throws ApiException {
    return getChildrenAliasWithHttpInfo(path, areaId, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getChildrenAliasWithHttpInfo(String path, String areaId, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getChildrenAlias");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getChildrenAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param responseBuilder  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getContent(String path, String areaId, String responseBuilder) throws ApiException {

    getContentWithHttpInfo(path, areaId, responseBuilder);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param responseBuilder  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentWithHttpInfo(String path, String areaId, String responseBuilder) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContent");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getContent");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/content{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "responseBuilder", responseBuilder));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @throws ApiException if fails to make API call
   */
  public void getContentAlias(String path, String areaId) throws ApiException {

    getContentAliasWithHttpInfo(path, areaId);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getContentAliasWithHttpInfo(String path, String areaId) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getContentAlias");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getContentAlias");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/files{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param licenseKey  (required)
   * @param fields  (optional)
   * @return LicenseKey
   * @throws ApiException if fails to make API call
   */
  public LicenseKey getLicenseKey(String licenseKey, String fields) throws ApiException {
    return getLicenseKeyWithHttpInfo(licenseKey, fields).getData();
      }

  /**
   * 
   * 
   * @param licenseKey  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;LicenseKey&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<LicenseKey> getLicenseKeyWithHttpInfo(String licenseKey, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'licenseKey' is set
    if (licenseKey == null) {
      throw new ApiException(400, "Missing the required parameter 'licenseKey' when calling getLicenseKey");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/licensingData/licenseKeys/{licenseKey}"
      .replaceAll("\\{" + "licenseKey" + "\\}", apiClient.escapeString(licenseKey.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<LicenseKey> __localVarReturnType = new GenericType<LicenseKey>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return LicenseKeys
   * @throws ApiException if fails to make API call
   */
  public LicenseKeys getLicenseKeys(String fields) throws ApiException {
    return getLicenseKeysWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;LicenseKeys&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<LicenseKeys> getLicenseKeysWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/licensingData/licenseKeys";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<LicenseKeys> __localVarReturnType = new GenericType<LicenseKeys>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return LicensingData
   * @throws ApiException if fails to make API call
   */
  public LicensingData getLicensingData(String fields) throws ApiException {
    return getLicensingDataWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;LicensingData&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<LicensingData> getLicensingDataWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/licensingData";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<LicensingData> __localVarReturnType = new GenericType<LicensingData>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param fields  (optional)
   * @return File
   * @throws ApiException if fails to make API call
   */
  public File getMetadata(String path, String areaId, String fields) throws ApiException {
    return getMetadataWithHttpInfo(path, areaId, fields).getData();
      }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param fields  (optional)
   * @return ApiResponse&lt;File&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<File> getMetadataWithHttpInfo(String path, String areaId, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getMetadata");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getMetadata");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/metadata{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<File> __localVarReturnType = new GenericType<File>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return Files
   * @throws ApiException if fails to make API call
   */
  public Files getRoot(String areaId, String basePath, String locator, String fields) throws ApiException {
    return getRootWithHttpInfo(areaId, basePath, locator, fields).getData();
      }

  /**
   * 
   * 
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param fields  (optional)
   * @return ApiResponse&lt;Files&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Files> getRootWithHttpInfo(String areaId, String basePath, String locator, String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getRoot");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}"
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Files> __localVarReturnType = new GenericType<Files>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @throws ApiException if fails to make API call
   */
  public void getZipped(String path, String areaId, String basePath, String locator, String name) throws ApiException {

    getZippedWithHttpInfo(path, areaId, basePath, locator, name);
  }

  /**
   * 
   * 
   * @param path  (required)
   * @param areaId  (required)
   * @param basePath  (optional)
   * @param locator  (optional)
   * @param name  (optional)
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Void> getZippedWithHttpInfo(String path, String areaId, String basePath, String locator, String name) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'path' is set
    if (path == null) {
      throw new ApiException(400, "Missing the required parameter 'path' when calling getZipped");
    }
    
    // verify the required parameter 'areaId' is set
    if (areaId == null) {
      throw new ApiException(400, "Missing the required parameter 'areaId' when calling getZipped");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/files/{areaId}/archived{path}"
      .replaceAll("\\{" + "path" + "\\}", apiClient.escapeString(path.toString()))
      .replaceAll("\\{" + "areaId" + "\\}", apiClient.escapeString(areaId.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "basePath", basePath));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "locator", locator));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "name", name));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };


    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, null);
  }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Metrics
   * @throws ApiException if fails to make API call
   */
  public Metrics serveMetrics(String fields) throws ApiException {
    return serveMetricsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Metrics&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Metrics> serveMetricsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/metrics";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Metrics> __localVarReturnType = new GenericType<Metrics>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Plugins
   * @throws ApiException if fails to make API call
   */
  public Plugins servePlugins(String fields) throws ApiException {
    return servePluginsWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Plugins&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Plugins> servePluginsWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/plugins";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Plugins> __localVarReturnType = new GenericType<Plugins>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fields  (optional)
   * @return Server
   * @throws ApiException if fails to make API call
   */
  public Server serveServerInfo(String fields) throws ApiException {
    return serveServerInfoWithHttpInfo(fields).getData();
      }

  /**
   * 
   * 
   * @param fields  (optional)
   * @return ApiResponse&lt;Server&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<Server> serveServerInfoWithHttpInfo(String fields) throws ApiException {
    Object __localVarPostBody = null;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fields", fields));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<Server> __localVarReturnType = new GenericType<Server>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param field  (required)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String serveServerVersion(String field) throws ApiException {
    return serveServerVersionWithHttpInfo(field).getData();
      }

  /**
   * 
   * 
   * @param field  (required)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> serveServerVersionWithHttpInfo(String field) throws ApiException {
    Object __localVarPostBody = null;
    
    // verify the required parameter 'field' is set
    if (field == null) {
      throw new ApiException(400, "Missing the required parameter 'field' when calling serveServerVersion");
    }
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/{field}"
      .replaceAll("\\{" + "field" + "\\}", apiClient.escapeString(field.toString()));

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();


    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "GET", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
  /**
   * 
   * 
   * @param fileName  (optional)
   * @param addTimestamp  (optional)
   * @param includeConfigs  (optional)
   * @param includeDatabase  (optional)
   * @param includeBuildLogs  (optional)
   * @param includePersonalChanges  (optional)
   * @param includeRunningBuilds  (optional)
   * @param includeSupplimentaryData  (optional)
   * @param body  (optional)
   * @return String
   * @throws ApiException if fails to make API call
   */
  public String startBackup(String fileName, Boolean addTimestamp, Boolean includeConfigs, Boolean includeDatabase, Boolean includeBuildLogs, Boolean includePersonalChanges, Boolean includeRunningBuilds, Boolean includeSupplimentaryData, BackupProcessManager body) throws ApiException {
    return startBackupWithHttpInfo(fileName, addTimestamp, includeConfigs, includeDatabase, includeBuildLogs, includePersonalChanges, includeRunningBuilds, includeSupplimentaryData, body).getData();
      }

  /**
   * 
   * 
   * @param fileName  (optional)
   * @param addTimestamp  (optional)
   * @param includeConfigs  (optional)
   * @param includeDatabase  (optional)
   * @param includeBuildLogs  (optional)
   * @param includePersonalChanges  (optional)
   * @param includeRunningBuilds  (optional)
   * @param includeSupplimentaryData  (optional)
   * @param body  (optional)
   * @return ApiResponse&lt;String&gt;
   * @throws ApiException if fails to make API call
   */
  public ApiResponse<String> startBackupWithHttpInfo(String fileName, Boolean addTimestamp, Boolean includeConfigs, Boolean includeDatabase, Boolean includeBuildLogs, Boolean includePersonalChanges, Boolean includeRunningBuilds, Boolean includeSupplimentaryData, BackupProcessManager body) throws ApiException {
    Object __localVarPostBody = body;
    
    // create path and map variables
    String __localVarPath = "/app/rest/server/backup";

    // query params
    java.util.List<Pair> __localVarQueryParams = new java.util.ArrayList<Pair>();
    java.util.Map<String, String> __localVarHeaderParams = new java.util.HashMap<String, String>();
    java.util.Map<String, Object> __localVarFormParams = new java.util.HashMap<String, Object>();

    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "fileName", fileName));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "addTimestamp", addTimestamp));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeConfigs", includeConfigs));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeDatabase", includeDatabase));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeBuildLogs", includeBuildLogs));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includePersonalChanges", includePersonalChanges));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeRunningBuilds", includeRunningBuilds));
    __localVarQueryParams.addAll(apiClient.parameterToPairs("", "includeSupplimentaryData", includeSupplimentaryData));

    
    
    final String[] __localVarAccepts = {
      
    };
    final String __localVarAccept = apiClient.selectHeaderAccept(__localVarAccepts);

    final String[] __localVarContentTypes = {
      
    };
    final String __localVarContentType = apiClient.selectHeaderContentType(__localVarContentTypes);

    String[] __localVarAuthNames = { "Basic", "Bearer" };

    GenericType<String> __localVarReturnType = new GenericType<String>() {};
    return apiClient.invokeAPI(__localVarPath, "POST", __localVarQueryParams, __localVarPostBody, __localVarHeaderParams, __localVarFormParams, __localVarAccept, __localVarContentType, __localVarAuthNames, __localVarReturnType);
      }
}
