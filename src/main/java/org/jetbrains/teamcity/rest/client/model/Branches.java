/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.jetbrains.teamcity.rest.client.model.Branch;
import com.fasterxml.jackson.dataformat.xml.annotation.*;
import javax.xml.bind.annotation.*;

/**
 * Branches
 */

@XmlRootElement(name = "branches")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "branches")
public class Branches {
  @JsonProperty("count")
  @JacksonXmlProperty(isAttribute = true, localName = "count")
  @XmlAttribute(name = "count")
  private Integer count = null;

  @JsonProperty("href")
  @JacksonXmlProperty(isAttribute = true, localName = "href")
  @XmlAttribute(name = "href")
  private String href = null;

  @JsonProperty("branch")
  // Is a container wrapped=false
  // items.name=branch items.baseName=branch items.xmlName= items.xmlNamespace=
  // items.example= items.type=Branch
  @XmlElement(name = "branch")
  private java.util.List<Branch> branch = null;

  public Branches count(Integer count) {
    this.count = count;
    return this;
  }

   /**
   * Get count
   * @return count
  **/
  @ApiModelProperty(value = "")
  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Branches href(String href) {
    this.href = href;
    return this;
  }

   /**
   * Get href
   * @return href
  **/
  @ApiModelProperty(value = "")
  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public Branches branch(java.util.List<Branch> branch) {
    this.branch = branch;
    return this;
  }

  public Branches addBranchItem(Branch branchItem) {
    if (this.branch == null) {
      this.branch = new java.util.ArrayList<>();
    }
    this.branch.add(branchItem);
    return this;
  }

   /**
   * Get branch
   * @return branch
  **/
  @ApiModelProperty(value = "")
  public java.util.List<Branch> getBranch() {
    return branch;
  }

  public void setBranch(java.util.List<Branch> branch) {
    this.branch = branch;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Branches branches = (Branches) o;
    return Objects.equals(this.count, branches.count) &&
        Objects.equals(this.href, branches.href) &&
        Objects.equals(this.branch, branches.branch);
  }

  @Override
  public int hashCode() {
    return Objects.hash(count, href, branch);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Branches {\n");
    
    sb.append("    count: ").append(toIndentedString(count)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    branch: ").append(toIndentedString(branch)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

