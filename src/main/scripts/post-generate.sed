#
# post-generate.sed
#
# vi: ft=sed :
#

s/\b__callback\b/callback/g
s/\b__apiClient\b/apiClient/g
s/@Test\b/@org.junit.Test/g
/\bimport\s+com\.jetbrains\.teamcity\.rest\.client\.model\.ArrayList\s*;/d
/\bimport\s+org\.junit\.Test\s*;/d

# Add HTTP Basic and OAuth 2.0 authentication support.
s/\bString\s*\[\s*\]\s+__localVarAuthNames\s+=\s+new\s+String\s*\[\s*\]\s+\{\s+\}\s*;/String[] __localVarAuthNames = { "Basic", "Bearer" };/g
