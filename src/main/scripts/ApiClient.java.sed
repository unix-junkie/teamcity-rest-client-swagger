#
# ApiClient.java.sed
#
# vi: ft=sed :
#

# Enable OAuth 2.0 support by default (no longer requires subclassing ApiClient)
/\bauthentications\s*\.\s*put\s*\(\s*"Basic"\s*,\s*new\s+HttpBasicAuth\s*\(\s*\)\s*\)\s*;/a \    authentications.put("Bearer", new OAuth());

