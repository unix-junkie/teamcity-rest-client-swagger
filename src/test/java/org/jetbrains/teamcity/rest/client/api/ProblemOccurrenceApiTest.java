/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.model.ProblemOccurrence;
import org.jetbrains.teamcity.rest.client.model.ProblemOccurrences;
import org.junit.Ignore;


/**
 * API tests for ProblemOccurrenceApi
 */
@Ignore
public class ProblemOccurrenceApiTest {

    private final ProblemOccurrenceApi api = new ProblemOccurrenceApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getProblemsTest() throws ApiException {
        String locator = null;
        String fields = null;
        ProblemOccurrences response = api.getProblems(locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveInstanceTest() throws ApiException {
        String problemLocator = null;
        String fields = null;
        ProblemOccurrence response = api.serveInstance(problemLocator, fields);

        // TODO: test validations
    }
    
}
