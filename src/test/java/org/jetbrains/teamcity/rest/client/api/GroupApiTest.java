/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.model.Group;
import org.jetbrains.teamcity.rest.client.model.Groups;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Role;
import org.jetbrains.teamcity.rest.client.model.Roles;
import org.junit.Ignore;


/**
 * API tests for GroupApi
 */
@Ignore
public class GroupApiTest {

    private final GroupApi api = new GroupApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void addGroupTest() throws ApiException {
        Group body = null;
        String fields = null;
        Group response = api.addGroup(body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void addRoleTest() throws ApiException {
        String groupLocator = null;
        Role body = null;
        Role response = api.addRole(groupLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void addRoleSimpleTest() throws ApiException {
        String groupLocator = null;
        String roleId = null;
        String scope = null;
        Role response = api.addRoleSimple(groupLocator, roleId, scope);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteGroupTest() throws ApiException {
        String groupLocator = null;
        api.deleteGroup(groupLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteRoleTest() throws ApiException {
        String groupLocator = null;
        String roleId = null;
        String scope = null;
        api.deleteRole(groupLocator, roleId, scope);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParentGroupsTest() throws ApiException {
        String groupLocator = null;
        String fields = null;
        Groups response = api.getParentGroups(groupLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getPermissionsTest() throws ApiException {
        String groupLocator = null;
        String response = api.getPermissions(groupLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getPropertiesTest() throws ApiException {
        String groupLocator = null;
        String fields = null;
        Properties response = api.getProperties(groupLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void listRoleTest() throws ApiException {
        String groupLocator = null;
        String roleId = null;
        String scope = null;
        Role response = api.listRole(groupLocator, roleId, scope);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void listRolesTest() throws ApiException {
        String groupLocator = null;
        Roles response = api.listRoles(groupLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void putUserPropertyTest() throws ApiException {
        String groupLocator = null;
        String name = null;
        String body = null;
        String response = api.putUserProperty(groupLocator, name, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void removeUserPropertyTest() throws ApiException {
        String groupLocator = null;
        String name = null;
        api.removeUserProperty(groupLocator, name);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveGroupTest() throws ApiException {
        String groupLocator = null;
        String fields = null;
        Group response = api.serveGroup(groupLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveGroupsTest() throws ApiException {
        String fields = null;
        Groups response = api.serveGroups(fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveUserPropertiesTest() throws ApiException {
        String groupLocator = null;
        String name = null;
        String response = api.serveUserProperties(groupLocator, name);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParentGroupsTest() throws ApiException {
        String groupLocator = null;
        Groups body = null;
        String fields = null;
        Groups response = api.setParentGroups(groupLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setRolesTest() throws ApiException {
        String groupLocator = null;
        Roles body = null;
        Roles response = api.setRoles(groupLocator, body);

        // TODO: test validations
    }
    
}
