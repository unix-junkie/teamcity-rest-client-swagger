/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.VcsRoot;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstance;
import org.jetbrains.teamcity.rest.client.model.VcsRootInstances;
import org.jetbrains.teamcity.rest.client.model.VcsRoots;
import org.junit.Ignore;


/**
 * API tests for VcsRootApi
 */
@Ignore
public class VcsRootApiTest {

    private final VcsRootApi api = new VcsRootApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void addRootTest() throws ApiException {
        VcsRoot body = null;
        String fields = null;
        VcsRoot response = api.addRoot(body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void changePropertiesTest() throws ApiException {
        String vcsRootLocator = null;
        Properties body = null;
        String fields = null;
        Properties response = api.changeProperties(vcsRootLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteAllPropertiesTest() throws ApiException {
        String vcsRootLocator = null;
        api.deleteAllProperties(vcsRootLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteParameterTest() throws ApiException {
        String vcsRootLocator = null;
        String name = null;
        api.deleteParameter(vcsRootLocator, name);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteRootTest() throws ApiException {
        String vcsRootLocator = null;
        api.deleteRoot(vcsRootLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getSettingsFileTest() throws ApiException {
        String vcsRootLocator = null;
        String response = api.getSettingsFile(vcsRootLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void putParameterTest() throws ApiException {
        String vcsRootLocator = null;
        String name = null;
        String body = null;
        String response = api.putParameter(vcsRootLocator, name, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveFieldTest() throws ApiException {
        String vcsRootLocator = null;
        String field = null;
        String response = api.serveField(vcsRootLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveInstanceFieldTest() throws ApiException {
        String vcsRootLocator = null;
        String vcsRootInstanceLocator = null;
        String field = null;
        String response = api.serveInstanceField(vcsRootLocator, vcsRootInstanceLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void servePropertiesTest() throws ApiException {
        String vcsRootLocator = null;
        String fields = null;
        Properties response = api.serveProperties(vcsRootLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void servePropertyTest() throws ApiException {
        String vcsRootLocator = null;
        String name = null;
        String response = api.serveProperty(vcsRootLocator, name);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveRootTest() throws ApiException {
        String vcsRootLocator = null;
        String fields = null;
        VcsRoot response = api.serveRoot(vcsRootLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveRootInstanceTest() throws ApiException {
        String vcsRootLocator = null;
        String vcsRootInstanceLocator = null;
        String fields = null;
        VcsRootInstance response = api.serveRootInstance(vcsRootLocator, vcsRootInstanceLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveRootInstancePropertiesTest() throws ApiException {
        String vcsRootLocator = null;
        String vcsRootInstanceLocator = null;
        String fields = null;
        Properties response = api.serveRootInstanceProperties(vcsRootLocator, vcsRootInstanceLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveRootInstancesTest() throws ApiException {
        String vcsRootLocator = null;
        String fields = null;
        VcsRootInstances response = api.serveRootInstances(vcsRootLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveRootsTest() throws ApiException {
        String locator = null;
        String fields = null;
        VcsRoots response = api.serveRoots(locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setFieldTest() throws ApiException {
        String vcsRootLocator = null;
        String field = null;
        String body = null;
        String response = api.setField(vcsRootLocator, field, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setInstanceFieldTest() throws ApiException {
        String vcsRootLocator = null;
        String vcsRootInstanceLocator = null;
        String field = null;
        String body = null;
        String response = api.setInstanceField(vcsRootLocator, vcsRootInstanceLocator, field, body);

        // TODO: test validations
    }
    
}
