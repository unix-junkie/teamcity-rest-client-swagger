/*
 * TeamCity REST API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2018.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package org.jetbrains.teamcity.rest.client.api;

import org.jetbrains.teamcity.rest.client.swagger.ApiException;
import org.jetbrains.teamcity.rest.client.model.AgentPool;
import org.jetbrains.teamcity.rest.client.model.AgentPools;
import org.jetbrains.teamcity.rest.client.model.Branches;
import org.jetbrains.teamcity.rest.client.model.Build;
import org.jetbrains.teamcity.rest.client.model.BuildType;
import org.jetbrains.teamcity.rest.client.model.BuildTypes;
import org.jetbrains.teamcity.rest.client.model.Builds;
import org.jetbrains.teamcity.rest.client.model.NewBuildTypeDescription;
import org.jetbrains.teamcity.rest.client.model.NewProjectDescription;
import org.jetbrains.teamcity.rest.client.model.Project;
import org.jetbrains.teamcity.rest.client.model.ProjectFeature;
import org.jetbrains.teamcity.rest.client.model.ProjectFeatures;
import org.jetbrains.teamcity.rest.client.model.Projects;
import org.jetbrains.teamcity.rest.client.model.Properties;
import org.jetbrains.teamcity.rest.client.model.Property;
import org.jetbrains.teamcity.rest.client.model.Type;
import org.junit.Ignore;


/**
 * API tests for ProjectApi
 */
@Ignore
public class ProjectApiTest {

    private final ProjectApi api = new ProjectApi();

    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void addTest() throws ApiException {
        String projectLocator = null;
        ProjectFeature body = null;
        String fields = null;
        Object response = api.add(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void createBuildTypeTest() throws ApiException {
        String projectLocator = null;
        NewBuildTypeDescription body = null;
        String fields = null;
        BuildType response = api.createBuildType(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void createBuildTypeTemplateTest() throws ApiException {
        String projectLocator = null;
        NewBuildTypeDescription body = null;
        String fields = null;
        BuildType response = api.createBuildTypeTemplate(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void createProjectTest() throws ApiException {
        NewProjectDescription body = null;
        Project response = api.createProject(body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void createSecureTokenTest() throws ApiException {
        String projectLocator = null;
        String body = null;
        String response = api.createSecureToken(projectLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteTest() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        api.delete(featureLocator, projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteAllParametersTest() throws ApiException {
        String projectLocator = null;
        api.deleteAllParameters(projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteAllParameters_0Test() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        String fields = null;
        api.deleteAllParameters_0(featureLocator, projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteParameterTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        api.deleteParameter(name, projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteParameter_0Test() throws ApiException {
        String name = null;
        String featureLocator = null;
        String projectLocator = null;
        String fields = null;
        api.deleteParameter_0(name, featureLocator, projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteProjectTest() throws ApiException {
        String projectLocator = null;
        api.deleteProject(projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void deleteProjectAgentPoolsTest() throws ApiException {
        String projectLocator = null;
        String agentPoolLocator = null;
        api.deleteProjectAgentPools(projectLocator, agentPoolLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getTest() throws ApiException {
        String projectLocator = null;
        String locator = null;
        String fields = null;
        Object response = api.get(projectLocator, locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getBranchesTest() throws ApiException {
        String projectLocator = null;
        String locator = null;
        String fields = null;
        Branches response = api.getBranches(projectLocator, locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getBuildTypesOrderTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        BuildTypes response = api.getBuildTypesOrder(projectLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getDefaultTemplateTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        BuildType response = api.getDefaultTemplate(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getExampleNewProjectDescriptionTest() throws ApiException {
        String projectLocator = null;
        String id = null;
        NewProjectDescription response = api.getExampleNewProjectDescription(projectLocator, id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getExampleNewProjectDescriptionCompatibilityVersion1Test() throws ApiException {
        String projectLocator = null;
        String id = null;
        NewProjectDescription response = api.getExampleNewProjectDescriptionCompatibilityVersion1(projectLocator, id);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameterTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        String fields = null;
        Property response = api.getParameter(name, projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameterTypeTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        Type response = api.getParameterType(name, projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameterTypeRawValueTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        String response = api.getParameterTypeRawValue(name, projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameterValueLongTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        String response = api.getParameterValueLong(name, projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameterValueLong_0Test() throws ApiException {
        String name = null;
        String featureLocator = null;
        String projectLocator = null;
        String fields = null;
        String response = api.getParameterValueLong_0(name, featureLocator, projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameter_0Test() throws ApiException {
        String name = null;
        String featureLocator = null;
        String projectLocator = null;
        String fields = null;
        String fields2 = null;
        Property response = api.getParameter_0(name, featureLocator, projectLocator, fields, fields2);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParametersTest() throws ApiException {
        String projectLocator = null;
        String locator = null;
        String fields = null;
        Properties response = api.getParameters(projectLocator, locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParameters_0Test() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        String locator = null;
        String fields = null;
        String fields2 = null;
        Properties response = api.getParameters_0(featureLocator, projectLocator, locator, fields, fields2);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getParentProjectTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        Project response = api.getParentProject(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getProjectAgentPoolsTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        AgentPools response = api.getProjectAgentPools(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getProjectsOrderTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        Projects response = api.getProjectsOrder(projectLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getSecureValueTest() throws ApiException {
        String projectLocator = null;
        String token = null;
        String response = api.getSecureValue(projectLocator, token);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getSettingsFileTest() throws ApiException {
        String projectLocator = null;
        String response = api.getSettingsFile(projectLocator);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void getSingleTest() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        String fields = null;
        Object response = api.getSingle(featureLocator, projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void reloadSettingsFileTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        Project response = api.reloadSettingsFile(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void removeDefaultTemplateTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        api.removeDefaultTemplate(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void replaceTest() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        ProjectFeature body = null;
        String fields = null;
        Object response = api.replace(featureLocator, projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void replaceAllTest() throws ApiException {
        String projectLocator = null;
        ProjectFeatures body = null;
        String fields = null;
        Object response = api.replaceAll(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildFieldWithProjectTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String buildLocator = null;
        String field = null;
        String response = api.serveBuildFieldWithProject(projectLocator, btLocator, buildLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildTypeTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String fields = null;
        BuildType response = api.serveBuildType(projectLocator, btLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildTypeFieldWithProjectTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String field = null;
        String response = api.serveBuildTypeFieldWithProject(projectLocator, btLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildTypeTemplatesTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String fields = null;
        BuildType response = api.serveBuildTypeTemplates(projectLocator, btLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildTypesInProjectTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        BuildTypes response = api.serveBuildTypesInProject(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildWithProjectTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String buildLocator = null;
        String fields = null;
        Build response = api.serveBuildWithProject(projectLocator, btLocator, buildLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveBuildsTest() throws ApiException {
        String projectLocator = null;
        String btLocator = null;
        String status = null;
        String triggeredByUser = null;
        Boolean includePersonal = null;
        Boolean includeCanceled = null;
        Boolean onlyPinned = null;
        java.util.List<String> tag = null;
        String agentName = null;
        String sinceBuild = null;
        String sinceDate = null;
        Long start = null;
        Integer count = null;
        String locator = null;
        String fields = null;
        Builds response = api.serveBuilds(projectLocator, btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveProjectTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        Project response = api.serveProject(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveProjectFieldTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        String response = api.serveProjectField(projectLocator, field);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveProjectsTest() throws ApiException {
        String locator = null;
        String fields = null;
        Projects response = api.serveProjects(locator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void serveTemplatesInProjectTest() throws ApiException {
        String projectLocator = null;
        String fields = null;
        BuildTypes response = api.serveTemplatesInProject(projectLocator, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setBuildTypesOrderTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        BuildTypes body = null;
        BuildTypes response = api.setBuildTypesOrder(projectLocator, field, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setDefaultTemplateTest() throws ApiException {
        String projectLocator = null;
        BuildType body = null;
        String fields = null;
        BuildType response = api.setDefaultTemplate(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameterTest() throws ApiException {
        String projectLocator = null;
        Property body = null;
        String fields = null;
        Property response = api.setParameter(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameterTypeTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        Type body = null;
        Type response = api.setParameterType(name, projectLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameterTypeRawValueTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        String body = null;
        String response = api.setParameterTypeRawValue(name, projectLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameterValueLongTest() throws ApiException {
        String name = null;
        String projectLocator = null;
        String body = null;
        String response = api.setParameterValueLong(name, projectLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameterValueLong_0Test() throws ApiException {
        String name = null;
        String featureLocator = null;
        String projectLocator = null;
        String body = null;
        String fields = null;
        String response = api.setParameterValueLong_0(name, featureLocator, projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameter_0Test() throws ApiException {
        String name = null;
        String projectLocator = null;
        Property body = null;
        String fields = null;
        Property response = api.setParameter_0(name, projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameter_1Test() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        Property body = null;
        String fields = null;
        String fields2 = null;
        Property response = api.setParameter_1(featureLocator, projectLocator, body, fields, fields2);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameter_2Test() throws ApiException {
        String name = null;
        String featureLocator = null;
        String projectLocator = null;
        Property body = null;
        String fields = null;
        String fields2 = null;
        Property response = api.setParameter_2(name, featureLocator, projectLocator, body, fields, fields2);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParametersTest() throws ApiException {
        String projectLocator = null;
        Properties body = null;
        String fields = null;
        Properties response = api.setParameters(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParameters_0Test() throws ApiException {
        String featureLocator = null;
        String projectLocator = null;
        Properties body = null;
        String fields = null;
        String fields2 = null;
        Properties response = api.setParameters_0(featureLocator, projectLocator, body, fields, fields2);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setParentProjectTest() throws ApiException {
        String projectLocator = null;
        Project body = null;
        String fields = null;
        Project response = api.setParentProject(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setProjectAgentPoolsTest() throws ApiException {
        String projectLocator = null;
        AgentPools body = null;
        String fields = null;
        AgentPools response = api.setProjectAgentPools(projectLocator, body, fields);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setProjectAgentPools_0Test() throws ApiException {
        String projectLocator = null;
        AgentPool body = null;
        AgentPool response = api.setProjectAgentPools_0(projectLocator, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setProjectFieldTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        String body = null;
        String response = api.setProjectField(projectLocator, field, body);

        // TODO: test validations
    }
    
    /**
     * 
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @org.junit.Test
    public void setProjectsOrderTest() throws ApiException {
        String projectLocator = null;
        String field = null;
        Projects body = null;
        Projects response = api.setProjectsOrder(projectLocator, field, body);

        // TODO: test validations
    }
    
}
