#!/bin/bash

LANGUAGE='java'
INPUT_FILE="$(dirname $0)/swagger.yaml"
CONFIG_FILE="$(dirname $0)/swagger-config-${LANGUAGE}.json"
SWAGGER_VERSION='2.4.8'
SWAGGER_JAR='swagger-codegen-cli.jar'
SWAGGER_CODEGEN_ARGS="generate -l ${LANGUAGE} -c ${CONFIG_FILE} -i ${INPUT_FILE}"
JAVA_OPTIONS='-Xmx16g'
SCRIPT_DIR="$(dirname $0)/src/main/scripts"

if [[ ! -f "${INPUT_FILE}" ]]
then
	echo "$(basename ${INPUT_FILE}) should be placed in same directory as this script (${PWD})"
	exit 1
fi

if [[ ! -f "${CONFIG_FILE}" ]]; then
	echo "$(basename ${CONFIG_FILE}) should be placed in same directory as this script (${PWD})"
	exit 1
fi

if command -v swager-codegen >/dev/null
then
	SWAGGER_CMD_LINE='swagger-codegen'
elif [[ -f "$(dirname $0)/${SWAGGER_JAR}" ]]
then
	SWAGGER_CMD_LINE="java ${JAVA_OPTIONS} -jar ${SWAGGER_JAR}"
else
	wget -c https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/${SWAGGER_VERSION}/swagger-codegen-cli-${SWAGGER_VERSION}.jar -O ${SWAGGER_JAR}
	SWAGGER_CMD_LINE="java ${JAVA_OPTIONS} -jar ${SWAGGER_JAR}"
fi

rm -rf "$(dirname $0)/src/main/java" "$(dirname $0)/src/test/java" "$(dirname $0)/docs"

${SWAGGER_CMD_LINE} ${SWAGGER_CODEGEN_ARGS}

rm -rf "$(dirname $0)/.swagger-codegen" "$(dirname $0)/gradle"

# Fix weird generated stuff
find "$(dirname $0)/src/main/java" "$(dirname $0)/src/test/java" -type f -name '*\.java' -exec sed -E -i'.orig' -f "${SCRIPT_DIR}/post-generate.sed" '{}' ';'
find "$(dirname $0)/src/main/java" -type f -name 'File.java' -exec sed -E -i'.orig' -f "${SCRIPT_DIR}/File.java.sed" '{}' ';'
find "$(dirname $0)/src/main/java" -type f -name 'ApiClient.java' -exec sed -E -i'.orig' -f "${SCRIPT_DIR}/ApiClient.java.sed" '{}' ';'

find "$(dirname $0)/src/main/java" "$(dirname $0)/src/test/java" -type f -name '*\.java\.orig' -delete
