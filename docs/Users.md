
# Users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**user** | [**java.util.List&lt;User&gt;**](User.md) |  |  [optional]



