
# Triggers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**trigger** | [**java.util.List&lt;Trigger&gt;**](Trigger.md) |  |  [optional]



