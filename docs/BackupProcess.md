
# BackupProcess

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**briefInfo** | [**BackupProcessInfo**](BackupProcessInfo.md) |  |  [optional]
**progressInfo** | [**ProgressInfo**](ProgressInfo.md) |  |  [optional]
**processKind** | [**ProcessKindEnum**](#ProcessKindEnum) |  |  [optional]
**finished** | **Boolean** |  |  [optional]
**progressStatus** | [**ProgressStatusEnum**](#ProgressStatusEnum) |  |  [optional]
**exceptions** | [**java.util.List&lt;Exception&gt;**](Exception.md) |  |  [optional]
**processId** | **Integer** |  |  [optional]


<a name="ProcessKindEnum"></a>
## Enum: ProcessKindEnum
Name | Value
---- | -----
BACKUP | &quot;Backup&quot;
RESTORE | &quot;Restore&quot;
CLEANUP | &quot;Cleanup&quot;
IMPORT | &quot;Import&quot;


<a name="ProgressStatusEnum"></a>
## Enum: ProgressStatusEnum
Name | Value
---- | -----
RUNNING | &quot;Running&quot;
CANCELLING | &quot;Cancelling&quot;
CANCELLED | &quot;Cancelled&quot;
FINISHED | &quot;Finished&quot;
FAULT | &quot;Fault&quot;



