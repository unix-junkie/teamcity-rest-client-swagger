
# Sessions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**maxActive** | **Integer** |  |  [optional]
**sessionCounter** | **Integer** |  |  [optional]
**sessionCreateRate** | **Integer** |  |  [optional]
**sessionExpireRate** | **Integer** |  |  [optional]
**sessionMaxAliveTime** | **Integer** |  |  [optional]
**session** | [**java.util.List&lt;Session&gt;**](Session.md) |  |  [optional]



