
# Files

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**file** | [**java.util.List&lt;File&gt;**](File.md) |  |  [optional]



