# UserApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addGroup**](UserApi.md#addGroup) | **POST** /app/rest/users/{userLocator}/groups | 
[**addRole**](UserApi.md#addRole) | **POST** /app/rest/users/{userLocator}/roles | 
[**addRoleSimple**](UserApi.md#addRoleSimple) | **PUT** /app/rest/users/{userLocator}/roles/{roleId}/{scope} | 
[**addRoleSimplePost**](UserApi.md#addRoleSimplePost) | **POST** /app/rest/users/{userLocator}/roles/{roleId}/{scope} | 
[**createToken**](UserApi.md#createToken) | **POST** /app/rest/users/{userLocator}/tokens/{name} | 
[**createUser**](UserApi.md#createUser) | **POST** /app/rest/users | 
[**deleteRememberMe**](UserApi.md#deleteRememberMe) | **DELETE** /app/rest/users/{userLocator}/debug/rememberMe | 
[**deleteRole**](UserApi.md#deleteRole) | **DELETE** /app/rest/users/{userLocator}/roles/{roleId}/{scope} | 
[**deleteToken**](UserApi.md#deleteToken) | **DELETE** /app/rest/users/{userLocator}/tokens/{name} | 
[**deleteUser**](UserApi.md#deleteUser) | **DELETE** /app/rest/users/{userLocator} | 
[**deleteUserField**](UserApi.md#deleteUserField) | **DELETE** /app/rest/users/{userLocator}/{field} | 
[**getGroup**](UserApi.md#getGroup) | **GET** /app/rest/users/{userLocator}/groups/{groupLocator} | 
[**getGroups**](UserApi.md#getGroups) | **GET** /app/rest/users/{userLocator}/groups | 
[**getPermissions**](UserApi.md#getPermissions) | **GET** /app/rest/users/{userLocator}/debug/permissions | 
[**getPermissions_0**](UserApi.md#getPermissions_0) | **GET** /app/rest/users/{userLocator}/permissions | 
[**getTokens**](UserApi.md#getTokens) | **GET** /app/rest/users/{userLocator}/tokens | 
[**listRole**](UserApi.md#listRole) | **GET** /app/rest/users/{userLocator}/roles/{roleId}/{scope} | 
[**listRoles**](UserApi.md#listRoles) | **GET** /app/rest/users/{userLocator}/roles | 
[**putUserProperty**](UserApi.md#putUserProperty) | **PUT** /app/rest/users/{userLocator}/properties/{name} | 
[**removeGroup**](UserApi.md#removeGroup) | **DELETE** /app/rest/users/{userLocator}/groups/{groupLocator} | 
[**removeUserProperty**](UserApi.md#removeUserProperty) | **DELETE** /app/rest/users/{userLocator}/properties/{name} | 
[**replaceGroups**](UserApi.md#replaceGroups) | **PUT** /app/rest/users/{userLocator}/groups | 
[**replaceRoles**](UserApi.md#replaceRoles) | **PUT** /app/rest/users/{userLocator}/roles | 
[**serveUser**](UserApi.md#serveUser) | **GET** /app/rest/users/{userLocator} | 
[**serveUserField**](UserApi.md#serveUserField) | **GET** /app/rest/users/{userLocator}/{field} | 
[**serveUserProperties**](UserApi.md#serveUserProperties) | **GET** /app/rest/users/{userLocator}/properties | 
[**serveUserProperty**](UserApi.md#serveUserProperty) | **GET** /app/rest/users/{userLocator}/properties/{name} | 
[**serveUsers**](UserApi.md#serveUsers) | **GET** /app/rest/users | 
[**setUserField**](UserApi.md#setUserField) | **PUT** /app/rest/users/{userLocator}/{field} | 
[**updateUser**](UserApi.md#updateUser) | **PUT** /app/rest/users/{userLocator} | 


<a name="addGroup"></a>
# **addGroup**
> Group addGroup(userLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
Group body = new Group(); // Group | 
String fields = "fields_example"; // String | 
try {
    Group result = apiInstance.addGroup(userLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#addGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **body** | [**Group**](Group.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addRole"></a>
# **addRole**
> Role addRole(userLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
Role body = new Role(); // Role | 
try {
    Role result = apiInstance.addRole(userLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#addRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **body** | [**Role**](Role.md)|  | [optional]

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addRoleSimple"></a>
# **addRoleSimple**
> Role addRoleSimple(userLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    Role result = apiInstance.addRoleSimple(userLocator, roleId, scope);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#addRoleSimple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addRoleSimplePost"></a>
# **addRoleSimplePost**
> addRoleSimplePost(userLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    apiInstance.addRoleSimplePost(userLocator, roleId, scope);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#addRoleSimplePost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createToken"></a>
# **createToken**
> Token createToken(userLocator, name, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String name = "name_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Token result = apiInstance.createToken(userLocator, name, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#createToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **name** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Token**](Token.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createUser"></a>
# **createUser**
> User createUser(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
User body = new User(); // User | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.createUser(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#createUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRememberMe"></a>
# **deleteRememberMe**
> deleteRememberMe(userLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
try {
    apiInstance.deleteRememberMe(userLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteRememberMe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRole"></a>
# **deleteRole**
> deleteRole(userLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    apiInstance.deleteRole(userLocator, roleId, scope);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteToken"></a>
# **deleteToken**
> deleteToken(userLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.deleteToken(userLocator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **name** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(userLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
try {
    apiInstance.deleteUser(userLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteUserField"></a>
# **deleteUserField**
> deleteUserField(userLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    apiInstance.deleteUserField(userLocator, field);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#deleteUserField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **field** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGroup"></a>
# **getGroup**
> Group getGroup(userLocator, groupLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String groupLocator = "groupLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Group result = apiInstance.getGroup(userLocator, groupLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **groupLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getGroups"></a>
# **getGroups**
> Groups getGroups(userLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Groups result = apiInstance.getGroups(userLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Groups**](Groups.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPermissions"></a>
# **getPermissions**
> String getPermissions(userLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
try {
    String result = apiInstance.getPermissions(userLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getPermissions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPermissions_0"></a>
# **getPermissions_0**
> PermissionAssignments getPermissions_0(userLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    PermissionAssignments result = apiInstance.getPermissions_0(userLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getPermissions_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**PermissionAssignments**](PermissionAssignments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTokens"></a>
# **getTokens**
> Tokens getTokens(userLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Tokens result = apiInstance.getTokens(userLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getTokens");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Tokens**](Tokens.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="listRole"></a>
# **listRole**
> Role listRole(userLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    Role result = apiInstance.listRole(userLocator, roleId, scope);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#listRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="listRoles"></a>
# **listRoles**
> Roles listRoles(userLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
try {
    Roles result = apiInstance.listRoles(userLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#listRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putUserProperty"></a>
# **putUserProperty**
> String putUserProperty(userLocator, name, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String name = "name_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.putUserProperty(userLocator, name, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#putUserProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **name** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeGroup"></a>
# **removeGroup**
> removeGroup(userLocator, groupLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String groupLocator = "groupLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.removeGroup(userLocator, groupLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#removeGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **groupLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeUserProperty"></a>
# **removeUserProperty**
> removeUserProperty(userLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.removeUserProperty(userLocator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#removeUserProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **name** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceGroups"></a>
# **replaceGroups**
> Groups replaceGroups(userLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
Groups body = new Groups(); // Groups | 
String fields = "fields_example"; // String | 
try {
    Groups result = apiInstance.replaceGroups(userLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#replaceGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **body** | [**Groups**](Groups.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Groups**](Groups.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceRoles"></a>
# **replaceRoles**
> Roles replaceRoles(userLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
Roles body = new Roles(); // Roles | 
try {
    Roles result = apiInstance.replaceRoles(userLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#replaceRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **body** | [**Roles**](Roles.md)|  | [optional]

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUser"></a>
# **serveUser**
> User serveUser(userLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.serveUser(userLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#serveUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUserField"></a>
# **serveUserField**
> String serveUserField(userLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveUserField(userLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#serveUserField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUserProperties"></a>
# **serveUserProperties**
> Properties serveUserProperties(userLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveUserProperties(userLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#serveUserProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUserProperty"></a>
# **serveUserProperty**
> String serveUserProperty(userLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    String result = apiInstance.serveUserProperty(userLocator, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#serveUserProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **name** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUsers"></a>
# **serveUsers**
> Users serveUsers(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Users result = apiInstance.serveUsers(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#serveUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Users**](Users.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setUserField"></a>
# **setUserField**
> String setUserField(userLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setUserField(userLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#setUserField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateUser"></a>
# **updateUser**
> User updateUser(userLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.UserApi;


UserApi apiInstance = new UserApi();
String userLocator = "userLocator_example"; // String | 
User body = new User(); // User | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.updateUser(userLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#updateUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userLocator** | **String**|  |
 **body** | [**User**](User.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

