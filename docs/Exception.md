
# Exception

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cause** | [**Throwable**](Throwable.md) |  |  [optional]
**stackTrace** | [**java.util.List&lt;StackTraceElement&gt;**](StackTraceElement.md) |  |  [optional]
**message** | **String** |  |  [optional]
**localizedMessage** | **String** |  |  [optional]
**suppressed** | [**java.util.List&lt;Throwable&gt;**](Throwable.md) |  |  [optional]



