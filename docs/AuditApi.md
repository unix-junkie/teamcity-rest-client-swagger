# AuditApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](AuditApi.md#get) | **GET** /app/rest/audit | 
[**getSingle**](AuditApi.md#getSingle) | **GET** /app/rest/audit/{auditEventLocator} | 


<a name="get"></a>
# **get**
> AuditEvents get(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AuditApi;


AuditApi apiInstance = new AuditApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AuditEvents result = apiInstance.get(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuditApi#get");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AuditEvents**](AuditEvents.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSingle"></a>
# **getSingle**
> AuditEvent getSingle(auditEventLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AuditApi;


AuditApi apiInstance = new AuditApi();
String auditEventLocator = "auditEventLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AuditEvent result = apiInstance.getSingle(auditEventLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuditApi#getSingle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **auditEventLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AuditEvent**](AuditEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

