# ServerApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLicenseKeys**](ServerApi.md#addLicenseKeys) | **POST** /app/rest/server/licensingData/licenseKeys | 
[**deleteLicenseKey**](ServerApi.md#deleteLicenseKey) | **DELETE** /app/rest/server/licensingData/licenseKeys/{licenseKey} | 
[**getBackupStatus**](ServerApi.md#getBackupStatus) | **GET** /app/rest/server/backup | 
[**getChildren**](ServerApi.md#getChildren) | **GET** /app/rest/server/files/{areaId}/children{path} | 
[**getChildrenAlias**](ServerApi.md#getChildrenAlias) | **GET** /app/rest/server/files/{areaId}/{path} | 
[**getContent**](ServerApi.md#getContent) | **GET** /app/rest/server/files/{areaId}/content{path} | 
[**getContentAlias**](ServerApi.md#getContentAlias) | **GET** /app/rest/server/files/{areaId}/files{path} | 
[**getLicenseKey**](ServerApi.md#getLicenseKey) | **GET** /app/rest/server/licensingData/licenseKeys/{licenseKey} | 
[**getLicenseKeys**](ServerApi.md#getLicenseKeys) | **GET** /app/rest/server/licensingData/licenseKeys | 
[**getLicensingData**](ServerApi.md#getLicensingData) | **GET** /app/rest/server/licensingData | 
[**getMetadata**](ServerApi.md#getMetadata) | **GET** /app/rest/server/files/{areaId}/metadata{path} | 
[**getRoot**](ServerApi.md#getRoot) | **GET** /app/rest/server/files/{areaId} | 
[**getZipped**](ServerApi.md#getZipped) | **GET** /app/rest/server/files/{areaId}/archived{path} | 
[**serveMetrics**](ServerApi.md#serveMetrics) | **GET** /app/rest/server/metrics | 
[**servePlugins**](ServerApi.md#servePlugins) | **GET** /app/rest/server/plugins | 
[**serveServerInfo**](ServerApi.md#serveServerInfo) | **GET** /app/rest/server | 
[**serveServerVersion**](ServerApi.md#serveServerVersion) | **GET** /app/rest/server/{field} | 
[**startBackup**](ServerApi.md#startBackup) | **POST** /app/rest/server/backup | 


<a name="addLicenseKeys"></a>
# **addLicenseKeys**
> LicenseKeys addLicenseKeys(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    LicenseKeys result = apiInstance.addLicenseKeys(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#addLicenseKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**LicenseKeys**](LicenseKeys.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteLicenseKey"></a>
# **deleteLicenseKey**
> deleteLicenseKey(licenseKey)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String licenseKey = "licenseKey_example"; // String | 
try {
    apiInstance.deleteLicenseKey(licenseKey);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#deleteLicenseKey");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licenseKey** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBackupStatus"></a>
# **getBackupStatus**
> String getBackupStatus(body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
BackupProcessManager body = new BackupProcessManager(); // BackupProcessManager | 
try {
    String result = apiInstance.getBackupStatus(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getBackupStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BackupProcessManager**](BackupProcessManager.md)|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildren"></a>
# **getChildren**
> Files getChildren(path, areaId, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getChildren(path, areaId, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getChildren");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildrenAlias"></a>
# **getChildrenAlias**
> Files getChildrenAlias(path, areaId, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getChildrenAlias(path, areaId, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getChildrenAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContent"></a>
# **getContent**
> getContent(path, areaId, responseBuilder)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
String responseBuilder = "responseBuilder_example"; // String | 
try {
    apiInstance.getContent(path, areaId, responseBuilder);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getContent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |
 **responseBuilder** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContentAlias"></a>
# **getContentAlias**
> getContentAlias(path, areaId)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
try {
    apiInstance.getContentAlias(path, areaId);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getContentAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicenseKey"></a>
# **getLicenseKey**
> LicenseKey getLicenseKey(licenseKey, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String licenseKey = "licenseKey_example"; // String | 
String fields = "fields_example"; // String | 
try {
    LicenseKey result = apiInstance.getLicenseKey(licenseKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getLicenseKey");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licenseKey** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**LicenseKey**](LicenseKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicenseKeys"></a>
# **getLicenseKeys**
> LicenseKeys getLicenseKeys(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fields = "fields_example"; // String | 
try {
    LicenseKeys result = apiInstance.getLicenseKeys(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getLicenseKeys");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**LicenseKeys**](LicenseKeys.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLicensingData"></a>
# **getLicensingData**
> LicensingData getLicensingData(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fields = "fields_example"; // String | 
try {
    LicensingData result = apiInstance.getLicensingData(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getLicensingData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**LicensingData**](LicensingData.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMetadata"></a>
# **getMetadata**
> File getMetadata(path, areaId, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    File result = apiInstance.getMetadata(path, areaId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRoot"></a>
# **getRoot**
> Files getRoot(areaId, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String areaId = "areaId_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getRoot(areaId, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **areaId** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getZipped"></a>
# **getZipped**
> getZipped(path, areaId, basePath, locator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String path = "path_example"; // String | 
String areaId = "areaId_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.getZipped(path, areaId, basePath, locator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#getZipped");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **areaId** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **name** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveMetrics"></a>
# **serveMetrics**
> Metrics serveMetrics(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fields = "fields_example"; // String | 
try {
    Metrics result = apiInstance.serveMetrics(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#serveMetrics");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Metrics**](Metrics.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="servePlugins"></a>
# **servePlugins**
> Plugins servePlugins(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fields = "fields_example"; // String | 
try {
    Plugins result = apiInstance.servePlugins(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#servePlugins");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Plugins**](Plugins.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveServerInfo"></a>
# **serveServerInfo**
> Server serveServerInfo(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fields = "fields_example"; // String | 
try {
    Server result = apiInstance.serveServerInfo(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#serveServerInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Server**](Server.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveServerVersion"></a>
# **serveServerVersion**
> String serveServerVersion(field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveServerVersion(field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#serveServerVersion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="startBackup"></a>
# **startBackup**
> String startBackup(fileName, addTimestamp, includeConfigs, includeDatabase, includeBuildLogs, includePersonalChanges, includeRunningBuilds, includeSupplimentaryData, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ServerApi;


ServerApi apiInstance = new ServerApi();
String fileName = "fileName_example"; // String | 
Boolean addTimestamp = true; // Boolean | 
Boolean includeConfigs = true; // Boolean | 
Boolean includeDatabase = true; // Boolean | 
Boolean includeBuildLogs = true; // Boolean | 
Boolean includePersonalChanges = true; // Boolean | 
Boolean includeRunningBuilds = true; // Boolean | 
Boolean includeSupplimentaryData = true; // Boolean | 
BackupProcessManager body = new BackupProcessManager(); // BackupProcessManager | 
try {
    String result = apiInstance.startBackup(fileName, addTimestamp, includeConfigs, includeDatabase, includeBuildLogs, includePersonalChanges, includeRunningBuilds, includeSupplimentaryData, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ServerApi#startBackup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileName** | **String**|  | [optional]
 **addTimestamp** | **Boolean**|  | [optional]
 **includeConfigs** | **Boolean**|  | [optional]
 **includeDatabase** | **Boolean**|  | [optional]
 **includeBuildLogs** | **Boolean**|  | [optional]
 **includePersonalChanges** | **Boolean**|  | [optional]
 **includeRunningBuilds** | **Boolean**|  | [optional]
 **includeSupplimentaryData** | **Boolean**|  | [optional]
 **body** | [**BackupProcessManager**](BackupProcessManager.md)|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

