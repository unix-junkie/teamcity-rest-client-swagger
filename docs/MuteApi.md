# MuteApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createInstance**](MuteApi.md#createInstance) | **POST** /app/rest/mutes | 
[**createInstances**](MuteApi.md#createInstances) | **POST** /app/rest/mutes/multiple | 
[**deleteInstance**](MuteApi.md#deleteInstance) | **DELETE** /app/rest/mutes/{muteLocator} | 
[**getMutes**](MuteApi.md#getMutes) | **GET** /app/rest/mutes | 
[**serveInstance**](MuteApi.md#serveInstance) | **GET** /app/rest/mutes/{muteLocator} | 


<a name="createInstance"></a>
# **createInstance**
> Mute createInstance(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.MuteApi;


MuteApi apiInstance = new MuteApi();
Mute body = new Mute(); // Mute | 
String fields = "fields_example"; // String | 
try {
    Mute result = apiInstance.createInstance(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MuteApi#createInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Mute**](Mute.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Mute**](Mute.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createInstances"></a>
# **createInstances**
> Mutes createInstances(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.MuteApi;


MuteApi apiInstance = new MuteApi();
Mutes body = new Mutes(); // Mutes | 
String fields = "fields_example"; // String | 
try {
    Mutes result = apiInstance.createInstances(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MuteApi#createInstances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Mutes**](Mutes.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Mutes**](Mutes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteInstance"></a>
# **deleteInstance**
> deleteInstance(muteLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.MuteApi;


MuteApi apiInstance = new MuteApi();
String muteLocator = "muteLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    apiInstance.deleteInstance(muteLocator, body);
} catch (ApiException e) {
    System.err.println("Exception when calling MuteApi#deleteInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **muteLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMutes"></a>
# **getMutes**
> Mutes getMutes(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.MuteApi;


MuteApi apiInstance = new MuteApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Mutes result = apiInstance.getMutes(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MuteApi#getMutes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Mutes**](Mutes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> Mute serveInstance(muteLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.MuteApi;


MuteApi apiInstance = new MuteApi();
String muteLocator = "muteLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Mute result = apiInstance.serveInstance(muteLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MuteApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **muteLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Mute**](Mute.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

