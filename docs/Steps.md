
# Steps

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**step** | [**java.util.List&lt;Step&gt;**](Step.md) |  |  [optional]



