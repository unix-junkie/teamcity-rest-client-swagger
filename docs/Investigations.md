
# Investigations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**href** | **String** |  |  [optional]
**investigation** | [**java.util.List&lt;Investigation&gt;**](Investigation.md) |  |  [optional]



