
# ProjectFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**projectFeature** | [**java.util.List&lt;ProjectFeature&gt;**](ProjectFeature.md) |  |  [optional]



