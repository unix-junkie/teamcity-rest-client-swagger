# BuildTypeApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAgentRequirement**](BuildTypeApi.md#addAgentRequirement) | **POST** /app/rest/buildTypes/{btLocator}/agent-requirements | 
[**addArtifactDep**](BuildTypeApi.md#addArtifactDep) | **POST** /app/rest/buildTypes/{btLocator}/artifact-dependencies | 
[**addBuildType**](BuildTypeApi.md#addBuildType) | **POST** /app/rest/buildTypes | 
[**addFeature**](BuildTypeApi.md#addFeature) | **POST** /app/rest/buildTypes/{btLocator}/features | 
[**addFeatureParameter**](BuildTypeApi.md#addFeatureParameter) | **PUT** /app/rest/buildTypes/{btLocator}/features/{featureId}/parameters/{parameterName} | 
[**addSnapshotDep**](BuildTypeApi.md#addSnapshotDep) | **POST** /app/rest/buildTypes/{btLocator}/snapshot-dependencies | 
[**addStep**](BuildTypeApi.md#addStep) | **POST** /app/rest/buildTypes/{btLocator}/steps | 
[**addStepParameter**](BuildTypeApi.md#addStepParameter) | **PUT** /app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters/{parameterName} | 
[**addTemplate**](BuildTypeApi.md#addTemplate) | **POST** /app/rest/buildTypes/{btLocator}/templates | 
[**addTrigger**](BuildTypeApi.md#addTrigger) | **POST** /app/rest/buildTypes/{btLocator}/triggers | 
[**addVcsRootEntry**](BuildTypeApi.md#addVcsRootEntry) | **POST** /app/rest/buildTypes/{btLocator}/vcs-root-entries | 
[**changeArtifactDepSetting**](BuildTypeApi.md#changeArtifactDepSetting) | **PUT** /app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}/{fieldName} | 
[**changeFeatureSetting**](BuildTypeApi.md#changeFeatureSetting) | **PUT** /app/rest/buildTypes/{btLocator}/features/{featureId}/{name} | 
[**changeRequirementSetting**](BuildTypeApi.md#changeRequirementSetting) | **PUT** /app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}/{fieldName} | 
[**changeStepSetting**](BuildTypeApi.md#changeStepSetting) | **PUT** /app/rest/buildTypes/{btLocator}/steps/{stepId}/{fieldName} | 
[**changeTriggerSetting**](BuildTypeApi.md#changeTriggerSetting) | **PUT** /app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}/{fieldName} | 
[**deleteAgentRequirement**](BuildTypeApi.md#deleteAgentRequirement) | **DELETE** /app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator} | 
[**deleteAllParameters**](BuildTypeApi.md#deleteAllParameters) | **DELETE** /app/rest/buildTypes/{btLocator}/parameters | 
[**deleteAllParameters_0**](BuildTypeApi.md#deleteAllParameters_0) | **DELETE** /app/rest/buildTypes/{btLocator}/settings | 
[**deleteArtifactDep**](BuildTypeApi.md#deleteArtifactDep) | **DELETE** /app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator} | 
[**deleteBuildType**](BuildTypeApi.md#deleteBuildType) | **DELETE** /app/rest/buildTypes/{btLocator} | 
[**deleteFeature**](BuildTypeApi.md#deleteFeature) | **DELETE** /app/rest/buildTypes/{btLocator}/features/{featureId} | 
[**deleteParameter**](BuildTypeApi.md#deleteParameter) | **DELETE** /app/rest/buildTypes/{btLocator}/parameters/{name} | 
[**deleteParameter_0**](BuildTypeApi.md#deleteParameter_0) | **DELETE** /app/rest/buildTypes/{btLocator}/settings/{name} | 
[**deleteSnapshotDep**](BuildTypeApi.md#deleteSnapshotDep) | **DELETE** /app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator} | 
[**deleteStep**](BuildTypeApi.md#deleteStep) | **DELETE** /app/rest/buildTypes/{btLocator}/steps/{stepId} | 
[**deleteTrigger**](BuildTypeApi.md#deleteTrigger) | **DELETE** /app/rest/buildTypes/{btLocator}/triggers/{triggerLocator} | 
[**deleteVcsRootEntry**](BuildTypeApi.md#deleteVcsRootEntry) | **DELETE** /app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator} | 
[**getAgentRequirement**](BuildTypeApi.md#getAgentRequirement) | **GET** /app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator} | 
[**getAgentRequirements**](BuildTypeApi.md#getAgentRequirements) | **GET** /app/rest/buildTypes/{btLocator}/agent-requirements | 
[**getAliases**](BuildTypeApi.md#getAliases) | **GET** /app/rest/buildTypes/{btLocator}/aliases | 
[**getArtifactDep**](BuildTypeApi.md#getArtifactDep) | **GET** /app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator} | 
[**getArtifactDepSetting**](BuildTypeApi.md#getArtifactDepSetting) | **GET** /app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator}/{fieldName} | 
[**getArtifactDeps**](BuildTypeApi.md#getArtifactDeps) | **GET** /app/rest/buildTypes/{btLocator}/artifact-dependencies | 
[**getBuildTypes**](BuildTypeApi.md#getBuildTypes) | **GET** /app/rest/buildTypes | 
[**getChildren**](BuildTypeApi.md#getChildren) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/children{path} | 
[**getChildrenAlias**](BuildTypeApi.md#getChildrenAlias) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/{path} | 
[**getContent**](BuildTypeApi.md#getContent) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/content{path} | 
[**getContentAlias**](BuildTypeApi.md#getContentAlias) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/files{path} | 
[**getCurrentVcsInstances**](BuildTypeApi.md#getCurrentVcsInstances) | **GET** /app/rest/buildTypes/{btLocator}/vcsRootInstances | 
[**getCurrentVcsInstancesObsolete**](BuildTypeApi.md#getCurrentVcsInstancesObsolete) | **GET** /app/rest/buildTypes/{btLocator}/vcs-root-instances | 
[**getExampleNewProjectDescription**](BuildTypeApi.md#getExampleNewProjectDescription) | **GET** /app/rest/buildTypes/{btLocator}/example/newBuildTypeDescription | 
[**getExampleNewProjectDescriptionCompatibilityVersion1**](BuildTypeApi.md#getExampleNewProjectDescriptionCompatibilityVersion1) | **GET** /app/rest/buildTypes/{btLocator}/newBuildTypeDescription | 
[**getFeature**](BuildTypeApi.md#getFeature) | **GET** /app/rest/buildTypes/{btLocator}/features/{featureId} | 
[**getFeatureParameter**](BuildTypeApi.md#getFeatureParameter) | **GET** /app/rest/buildTypes/{btLocator}/features/{featureId}/parameters/{parameterName} | 
[**getFeatureParameters**](BuildTypeApi.md#getFeatureParameters) | **GET** /app/rest/buildTypes/{btLocator}/features/{featureId}/parameters | 
[**getFeatureSetting**](BuildTypeApi.md#getFeatureSetting) | **GET** /app/rest/buildTypes/{btLocator}/features/{featureId}/{name} | 
[**getFeatures**](BuildTypeApi.md#getFeatures) | **GET** /app/rest/buildTypes/{btLocator}/features | 
[**getInvestigations**](BuildTypeApi.md#getInvestigations) | **GET** /app/rest/buildTypes/{btLocator}/investigations | 
[**getMetadata**](BuildTypeApi.md#getMetadata) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/metadata{path} | 
[**getParameter**](BuildTypeApi.md#getParameter) | **GET** /app/rest/buildTypes/{btLocator}/parameters/{name} | 
[**getParameterType**](BuildTypeApi.md#getParameterType) | **GET** /app/rest/buildTypes/{btLocator}/parameters/{name}/type | 
[**getParameterTypeRawValue**](BuildTypeApi.md#getParameterTypeRawValue) | **GET** /app/rest/buildTypes/{btLocator}/parameters/{name}/type/rawValue | 
[**getParameterValueLong**](BuildTypeApi.md#getParameterValueLong) | **GET** /app/rest/buildTypes/{btLocator}/parameters/{name}/value | 
[**getParameterValueLong_0**](BuildTypeApi.md#getParameterValueLong_0) | **GET** /app/rest/buildTypes/{btLocator}/settings/{name}/value | 
[**getParameter_0**](BuildTypeApi.md#getParameter_0) | **GET** /app/rest/buildTypes/{btLocator}/settings/{name} | 
[**getParameters**](BuildTypeApi.md#getParameters) | **GET** /app/rest/buildTypes/{btLocator}/parameters | 
[**getParameters_0**](BuildTypeApi.md#getParameters_0) | **GET** /app/rest/buildTypes/{btLocator}/settings | 
[**getRequirementSetting**](BuildTypeApi.md#getRequirementSetting) | **GET** /app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator}/{fieldName} | 
[**getRoot**](BuildTypeApi.md#getRoot) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest | 
[**getSettingsFile**](BuildTypeApi.md#getSettingsFile) | **GET** /app/rest/buildTypes/{btLocator}/settingsFile | 
[**getSnapshotDep**](BuildTypeApi.md#getSnapshotDep) | **GET** /app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator} | 
[**getSnapshotDeps**](BuildTypeApi.md#getSnapshotDeps) | **GET** /app/rest/buildTypes/{btLocator}/snapshot-dependencies | 
[**getStep**](BuildTypeApi.md#getStep) | **GET** /app/rest/buildTypes/{btLocator}/steps/{stepId} | 
[**getStepParameter**](BuildTypeApi.md#getStepParameter) | **GET** /app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters/{parameterName} | 
[**getStepParameters**](BuildTypeApi.md#getStepParameters) | **GET** /app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters | 
[**getStepSetting**](BuildTypeApi.md#getStepSetting) | **GET** /app/rest/buildTypes/{btLocator}/steps/{stepId}/{fieldName} | 
[**getSteps**](BuildTypeApi.md#getSteps) | **GET** /app/rest/buildTypes/{btLocator}/steps | 
[**getTemplate**](BuildTypeApi.md#getTemplate) | **GET** /app/rest/buildTypes/{btLocator}/templates/{templateLocator} | 
[**getTemplates**](BuildTypeApi.md#getTemplates) | **GET** /app/rest/buildTypes/{btLocator}/templates | 
[**getTrigger**](BuildTypeApi.md#getTrigger) | **GET** /app/rest/buildTypes/{btLocator}/triggers/{triggerLocator} | 
[**getTriggerSetting**](BuildTypeApi.md#getTriggerSetting) | **GET** /app/rest/buildTypes/{btLocator}/triggers/{triggerLocator}/{fieldName} | 
[**getTriggers**](BuildTypeApi.md#getTriggers) | **GET** /app/rest/buildTypes/{btLocator}/triggers | 
[**getVCSLabelingOptions**](BuildTypeApi.md#getVCSLabelingOptions) | **GET** /app/rest/buildTypes/{btLocator}/vcsLabeling | 
[**getVcsRootEntries**](BuildTypeApi.md#getVcsRootEntries) | **GET** /app/rest/buildTypes/{btLocator}/vcs-root-entries | 
[**getVcsRootEntry**](BuildTypeApi.md#getVcsRootEntry) | **GET** /app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator} | 
[**getVcsRootEntryCheckoutRules**](BuildTypeApi.md#getVcsRootEntryCheckoutRules) | **GET** /app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}/checkout-rules | 
[**getZipped**](BuildTypeApi.md#getZipped) | **GET** /app/rest/buildTypes/{btLocator}/vcs/files/latest/archived{path} | 
[**removeAllTemplates**](BuildTypeApi.md#removeAllTemplates) | **DELETE** /app/rest/buildTypes/{btLocator}/templates | 
[**removeTemplate**](BuildTypeApi.md#removeTemplate) | **DELETE** /app/rest/buildTypes/{btLocator}/templates/{templateLocator} | 
[**replaceAgentRequirement**](BuildTypeApi.md#replaceAgentRequirement) | **PUT** /app/rest/buildTypes/{btLocator}/agent-requirements/{agentRequirementLocator} | 
[**replaceAgentRequirements**](BuildTypeApi.md#replaceAgentRequirements) | **PUT** /app/rest/buildTypes/{btLocator}/agent-requirements | 
[**replaceArtifactDep**](BuildTypeApi.md#replaceArtifactDep) | **PUT** /app/rest/buildTypes/{btLocator}/artifact-dependencies/{artifactDepLocator} | 
[**replaceArtifactDeps**](BuildTypeApi.md#replaceArtifactDeps) | **PUT** /app/rest/buildTypes/{btLocator}/artifact-dependencies | 
[**replaceFeature**](BuildTypeApi.md#replaceFeature) | **PUT** /app/rest/buildTypes/{btLocator}/features/{featureId} | 
[**replaceFeatureParameters**](BuildTypeApi.md#replaceFeatureParameters) | **PUT** /app/rest/buildTypes/{btLocator}/features/{featureId}/parameters | 
[**replaceFeatures**](BuildTypeApi.md#replaceFeatures) | **PUT** /app/rest/buildTypes/{btLocator}/features | 
[**replaceSnapshotDep**](BuildTypeApi.md#replaceSnapshotDep) | **PUT** /app/rest/buildTypes/{btLocator}/snapshot-dependencies/{snapshotDepLocator} | 
[**replaceSnapshotDeps**](BuildTypeApi.md#replaceSnapshotDeps) | **PUT** /app/rest/buildTypes/{btLocator}/snapshot-dependencies | 
[**replaceStep**](BuildTypeApi.md#replaceStep) | **PUT** /app/rest/buildTypes/{btLocator}/steps/{stepId} | 
[**replaceStepParameters**](BuildTypeApi.md#replaceStepParameters) | **PUT** /app/rest/buildTypes/{btLocator}/steps/{stepId}/parameters | 
[**replaceSteps**](BuildTypeApi.md#replaceSteps) | **PUT** /app/rest/buildTypes/{btLocator}/steps | 
[**replaceTrigger**](BuildTypeApi.md#replaceTrigger) | **PUT** /app/rest/buildTypes/{btLocator}/triggers/{triggerLocator} | 
[**replaceTriggers**](BuildTypeApi.md#replaceTriggers) | **PUT** /app/rest/buildTypes/{btLocator}/triggers | 
[**replaceVcsRootEntries**](BuildTypeApi.md#replaceVcsRootEntries) | **PUT** /app/rest/buildTypes/{btLocator}/vcs-root-entries | 
[**serveBranches**](BuildTypeApi.md#serveBranches) | **GET** /app/rest/buildTypes/{btLocator}/branches | 
[**serveBuildField**](BuildTypeApi.md#serveBuildField) | **GET** /app/rest/buildTypes/{btLocator}/builds/{buildLocator}/{field} | 
[**serveBuildTypeBuildsTags**](BuildTypeApi.md#serveBuildTypeBuildsTags) | **GET** /app/rest/buildTypes/{btLocator}/buildTags | 
[**serveBuildTypeField**](BuildTypeApi.md#serveBuildTypeField) | **GET** /app/rest/buildTypes/{btLocator}/{field} | 
[**serveBuildTypeXML**](BuildTypeApi.md#serveBuildTypeXML) | **GET** /app/rest/buildTypes/{btLocator} | 
[**serveBuildWithProject**](BuildTypeApi.md#serveBuildWithProject) | **GET** /app/rest/buildTypes/{btLocator}/builds/{buildLocator} | 
[**serveBuilds**](BuildTypeApi.md#serveBuilds) | **GET** /app/rest/buildTypes/{btLocator}/builds | 
[**setBuildTypeField**](BuildTypeApi.md#setBuildTypeField) | **PUT** /app/rest/buildTypes/{btLocator}/{field} | 
[**setParameter**](BuildTypeApi.md#setParameter) | **POST** /app/rest/buildTypes/{btLocator}/parameters | 
[**setParameterType**](BuildTypeApi.md#setParameterType) | **PUT** /app/rest/buildTypes/{btLocator}/parameters/{name}/type | 
[**setParameterTypeRawValue**](BuildTypeApi.md#setParameterTypeRawValue) | **PUT** /app/rest/buildTypes/{btLocator}/parameters/{name}/type/rawValue | 
[**setParameterValueLong**](BuildTypeApi.md#setParameterValueLong) | **PUT** /app/rest/buildTypes/{btLocator}/parameters/{name}/value | 
[**setParameterValueLong_0**](BuildTypeApi.md#setParameterValueLong_0) | **PUT** /app/rest/buildTypes/{btLocator}/settings/{name}/value | 
[**setParameter_0**](BuildTypeApi.md#setParameter_0) | **PUT** /app/rest/buildTypes/{btLocator}/parameters/{name} | 
[**setParameter_1**](BuildTypeApi.md#setParameter_1) | **POST** /app/rest/buildTypes/{btLocator}/settings | 
[**setParameter_2**](BuildTypeApi.md#setParameter_2) | **PUT** /app/rest/buildTypes/{btLocator}/settings/{name} | 
[**setParameters**](BuildTypeApi.md#setParameters) | **PUT** /app/rest/buildTypes/{btLocator}/parameters | 
[**setParameters_0**](BuildTypeApi.md#setParameters_0) | **PUT** /app/rest/buildTypes/{btLocator}/settings | 
[**setTemplates**](BuildTypeApi.md#setTemplates) | **PUT** /app/rest/buildTypes/{btLocator}/templates | 
[**setVCSLabelingOptions**](BuildTypeApi.md#setVCSLabelingOptions) | **PUT** /app/rest/buildTypes/{btLocator}/vcsLabeling | 
[**updateVcsRootEntry**](BuildTypeApi.md#updateVcsRootEntry) | **PUT** /app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator} | 
[**updateVcsRootEntryCheckoutRules**](BuildTypeApi.md#updateVcsRootEntryCheckoutRules) | **PUT** /app/rest/buildTypes/{btLocator}/vcs-root-entries/{vcsRootLocator}/checkout-rules | 


<a name="addAgentRequirement"></a>
# **addAgentRequirement**
> AgentRequirement addAgentRequirement(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
AgentRequirement body = new AgentRequirement(); // AgentRequirement | 
try {
    AgentRequirement result = apiInstance.addAgentRequirement(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addAgentRequirement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**AgentRequirement**](AgentRequirement.md)|  | [optional]

### Return type

[**AgentRequirement**](AgentRequirement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addArtifactDep"></a>
# **addArtifactDep**
> ArtifactDependency addArtifactDep(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
ArtifactDependency body = new ArtifactDependency(); // ArtifactDependency | 
try {
    ArtifactDependency result = apiInstance.addArtifactDep(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addArtifactDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**ArtifactDependency**](ArtifactDependency.md)|  | [optional]

### Return type

[**ArtifactDependency**](ArtifactDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addBuildType"></a>
# **addBuildType**
> BuildType addBuildType(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
BuildType body = new BuildType(); // BuildType | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.addBuildType(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addBuildType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BuildType**](BuildType.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addFeature"></a>
# **addFeature**
> Feature addFeature(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Feature body = new Feature(); // Feature | 
try {
    Feature result = apiInstance.addFeature(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addFeature");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Feature**](Feature.md)|  | [optional]

### Return type

[**Feature**](Feature.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addFeatureParameter"></a>
# **addFeatureParameter**
> String addFeatureParameter(btLocator, featureId, parameterName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String parameterName = "parameterName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.addFeatureParameter(btLocator, featureId, parameterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addFeatureParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **parameterName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addSnapshotDep"></a>
# **addSnapshotDep**
> SnapshotDependency addSnapshotDep(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
SnapshotDependency body = new SnapshotDependency(); // SnapshotDependency | 
try {
    SnapshotDependency result = apiInstance.addSnapshotDep(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addSnapshotDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**SnapshotDependency**](SnapshotDependency.md)|  | [optional]

### Return type

[**SnapshotDependency**](SnapshotDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addStep"></a>
# **addStep**
> Step addStep(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Step body = new Step(); // Step | 
try {
    Step result = apiInstance.addStep(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Step**](Step.md)|  | [optional]

### Return type

[**Step**](Step.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addStepParameter"></a>
# **addStepParameter**
> String addStepParameter(btLocator, stepId, parameterName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String parameterName = "parameterName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.addStepParameter(btLocator, stepId, parameterName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addStepParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **parameterName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addTemplate"></a>
# **addTemplate**
> BuildType addTemplate(btLocator, body, optimizeSettings, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
BuildType body = new BuildType(); // BuildType | 
Boolean optimizeSettings = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.addTemplate(btLocator, body, optimizeSettings, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**BuildType**](BuildType.md)|  | [optional]
 **optimizeSettings** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addTrigger"></a>
# **addTrigger**
> Trigger addTrigger(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Trigger body = new Trigger(); // Trigger | 
try {
    Trigger result = apiInstance.addTrigger(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addTrigger");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Trigger**](Trigger.md)|  | [optional]

### Return type

[**Trigger**](Trigger.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addVcsRootEntry"></a>
# **addVcsRootEntry**
> VcsRootEntry addVcsRootEntry(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
VcsRootEntry body = new VcsRootEntry(); // VcsRootEntry | 
String fields = "fields_example"; // String | 
try {
    VcsRootEntry result = apiInstance.addVcsRootEntry(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#addVcsRootEntry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**VcsRootEntry**](VcsRootEntry.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootEntry**](VcsRootEntry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeArtifactDepSetting"></a>
# **changeArtifactDepSetting**
> String changeArtifactDepSetting(btLocator, artifactDepLocator, fieldName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String artifactDepLocator = "artifactDepLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.changeArtifactDepSetting(btLocator, artifactDepLocator, fieldName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#changeArtifactDepSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **artifactDepLocator** | **String**|  |
 **fieldName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeFeatureSetting"></a>
# **changeFeatureSetting**
> String changeFeatureSetting(btLocator, featureId, name, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String name = "name_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.changeFeatureSetting(btLocator, featureId, name, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#changeFeatureSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **name** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeRequirementSetting"></a>
# **changeRequirementSetting**
> String changeRequirementSetting(btLocator, agentRequirementLocator, fieldName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String agentRequirementLocator = "agentRequirementLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.changeRequirementSetting(btLocator, agentRequirementLocator, fieldName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#changeRequirementSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **agentRequirementLocator** | **String**|  |
 **fieldName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeStepSetting"></a>
# **changeStepSetting**
> String changeStepSetting(btLocator, stepId, fieldName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String fieldName = "fieldName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.changeStepSetting(btLocator, stepId, fieldName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#changeStepSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **fieldName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeTriggerSetting"></a>
# **changeTriggerSetting**
> String changeTriggerSetting(btLocator, triggerLocator, fieldName, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String triggerLocator = "triggerLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.changeTriggerSetting(btLocator, triggerLocator, fieldName, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#changeTriggerSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **triggerLocator** | **String**|  |
 **fieldName** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAgentRequirement"></a>
# **deleteAgentRequirement**
> deleteAgentRequirement(btLocator, agentRequirementLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String agentRequirementLocator = "agentRequirementLocator_example"; // String | 
try {
    apiInstance.deleteAgentRequirement(btLocator, agentRequirementLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteAgentRequirement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **agentRequirementLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllParameters"></a>
# **deleteAllParameters**
> deleteAllParameters(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    apiInstance.deleteAllParameters(btLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteAllParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllParameters_0"></a>
# **deleteAllParameters_0**
> deleteAllParameters_0(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    apiInstance.deleteAllParameters_0(btLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteAllParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteArtifactDep"></a>
# **deleteArtifactDep**
> deleteArtifactDep(btLocator, artifactDepLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String artifactDepLocator = "artifactDepLocator_example"; // String | 
try {
    apiInstance.deleteArtifactDep(btLocator, artifactDepLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteArtifactDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **artifactDepLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteBuildType"></a>
# **deleteBuildType**
> deleteBuildType(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    apiInstance.deleteBuildType(btLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteBuildType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteFeature"></a>
# **deleteFeature**
> deleteFeature(btLocator, featureId)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
try {
    apiInstance.deleteFeature(btLocator, featureId);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteFeature");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter"></a>
# **deleteParameter**
> deleteParameter(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    apiInstance.deleteParameter(name, btLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter_0"></a>
# **deleteParameter_0**
> deleteParameter_0(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    apiInstance.deleteParameter_0(name, btLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteSnapshotDep"></a>
# **deleteSnapshotDep**
> deleteSnapshotDep(btLocator, snapshotDepLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String snapshotDepLocator = "snapshotDepLocator_example"; // String | 
try {
    apiInstance.deleteSnapshotDep(btLocator, snapshotDepLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteSnapshotDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **snapshotDepLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteStep"></a>
# **deleteStep**
> deleteStep(btLocator, stepId)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
try {
    apiInstance.deleteStep(btLocator, stepId);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteTrigger"></a>
# **deleteTrigger**
> deleteTrigger(btLocator, triggerLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String triggerLocator = "triggerLocator_example"; // String | 
try {
    apiInstance.deleteTrigger(btLocator, triggerLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteTrigger");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **triggerLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteVcsRootEntry"></a>
# **deleteVcsRootEntry**
> deleteVcsRootEntry(btLocator, vcsRootLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String vcsRootLocator = "vcsRootLocator_example"; // String | 
try {
    apiInstance.deleteVcsRootEntry(btLocator, vcsRootLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#deleteVcsRootEntry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **vcsRootLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAgentRequirement"></a>
# **getAgentRequirement**
> AgentRequirement getAgentRequirement(btLocator, agentRequirementLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String agentRequirementLocator = "agentRequirementLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentRequirement result = apiInstance.getAgentRequirement(btLocator, agentRequirementLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getAgentRequirement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **agentRequirementLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AgentRequirement**](AgentRequirement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAgentRequirements"></a>
# **getAgentRequirements**
> AgentRequirements getAgentRequirements(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentRequirements result = apiInstance.getAgentRequirements(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getAgentRequirements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AgentRequirements**](AgentRequirements.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAliases"></a>
# **getAliases**
> Items getAliases(btLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    Items result = apiInstance.getAliases(btLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getAliases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **field** | **String**|  |

### Return type

[**Items**](Items.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArtifactDep"></a>
# **getArtifactDep**
> ArtifactDependency getArtifactDep(btLocator, artifactDepLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String artifactDepLocator = "artifactDepLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ArtifactDependency result = apiInstance.getArtifactDep(btLocator, artifactDepLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getArtifactDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **artifactDepLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**ArtifactDependency**](ArtifactDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArtifactDepSetting"></a>
# **getArtifactDepSetting**
> String getArtifactDepSetting(btLocator, artifactDepLocator, fieldName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String artifactDepLocator = "artifactDepLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
try {
    String result = apiInstance.getArtifactDepSetting(btLocator, artifactDepLocator, fieldName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getArtifactDepSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **artifactDepLocator** | **String**|  |
 **fieldName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArtifactDeps"></a>
# **getArtifactDeps**
> ArtifactDependencies getArtifactDeps(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ArtifactDependencies result = apiInstance.getArtifactDeps(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getArtifactDeps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**ArtifactDependencies**](ArtifactDependencies.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuildTypes"></a>
# **getBuildTypes**
> BuildTypes getBuildTypes(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.getBuildTypes(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getBuildTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildren"></a>
# **getChildren**
> Files getChildren(path, btLocator, basePath, locator, fields, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    Files result = apiInstance.getChildren(path, btLocator, basePath, locator, fields, resolveParameters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getChildren");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildrenAlias"></a>
# **getChildrenAlias**
> Files getChildrenAlias(path, btLocator, basePath, locator, fields, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    Files result = apiInstance.getChildrenAlias(path, btLocator, basePath, locator, fields, resolveParameters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getChildrenAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContent"></a>
# **getContent**
> getContent(path, btLocator, responseBuilder, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String responseBuilder = "responseBuilder_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    apiInstance.getContent(path, btLocator, responseBuilder, resolveParameters);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getContent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **responseBuilder** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContentAlias"></a>
# **getContentAlias**
> getContentAlias(path, btLocator, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    apiInstance.getContentAlias(path, btLocator, resolveParameters);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getContentAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCurrentVcsInstances"></a>
# **getCurrentVcsInstances**
> VcsRootInstances getCurrentVcsInstances(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.getCurrentVcsInstances(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getCurrentVcsInstances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCurrentVcsInstancesObsolete"></a>
# **getCurrentVcsInstancesObsolete**
> VcsRootInstances getCurrentVcsInstancesObsolete(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.getCurrentVcsInstancesObsolete(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getCurrentVcsInstancesObsolete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getExampleNewProjectDescription"></a>
# **getExampleNewProjectDescription**
> NewBuildTypeDescription getExampleNewProjectDescription(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    NewBuildTypeDescription result = apiInstance.getExampleNewProjectDescription(btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getExampleNewProjectDescription");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

[**NewBuildTypeDescription**](NewBuildTypeDescription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getExampleNewProjectDescriptionCompatibilityVersion1"></a>
# **getExampleNewProjectDescriptionCompatibilityVersion1**
> NewBuildTypeDescription getExampleNewProjectDescriptionCompatibilityVersion1(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    NewBuildTypeDescription result = apiInstance.getExampleNewProjectDescriptionCompatibilityVersion1(btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getExampleNewProjectDescriptionCompatibilityVersion1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

[**NewBuildTypeDescription**](NewBuildTypeDescription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFeature"></a>
# **getFeature**
> Feature getFeature(btLocator, featureId, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Feature result = apiInstance.getFeature(btLocator, featureId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getFeature");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Feature**](Feature.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFeatureParameter"></a>
# **getFeatureParameter**
> String getFeatureParameter(btLocator, featureId, parameterName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String parameterName = "parameterName_example"; // String | 
try {
    String result = apiInstance.getFeatureParameter(btLocator, featureId, parameterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getFeatureParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **parameterName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFeatureParameters"></a>
# **getFeatureParameters**
> Properties getFeatureParameters(btLocator, featureId, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getFeatureParameters(btLocator, featureId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getFeatureParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFeatureSetting"></a>
# **getFeatureSetting**
> String getFeatureSetting(btLocator, featureId, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String name = "name_example"; // String | 
try {
    String result = apiInstance.getFeatureSetting(btLocator, featureId, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getFeatureSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **name** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getFeatures"></a>
# **getFeatures**
> Features getFeatures(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Features result = apiInstance.getFeatures(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Features**](Features.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getInvestigations"></a>
# **getInvestigations**
> Investigations getInvestigations(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Investigations result = apiInstance.getInvestigations(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getInvestigations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Investigations**](Investigations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMetadata"></a>
# **getMetadata**
> File getMetadata(path, btLocator, fields, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    File result = apiInstance.getMetadata(path, btLocator, fields, resolveParameters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter"></a>
# **getParameter**
> Property getParameter(name, btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.getParameter(name, btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterType"></a>
# **getParameterType**
> Type getParameterType(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    Type result = apiInstance.getParameterType(name, btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameterType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

[**Type**](Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterTypeRawValue"></a>
# **getParameterTypeRawValue**
> String getParameterTypeRawValue(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    String result = apiInstance.getParameterTypeRawValue(name, btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameterTypeRawValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterValueLong"></a>
# **getParameterValueLong**
> String getParameterValueLong(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    String result = apiInstance.getParameterValueLong(name, btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterValueLong_0"></a>
# **getParameterValueLong_0**
> String getParameterValueLong_0(name, btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
try {
    String result = apiInstance.getParameterValueLong_0(name, btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameterValueLong_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter_0"></a>
# **getParameter_0**
> Property getParameter_0(name, btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.getParameter_0(name, btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameters"></a>
# **getParameters**
> Properties getParameters(btLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getParameters(btLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameters_0"></a>
# **getParameters_0**
> Properties getParameters_0(btLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getParameters_0(btLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRequirementSetting"></a>
# **getRequirementSetting**
> String getRequirementSetting(btLocator, agentRequirementLocator, fieldName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String agentRequirementLocator = "agentRequirementLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
try {
    String result = apiInstance.getRequirementSetting(btLocator, agentRequirementLocator, fieldName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getRequirementSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **agentRequirementLocator** | **String**|  |
 **fieldName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRoot"></a>
# **getRoot**
> Files getRoot(btLocator, basePath, locator, fields, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    Files result = apiInstance.getRoot(btLocator, basePath, locator, fields, resolveParameters);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSettingsFile"></a>
# **getSettingsFile**
> String getSettingsFile(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    String result = apiInstance.getSettingsFile(btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getSettingsFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSnapshotDep"></a>
# **getSnapshotDep**
> SnapshotDependency getSnapshotDep(btLocator, snapshotDepLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String snapshotDepLocator = "snapshotDepLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    SnapshotDependency result = apiInstance.getSnapshotDep(btLocator, snapshotDepLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getSnapshotDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **snapshotDepLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**SnapshotDependency**](SnapshotDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSnapshotDeps"></a>
# **getSnapshotDeps**
> SnapshotDependencies getSnapshotDeps(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    SnapshotDependencies result = apiInstance.getSnapshotDeps(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getSnapshotDeps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**SnapshotDependencies**](SnapshotDependencies.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getStep"></a>
# **getStep**
> Step getStep(btLocator, stepId, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Step result = apiInstance.getStep(btLocator, stepId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Step**](Step.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getStepParameter"></a>
# **getStepParameter**
> String getStepParameter(btLocator, stepId, parameterName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String parameterName = "parameterName_example"; // String | 
try {
    String result = apiInstance.getStepParameter(btLocator, stepId, parameterName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getStepParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **parameterName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getStepParameters"></a>
# **getStepParameters**
> Properties getStepParameters(btLocator, stepId, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getStepParameters(btLocator, stepId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getStepParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getStepSetting"></a>
# **getStepSetting**
> String getStepSetting(btLocator, stepId, fieldName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String fieldName = "fieldName_example"; // String | 
try {
    String result = apiInstance.getStepSetting(btLocator, stepId, fieldName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getStepSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **fieldName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSteps"></a>
# **getSteps**
> Steps getSteps(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Steps result = apiInstance.getSteps(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getSteps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Steps**](Steps.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTemplate"></a>
# **getTemplate**
> BuildType getTemplate(btLocator, templateLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String templateLocator = "templateLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.getTemplate(btLocator, templateLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **templateLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTemplates"></a>
# **getTemplates**
> BuildTypes getTemplates(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.getTemplates(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTrigger"></a>
# **getTrigger**
> Trigger getTrigger(btLocator, triggerLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String triggerLocator = "triggerLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Trigger result = apiInstance.getTrigger(btLocator, triggerLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getTrigger");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **triggerLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Trigger**](Trigger.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTriggerSetting"></a>
# **getTriggerSetting**
> String getTriggerSetting(btLocator, triggerLocator, fieldName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String triggerLocator = "triggerLocator_example"; // String | 
String fieldName = "fieldName_example"; // String | 
try {
    String result = apiInstance.getTriggerSetting(btLocator, triggerLocator, fieldName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getTriggerSetting");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **triggerLocator** | **String**|  |
 **fieldName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTriggers"></a>
# **getTriggers**
> Triggers getTriggers(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Triggers result = apiInstance.getTriggers(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getTriggers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Triggers**](Triggers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVCSLabelingOptions"></a>
# **getVCSLabelingOptions**
> VcsLabeling getVCSLabelingOptions(btLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
try {
    VcsLabeling result = apiInstance.getVCSLabelingOptions(btLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getVCSLabelingOptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |

### Return type

[**VcsLabeling**](VcsLabeling.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVcsRootEntries"></a>
# **getVcsRootEntries**
> VcsRootEntries getVcsRootEntries(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootEntries result = apiInstance.getVcsRootEntries(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getVcsRootEntries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootEntries**](VcsRootEntries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVcsRootEntry"></a>
# **getVcsRootEntry**
> VcsRootEntry getVcsRootEntry(btLocator, vcsRootLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootEntry result = apiInstance.getVcsRootEntry(btLocator, vcsRootLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getVcsRootEntry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **vcsRootLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootEntry**](VcsRootEntry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getVcsRootEntryCheckoutRules"></a>
# **getVcsRootEntryCheckoutRules**
> String getVcsRootEntryCheckoutRules(btLocator, vcsRootLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String vcsRootLocator = "vcsRootLocator_example"; // String | 
try {
    String result = apiInstance.getVcsRootEntryCheckoutRules(btLocator, vcsRootLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getVcsRootEntryCheckoutRules");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **vcsRootLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getZipped"></a>
# **getZipped**
> getZipped(path, btLocator, basePath, locator, name, resolveParameters)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String path = "path_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String name = "name_example"; // String | 
Boolean resolveParameters = true; // Boolean | 
try {
    apiInstance.getZipped(path, btLocator, basePath, locator, name, resolveParameters);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#getZipped");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **btLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **name** | **String**|  | [optional]
 **resolveParameters** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeAllTemplates"></a>
# **removeAllTemplates**
> removeAllTemplates(btLocator, inlineSettings)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
Boolean inlineSettings = true; // Boolean | 
try {
    apiInstance.removeAllTemplates(btLocator, inlineSettings);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#removeAllTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **inlineSettings** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeTemplate"></a>
# **removeTemplate**
> removeTemplate(btLocator, templateLocator, inlineSettings)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String templateLocator = "templateLocator_example"; // String | 
Boolean inlineSettings = true; // Boolean | 
try {
    apiInstance.removeTemplate(btLocator, templateLocator, inlineSettings);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#removeTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **templateLocator** | **String**|  |
 **inlineSettings** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceAgentRequirement"></a>
# **replaceAgentRequirement**
> AgentRequirement replaceAgentRequirement(btLocator, agentRequirementLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String agentRequirementLocator = "agentRequirementLocator_example"; // String | 
String fields = "fields_example"; // String | 
AgentRequirement body = new AgentRequirement(); // AgentRequirement | 
try {
    AgentRequirement result = apiInstance.replaceAgentRequirement(btLocator, agentRequirementLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceAgentRequirement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **agentRequirementLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**AgentRequirement**](AgentRequirement.md)|  | [optional]

### Return type

[**AgentRequirement**](AgentRequirement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceAgentRequirements"></a>
# **replaceAgentRequirements**
> AgentRequirements replaceAgentRequirements(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
AgentRequirements body = new AgentRequirements(); // AgentRequirements | 
try {
    AgentRequirements result = apiInstance.replaceAgentRequirements(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceAgentRequirements");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**AgentRequirements**](AgentRequirements.md)|  | [optional]

### Return type

[**AgentRequirements**](AgentRequirements.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceArtifactDep"></a>
# **replaceArtifactDep**
> ArtifactDependency replaceArtifactDep(btLocator, artifactDepLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String artifactDepLocator = "artifactDepLocator_example"; // String | 
String fields = "fields_example"; // String | 
ArtifactDependency body = new ArtifactDependency(); // ArtifactDependency | 
try {
    ArtifactDependency result = apiInstance.replaceArtifactDep(btLocator, artifactDepLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceArtifactDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **artifactDepLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**ArtifactDependency**](ArtifactDependency.md)|  | [optional]

### Return type

[**ArtifactDependency**](ArtifactDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceArtifactDeps"></a>
# **replaceArtifactDeps**
> ArtifactDependencies replaceArtifactDeps(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
ArtifactDependencies body = new ArtifactDependencies(); // ArtifactDependencies | 
try {
    ArtifactDependencies result = apiInstance.replaceArtifactDeps(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceArtifactDeps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**ArtifactDependencies**](ArtifactDependencies.md)|  | [optional]

### Return type

[**ArtifactDependencies**](ArtifactDependencies.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceFeature"></a>
# **replaceFeature**
> Feature replaceFeature(btLocator, featureId, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
String fields = "fields_example"; // String | 
Feature body = new Feature(); // Feature | 
try {
    Feature result = apiInstance.replaceFeature(btLocator, featureId, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceFeature");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Feature**](Feature.md)|  | [optional]

### Return type

[**Feature**](Feature.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceFeatureParameters"></a>
# **replaceFeatureParameters**
> Properties replaceFeatureParameters(btLocator, featureId, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String featureId = "featureId_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.replaceFeatureParameters(btLocator, featureId, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceFeatureParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **featureId** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceFeatures"></a>
# **replaceFeatures**
> Features replaceFeatures(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Features body = new Features(); // Features | 
try {
    Features result = apiInstance.replaceFeatures(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Features**](Features.md)|  | [optional]

### Return type

[**Features**](Features.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceSnapshotDep"></a>
# **replaceSnapshotDep**
> SnapshotDependency replaceSnapshotDep(btLocator, snapshotDepLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String snapshotDepLocator = "snapshotDepLocator_example"; // String | 
String fields = "fields_example"; // String | 
SnapshotDependency body = new SnapshotDependency(); // SnapshotDependency | 
try {
    SnapshotDependency result = apiInstance.replaceSnapshotDep(btLocator, snapshotDepLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceSnapshotDep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **snapshotDepLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**SnapshotDependency**](SnapshotDependency.md)|  | [optional]

### Return type

[**SnapshotDependency**](SnapshotDependency.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceSnapshotDeps"></a>
# **replaceSnapshotDeps**
> SnapshotDependencies replaceSnapshotDeps(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
SnapshotDependencies body = new SnapshotDependencies(); // SnapshotDependencies | 
try {
    SnapshotDependencies result = apiInstance.replaceSnapshotDeps(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceSnapshotDeps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**SnapshotDependencies**](SnapshotDependencies.md)|  | [optional]

### Return type

[**SnapshotDependencies**](SnapshotDependencies.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceStep"></a>
# **replaceStep**
> Step replaceStep(btLocator, stepId, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
String fields = "fields_example"; // String | 
Step body = new Step(); // Step | 
try {
    Step result = apiInstance.replaceStep(btLocator, stepId, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceStep");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Step**](Step.md)|  | [optional]

### Return type

[**Step**](Step.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceStepParameters"></a>
# **replaceStepParameters**
> Properties replaceStepParameters(btLocator, stepId, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String stepId = "stepId_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.replaceStepParameters(btLocator, stepId, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceStepParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **stepId** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceSteps"></a>
# **replaceSteps**
> Steps replaceSteps(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Steps body = new Steps(); // Steps | 
try {
    Steps result = apiInstance.replaceSteps(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceSteps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Steps**](Steps.md)|  | [optional]

### Return type

[**Steps**](Steps.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceTrigger"></a>
# **replaceTrigger**
> Trigger replaceTrigger(btLocator, triggerLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String triggerLocator = "triggerLocator_example"; // String | 
String fields = "fields_example"; // String | 
Trigger body = new Trigger(); // Trigger | 
try {
    Trigger result = apiInstance.replaceTrigger(btLocator, triggerLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceTrigger");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **triggerLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Trigger**](Trigger.md)|  | [optional]

### Return type

[**Trigger**](Trigger.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceTriggers"></a>
# **replaceTriggers**
> Triggers replaceTriggers(btLocator, fields, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
Triggers body = new Triggers(); // Triggers | 
try {
    Triggers result = apiInstance.replaceTriggers(btLocator, fields, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceTriggers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **body** | [**Triggers**](Triggers.md)|  | [optional]

### Return type

[**Triggers**](Triggers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceVcsRootEntries"></a>
# **replaceVcsRootEntries**
> VcsRootEntries replaceVcsRootEntries(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
VcsRootEntries body = new VcsRootEntries(); // VcsRootEntries | 
String fields = "fields_example"; // String | 
try {
    VcsRootEntries result = apiInstance.replaceVcsRootEntries(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#replaceVcsRootEntries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**VcsRootEntries**](VcsRootEntries.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootEntries**](VcsRootEntries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBranches"></a>
# **serveBranches**
> Branches serveBranches(btLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Branches result = apiInstance.serveBranches(btLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Branches**](Branches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildField"></a>
# **serveBuildField**
> String serveBuildField(btLocator, buildLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildField(btLocator, buildLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuildField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **buildLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypeBuildsTags"></a>
# **serveBuildTypeBuildsTags**
> Tags serveBuildTypeBuildsTags(btLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    Tags result = apiInstance.serveBuildTypeBuildsTags(btLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuildTypeBuildsTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **field** | **String**|  |

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypeField"></a>
# **serveBuildTypeField**
> String serveBuildTypeField(btLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildTypeField(btLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuildTypeField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypeXML"></a>
# **serveBuildTypeXML**
> BuildType serveBuildTypeXML(btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.serveBuildTypeXML(btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuildTypeXML");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildWithProject"></a>
# **serveBuildWithProject**
> Build serveBuildWithProject(btLocator, buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.serveBuildWithProject(btLocator, buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuildWithProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuilds"></a>
# **serveBuilds**
> Builds serveBuilds(btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String status = "status_example"; // String | 
String triggeredByUser = "triggeredByUser_example"; // String | 
Boolean includePersonal = true; // Boolean | 
Boolean includeCanceled = true; // Boolean | 
Boolean onlyPinned = true; // Boolean | 
java.util.List<String> tag = Arrays.asList("tag_example"); // java.util.List<String> | 
String agentName = "agentName_example"; // String | 
String sinceBuild = "sinceBuild_example"; // String | 
String sinceDate = "sinceDate_example"; // String | 
Long start = 789L; // Long | 
Integer count = 56; // Integer | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.serveBuilds(btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#serveBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **status** | **String**|  | [optional]
 **triggeredByUser** | **String**|  | [optional]
 **includePersonal** | **Boolean**|  | [optional]
 **includeCanceled** | **Boolean**|  | [optional]
 **onlyPinned** | **Boolean**|  | [optional]
 **tag** | [**java.util.List&lt;String&gt;**](String.md)|  | [optional]
 **agentName** | **String**|  | [optional]
 **sinceBuild** | **String**|  | [optional]
 **sinceDate** | **String**|  | [optional]
 **start** | **Long**|  | [optional]
 **count** | **Integer**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildTypeField"></a>
# **setBuildTypeField**
> String setBuildTypeField(btLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setBuildTypeField(btLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setBuildTypeField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter"></a>
# **setParameter**
> Property setParameter(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterType"></a>
# **setParameterType**
> Type setParameterType(name, btLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
Type body = new Type(); // Type | 
try {
    Type result = apiInstance.setParameterType(name, btLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameterType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | [**Type**](Type.md)|  | [optional]

### Return type

[**Type**](Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterTypeRawValue"></a>
# **setParameterTypeRawValue**
> String setParameterTypeRawValue(name, btLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setParameterTypeRawValue(name, btLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameterTypeRawValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterValueLong"></a>
# **setParameterValueLong**
> String setParameterValueLong(name, btLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setParameterValueLong(name, btLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterValueLong_0"></a>
# **setParameterValueLong_0**
> String setParameterValueLong_0(name, btLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setParameterValueLong_0(name, btLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameterValueLong_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_0"></a>
# **setParameter_0**
> Property setParameter_0(name, btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_0(name, btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_1"></a>
# **setParameter_1**
> Property setParameter_1(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_1(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameter_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_2"></a>
# **setParameter_2**
> Property setParameter_2(name, btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String name = "name_example"; // String | 
String btLocator = "btLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_2(name, btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameter_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **btLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameters"></a>
# **setParameters**
> Properties setParameters(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.setParameters(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameters_0"></a>
# **setParameters_0**
> Properties setParameters_0(btLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.setParameters_0(btLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setTemplates"></a>
# **setTemplates**
> BuildTypes setTemplates(btLocator, body, optimizeSettings, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
BuildTypes body = new BuildTypes(); // BuildTypes | 
Boolean optimizeSettings = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.setTemplates(btLocator, body, optimizeSettings, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**BuildTypes**](BuildTypes.md)|  | [optional]
 **optimizeSettings** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setVCSLabelingOptions"></a>
# **setVCSLabelingOptions**
> VcsLabeling setVCSLabelingOptions(btLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
VcsLabeling body = new VcsLabeling(); // VcsLabeling | 
try {
    VcsLabeling result = apiInstance.setVCSLabelingOptions(btLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#setVCSLabelingOptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **body** | [**VcsLabeling**](VcsLabeling.md)|  | [optional]

### Return type

[**VcsLabeling**](VcsLabeling.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateVcsRootEntry"></a>
# **updateVcsRootEntry**
> VcsRootEntry updateVcsRootEntry(btLocator, vcsRootLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String vcsRootLocator = "vcsRootLocator_example"; // String | 
VcsRootEntry body = new VcsRootEntry(); // VcsRootEntry | 
String fields = "fields_example"; // String | 
try {
    VcsRootEntry result = apiInstance.updateVcsRootEntry(btLocator, vcsRootLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#updateVcsRootEntry");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **vcsRootLocator** | **String**|  |
 **body** | [**VcsRootEntry**](VcsRootEntry.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootEntry**](VcsRootEntry.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="updateVcsRootEntryCheckoutRules"></a>
# **updateVcsRootEntryCheckoutRules**
> String updateVcsRootEntryCheckoutRules(btLocator, vcsRootLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildTypeApi;


BuildTypeApi apiInstance = new BuildTypeApi();
String btLocator = "btLocator_example"; // String | 
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.updateVcsRootEntryCheckoutRules(btLocator, vcsRootLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildTypeApi#updateVcsRootEntryCheckoutRules");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **btLocator** | **String**|  |
 **vcsRootLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

