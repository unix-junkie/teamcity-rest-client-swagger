
# Properties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**property** | [**java.util.List&lt;Property&gt;**](Property.md) |  |  [optional]



