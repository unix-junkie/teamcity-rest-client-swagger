
# VcsLabeling

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**labelName** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**branchFilter** | **String** |  |  [optional]
**vcsRoots** | [**VcsRoots**](VcsRoots.md) |  |  [optional]



