
# Agent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**typeId** | **Integer** |  |  [optional]
**connected** | **Boolean** |  |  [optional]
**enabled** | **Boolean** |  |  [optional]
**authorized** | **Boolean** |  |  [optional]
**uptodate** | **Boolean** |  |  [optional]
**ip** | **String** |  |  [optional]
**protocol** | **String** |  |  [optional]
**version** | **String** |  |  [optional]
**lastActivityTime** | **String** |  |  [optional]
**disconnectionComment** | **String** |  |  [optional]
**href** | **String** |  |  [optional]
**webUrl** | **String** |  |  [optional]
**build** | [**Build**](Build.md) |  |  [optional]
**links** | [**Links**](Links.md) |  |  [optional]
**enabledInfo** | [**EnabledInfo**](EnabledInfo.md) |  |  [optional]
**authorizedInfo** | [**AuthorizedInfo**](AuthorizedInfo.md) |  |  [optional]
**properties** | [**Properties**](Properties.md) |  |  [optional]
**environment** | [**Environment**](Environment.md) |  |  [optional]
**pool** | [**AgentPool**](AgentPool.md) |  |  [optional]
**compatibilityPolicy** | [**CompatibilityPolicy**](CompatibilityPolicy.md) |  |  [optional]
**compatibleBuildTypes** | [**BuildTypes**](BuildTypes.md) |  |  [optional]
**incompatibleBuildTypes** | [**Compatibilities**](Compatibilities.md) |  |  [optional]
**locator** | **String** |  |  [optional]



