# DefaultApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**serveApiVersion**](DefaultApi.md#serveApiVersion) | **GET** /app/rest/apiVersion | 
[**serveBuildFieldShort**](DefaultApi.md#serveBuildFieldShort) | **GET** /app/rest/{projectLocator}/{btLocator}/{buildLocator}/{field} | 
[**servePluginInfo**](DefaultApi.md#servePluginInfo) | **GET** /app/rest/info | 
[**serveRoot**](DefaultApi.md#serveRoot) | **GET** /app/rest | 
[**serveVersion**](DefaultApi.md#serveVersion) | **GET** /app/rest/version | 


<a name="serveApiVersion"></a>
# **serveApiVersion**
> String serveApiVersion()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    String result = apiInstance.serveApiVersion();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#serveApiVersion");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildFieldShort"></a>
# **serveBuildFieldShort**
> String serveBuildFieldShort(projectLocator, btLocator, buildLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildFieldShort(projectLocator, btLocator, buildLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#serveBuildFieldShort");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **buildLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="servePluginInfo"></a>
# **servePluginInfo**
> Plugin servePluginInfo(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String fields = "fields_example"; // String | 
try {
    Plugin result = apiInstance.servePluginInfo(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#servePluginInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Plugin**](Plugin.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRoot"></a>
# **serveRoot**
> String serveRoot()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    String result = apiInstance.serveRoot();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#serveRoot");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveVersion"></a>
# **serveVersion**
> String serveVersion()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    String result = apiInstance.serveVersion();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#serveVersion");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

