
# Projects

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**project** | [**java.util.List&lt;Project&gt;**](Project.md) |  |  [optional]



