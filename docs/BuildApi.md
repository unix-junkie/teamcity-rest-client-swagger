# BuildApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addTags**](BuildApi.md#addTags) | **POST** /app/rest/builds/{buildLocator}/tags | 
[**addTagsMultiple**](BuildApi.md#addTagsMultiple) | **POST** /app/rest/builds/multiple/{buildLocator}/tags | 
[**cancelBuild**](BuildApi.md#cancelBuild) | **POST** /app/rest/builds/{buildLocator} | 
[**cancelBuild_0**](BuildApi.md#cancelBuild_0) | **GET** /app/rest/builds/{buildLocator}/example/buildCancelRequest | 
[**cancelMultiple**](BuildApi.md#cancelMultiple) | **POST** /app/rest/builds/multiple/{buildLocator} | 
[**deleteAllParameters**](BuildApi.md#deleteAllParameters) | **DELETE** /app/rest/builds/{buildLocator}/attributes | 
[**deleteBuild**](BuildApi.md#deleteBuild) | **DELETE** /app/rest/builds/{buildLocator} | 
[**deleteBuilds**](BuildApi.md#deleteBuilds) | **DELETE** /app/rest/builds | 
[**deleteComment**](BuildApi.md#deleteComment) | **DELETE** /app/rest/builds/{buildLocator}/comment | 
[**deleteCommentMultiple**](BuildApi.md#deleteCommentMultiple) | **DELETE** /app/rest/builds/multiple/{buildLocator}/comment | 
[**deleteMultiple**](BuildApi.md#deleteMultiple) | **DELETE** /app/rest/builds/multiple/{buildLocator} | 
[**deleteParameter**](BuildApi.md#deleteParameter) | **DELETE** /app/rest/builds/{buildLocator}/attributes/{name} | 
[**getArtifactDependencyChanges**](BuildApi.md#getArtifactDependencyChanges) | **GET** /app/rest/builds/{buildLocator}/artifactDependencyChanges | 
[**getArtifactsDirectory**](BuildApi.md#getArtifactsDirectory) | **GET** /app/rest/builds/{buildLocator}/artifactsDirectory | 
[**getBuildNumber**](BuildApi.md#getBuildNumber) | **GET** /app/rest/builds/{buildLocator}/number | 
[**getBuildStatusText**](BuildApi.md#getBuildStatusText) | **GET** /app/rest/builds/{buildLocator}/statusText | 
[**getCanceledInfo**](BuildApi.md#getCanceledInfo) | **GET** /app/rest/builds/{buildLocator}/canceledInfo | 
[**getChildren**](BuildApi.md#getChildren) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/children{path} | 
[**getChildrenAlias**](BuildApi.md#getChildrenAlias) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/{path} | 
[**getContent**](BuildApi.md#getContent) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/content{path} | 
[**getContentAlias**](BuildApi.md#getContentAlias) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/files{path} | 
[**getMetadata**](BuildApi.md#getMetadata) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/metadata{path} | 
[**getMultiple**](BuildApi.md#getMultiple) | **GET** /app/rest/builds/multiple/{buildLocator} | 
[**getParameter**](BuildApi.md#getParameter) | **GET** /app/rest/builds/{buildLocator}/attributes/{name} | 
[**getParameterValueLong**](BuildApi.md#getParameterValueLong) | **GET** /app/rest/builds/{buildLocator}/attributes/{name}/value | 
[**getParameter_0**](BuildApi.md#getParameter_0) | **GET** /app/rest/builds/{buildLocator}/resulting-properties/{propertyName} | 
[**getParameters**](BuildApi.md#getParameters) | **GET** /app/rest/builds/{buildLocator}/attributes | 
[**getPinData**](BuildApi.md#getPinData) | **GET** /app/rest/builds/{buildLocator}/pinInfo | 
[**getPinned**](BuildApi.md#getPinned) | **GET** /app/rest/builds/{buildLocator}/pin | 
[**getProblems**](BuildApi.md#getProblems) | **GET** /app/rest/builds/{buildLocator}/problemOccurrences | 
[**getResolvedParameter**](BuildApi.md#getResolvedParameter) | **GET** /app/rest/builds/{buildLocator}/resolved/{value} | 
[**getRoot**](BuildApi.md#getRoot) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts | 
[**getTests**](BuildApi.md#getTests) | **GET** /app/rest/builds/{buildLocator}/testOccurrences | 
[**getZipped**](BuildApi.md#getZipped) | **GET** /app/rest/builds/aggregated/{buildLocator}/artifacts/archived{path} | 
[**pinBuild**](BuildApi.md#pinBuild) | **PUT** /app/rest/builds/{buildLocator}/pin | 
[**pinMultiple**](BuildApi.md#pinMultiple) | **PUT** /app/rest/builds/multiple/{buildLocator}/pinInfo | 
[**removeTagsMultiple**](BuildApi.md#removeTagsMultiple) | **DELETE** /app/rest/builds/multiple/{buildLocator}/tags | 
[**replaceComment**](BuildApi.md#replaceComment) | **PUT** /app/rest/builds/{buildLocator}/comment | 
[**replaceCommentMultiple**](BuildApi.md#replaceCommentMultiple) | **PUT** /app/rest/builds/multiple/{buildLocator}/comment | 
[**replaceTags**](BuildApi.md#replaceTags) | **PUT** /app/rest/builds/{buildLocator}/tags | 
[**resetBuildFinishParameters**](BuildApi.md#resetBuildFinishParameters) | **DELETE** /app/rest/builds/{buildLocator}/caches/finishProperties | 
[**serveAggregatedBuildStatus**](BuildApi.md#serveAggregatedBuildStatus) | **GET** /app/rest/builds/aggregated/{buildLocator}/status | 
[**serveAggregatedBuildStatusIcon**](BuildApi.md#serveAggregatedBuildStatusIcon) | **GET** /app/rest/builds/aggregated/{buildLocator}/statusIcon{suffix} | 
[**serveAllBuilds**](BuildApi.md#serveAllBuilds) | **GET** /app/rest/builds | 
[**serveBuild**](BuildApi.md#serveBuild) | **GET** /app/rest/builds/{buildLocator} | 
[**serveBuildActualParameters**](BuildApi.md#serveBuildActualParameters) | **GET** /app/rest/builds/{buildLocator}/resulting-properties | 
[**serveBuildFieldByBuildOnly**](BuildApi.md#serveBuildFieldByBuildOnly) | **GET** /app/rest/builds/{buildLocator}/{field} | 
[**serveBuildRelatedIssues**](BuildApi.md#serveBuildRelatedIssues) | **GET** /app/rest/builds/{buildLocator}/relatedIssues | 
[**serveBuildRelatedIssuesOld**](BuildApi.md#serveBuildRelatedIssuesOld) | **GET** /app/rest/builds/{buildLocator}/related-issues | 
[**serveBuildStatisticValue**](BuildApi.md#serveBuildStatisticValue) | **GET** /app/rest/builds/{buildLocator}/statistics/{name} | 
[**serveBuildStatisticValues**](BuildApi.md#serveBuildStatisticValues) | **GET** /app/rest/builds/{buildLocator}/statistics | 
[**serveBuildStatusIcon**](BuildApi.md#serveBuildStatusIcon) | **GET** /app/rest/builds/{buildLocator}/statusIcon{suffix} | 
[**serveSourceFile**](BuildApi.md#serveSourceFile) | **GET** /app/rest/builds/{buildLocator}/sources/files/{fileName} | 
[**serveTags**](BuildApi.md#serveTags) | **GET** /app/rest/builds/{buildLocator}/tags | 
[**setBuildNumber**](BuildApi.md#setBuildNumber) | **PUT** /app/rest/builds/{buildLocator}/number | 
[**setBuildPinData**](BuildApi.md#setBuildPinData) | **PUT** /app/rest/builds/{buildLocator}/pinInfo | 
[**setBuildStatusText**](BuildApi.md#setBuildStatusText) | **PUT** /app/rest/builds/{buildLocator}/statusText | 
[**setParameter**](BuildApi.md#setParameter) | **POST** /app/rest/builds/{buildLocator}/attributes | 
[**setParameterValueLong**](BuildApi.md#setParameterValueLong) | **PUT** /app/rest/builds/{buildLocator}/attributes/{name}/value | 
[**setParameter_0**](BuildApi.md#setParameter_0) | **PUT** /app/rest/builds/{buildLocator}/attributes/{name} | 
[**setParameters**](BuildApi.md#setParameters) | **PUT** /app/rest/builds/{buildLocator}/attributes | 
[**unpinBuild**](BuildApi.md#unpinBuild) | **DELETE** /app/rest/builds/{buildLocator}/pin | 


<a name="addTags"></a>
# **addTags**
> Tags addTags(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
Tags body = new Tags(); // Tags | 
String fields = "fields_example"; // String | 
try {
    Tags result = apiInstance.addTags(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#addTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Tags**](Tags.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addTagsMultiple"></a>
# **addTagsMultiple**
> MultipleOperationResult addTagsMultiple(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
Tags body = new Tags(); // Tags | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.addTagsMultiple(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#addTagsMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Tags**](Tags.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelBuild"></a>
# **cancelBuild**
> Build cancelBuild(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
BuildCancelRequest body = new BuildCancelRequest(); // BuildCancelRequest | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.cancelBuild(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#cancelBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**BuildCancelRequest**](BuildCancelRequest.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelBuild_0"></a>
# **cancelBuild_0**
> BuildCancelRequest cancelBuild_0(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    BuildCancelRequest result = apiInstance.cancelBuild_0(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#cancelBuild_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

[**BuildCancelRequest**](BuildCancelRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelMultiple"></a>
# **cancelMultiple**
> MultipleOperationResult cancelMultiple(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
BuildCancelRequest body = new BuildCancelRequest(); // BuildCancelRequest | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.cancelMultiple(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#cancelMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**BuildCancelRequest**](BuildCancelRequest.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllParameters"></a>
# **deleteAllParameters**
> deleteAllParameters(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteAllParameters(buildLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteAllParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteBuild"></a>
# **deleteBuild**
> deleteBuild(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    apiInstance.deleteBuild(buildLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteBuilds"></a>
# **deleteBuilds**
> deleteBuilds(locator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String locator = "locator_example"; // String | 
try {
    apiInstance.deleteBuilds(locator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteComment"></a>
# **deleteComment**
> deleteComment(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    apiInstance.deleteComment(buildLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteCommentMultiple"></a>
# **deleteCommentMultiple**
> MultipleOperationResult deleteCommentMultiple(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.deleteCommentMultiple(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteCommentMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteMultiple"></a>
# **deleteMultiple**
> MultipleOperationResult deleteMultiple(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.deleteMultiple(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter"></a>
# **deleteParameter**
> deleteParameter(name, buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String name = "name_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteParameter(name, buildLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#deleteParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArtifactDependencyChanges"></a>
# **getArtifactDependencyChanges**
> BuildChanges getArtifactDependencyChanges(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildChanges result = apiInstance.getArtifactDependencyChanges(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getArtifactDependencyChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildChanges**](BuildChanges.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getArtifactsDirectory"></a>
# **getArtifactsDirectory**
> String getArtifactsDirectory(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.getArtifactsDirectory(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getArtifactsDirectory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuildNumber"></a>
# **getBuildNumber**
> String getBuildNumber(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.getBuildNumber(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getBuildNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuildStatusText"></a>
# **getBuildStatusText**
> String getBuildStatusText(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.getBuildStatusText(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getBuildStatusText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCanceledInfo"></a>
# **getCanceledInfo**
> Comment getCanceledInfo(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comment result = apiInstance.getCanceledInfo(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getCanceledInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Comment**](Comment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildren"></a>
# **getChildren**
> Files getChildren(path, buildLocator, basePath, locator, fields, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    Files result = apiInstance.getChildren(path, buildLocator, basePath, locator, fields, logBuildUsage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getChildren");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildrenAlias"></a>
# **getChildrenAlias**
> Files getChildrenAlias(path, buildLocator, basePath, locator, fields, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    Files result = apiInstance.getChildrenAlias(path, buildLocator, basePath, locator, fields, logBuildUsage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getChildrenAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContent"></a>
# **getContent**
> getContent(path, buildLocator, responseBuilder, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String responseBuilder = "responseBuilder_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    apiInstance.getContent(path, buildLocator, responseBuilder, logBuildUsage);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getContent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **responseBuilder** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContentAlias"></a>
# **getContentAlias**
> getContentAlias(path, buildLocator, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    apiInstance.getContentAlias(path, buildLocator, logBuildUsage);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getContentAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMetadata"></a>
# **getMetadata**
> File getMetadata(path, buildLocator, fields, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    File result = apiInstance.getMetadata(path, buildLocator, fields, logBuildUsage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMultiple"></a>
# **getMultiple**
> Builds getMultiple(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.getMultiple(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter"></a>
# **getParameter**
> Property getParameter(name, buildLocator, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String name = "name_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.getParameter(name, buildLocator, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterValueLong"></a>
# **getParameterValueLong**
> String getParameterValueLong(name, buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String name = "name_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    String result = apiInstance.getParameterValueLong(name, buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter_0"></a>
# **getParameter_0**
> String getParameter_0(buildLocator, propertyName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String propertyName = "propertyName_example"; // String | 
try {
    String result = apiInstance.getParameter_0(buildLocator, propertyName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **propertyName** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameters"></a>
# **getParameters**
> Properties getParameters(buildLocator, locator, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Properties result = apiInstance.getParameters(buildLocator, locator, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPinData"></a>
# **getPinData**
> PinInfo getPinData(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    PinInfo result = apiInstance.getPinData(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getPinData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**PinInfo**](PinInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPinned"></a>
# **getPinned**
> String getPinned(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.getPinned(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getPinned");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getProblems"></a>
# **getProblems**
> ProblemOccurrences getProblems(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ProblemOccurrences result = apiInstance.getProblems(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getProblems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**ProblemOccurrences**](ProblemOccurrences.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getResolvedParameter"></a>
# **getResolvedParameter**
> String getResolvedParameter(buildLocator, value)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String value = "value_example"; // String | 
try {
    String result = apiInstance.getResolvedParameter(buildLocator, value);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getResolvedParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **value** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRoot"></a>
# **getRoot**
> Files getRoot(buildLocator, basePath, locator, fields, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    Files result = apiInstance.getRoot(buildLocator, basePath, locator, fields, logBuildUsage);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getTests"></a>
# **getTests**
> TestOccurrences getTests(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    TestOccurrences result = apiInstance.getTests(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getTests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**TestOccurrences**](TestOccurrences.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getZipped"></a>
# **getZipped**
> getZipped(path, buildLocator, basePath, locator, name, logBuildUsage)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String path = "path_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String name = "name_example"; // String | 
Boolean logBuildUsage = true; // Boolean | 
try {
    apiInstance.getZipped(path, buildLocator, basePath, locator, name, logBuildUsage);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#getZipped");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **buildLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **name** | **String**|  | [optional]
 **logBuildUsage** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="pinBuild"></a>
# **pinBuild**
> pinBuild(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    apiInstance.pinBuild(buildLocator, body);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#pinBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="pinMultiple"></a>
# **pinMultiple**
> MultipleOperationResult pinMultiple(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
PinInfo body = new PinInfo(); // PinInfo | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.pinMultiple(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#pinMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**PinInfo**](PinInfo.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeTagsMultiple"></a>
# **removeTagsMultiple**
> MultipleOperationResult removeTagsMultiple(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
Tags body = new Tags(); // Tags | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.removeTagsMultiple(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#removeTagsMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Tags**](Tags.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceComment"></a>
# **replaceComment**
> replaceComment(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    apiInstance.replaceComment(buildLocator, body);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#replaceComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceCommentMultiple"></a>
# **replaceCommentMultiple**
> MultipleOperationResult replaceCommentMultiple(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    MultipleOperationResult result = apiInstance.replaceCommentMultiple(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#replaceCommentMultiple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MultipleOperationResult**](MultipleOperationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceTags"></a>
# **replaceTags**
> Tags replaceTags(buildLocator, locator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String locator = "locator_example"; // String | 
Tags body = new Tags(); // Tags | 
String fields = "fields_example"; // String | 
try {
    Tags result = apiInstance.replaceTags(buildLocator, locator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#replaceTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **body** | [**Tags**](Tags.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="resetBuildFinishParameters"></a>
# **resetBuildFinishParameters**
> resetBuildFinishParameters(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    apiInstance.resetBuildFinishParameters(buildLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#resetBuildFinishParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAggregatedBuildStatus"></a>
# **serveAggregatedBuildStatus**
> String serveAggregatedBuildStatus(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.serveAggregatedBuildStatus(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveAggregatedBuildStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAggregatedBuildStatusIcon"></a>
# **serveAggregatedBuildStatusIcon**
> serveAggregatedBuildStatusIcon(buildLocator, suffix)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String suffix = "suffix_example"; // String | 
try {
    apiInstance.serveAggregatedBuildStatusIcon(buildLocator, suffix);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveAggregatedBuildStatusIcon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **suffix** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAllBuilds"></a>
# **serveAllBuilds**
> Builds serveAllBuilds(buildType, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildType = "buildType_example"; // String | 
String status = "status_example"; // String | 
String triggeredByUser = "triggeredByUser_example"; // String | 
Boolean includePersonal = true; // Boolean | 
Boolean includeCanceled = true; // Boolean | 
Boolean onlyPinned = true; // Boolean | 
java.util.List<String> tag = Arrays.asList("tag_example"); // java.util.List<String> | 
String agentName = "agentName_example"; // String | 
String sinceBuild = "sinceBuild_example"; // String | 
String sinceDate = "sinceDate_example"; // String | 
Long start = 789L; // Long | 
Integer count = 56; // Integer | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.serveAllBuilds(buildType, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveAllBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildType** | **String**|  | [optional]
 **status** | **String**|  | [optional]
 **triggeredByUser** | **String**|  | [optional]
 **includePersonal** | **Boolean**|  | [optional]
 **includeCanceled** | **Boolean**|  | [optional]
 **onlyPinned** | **Boolean**|  | [optional]
 **tag** | [**java.util.List&lt;String&gt;**](String.md)|  | [optional]
 **agentName** | **String**|  | [optional]
 **sinceBuild** | **String**|  | [optional]
 **sinceDate** | **String**|  | [optional]
 **start** | **Long**|  | [optional]
 **count** | **Integer**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuild"></a>
# **serveBuild**
> Build serveBuild(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.serveBuild(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildActualParameters"></a>
# **serveBuildActualParameters**
> Properties serveBuildActualParameters(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveBuildActualParameters(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildActualParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildFieldByBuildOnly"></a>
# **serveBuildFieldByBuildOnly**
> String serveBuildFieldByBuildOnly(buildLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildFieldByBuildOnly(buildLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildFieldByBuildOnly");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildRelatedIssues"></a>
# **serveBuildRelatedIssues**
> IssuesUsages serveBuildRelatedIssues(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    IssuesUsages result = apiInstance.serveBuildRelatedIssues(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildRelatedIssues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**IssuesUsages**](IssuesUsages.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildRelatedIssuesOld"></a>
# **serveBuildRelatedIssuesOld**
> IssuesUsages serveBuildRelatedIssuesOld(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    IssuesUsages result = apiInstance.serveBuildRelatedIssuesOld(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildRelatedIssuesOld");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**IssuesUsages**](IssuesUsages.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildStatisticValue"></a>
# **serveBuildStatisticValue**
> String serveBuildStatisticValue(buildLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    String result = apiInstance.serveBuildStatisticValue(buildLocator, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildStatisticValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **name** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildStatisticValues"></a>
# **serveBuildStatisticValues**
> Properties serveBuildStatisticValues(buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveBuildStatisticValues(buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildStatisticValues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildStatusIcon"></a>
# **serveBuildStatusIcon**
> serveBuildStatusIcon(buildLocator, suffix)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String suffix = "suffix_example"; // String | 
try {
    apiInstance.serveBuildStatusIcon(buildLocator, suffix);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveBuildStatusIcon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **suffix** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveSourceFile"></a>
# **serveSourceFile**
> serveSourceFile(buildLocator, fileName)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String fileName = "fileName_example"; // String | 
try {
    apiInstance.serveSourceFile(buildLocator, fileName);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveSourceFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **fileName** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveTags"></a>
# **serveTags**
> Tags serveTags(buildLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Tags result = apiInstance.serveTags(buildLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#serveTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildNumber"></a>
# **setBuildNumber**
> String setBuildNumber(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setBuildNumber(buildLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setBuildNumber");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildPinData"></a>
# **setBuildPinData**
> PinInfo setBuildPinData(buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
PinInfo body = new PinInfo(); // PinInfo | 
String fields = "fields_example"; // String | 
try {
    PinInfo result = apiInstance.setBuildPinData(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setBuildPinData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**PinInfo**](PinInfo.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**PinInfo**](PinInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildStatusText"></a>
# **setBuildStatusText**
> String setBuildStatusText(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setBuildStatusText(buildLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setBuildStatusText");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter"></a>
# **setParameter**
> Property setParameter(buildLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter(buildLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterValueLong"></a>
# **setParameterValueLong**
> String setParameterValueLong(name, buildLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String name = "name_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    String result = apiInstance.setParameterValueLong(name, buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_0"></a>
# **setParameter_0**
> Property setParameter_0(name, buildLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String name = "name_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_0(name, buildLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **buildLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameters"></a>
# **setParameters**
> Properties setParameters(buildLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Properties result = apiInstance.setParameters(buildLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#setParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="unpinBuild"></a>
# **unpinBuild**
> unpinBuild(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildApi;


BuildApi apiInstance = new BuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    apiInstance.unpinBuild(buildLocator, body);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildApi#unpinBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

