
# Tests

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**_default** | **Boolean** |  |  [optional]
**test** | [**java.util.List&lt;Test&gt;**](Test.md) |  |  [optional]



