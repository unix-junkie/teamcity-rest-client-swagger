
# Datas

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**data** | [**java.util.List&lt;MetaData&gt;**](MetaData.md) |  |  [optional]



