
# Links

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**link** | [**java.util.List&lt;Link&gt;**](Link.md) |  |  [optional]



