
# Servers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**server** | [**java.util.List&lt;FederationServer&gt;**](FederationServer.md) |  |  [optional]



