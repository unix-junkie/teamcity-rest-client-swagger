
# BuildChanges

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**buildChange** | [**java.util.List&lt;BuildChange&gt;**](BuildChange.md) |  |  [optional]



