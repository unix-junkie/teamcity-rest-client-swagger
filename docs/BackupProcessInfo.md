
# BackupProcessInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTimestamp** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**finishTimestamp** | [**OffsetDateTime**](OffsetDateTime.md) |  |  [optional]
**fileSize** | **Long** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) |  |  [optional]
**id** | **Integer** |  |  [optional]
**fileName** | **String** |  |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
RUNNING | &quot;Running&quot;
CANCELLING | &quot;Cancelling&quot;
CANCELLED | &quot;Cancelled&quot;
FINISHED | &quot;Finished&quot;
FAULT | &quot;Fault&quot;



