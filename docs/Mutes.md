
# Mutes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**_default** | **Boolean** |  |  [optional]
**href** | **String** |  |  [optional]
**mute** | [**java.util.List&lt;Mute&gt;**](Mute.md) |  |  [optional]



