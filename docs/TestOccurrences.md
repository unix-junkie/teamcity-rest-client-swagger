
# TestOccurrences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**testOccurrence** | [**java.util.List&lt;TestOccurrence&gt;**](TestOccurrence.md) |  |  [optional]
**_default** | **Boolean** |  |  [optional]
**passed** | **Integer** |  |  [optional]
**failed** | **Integer** |  |  [optional]
**newFailed** | **Integer** |  |  [optional]
**ignored** | **Integer** |  |  [optional]
**muted** | **Integer** |  |  [optional]



