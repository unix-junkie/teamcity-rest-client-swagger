# TestApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTests**](TestApi.md#getTests) | **GET** /app/rest/tests | 
[**serveInstance**](TestApi.md#serveInstance) | **GET** /app/rest/tests/{testLocator} | 


<a name="getTests"></a>
# **getTests**
> Tests getTests(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.TestApi;


TestApi apiInstance = new TestApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Tests result = apiInstance.getTests(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TestApi#getTests");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tests**](Tests.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> Test serveInstance(testLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.TestApi;


TestApi apiInstance = new TestApi();
String testLocator = "testLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Test result = apiInstance.serveInstance(testLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TestApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Test**](Test.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

