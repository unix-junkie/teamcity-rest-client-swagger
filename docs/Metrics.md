
# Metrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**metric** | [**java.util.List&lt;Metric&gt;**](Metric.md) |  |  [optional]



