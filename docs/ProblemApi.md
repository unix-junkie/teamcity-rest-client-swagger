# ProblemApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProblems**](ProblemApi.md#getProblems) | **GET** /app/rest/problems | 
[**serveInstance**](ProblemApi.md#serveInstance) | **GET** /app/rest/problems/{problemLocator} | 


<a name="getProblems"></a>
# **getProblems**
> Problems getProblems(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProblemApi;


ProblemApi apiInstance = new ProblemApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Problems result = apiInstance.getProblems(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProblemApi#getProblems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Problems**](Problems.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> Problem serveInstance(problemLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProblemApi;


ProblemApi apiInstance = new ProblemApi();
String problemLocator = "problemLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Problem result = apiInstance.serveInstance(problemLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProblemApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **problemLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Problem**](Problem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

