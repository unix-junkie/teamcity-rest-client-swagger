# BuildQueueApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addTags**](BuildQueueApi.md#addTags) | **POST** /app/rest/buildQueue/{buildLocator}/tags | 
[**cancelBuild**](BuildQueueApi.md#cancelBuild) | **GET** /app/rest/buildQueue/{buildLocator}/example/buildCancelRequest | 
[**cancelBuild_0**](BuildQueueApi.md#cancelBuild_0) | **POST** /app/rest/buildQueue/{queuedBuildLocator} | 
[**deleteBuild**](BuildQueueApi.md#deleteBuild) | **DELETE** /app/rest/buildQueue/{queuedBuildLocator} | 
[**deleteBuildsExperimental**](BuildQueueApi.md#deleteBuildsExperimental) | **DELETE** /app/rest/buildQueue | 
[**getBuild**](BuildQueueApi.md#getBuild) | **GET** /app/rest/buildQueue/{queuedBuildLocator} | 
[**getBuilds**](BuildQueueApi.md#getBuilds) | **GET** /app/rest/buildQueue | 
[**queueNewBuild**](BuildQueueApi.md#queueNewBuild) | **POST** /app/rest/buildQueue | 
[**replaceBuilds**](BuildQueueApi.md#replaceBuilds) | **PUT** /app/rest/buildQueue | 
[**replaceTags**](BuildQueueApi.md#replaceTags) | **PUT** /app/rest/buildQueue/{buildLocator}/tags | 
[**serveBuildFieldByBuildOnly**](BuildQueueApi.md#serveBuildFieldByBuildOnly) | **GET** /app/rest/buildQueue/{buildLocator}/{field} | 
[**serveCompatibleAgents**](BuildQueueApi.md#serveCompatibleAgents) | **GET** /app/rest/buildQueue/{queuedBuildLocator}/compatibleAgents | 
[**serveTags**](BuildQueueApi.md#serveTags) | **GET** /app/rest/buildQueue/{buildLocator}/tags | 
[**setBuildQueueOrder**](BuildQueueApi.md#setBuildQueueOrder) | **PUT** /app/rest/buildQueue/order | 
[**setBuildQueuePosition**](BuildQueueApi.md#setBuildQueuePosition) | **GET** /app/rest/buildQueue/order/{queuePosition} | 
[**setBuildQueuePosition_0**](BuildQueueApi.md#setBuildQueuePosition_0) | **PUT** /app/rest/buildQueue/order/{queuePosition} | 


<a name="addTags"></a>
# **addTags**
> addTags(buildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String buildLocator = "buildLocator_example"; // String | 
Tags body = new Tags(); // Tags | 
try {
    apiInstance.addTags(buildLocator, body);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#addTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | [**Tags**](Tags.md)|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelBuild"></a>
# **cancelBuild**
> BuildCancelRequest cancelBuild(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    BuildCancelRequest result = apiInstance.cancelBuild(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#cancelBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

[**BuildCancelRequest**](BuildCancelRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="cancelBuild_0"></a>
# **cancelBuild_0**
> Build cancelBuild_0(queuedBuildLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuedBuildLocator = "queuedBuildLocator_example"; // String | 
BuildCancelRequest body = new BuildCancelRequest(); // BuildCancelRequest | 
try {
    Build result = apiInstance.cancelBuild_0(queuedBuildLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#cancelBuild_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuedBuildLocator** | **String**|  |
 **body** | [**BuildCancelRequest**](BuildCancelRequest.md)|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteBuild"></a>
# **deleteBuild**
> deleteBuild(queuedBuildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuedBuildLocator = "queuedBuildLocator_example"; // String | 
try {
    apiInstance.deleteBuild(queuedBuildLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#deleteBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuedBuildLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteBuildsExperimental"></a>
# **deleteBuildsExperimental**
> deleteBuildsExperimental(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteBuildsExperimental(locator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#deleteBuildsExperimental");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuild"></a>
# **getBuild**
> Build getBuild(queuedBuildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuedBuildLocator = "queuedBuildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.getBuild(queuedBuildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#getBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuedBuildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuilds"></a>
# **getBuilds**
> Builds getBuilds(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.getBuilds(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#getBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="queueNewBuild"></a>
# **queueNewBuild**
> Build queueNewBuild(body, moveToTop)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
Build body = new Build(); // Build | 
Boolean moveToTop = true; // Boolean | 
try {
    Build result = apiInstance.queueNewBuild(body, moveToTop);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#queueNewBuild");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Build**](Build.md)|  | [optional]
 **moveToTop** | **Boolean**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceBuilds"></a>
# **replaceBuilds**
> Builds replaceBuilds(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
Builds body = new Builds(); // Builds | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.replaceBuilds(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#replaceBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Builds**](Builds.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceTags"></a>
# **replaceTags**
> Tags replaceTags(buildLocator, locator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String buildLocator = "buildLocator_example"; // String | 
String locator = "locator_example"; // String | 
Tags body = new Tags(); // Tags | 
String fields = "fields_example"; // String | 
try {
    Tags result = apiInstance.replaceTags(buildLocator, locator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#replaceTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **body** | [**Tags**](Tags.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildFieldByBuildOnly"></a>
# **serveBuildFieldByBuildOnly**
> String serveBuildFieldByBuildOnly(buildLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String buildLocator = "buildLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildFieldByBuildOnly(buildLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#serveBuildFieldByBuildOnly");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveCompatibleAgents"></a>
# **serveCompatibleAgents**
> Agents serveCompatibleAgents(queuedBuildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuedBuildLocator = "queuedBuildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Agents result = apiInstance.serveCompatibleAgents(queuedBuildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#serveCompatibleAgents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuedBuildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Agents**](Agents.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveTags"></a>
# **serveTags**
> Tags serveTags(buildLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String buildLocator = "buildLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Tags result = apiInstance.serveTags(buildLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#serveTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Tags**](Tags.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildQueueOrder"></a>
# **setBuildQueueOrder**
> Builds setBuildQueueOrder(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
Builds body = new Builds(); // Builds | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.setBuildQueueOrder(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#setBuildQueueOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Builds**](Builds.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildQueuePosition"></a>
# **setBuildQueuePosition**
> Build setBuildQueuePosition(queuePosition, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuePosition = "queuePosition_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.setBuildQueuePosition(queuePosition, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#setBuildQueuePosition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuePosition** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildQueuePosition_0"></a>
# **setBuildQueuePosition_0**
> Build setBuildQueuePosition_0(queuePosition, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.BuildQueueApi;


BuildQueueApi apiInstance = new BuildQueueApi();
String queuePosition = "queuePosition_example"; // String | 
Build body = new Build(); // Build | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.setBuildQueuePosition_0(queuePosition, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BuildQueueApi#setBuildQueuePosition_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **queuePosition** | **String**|  |
 **body** | [**Build**](Build.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

