
# Entries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**entry** | [**java.util.List&lt;Entry&gt;**](Entry.md) |  |  [optional]



