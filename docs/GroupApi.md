# GroupApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addGroup**](GroupApi.md#addGroup) | **POST** /app/rest/userGroups | 
[**addRole**](GroupApi.md#addRole) | **POST** /app/rest/userGroups/{groupLocator}/roles | 
[**addRoleSimple**](GroupApi.md#addRoleSimple) | **POST** /app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope} | 
[**deleteGroup**](GroupApi.md#deleteGroup) | **DELETE** /app/rest/userGroups/{groupLocator} | 
[**deleteRole**](GroupApi.md#deleteRole) | **DELETE** /app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope} | 
[**getParentGroups**](GroupApi.md#getParentGroups) | **GET** /app/rest/userGroups/{groupLocator}/parent-groups | 
[**getPermissions**](GroupApi.md#getPermissions) | **GET** /app/rest/userGroups/{groupLocator}/debug/permissions | 
[**getProperties**](GroupApi.md#getProperties) | **GET** /app/rest/userGroups/{groupLocator}/properties | 
[**listRole**](GroupApi.md#listRole) | **GET** /app/rest/userGroups/{groupLocator}/roles/{roleId}/{scope} | 
[**listRoles**](GroupApi.md#listRoles) | **GET** /app/rest/userGroups/{groupLocator}/roles | 
[**putUserProperty**](GroupApi.md#putUserProperty) | **PUT** /app/rest/userGroups/{groupLocator}/properties/{name} | 
[**removeUserProperty**](GroupApi.md#removeUserProperty) | **DELETE** /app/rest/userGroups/{groupLocator}/properties/{name} | 
[**serveGroup**](GroupApi.md#serveGroup) | **GET** /app/rest/userGroups/{groupLocator} | 
[**serveGroups**](GroupApi.md#serveGroups) | **GET** /app/rest/userGroups | 
[**serveUserProperties**](GroupApi.md#serveUserProperties) | **GET** /app/rest/userGroups/{groupLocator}/properties/{name} | 
[**setParentGroups**](GroupApi.md#setParentGroups) | **PUT** /app/rest/userGroups/{groupLocator}/parent-groups | 
[**setRoles**](GroupApi.md#setRoles) | **PUT** /app/rest/userGroups/{groupLocator}/roles | 


<a name="addGroup"></a>
# **addGroup**
> Group addGroup(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
Group body = new Group(); // Group | 
String fields = "fields_example"; // String | 
try {
    Group result = apiInstance.addGroup(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#addGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Group**](Group.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addRole"></a>
# **addRole**
> Role addRole(groupLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
Role body = new Role(); // Role | 
try {
    Role result = apiInstance.addRole(groupLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#addRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **body** | [**Role**](Role.md)|  | [optional]

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addRoleSimple"></a>
# **addRoleSimple**
> Role addRoleSimple(groupLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    Role result = apiInstance.addRoleSimple(groupLocator, roleId, scope);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#addRoleSimple");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteGroup"></a>
# **deleteGroup**
> deleteGroup(groupLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
try {
    apiInstance.deleteGroup(groupLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#deleteGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRole"></a>
# **deleteRole**
> deleteRole(groupLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    apiInstance.deleteRole(groupLocator, roleId, scope);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#deleteRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParentGroups"></a>
# **getParentGroups**
> Groups getParentGroups(groupLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Groups result = apiInstance.getParentGroups(groupLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#getParentGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Groups**](Groups.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPermissions"></a>
# **getPermissions**
> String getPermissions(groupLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
try {
    String result = apiInstance.getPermissions(groupLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#getPermissions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getProperties"></a>
# **getProperties**
> Properties getProperties(groupLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getProperties(groupLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#getProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="listRole"></a>
# **listRole**
> Role listRole(groupLocator, roleId, scope)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String roleId = "roleId_example"; // String | 
String scope = "scope_example"; // String | 
try {
    Role result = apiInstance.listRole(groupLocator, roleId, scope);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#listRole");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **roleId** | **String**|  |
 **scope** | **String**|  |

### Return type

[**Role**](Role.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="listRoles"></a>
# **listRoles**
> Roles listRoles(groupLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
try {
    Roles result = apiInstance.listRoles(groupLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#listRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putUserProperty"></a>
# **putUserProperty**
> String putUserProperty(groupLocator, name, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String name = "name_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.putUserProperty(groupLocator, name, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#putUserProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **name** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeUserProperty"></a>
# **removeUserProperty**
> removeUserProperty(groupLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.removeUserProperty(groupLocator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#removeUserProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **name** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveGroup"></a>
# **serveGroup**
> Group serveGroup(groupLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Group result = apiInstance.serveGroup(groupLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#serveGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Group**](Group.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveGroups"></a>
# **serveGroups**
> Groups serveGroups(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String fields = "fields_example"; // String | 
try {
    Groups result = apiInstance.serveGroups(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#serveGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Groups**](Groups.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveUserProperties"></a>
# **serveUserProperties**
> String serveUserProperties(groupLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    String result = apiInstance.serveUserProperties(groupLocator, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#serveUserProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **name** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParentGroups"></a>
# **setParentGroups**
> Groups setParentGroups(groupLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
Groups body = new Groups(); // Groups | 
String fields = "fields_example"; // String | 
try {
    Groups result = apiInstance.setParentGroups(groupLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#setParentGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **body** | [**Groups**](Groups.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Groups**](Groups.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setRoles"></a>
# **setRoles**
> Roles setRoles(groupLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.GroupApi;


GroupApi apiInstance = new GroupApi();
String groupLocator = "groupLocator_example"; // String | 
Roles body = new Roles(); // Roles | 
try {
    Roles result = apiInstance.setRoles(groupLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GroupApi#setRoles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **groupLocator** | **String**|  |
 **body** | [**Roles**](Roles.md)|  | [optional]

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

