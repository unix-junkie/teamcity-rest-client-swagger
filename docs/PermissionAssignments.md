
# PermissionAssignments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**permissionAssignment** | [**java.util.List&lt;PermissionAssignment&gt;**](PermissionAssignment.md) |  |  [optional]



