
# Revisions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**revision** | [**java.util.List&lt;Revision&gt;**](Revision.md) |  |  [optional]



