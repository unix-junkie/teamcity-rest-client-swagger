
# Branches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**branch** | [**java.util.List&lt;Branch&gt;**](Branch.md) |  |  [optional]



