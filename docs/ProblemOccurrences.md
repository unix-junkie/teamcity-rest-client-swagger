
# ProblemOccurrences

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**problemOccurrence** | [**java.util.List&lt;ProblemOccurrence&gt;**](ProblemOccurrence.md) |  |  [optional]
**_default** | **Boolean** |  |  [optional]
**passed** | **Integer** |  |  [optional]
**failed** | **Integer** |  |  [optional]
**newFailed** | **Integer** |  |  [optional]
**ignored** | **Integer** |  |  [optional]
**muted** | **Integer** |  |  [optional]



