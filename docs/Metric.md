
# Metric

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**prometheusName** | **String** |  |  [optional]
**value** | **Double** |  |  [optional]
**metricTags** | [**MetricTags**](MetricTags.md) |  |  [optional]



