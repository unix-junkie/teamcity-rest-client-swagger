
# Plugins

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**plugin** | [**java.util.List&lt;Plugin&gt;**](Plugin.md) |  |  [optional]



