# ProjectApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add**](ProjectApi.md#add) | **POST** /app/rest/projects/{projectLocator}/projectFeatures | 
[**createBuildType**](ProjectApi.md#createBuildType) | **POST** /app/rest/projects/{projectLocator}/buildTypes | 
[**createBuildTypeTemplate**](ProjectApi.md#createBuildTypeTemplate) | **POST** /app/rest/projects/{projectLocator}/templates | 
[**createProject**](ProjectApi.md#createProject) | **POST** /app/rest/projects | 
[**createSecureToken**](ProjectApi.md#createSecureToken) | **POST** /app/rest/projects/{projectLocator}/secure/tokens | 
[**delete**](ProjectApi.md#delete) | **DELETE** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator} | 
[**deleteAllParameters**](ProjectApi.md#deleteAllParameters) | **DELETE** /app/rest/projects/{projectLocator}/parameters | 
[**deleteAllParameters_0**](ProjectApi.md#deleteAllParameters_0) | **DELETE** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties | 
[**deleteParameter**](ProjectApi.md#deleteParameter) | **DELETE** /app/rest/projects/{projectLocator}/parameters/{name} | 
[**deleteParameter_0**](ProjectApi.md#deleteParameter_0) | **DELETE** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name} | 
[**deleteProject**](ProjectApi.md#deleteProject) | **DELETE** /app/rest/projects/{projectLocator} | 
[**deleteProjectAgentPools**](ProjectApi.md#deleteProjectAgentPools) | **DELETE** /app/rest/projects/{projectLocator}/agentPools/{agentPoolLocator} | 
[**get**](ProjectApi.md#get) | **GET** /app/rest/projects/{projectLocator}/projectFeatures | 
[**getBranches**](ProjectApi.md#getBranches) | **GET** /app/rest/projects/{projectLocator}/branches | 
[**getBuildTypesOrder**](ProjectApi.md#getBuildTypesOrder) | **GET** /app/rest/projects/{projectLocator}/order/buildTypes | 
[**getDefaultTemplate**](ProjectApi.md#getDefaultTemplate) | **GET** /app/rest/projects/{projectLocator}/defaultTemplate | 
[**getExampleNewProjectDescription**](ProjectApi.md#getExampleNewProjectDescription) | **GET** /app/rest/projects/{projectLocator}/example/newProjectDescription | 
[**getExampleNewProjectDescriptionCompatibilityVersion1**](ProjectApi.md#getExampleNewProjectDescriptionCompatibilityVersion1) | **GET** /app/rest/projects/{projectLocator}/newProjectDescription | 
[**getParameter**](ProjectApi.md#getParameter) | **GET** /app/rest/projects/{projectLocator}/parameters/{name} | 
[**getParameterType**](ProjectApi.md#getParameterType) | **GET** /app/rest/projects/{projectLocator}/parameters/{name}/type | 
[**getParameterTypeRawValue**](ProjectApi.md#getParameterTypeRawValue) | **GET** /app/rest/projects/{projectLocator}/parameters/{name}/type/rawValue | 
[**getParameterValueLong**](ProjectApi.md#getParameterValueLong) | **GET** /app/rest/projects/{projectLocator}/parameters/{name}/value | 
[**getParameterValueLong_0**](ProjectApi.md#getParameterValueLong_0) | **GET** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}/value | 
[**getParameter_0**](ProjectApi.md#getParameter_0) | **GET** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name} | 
[**getParameters**](ProjectApi.md#getParameters) | **GET** /app/rest/projects/{projectLocator}/parameters | 
[**getParameters_0**](ProjectApi.md#getParameters_0) | **GET** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties | 
[**getParentProject**](ProjectApi.md#getParentProject) | **GET** /app/rest/projects/{projectLocator}/parentProject | 
[**getProjectAgentPools**](ProjectApi.md#getProjectAgentPools) | **GET** /app/rest/projects/{projectLocator}/agentPools | 
[**getProjectsOrder**](ProjectApi.md#getProjectsOrder) | **GET** /app/rest/projects/{projectLocator}/order/projects | 
[**getSecureValue**](ProjectApi.md#getSecureValue) | **GET** /app/rest/projects/{projectLocator}/secure/values/{token} | 
[**getSettingsFile**](ProjectApi.md#getSettingsFile) | **GET** /app/rest/projects/{projectLocator}/settingsFile | 
[**getSingle**](ProjectApi.md#getSingle) | **GET** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator} | 
[**reloadSettingsFile**](ProjectApi.md#reloadSettingsFile) | **GET** /app/rest/projects/{projectLocator}/latest | 
[**removeDefaultTemplate**](ProjectApi.md#removeDefaultTemplate) | **DELETE** /app/rest/projects/{projectLocator}/defaultTemplate | 
[**replace**](ProjectApi.md#replace) | **PUT** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator} | 
[**replaceAll**](ProjectApi.md#replaceAll) | **PUT** /app/rest/projects/{projectLocator}/projectFeatures | 
[**serveBuildFieldWithProject**](ProjectApi.md#serveBuildFieldWithProject) | **GET** /app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds/{buildLocator}/{field} | 
[**serveBuildType**](ProjectApi.md#serveBuildType) | **GET** /app/rest/projects/{projectLocator}/buildTypes/{btLocator} | 
[**serveBuildTypeFieldWithProject**](ProjectApi.md#serveBuildTypeFieldWithProject) | **GET** /app/rest/projects/{projectLocator}/buildTypes/{btLocator}/{field} | 
[**serveBuildTypeTemplates**](ProjectApi.md#serveBuildTypeTemplates) | **GET** /app/rest/projects/{projectLocator}/templates/{btLocator} | 
[**serveBuildTypesInProject**](ProjectApi.md#serveBuildTypesInProject) | **GET** /app/rest/projects/{projectLocator}/buildTypes | 
[**serveBuildWithProject**](ProjectApi.md#serveBuildWithProject) | **GET** /app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds/{buildLocator} | 
[**serveBuilds**](ProjectApi.md#serveBuilds) | **GET** /app/rest/projects/{projectLocator}/buildTypes/{btLocator}/builds | 
[**serveProject**](ProjectApi.md#serveProject) | **GET** /app/rest/projects/{projectLocator} | 
[**serveProjectField**](ProjectApi.md#serveProjectField) | **GET** /app/rest/projects/{projectLocator}/{field} | 
[**serveProjects**](ProjectApi.md#serveProjects) | **GET** /app/rest/projects | 
[**serveTemplatesInProject**](ProjectApi.md#serveTemplatesInProject) | **GET** /app/rest/projects/{projectLocator}/templates | 
[**setBuildTypesOrder**](ProjectApi.md#setBuildTypesOrder) | **PUT** /app/rest/projects/{projectLocator}/order/buildTypes | 
[**setDefaultTemplate**](ProjectApi.md#setDefaultTemplate) | **PUT** /app/rest/projects/{projectLocator}/defaultTemplate | 
[**setParameter**](ProjectApi.md#setParameter) | **POST** /app/rest/projects/{projectLocator}/parameters | 
[**setParameterType**](ProjectApi.md#setParameterType) | **PUT** /app/rest/projects/{projectLocator}/parameters/{name}/type | 
[**setParameterTypeRawValue**](ProjectApi.md#setParameterTypeRawValue) | **PUT** /app/rest/projects/{projectLocator}/parameters/{name}/type/rawValue | 
[**setParameterValueLong**](ProjectApi.md#setParameterValueLong) | **PUT** /app/rest/projects/{projectLocator}/parameters/{name}/value | 
[**setParameterValueLong_0**](ProjectApi.md#setParameterValueLong_0) | **PUT** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name}/value | 
[**setParameter_0**](ProjectApi.md#setParameter_0) | **PUT** /app/rest/projects/{projectLocator}/parameters/{name} | 
[**setParameter_1**](ProjectApi.md#setParameter_1) | **POST** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties | 
[**setParameter_2**](ProjectApi.md#setParameter_2) | **PUT** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties/{name} | 
[**setParameters**](ProjectApi.md#setParameters) | **PUT** /app/rest/projects/{projectLocator}/parameters | 
[**setParameters_0**](ProjectApi.md#setParameters_0) | **PUT** /app/rest/projects/{projectLocator}/projectFeatures/{featureLocator}/properties | 
[**setParentProject**](ProjectApi.md#setParentProject) | **PUT** /app/rest/projects/{projectLocator}/parentProject | 
[**setProjectAgentPools**](ProjectApi.md#setProjectAgentPools) | **PUT** /app/rest/projects/{projectLocator}/agentPools | 
[**setProjectAgentPools_0**](ProjectApi.md#setProjectAgentPools_0) | **POST** /app/rest/projects/{projectLocator}/agentPools | 
[**setProjectField**](ProjectApi.md#setProjectField) | **PUT** /app/rest/projects/{projectLocator}/{field} | 
[**setProjectsOrder**](ProjectApi.md#setProjectsOrder) | **PUT** /app/rest/projects/{projectLocator}/order/projects | 


<a name="add"></a>
# **add**
> Object add(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
ProjectFeature body = new ProjectFeature(); // ProjectFeature | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.add(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#add");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**ProjectFeature**](ProjectFeature.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createBuildType"></a>
# **createBuildType**
> BuildType createBuildType(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
NewBuildTypeDescription body = new NewBuildTypeDescription(); // NewBuildTypeDescription | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.createBuildType(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#createBuildType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**NewBuildTypeDescription**](NewBuildTypeDescription.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createBuildTypeTemplate"></a>
# **createBuildTypeTemplate**
> BuildType createBuildTypeTemplate(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
NewBuildTypeDescription body = new NewBuildTypeDescription(); // NewBuildTypeDescription | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.createBuildTypeTemplate(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#createBuildTypeTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**NewBuildTypeDescription**](NewBuildTypeDescription.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createProject"></a>
# **createProject**
> Project createProject(body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
NewProjectDescription body = new NewProjectDescription(); // NewProjectDescription | 
try {
    Project result = apiInstance.createProject(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#createProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**NewProjectDescription**](NewProjectDescription.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createSecureToken"></a>
# **createSecureToken**
> String createSecureToken(projectLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.createSecureToken(projectLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#createSecureToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="delete"></a>
# **delete**
> delete(featureLocator, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    apiInstance.delete(featureLocator, projectLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#delete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllParameters"></a>
# **deleteAllParameters**
> deleteAllParameters(projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
try {
    apiInstance.deleteAllParameters(projectLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteAllParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllParameters_0"></a>
# **deleteAllParameters_0**
> deleteAllParameters_0(featureLocator, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteAllParameters_0(featureLocator, projectLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteAllParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter"></a>
# **deleteParameter**
> deleteParameter(name, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    apiInstance.deleteParameter(name, projectLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter_0"></a>
# **deleteParameter_0**
> deleteParameter_0(name, featureLocator, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteParameter_0(name, featureLocator, projectLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteProject"></a>
# **deleteProject**
> deleteProject(projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
try {
    apiInstance.deleteProject(projectLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteProjectAgentPools"></a>
# **deleteProjectAgentPools**
> deleteProjectAgentPools(projectLocator, agentPoolLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String agentPoolLocator = "agentPoolLocator_example"; // String | 
try {
    apiInstance.deleteProjectAgentPools(projectLocator, agentPoolLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProjectAgentPools");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **agentPoolLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="get"></a>
# **get**
> Object get(projectLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.get(projectLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#get");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBranches"></a>
# **getBranches**
> Branches getBranches(projectLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Branches result = apiInstance.getBranches(projectLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getBranches");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Branches**](Branches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuildTypesOrder"></a>
# **getBuildTypesOrder**
> BuildTypes getBuildTypesOrder(projectLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    BuildTypes result = apiInstance.getBuildTypesOrder(projectLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getBuildTypesOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getDefaultTemplate"></a>
# **getDefaultTemplate**
> BuildType getDefaultTemplate(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.getDefaultTemplate(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getDefaultTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getExampleNewProjectDescription"></a>
# **getExampleNewProjectDescription**
> NewProjectDescription getExampleNewProjectDescription(projectLocator, id)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String id = "id_example"; // String | 
try {
    NewProjectDescription result = apiInstance.getExampleNewProjectDescription(projectLocator, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getExampleNewProjectDescription");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **id** | **String**|  | [optional]

### Return type

[**NewProjectDescription**](NewProjectDescription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getExampleNewProjectDescriptionCompatibilityVersion1"></a>
# **getExampleNewProjectDescriptionCompatibilityVersion1**
> NewProjectDescription getExampleNewProjectDescriptionCompatibilityVersion1(projectLocator, id)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String id = "id_example"; // String | 
try {
    NewProjectDescription result = apiInstance.getExampleNewProjectDescriptionCompatibilityVersion1(projectLocator, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getExampleNewProjectDescriptionCompatibilityVersion1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **id** | **String**|  | [optional]

### Return type

[**NewProjectDescription**](NewProjectDescription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter"></a>
# **getParameter**
> Property getParameter(name, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.getParameter(name, projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterType"></a>
# **getParameterType**
> Type getParameterType(name, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    Type result = apiInstance.getParameterType(name, projectLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameterType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

[**Type**](Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterTypeRawValue"></a>
# **getParameterTypeRawValue**
> String getParameterTypeRawValue(name, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    String result = apiInstance.getParameterTypeRawValue(name, projectLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameterTypeRawValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterValueLong"></a>
# **getParameterValueLong**
> String getParameterValueLong(name, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    String result = apiInstance.getParameterValueLong(name, projectLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameterValueLong_0"></a>
# **getParameterValueLong_0**
> String getParameterValueLong_0(name, featureLocator, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    String result = apiInstance.getParameterValueLong_0(name, featureLocator, projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameterValueLong_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameter_0"></a>
# **getParameter_0**
> Property getParameter_0(name, featureLocator, projectLocator, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.getParameter_0(name, featureLocator, projectLocator, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameters"></a>
# **getParameters**
> Properties getParameters(projectLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getParameters(projectLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParameters_0"></a>
# **getParameters_0**
> Properties getParameters_0(featureLocator, projectLocator, locator, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Properties result = apiInstance.getParameters_0(featureLocator, projectLocator, locator, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParentProject"></a>
# **getParentProject**
> Project getParentProject(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.getParentProject(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getParentProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getProjectAgentPools"></a>
# **getProjectAgentPools**
> AgentPools getProjectAgentPools(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentPools result = apiInstance.getProjectAgentPools(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getProjectAgentPools");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AgentPools**](AgentPools.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getProjectsOrder"></a>
# **getProjectsOrder**
> Projects getProjectsOrder(projectLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    Projects result = apiInstance.getProjectsOrder(projectLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getProjectsOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |

### Return type

[**Projects**](Projects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSecureValue"></a>
# **getSecureValue**
> String getSecureValue(projectLocator, token)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String token = "token_example"; // String | 
try {
    String result = apiInstance.getSecureValue(projectLocator, token);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getSecureValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **token** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSettingsFile"></a>
# **getSettingsFile**
> String getSettingsFile(projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
try {
    String result = apiInstance.getSettingsFile(projectLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getSettingsFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSingle"></a>
# **getSingle**
> Object getSingle(featureLocator, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getSingle(featureLocator, projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getSingle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="reloadSettingsFile"></a>
# **reloadSettingsFile**
> Project reloadSettingsFile(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.reloadSettingsFile(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#reloadSettingsFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="removeDefaultTemplate"></a>
# **removeDefaultTemplate**
> removeDefaultTemplate(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.removeDefaultTemplate(projectLocator, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#removeDefaultTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replace"></a>
# **replace**
> Object replace(featureLocator, projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
ProjectFeature body = new ProjectFeature(); // ProjectFeature | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.replace(featureLocator, projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#replace");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**ProjectFeature**](ProjectFeature.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceAll"></a>
# **replaceAll**
> Object replaceAll(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
ProjectFeatures body = new ProjectFeatures(); // ProjectFeatures | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.replaceAll(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#replaceAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**ProjectFeatures**](ProjectFeatures.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildFieldWithProject"></a>
# **serveBuildFieldWithProject**
> String serveBuildFieldWithProject(projectLocator, btLocator, buildLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildFieldWithProject(projectLocator, btLocator, buildLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildFieldWithProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **buildLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildType"></a>
# **serveBuildType**
> BuildType serveBuildType(projectLocator, btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.serveBuildType(projectLocator, btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypeFieldWithProject"></a>
# **serveBuildTypeFieldWithProject**
> String serveBuildTypeFieldWithProject(projectLocator, btLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveBuildTypeFieldWithProject(projectLocator, btLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildTypeFieldWithProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypeTemplates"></a>
# **serveBuildTypeTemplates**
> BuildType serveBuildTypeTemplates(projectLocator, btLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.serveBuildTypeTemplates(projectLocator, btLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildTypeTemplates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildTypesInProject"></a>
# **serveBuildTypesInProject**
> BuildTypes serveBuildTypesInProject(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.serveBuildTypesInProject(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildTypesInProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuildWithProject"></a>
# **serveBuildWithProject**
> Build serveBuildWithProject(projectLocator, btLocator, buildLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String buildLocator = "buildLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.serveBuildWithProject(projectLocator, btLocator, buildLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuildWithProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **buildLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveBuilds"></a>
# **serveBuilds**
> Builds serveBuilds(projectLocator, btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String btLocator = "btLocator_example"; // String | 
String status = "status_example"; // String | 
String triggeredByUser = "triggeredByUser_example"; // String | 
Boolean includePersonal = true; // Boolean | 
Boolean includeCanceled = true; // Boolean | 
Boolean onlyPinned = true; // Boolean | 
java.util.List<String> tag = Arrays.asList("tag_example"); // java.util.List<String> | 
String agentName = "agentName_example"; // String | 
String sinceBuild = "sinceBuild_example"; // String | 
String sinceDate = "sinceDate_example"; // String | 
Long start = 789L; // Long | 
Integer count = 56; // Integer | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.serveBuilds(projectLocator, btLocator, status, triggeredByUser, includePersonal, includeCanceled, onlyPinned, tag, agentName, sinceBuild, sinceDate, start, count, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **btLocator** | **String**|  |
 **status** | **String**|  | [optional]
 **triggeredByUser** | **String**|  | [optional]
 **includePersonal** | **Boolean**|  | [optional]
 **includeCanceled** | **Boolean**|  | [optional]
 **onlyPinned** | **Boolean**|  | [optional]
 **tag** | [**java.util.List&lt;String&gt;**](String.md)|  | [optional]
 **agentName** | **String**|  | [optional]
 **sinceBuild** | **String**|  | [optional]
 **sinceDate** | **String**|  | [optional]
 **start** | **Long**|  | [optional]
 **count** | **Integer**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveProject"></a>
# **serveProject**
> Project serveProject(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.serveProject(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveProjectField"></a>
# **serveProjectField**
> String serveProjectField(projectLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveProjectField(projectLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveProjectField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveProjects"></a>
# **serveProjects**
> Projects serveProjects(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Projects result = apiInstance.serveProjects(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Projects**](Projects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveTemplatesInProject"></a>
# **serveTemplatesInProject**
> BuildTypes serveTemplatesInProject(projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.serveTemplatesInProject(projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#serveTemplatesInProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setBuildTypesOrder"></a>
# **setBuildTypesOrder**
> BuildTypes setBuildTypesOrder(projectLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
BuildTypes body = new BuildTypes(); // BuildTypes | 
try {
    BuildTypes result = apiInstance.setBuildTypesOrder(projectLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setBuildTypesOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |
 **body** | [**BuildTypes**](BuildTypes.md)|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setDefaultTemplate"></a>
# **setDefaultTemplate**
> BuildType setDefaultTemplate(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
BuildType body = new BuildType(); // BuildType | 
String fields = "fields_example"; // String | 
try {
    BuildType result = apiInstance.setDefaultTemplate(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setDefaultTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**BuildType**](BuildType.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**BuildType**](BuildType.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter"></a>
# **setParameter**
> Property setParameter(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterType"></a>
# **setParameterType**
> Type setParameterType(name, projectLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
Type body = new Type(); // Type | 
try {
    Type result = apiInstance.setParameterType(name, projectLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameterType");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**Type**](Type.md)|  | [optional]

### Return type

[**Type**](Type.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterTypeRawValue"></a>
# **setParameterTypeRawValue**
> String setParameterTypeRawValue(name, projectLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setParameterTypeRawValue(name, projectLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameterTypeRawValue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterValueLong"></a>
# **setParameterValueLong**
> String setParameterValueLong(name, projectLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setParameterValueLong(name, projectLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameterValueLong");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameterValueLong_0"></a>
# **setParameterValueLong_0**
> String setParameterValueLong_0(name, featureLocator, projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    String result = apiInstance.setParameterValueLong_0(name, featureLocator, projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameterValueLong_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_0"></a>
# **setParameter_0**
> Property setParameter_0(name, projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_0(name, projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameter_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_1"></a>
# **setParameter_1**
> Property setParameter_1(featureLocator, projectLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_1(featureLocator, projectLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameter_1");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameter_2"></a>
# **setParameter_2**
> Property setParameter_2(name, featureLocator, projectLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
Property body = new Property(); // Property | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Property result = apiInstance.setParameter_2(name, featureLocator, projectLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameter_2");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**Property**](Property.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameters"></a>
# **setParameters**
> Properties setParameters(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.setParameters(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParameters_0"></a>
# **setParameters_0**
> Properties setParameters_0(featureLocator, projectLocator, body, fields, fields2)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String featureLocator = "featureLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
String fields2 = "fields_example"; // String | 
try {
    Properties result = apiInstance.setParameters_0(featureLocator, projectLocator, body, fields, fields2);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParameters_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **featureLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]
 **fields2** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setParentProject"></a>
# **setParentProject**
> Project setParentProject(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
Project body = new Project(); // Project | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.setParentProject(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setParentProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**Project**](Project.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setProjectAgentPools"></a>
# **setProjectAgentPools**
> AgentPools setProjectAgentPools(projectLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
AgentPools body = new AgentPools(); // AgentPools | 
String fields = "fields_example"; // String | 
try {
    AgentPools result = apiInstance.setProjectAgentPools(projectLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setProjectAgentPools");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**AgentPools**](AgentPools.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AgentPools**](AgentPools.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setProjectAgentPools_0"></a>
# **setProjectAgentPools_0**
> AgentPool setProjectAgentPools_0(projectLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
AgentPool body = new AgentPool(); // AgentPool | 
try {
    AgentPool result = apiInstance.setProjectAgentPools_0(projectLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setProjectAgentPools_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **body** | [**AgentPool**](AgentPool.md)|  | [optional]

### Return type

[**AgentPool**](AgentPool.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setProjectField"></a>
# **setProjectField**
> String setProjectField(projectLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setProjectField(projectLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setProjectField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setProjectsOrder"></a>
# **setProjectsOrder**
> Projects setProjectsOrder(projectLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProjectApi;


ProjectApi apiInstance = new ProjectApi();
String projectLocator = "projectLocator_example"; // String | 
String field = "field_example"; // String | 
Projects body = new Projects(); // Projects | 
try {
    Projects result = apiInstance.setProjectsOrder(projectLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#setProjectsOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectLocator** | **String**|  |
 **field** | **String**|  |
 **body** | [**Projects**](Projects.md)|  | [optional]

### Return type

[**Projects**](Projects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

