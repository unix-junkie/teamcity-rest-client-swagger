# VcsRootApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addRoot**](VcsRootApi.md#addRoot) | **POST** /app/rest/vcs-roots | 
[**changeProperties**](VcsRootApi.md#changeProperties) | **PUT** /app/rest/vcs-roots/{vcsRootLocator}/properties | 
[**deleteAllProperties**](VcsRootApi.md#deleteAllProperties) | **DELETE** /app/rest/vcs-roots/{vcsRootLocator}/properties | 
[**deleteParameter**](VcsRootApi.md#deleteParameter) | **DELETE** /app/rest/vcs-roots/{vcsRootLocator}/properties/{name} | 
[**deleteRoot**](VcsRootApi.md#deleteRoot) | **DELETE** /app/rest/vcs-roots/{vcsRootLocator} | 
[**getSettingsFile**](VcsRootApi.md#getSettingsFile) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/settingsFile | 
[**putParameter**](VcsRootApi.md#putParameter) | **PUT** /app/rest/vcs-roots/{vcsRootLocator}/properties/{name} | 
[**serveField**](VcsRootApi.md#serveField) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/{field} | 
[**serveInstanceField**](VcsRootApi.md#serveInstanceField) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/{field} | 
[**serveProperties**](VcsRootApi.md#serveProperties) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/properties | 
[**serveProperty**](VcsRootApi.md#serveProperty) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/properties/{name} | 
[**serveRoot**](VcsRootApi.md#serveRoot) | **GET** /app/rest/vcs-roots/{vcsRootLocator} | 
[**serveRootInstance**](VcsRootApi.md#serveRootInstance) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator} | 
[**serveRootInstanceProperties**](VcsRootApi.md#serveRootInstanceProperties) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/properties | 
[**serveRootInstances**](VcsRootApi.md#serveRootInstances) | **GET** /app/rest/vcs-roots/{vcsRootLocator}/instances | 
[**serveRoots**](VcsRootApi.md#serveRoots) | **GET** /app/rest/vcs-roots | 
[**setField**](VcsRootApi.md#setField) | **PUT** /app/rest/vcs-roots/{vcsRootLocator}/{field} | 
[**setInstanceField**](VcsRootApi.md#setInstanceField) | **PUT** /app/rest/vcs-roots/{vcsRootLocator}/instances/{vcsRootInstanceLocator}/{field} | 


<a name="addRoot"></a>
# **addRoot**
> VcsRoot addRoot(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
VcsRoot body = new VcsRoot(); // VcsRoot | 
String fields = "fields_example"; // String | 
try {
    VcsRoot result = apiInstance.addRoot(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#addRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**VcsRoot**](VcsRoot.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRoot**](VcsRoot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="changeProperties"></a>
# **changeProperties**
> Properties changeProperties(vcsRootLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
Properties body = new Properties(); // Properties | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.changeProperties(vcsRootLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#changeProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **body** | [**Properties**](Properties.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteAllProperties"></a>
# **deleteAllProperties**
> deleteAllProperties(vcsRootLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
try {
    apiInstance.deleteAllProperties(vcsRootLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#deleteAllProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteParameter"></a>
# **deleteParameter**
> deleteParameter(vcsRootLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.deleteParameter(vcsRootLocator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#deleteParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **name** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRoot"></a>
# **deleteRoot**
> deleteRoot(vcsRootLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
try {
    apiInstance.deleteRoot(vcsRootLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#deleteRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSettingsFile"></a>
# **getSettingsFile**
> String getSettingsFile(vcsRootLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
try {
    String result = apiInstance.getSettingsFile(vcsRootLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#getSettingsFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putParameter"></a>
# **putParameter**
> String putParameter(vcsRootLocator, name, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String name = "name_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.putParameter(vcsRootLocator, name, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#putParameter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **name** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveField"></a>
# **serveField**
> String serveField(vcsRootLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveField(vcsRootLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstanceField"></a>
# **serveInstanceField**
> String serveInstanceField(vcsRootLocator, vcsRootInstanceLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveInstanceField(vcsRootLocator, vcsRootInstanceLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveInstanceField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveProperties"></a>
# **serveProperties**
> Properties serveProperties(vcsRootLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveProperties(vcsRootLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveProperty"></a>
# **serveProperty**
> String serveProperty(vcsRootLocator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String name = "name_example"; // String | 
try {
    String result = apiInstance.serveProperty(vcsRootLocator, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveProperty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **name** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRoot"></a>
# **serveRoot**
> VcsRoot serveRoot(vcsRootLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRoot result = apiInstance.serveRoot(vcsRootLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRoot**](VcsRoot.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRootInstance"></a>
# **serveRootInstance**
> VcsRootInstance serveRootInstance(vcsRootLocator, vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstance result = apiInstance.serveRootInstance(vcsRootLocator, vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveRootInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstance**](VcsRootInstance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRootInstanceProperties"></a>
# **serveRootInstanceProperties**
> Properties serveRootInstanceProperties(vcsRootLocator, vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveRootInstanceProperties(vcsRootLocator, vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveRootInstanceProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRootInstances"></a>
# **serveRootInstances**
> VcsRootInstances serveRootInstances(vcsRootLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.serveRootInstances(vcsRootLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveRootInstances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRoots"></a>
# **serveRoots**
> VcsRoots serveRoots(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRoots result = apiInstance.serveRoots(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#serveRoots");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRoots**](VcsRoots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setField"></a>
# **setField**
> String setField(vcsRootLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setField(vcsRootLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#setField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setInstanceField"></a>
# **setInstanceField**
> String setInstanceField(vcsRootLocator, vcsRootInstanceLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootApi;


VcsRootApi apiInstance = new VcsRootApi();
String vcsRootLocator = "vcsRootLocator_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setInstanceField(vcsRootLocator, vcsRootInstanceLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootApi#setInstanceField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootLocator** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

