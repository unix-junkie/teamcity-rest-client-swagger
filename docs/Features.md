
# Features

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**feature** | [**java.util.List&lt;Feature&gt;**](Feature.md) |  |  [optional]



