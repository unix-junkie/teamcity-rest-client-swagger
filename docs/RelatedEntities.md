
# RelatedEntities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**entity** | [**java.util.List&lt;RelatedEntity&gt;**](RelatedEntity.md) |  |  [optional]



