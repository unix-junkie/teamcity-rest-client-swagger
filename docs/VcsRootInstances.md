
# VcsRootInstances

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**vcsRootInstance** | [**java.util.List&lt;VcsRootInstance&gt;**](VcsRootInstance.md) |  |  [optional]



