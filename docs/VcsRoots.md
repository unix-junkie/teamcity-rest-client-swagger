
# VcsRoots

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**href** | **String** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**vcsRoot** | [**java.util.List&lt;VcsRoot&gt;**](VcsRoot.md) |  |  [optional]



