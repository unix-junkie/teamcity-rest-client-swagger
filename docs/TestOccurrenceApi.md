# TestOccurrenceApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTestOccurrences**](TestOccurrenceApi.md#getTestOccurrences) | **GET** /app/rest/testOccurrences | 
[**serveInstance**](TestOccurrenceApi.md#serveInstance) | **GET** /app/rest/testOccurrences/{testLocator} | 


<a name="getTestOccurrences"></a>
# **getTestOccurrences**
> TestOccurrences getTestOccurrences(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.TestOccurrenceApi;


TestOccurrenceApi apiInstance = new TestOccurrenceApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    TestOccurrences result = apiInstance.getTestOccurrences(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TestOccurrenceApi#getTestOccurrences");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**TestOccurrences**](TestOccurrences.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> TestOccurrence serveInstance(testLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.TestOccurrenceApi;


TestOccurrenceApi apiInstance = new TestOccurrenceApi();
String testLocator = "testLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    TestOccurrence result = apiInstance.serveInstance(testLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TestOccurrenceApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **testLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**TestOccurrence**](TestOccurrence.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

