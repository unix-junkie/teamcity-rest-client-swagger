
# AuditEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**href** | **String** |  |  [optional]
**auditEvent** | [**java.util.List&lt;AuditEvent&gt;**](AuditEvent.md) |  |  [optional]



