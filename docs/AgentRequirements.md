
# AgentRequirements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**agentRequirement** | [**java.util.List&lt;AgentRequirement&gt;**](AgentRequirement.md) |  |  [optional]



