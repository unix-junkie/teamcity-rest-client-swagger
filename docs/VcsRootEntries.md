
# VcsRootEntries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**vcsRootEntry** | [**java.util.List&lt;VcsRootEntry&gt;**](VcsRootEntry.md) |  |  [optional]



