
# LicensingData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licenseUseExceeded** | **Boolean** |  |  [optional]
**maxAgents** | **Integer** |  |  [optional]
**unlimitedAgents** | **Boolean** |  |  [optional]
**agentsLeft** | **Integer** |  |  [optional]
**maxBuildTypes** | **Integer** |  |  [optional]
**unlimitedBuildTypes** | **Boolean** |  |  [optional]
**buildTypesLeft** | **Integer** |  |  [optional]
**serverLicenseType** | **String** |  |  [optional]
**serverEffectiveReleaseDate** | **String** |  |  [optional]
**licenseKeys** | [**LicenseKeys**](LicenseKeys.md) |  |  [optional]



