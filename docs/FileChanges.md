
# FileChanges

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**file** | [**java.util.List&lt;FileChange&gt;**](FileChange.md) |  |  [optional]



