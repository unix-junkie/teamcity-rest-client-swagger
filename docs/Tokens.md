
# Tokens

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**token** | [**java.util.List&lt;Token&gt;**](Token.md) |  |  [optional]



