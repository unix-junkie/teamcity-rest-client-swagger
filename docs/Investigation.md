
# Investigation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**href** | **String** |  |  [optional]
**assignee** | [**User**](User.md) |  |  [optional]
**assignment** | [**Comment**](Comment.md) |  |  [optional]
**scope** | [**ProblemScope**](ProblemScope.md) |  |  [optional]
**target** | [**ProblemTarget**](ProblemTarget.md) |  |  [optional]
**resolution** | [**Resolution**](Resolution.md) |  |  [optional]
**responsible** | [**User**](User.md) |  |  [optional]



