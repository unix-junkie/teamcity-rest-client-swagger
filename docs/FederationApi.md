# FederationApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addServer**](FederationApi.md#addServer) | **PUT** /app/rest/federation/servers | 
[**servers**](FederationApi.md#servers) | **GET** /app/rest/federation/servers | 


<a name="addServer"></a>
# **addServer**
> Servers addServer(body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.FederationApi;


FederationApi apiInstance = new FederationApi();
Servers body = new Servers(); // Servers | 
try {
    Servers result = apiInstance.addServer(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FederationApi#addServer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Servers**](Servers.md)|  | [optional]

### Return type

[**Servers**](Servers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="servers"></a>
# **servers**
> Servers servers(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.FederationApi;


FederationApi apiInstance = new FederationApi();
String fields = "fields_example"; // String | 
try {
    Servers result = apiInstance.servers(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FederationApi#servers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Servers**](Servers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

