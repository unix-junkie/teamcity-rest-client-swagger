
# Problems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**nextHref** | **String** |  |  [optional]
**prevHref** | **String** |  |  [optional]
**_default** | **Boolean** |  |  [optional]
**problem** | [**java.util.List&lt;Problem&gt;**](Problem.md) |  |  [optional]



