
# Compatibilities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**compatibility** | [**java.util.List&lt;Compatibility&gt;**](Compatibility.md) |  |  [optional]



