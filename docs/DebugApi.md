# DebugApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteCurrentRememberMe**](DebugApi.md#deleteCurrentRememberMe) | **DELETE** /app/rest/debug/currentRequest/rememberMe | 
[**emptyTask**](DebugApi.md#emptyTask) | **POST** /app/rest/debug/emptyTask | 
[**executeDBQuery**](DebugApi.md#executeDBQuery) | **GET** /app/rest/debug/database/query/{query} | 
[**getBuildChainOptimizationLog**](DebugApi.md#getBuildChainOptimizationLog) | **GET** /app/rest/debug/buildChainOptimizationLog/{buildLocator} | 
[**getCachedBuildPromotions**](DebugApi.md#getCachedBuildPromotions) | **GET** /app/rest/debug/caches/buildPromotions/content | 
[**getCachedBuildPromotionsStats**](DebugApi.md#getCachedBuildPromotionsStats) | **GET** /app/rest/debug/caches/buildPromotions/stats | 
[**getCachedBuildsStat**](DebugApi.md#getCachedBuildsStat) | **GET** /app/rest/debug/caches/builds/stats | 
[**getCurrentSession**](DebugApi.md#getCurrentSession) | **GET** /app/rest/debug/currentRequest/session | 
[**getCurrentSessionMaxInactiveInterval**](DebugApi.md#getCurrentSessionMaxInactiveInterval) | **GET** /app/rest/debug/currentRequest/session/maxInactiveSeconds | 
[**getCurrentUserPermissions**](DebugApi.md#getCurrentUserPermissions) | **GET** /app/rest/debug/currentUserPermissions | 
[**getDate**](DebugApi.md#getDate) | **GET** /app/rest/debug/date/{dateLocator} | 
[**getDiagnosticsPerfStats**](DebugApi.md#getDiagnosticsPerfStats) | **GET** /app/rest/debug/diagnostics/threadPerfStat/stats | 
[**getEnvironmentVariables**](DebugApi.md#getEnvironmentVariables) | **GET** /app/rest/debug/jvm/environmentVariables | 
[**getHashed**](DebugApi.md#getHashed) | **GET** /app/rest/debug/values/transform/{method} | 
[**getIpAddress**](DebugApi.md#getIpAddress) | **GET** /app/rest/debug/dns/lookup/{host} | 
[**getRawInvestigations**](DebugApi.md#getRawInvestigations) | **GET** /app/rest/debug/investigations | 
[**getRequestDetails**](DebugApi.md#getRequestDetails) | **GET** /app/rest/debug/currentRequest/details{extra} | 
[**getScrambled**](DebugApi.md#getScrambled) | **GET** /app/rest/debug/values/password/scrambled | 
[**getSessions**](DebugApi.md#getSessions) | **GET** /app/rest/debug/sessions | 
[**getSystemProperties**](DebugApi.md#getSystemProperties) | **GET** /app/rest/debug/jvm/systemProperties | 
[**getThreadDump**](DebugApi.md#getThreadDump) | **GET** /app/rest/debug/threadDump | 
[**getThreadInterrupted**](DebugApi.md#getThreadInterrupted) | **GET** /app/rest/debug/threads/{threadLocator}/interrupted | 
[**getUnscrambled**](DebugApi.md#getUnscrambled) | **GET** /app/rest/debug/values/password/unscrambled | 
[**interruptThread**](DebugApi.md#interruptThread) | **PUT** /app/rest/debug/threads/{threadLocator}/interrupted | 
[**invalidateCurrentSession**](DebugApi.md#invalidateCurrentSession) | **DELETE** /app/rest/debug/currentRequest/session | 
[**listDBTables**](DebugApi.md#listDBTables) | **GET** /app/rest/debug/database/tables | 
[**newRememberMe**](DebugApi.md#newRememberMe) | **POST** /app/rest/debug/currentRequest/rememberMe | 
[**postRequestDetails**](DebugApi.md#postRequestDetails) | **POST** /app/rest/debug/currentRequest/details{extra} | 
[**putRequestDetails**](DebugApi.md#putRequestDetails) | **PUT** /app/rest/debug/currentRequest/details{extra} | 
[**requestFinalization**](DebugApi.md#requestFinalization) | **POST** /app/rest/debug/jvm/finalization | 
[**requestGc**](DebugApi.md#requestGc) | **POST** /app/rest/debug/jvm/gc | 
[**saveMemoryDump**](DebugApi.md#saveMemoryDump) | **POST** /app/rest/debug/memory/dumps | 
[**scheduleCheckingForChanges**](DebugApi.md#scheduleCheckingForChanges) | **POST** /app/rest/debug/vcsCheckingForChangesQueue | 
[**setCurrentSessionMaxInactiveInterval**](DebugApi.md#setCurrentSessionMaxInactiveInterval) | **PUT** /app/rest/debug/currentRequest/session/maxInactiveSeconds | 


<a name="deleteCurrentRememberMe"></a>
# **deleteCurrentRememberMe**
> deleteCurrentRememberMe()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    apiInstance.deleteCurrentRememberMe();
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#deleteCurrentRememberMe");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="emptyTask"></a>
# **emptyTask**
> String emptyTask(time, load, memory, memoryChunks)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String time = "time_example"; // String | 
Integer load = 56; // Integer | 
Integer memory = 56; // Integer | 
Integer memoryChunks = 1; // Integer | 
try {
    String result = apiInstance.emptyTask(time, load, memory, memoryChunks);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#emptyTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **time** | **String**|  | [optional]
 **load** | **Integer**|  | [optional]
 **memory** | **Integer**|  | [optional]
 **memoryChunks** | **Integer**|  | [optional] [default to 1]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="executeDBQuery"></a>
# **executeDBQuery**
> String executeDBQuery(query, fieldDelimiter, dataRetrieveQuery, count)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String query = "query_example"; // String | 
String fieldDelimiter = ", "; // String | 
String dataRetrieveQuery = "dataRetrieveQuery_example"; // String | 
Integer count = 1000; // Integer | 
try {
    String result = apiInstance.executeDBQuery(query, fieldDelimiter, dataRetrieveQuery, count);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#executeDBQuery");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **query** | **String**|  |
 **fieldDelimiter** | **String**|  | [optional] [default to , ]
 **dataRetrieveQuery** | **String**|  | [optional]
 **count** | **Integer**|  | [optional] [default to 1000]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getBuildChainOptimizationLog"></a>
# **getBuildChainOptimizationLog**
> String getBuildChainOptimizationLog(buildLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String buildLocator = "buildLocator_example"; // String | 
try {
    String result = apiInstance.getBuildChainOptimizationLog(buildLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getBuildChainOptimizationLog");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCachedBuildPromotions"></a>
# **getCachedBuildPromotions**
> Builds getCachedBuildPromotions(buildTypeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String buildTypeLocator = "buildTypeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.getCachedBuildPromotions(buildTypeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCachedBuildPromotions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildTypeLocator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCachedBuildPromotionsStats"></a>
# **getCachedBuildPromotionsStats**
> Properties getCachedBuildPromotionsStats(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getCachedBuildPromotionsStats(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCachedBuildPromotionsStats");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCachedBuildsStat"></a>
# **getCachedBuildsStat**
> Properties getCachedBuildsStat(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getCachedBuildsStat(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCachedBuildsStat");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCurrentSession"></a>
# **getCurrentSession**
> Session getCurrentSession(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Session result = apiInstance.getCurrentSession(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCurrentSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Session**](Session.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCurrentSessionMaxInactiveInterval"></a>
# **getCurrentSessionMaxInactiveInterval**
> String getCurrentSessionMaxInactiveInterval()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    String result = apiInstance.getCurrentSessionMaxInactiveInterval();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCurrentSessionMaxInactiveInterval");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCurrentUserPermissions"></a>
# **getCurrentUserPermissions**
> String getCurrentUserPermissions()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    String result = apiInstance.getCurrentUserPermissions();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getCurrentUserPermissions");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getDate"></a>
# **getDate**
> String getDate(dateLocator, format, timezone)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String dateLocator = "dateLocator_example"; // String | 
String format = "format_example"; // String | 
String timezone = "timezone_example"; // String | 
try {
    String result = apiInstance.getDate(dateLocator, format, timezone);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getDate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dateLocator** | **String**|  |
 **format** | **String**|  | [optional]
 **timezone** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getDiagnosticsPerfStats"></a>
# **getDiagnosticsPerfStats**
> Properties getDiagnosticsPerfStats(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getDiagnosticsPerfStats(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getDiagnosticsPerfStats");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getEnvironmentVariables"></a>
# **getEnvironmentVariables**
> Properties getEnvironmentVariables(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getEnvironmentVariables(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getEnvironmentVariables");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getHashed"></a>
# **getHashed**
> String getHashed(method, value)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String method = "method_example"; // String | 
String value = "value_example"; // String | 
try {
    String result = apiInstance.getHashed(method, value);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getHashed");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **method** | **String**|  |
 **value** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getIpAddress"></a>
# **getIpAddress**
> Items getIpAddress(host)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String host = "host_example"; // String | 
try {
    Items result = apiInstance.getIpAddress(host);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getIpAddress");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **host** | **String**|  |

### Return type

[**Items**](Items.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRawInvestigations"></a>
# **getRawInvestigations**
> Investigations getRawInvestigations(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Investigations result = apiInstance.getRawInvestigations(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getRawInvestigations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Investigations**](Investigations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRequestDetails"></a>
# **getRequestDetails**
> String getRequestDetails(extra)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String extra = "extra_example"; // String | 
try {
    String result = apiInstance.getRequestDetails(extra);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getRequestDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **extra** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getScrambled"></a>
# **getScrambled**
> String getScrambled(value)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String value = "value_example"; // String | 
try {
    String result = apiInstance.getScrambled(value);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getScrambled");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSessions"></a>
# **getSessions**
> Sessions getSessions(manager, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
Long manager = 789L; // Long | 
String fields = "fields_example"; // String | 
try {
    Sessions result = apiInstance.getSessions(manager, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getSessions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **manager** | **Long**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Sessions**](Sessions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getSystemProperties"></a>
# **getSystemProperties**
> Properties getSystemProperties(fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.getSystemProperties(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getSystemProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getThreadDump"></a>
# **getThreadDump**
> String getThreadDump(lockedMonitors, lockedSynchronizers, detectLocks)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String lockedMonitors = "lockedMonitors_example"; // String | 
String lockedSynchronizers = "lockedSynchronizers_example"; // String | 
String detectLocks = "detectLocks_example"; // String | 
try {
    String result = apiInstance.getThreadDump(lockedMonitors, lockedSynchronizers, detectLocks);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getThreadDump");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lockedMonitors** | **String**|  | [optional]
 **lockedSynchronizers** | **String**|  | [optional]
 **detectLocks** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getThreadInterrupted"></a>
# **getThreadInterrupted**
> String getThreadInterrupted(threadLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String threadLocator = "threadLocator_example"; // String | 
try {
    String result = apiInstance.getThreadInterrupted(threadLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getThreadInterrupted");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **threadLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getUnscrambled"></a>
# **getUnscrambled**
> String getUnscrambled(value)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String value = "value_example"; // String | 
try {
    String result = apiInstance.getUnscrambled(value);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#getUnscrambled");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **value** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="interruptThread"></a>
# **interruptThread**
> String interruptThread(threadLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String threadLocator = "threadLocator_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.interruptThread(threadLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#interruptThread");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **threadLocator** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="invalidateCurrentSession"></a>
# **invalidateCurrentSession**
> invalidateCurrentSession()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    apiInstance.invalidateCurrentSession();
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#invalidateCurrentSession");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="listDBTables"></a>
# **listDBTables**
> String listDBTables()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    String result = apiInstance.listDBTables();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#listDBTables");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="newRememberMe"></a>
# **newRememberMe**
> String newRememberMe()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    String result = apiInstance.newRememberMe();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#newRememberMe");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="postRequestDetails"></a>
# **postRequestDetails**
> String postRequestDetails(extra)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String extra = "extra_example"; // String | 
try {
    String result = apiInstance.postRequestDetails(extra);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#postRequestDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **extra** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putRequestDetails"></a>
# **putRequestDetails**
> String putRequestDetails(extra)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String extra = "extra_example"; // String | 
try {
    String result = apiInstance.putRequestDetails(extra);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#putRequestDetails");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **extra** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="requestFinalization"></a>
# **requestFinalization**
> requestFinalization()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    apiInstance.requestFinalization();
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#requestFinalization");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="requestGc"></a>
# **requestGc**
> requestGc()



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
try {
    apiInstance.requestGc();
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#requestGc");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="saveMemoryDump"></a>
# **saveMemoryDump**
> String saveMemoryDump(archived)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
Boolean archived = true; // Boolean | 
try {
    String result = apiInstance.saveMemoryDump(archived);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#saveMemoryDump");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **archived** | **Boolean**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="scheduleCheckingForChanges"></a>
# **scheduleCheckingForChanges**
> VcsRootInstances scheduleCheckingForChanges(locator, requestor, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String locator = "locator_example"; // String | 
String requestor = "requestor_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.scheduleCheckingForChanges(locator, requestor, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#scheduleCheckingForChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **requestor** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setCurrentSessionMaxInactiveInterval"></a>
# **setCurrentSessionMaxInactiveInterval**
> String setCurrentSessionMaxInactiveInterval(body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.DebugApi;


DebugApi apiInstance = new DebugApi();
String body = "body_example"; // String | 
try {
    String result = apiInstance.setCurrentSessionMaxInactiveInterval(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DebugApi#setCurrentSessionMaxInactiveInterval");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

