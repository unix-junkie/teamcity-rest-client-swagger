
# IssuesUsages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **String** |  |  [optional]
**issueUsage** | [**java.util.List&lt;IssueUsage&gt;**](IssueUsage.md) |  |  [optional]
**count** | **Integer** |  |  [optional]



