
# Tags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**tag** | [**java.util.List&lt;Tag&gt;**](Tag.md) |  |  [optional]



