
# BranchVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  |  [optional]
**unspecified** | **Boolean** |  |  [optional]
**lastActivity** | **String** |  |  [optional]
**groupFlag** | **Boolean** |  |  [optional]
**builds** | [**Builds**](Builds.md) |  |  [optional]
**internalName** | **String** |  |  [optional]
**active** | **Boolean** |  |  [optional]
**name** | **String** |  |  [optional]
**_default** | **Boolean** |  |  [optional]



