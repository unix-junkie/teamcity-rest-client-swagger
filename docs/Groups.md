
# Groups

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**group** | [**java.util.List&lt;Group&gt;**](Group.md) |  |  [optional]



