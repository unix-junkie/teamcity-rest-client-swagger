
# TestRunMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**typedValues** | [**java.util.List&lt;TypedValue&gt;**](TypedValue.md) |  |  [optional]
**_default** | **Boolean** |  |  [optional]



