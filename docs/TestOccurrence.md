
# TestOccurrence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**ignored** | **Boolean** |  |  [optional]
**duration** | **Integer** |  |  [optional]
**runOrder** | **String** |  |  [optional]
**muted** | **Boolean** |  |  [optional]
**currentlyMuted** | **Boolean** |  |  [optional]
**currentlyInvestigated** | **Boolean** |  |  [optional]
**href** | **String** |  |  [optional]
**ignoreDetails** | **String** |  |  [optional]
**details** | **String** |  |  [optional]
**test** | [**Test**](Test.md) |  |  [optional]
**mute** | [**Mute**](Mute.md) |  |  [optional]
**build** | [**Build**](Build.md) |  |  [optional]
**firstFailed** | [**TestOccurrence**](TestOccurrence.md) |  |  [optional]
**nextFixed** | [**TestOccurrence**](TestOccurrence.md) |  |  [optional]
**invocations** | [**TestOccurrences**](TestOccurrences.md) |  |  [optional]
**metadata** | [**TestRunMetadata**](TestRunMetadata.md) |  |  [optional]
**logAnchor** | **String** |  |  [optional]



