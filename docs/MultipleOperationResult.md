
# MultipleOperationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**errorCount** | **Integer** |  |  [optional]
**operationResult** | [**java.util.List&lt;OperationResult&gt;**](OperationResult.md) |  |  [optional]



