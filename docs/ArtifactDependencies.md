
# ArtifactDependencies

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**artifactDependency** | [**java.util.List&lt;ArtifactDependency&gt;**](ArtifactDependency.md) |  |  [optional]
**replace** | **String** |  |  [optional]



