
# MetricTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**metricTag** | [**java.util.List&lt;MetricTag&gt;**](MetricTag.md) |  |  [optional]



