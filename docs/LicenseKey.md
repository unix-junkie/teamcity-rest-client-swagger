
# LicenseKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valid** | **Boolean** |  |  [optional]
**active** | **Boolean** |  |  [optional]
**expired** | **Boolean** |  |  [optional]
**obsolete** | **Boolean** |  |  [optional]
**expirationDate** | **String** |  |  [optional]
**maintenanceEndDate** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**servers** | **Integer** |  |  [optional]
**agents** | **Integer** |  |  [optional]
**unlimitedAgents** | **Boolean** |  |  [optional]
**buildTypes** | **Integer** |  |  [optional]
**unlimitedBuildTypes** | **Boolean** |  |  [optional]
**errorDetails** | **String** |  |  [optional]
**key** | **String** |  |  [optional]
**rawType** | **String** |  |  [optional]



