# AgentPoolApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAgent**](AgentPoolApi.md#addAgent) | **POST** /app/rest/agentPools/{agentPoolLocator}/agents | 
[**addProject**](AgentPoolApi.md#addProject) | **POST** /app/rest/agentPools/{agentPoolLocator}/projects | 
[**createPool**](AgentPoolApi.md#createPool) | **POST** /app/rest/agentPools | 
[**deletePool**](AgentPoolApi.md#deletePool) | **DELETE** /app/rest/agentPools/{agentPoolLocator} | 
[**deletePoolProject**](AgentPoolApi.md#deletePoolProject) | **DELETE** /app/rest/agentPools/{agentPoolLocator}/projects/{projectLocator} | 
[**deleteProjects**](AgentPoolApi.md#deleteProjects) | **DELETE** /app/rest/agentPools/{agentPoolLocator}/projects | 
[**getField**](AgentPoolApi.md#getField) | **GET** /app/rest/agentPools/{agentPoolLocator}/{field} | 
[**getPool**](AgentPoolApi.md#getPool) | **GET** /app/rest/agentPools/{agentPoolLocator} | 
[**getPoolAgents**](AgentPoolApi.md#getPoolAgents) | **GET** /app/rest/agentPools/{agentPoolLocator}/agents | 
[**getPoolProject**](AgentPoolApi.md#getPoolProject) | **GET** /app/rest/agentPools/{agentPoolLocator}/projects/{projectLocator} | 
[**getPoolProjects**](AgentPoolApi.md#getPoolProjects) | **GET** /app/rest/agentPools/{agentPoolLocator}/projects | 
[**getPools**](AgentPoolApi.md#getPools) | **GET** /app/rest/agentPools | 
[**replaceProjects**](AgentPoolApi.md#replaceProjects) | **PUT** /app/rest/agentPools/{agentPoolLocator}/projects | 
[**setField**](AgentPoolApi.md#setField) | **PUT** /app/rest/agentPools/{agentPoolLocator}/{field} | 


<a name="addAgent"></a>
# **addAgent**
> Agent addAgent(agentPoolLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
Agent body = new Agent(); // Agent | 
String fields = "fields_example"; // String | 
try {
    Agent result = apiInstance.addAgent(agentPoolLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#addAgent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **body** | [**Agent**](Agent.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Agent**](Agent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="addProject"></a>
# **addProject**
> Project addProject(agentPoolLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
Project body = new Project(); // Project | 
try {
    Project result = apiInstance.addProject(agentPoolLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#addProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **body** | [**Project**](Project.md)|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createPool"></a>
# **createPool**
> AgentPool createPool(body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
AgentPool body = new AgentPool(); // AgentPool | 
try {
    AgentPool result = apiInstance.createPool(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#createPool");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AgentPool**](AgentPool.md)|  | [optional]

### Return type

[**AgentPool**](AgentPool.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deletePool"></a>
# **deletePool**
> deletePool(agentPoolLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
try {
    apiInstance.deletePool(agentPoolLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#deletePool");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deletePoolProject"></a>
# **deletePoolProject**
> deletePoolProject(agentPoolLocator, projectLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
try {
    apiInstance.deletePoolProject(agentPoolLocator, projectLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#deletePoolProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **projectLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteProjects"></a>
# **deleteProjects**
> deleteProjects(agentPoolLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
try {
    apiInstance.deleteProjects(agentPoolLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#deleteProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getField"></a>
# **getField**
> String getField(agentPoolLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.getField(agentPoolLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPool"></a>
# **getPool**
> AgentPool getPool(agentPoolLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentPool result = apiInstance.getPool(agentPoolLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getPool");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AgentPool**](AgentPool.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPoolAgents"></a>
# **getPoolAgents**
> Agents getPoolAgents(agentPoolLocator, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Agents result = apiInstance.getPoolAgents(agentPoolLocator, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getPoolAgents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Agents**](Agents.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPoolProject"></a>
# **getPoolProject**
> Project getPoolProject(agentPoolLocator, projectLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String projectLocator = "projectLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.getPoolProject(agentPoolLocator, projectLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getPoolProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **projectLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPoolProjects"></a>
# **getPoolProjects**
> Projects getPoolProjects(agentPoolLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Projects result = apiInstance.getPoolProjects(agentPoolLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getPoolProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Projects**](Projects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getPools"></a>
# **getPools**
> AgentPools getPools(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentPools result = apiInstance.getPools(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#getPools");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AgentPools**](AgentPools.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceProjects"></a>
# **replaceProjects**
> Projects replaceProjects(agentPoolLocator, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
Projects body = new Projects(); // Projects | 
try {
    Projects result = apiInstance.replaceProjects(agentPoolLocator, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#replaceProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **body** | [**Projects**](Projects.md)|  | [optional]

### Return type

[**Projects**](Projects.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setField"></a>
# **setField**
> String setField(agentPoolLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentPoolApi;


AgentPoolApi apiInstance = new AgentPoolApi();
String agentPoolLocator = "agentPoolLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setField(agentPoolLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentPoolApi#setField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentPoolLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

