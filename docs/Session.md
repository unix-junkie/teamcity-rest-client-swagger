
# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**creationDate** | **String** |  |  [optional]
**lastAccessedDate** | **String** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]



