
# SnapshotDependencies

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  |  [optional]
**snapshotDependency** | [**java.util.List&lt;SnapshotDependency&gt;**](SnapshotDependency.md) |  |  [optional]



