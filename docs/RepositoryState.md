
# RepositoryState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **String** |  |  [optional]
**count** | **Integer** |  |  [optional]
**branch** | [**java.util.List&lt;BranchVersion&gt;**](BranchVersion.md) |  |  [optional]



