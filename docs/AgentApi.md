# AgentApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAgent**](AgentApi.md#deleteAgent) | **DELETE** /app/rest/agents/{agentLocator} | 
[**geIncompatibleBuildTypes**](AgentApi.md#geIncompatibleBuildTypes) | **GET** /app/rest/agents/{agentLocator}/incompatibleBuildTypes | 
[**getAgentPool**](AgentApi.md#getAgentPool) | **GET** /app/rest/agents/{agentLocator}/pool | 
[**getAllowedRunConfigurations**](AgentApi.md#getAllowedRunConfigurations) | **GET** /app/rest/agents/{agentLocator}/compatibilityPolicy | 
[**getAuthorizedInfo**](AgentApi.md#getAuthorizedInfo) | **GET** /app/rest/agents/{agentLocator}/authorizedInfo | 
[**getCompatibleBuildTypes**](AgentApi.md#getCompatibleBuildTypes) | **GET** /app/rest/agents/{agentLocator}/compatibleBuildTypes | 
[**getEnabledInfo**](AgentApi.md#getEnabledInfo) | **GET** /app/rest/agents/{agentLocator}/enabledInfo | 
[**serveAgent**](AgentApi.md#serveAgent) | **GET** /app/rest/agents/{agentLocator} | 
[**serveAgentField**](AgentApi.md#serveAgentField) | **GET** /app/rest/agents/{agentLocator}/{field} | 
[**serveAgents**](AgentApi.md#serveAgents) | **GET** /app/rest/agents | 
[**setAgentField**](AgentApi.md#setAgentField) | **PUT** /app/rest/agents/{agentLocator}/{field} | 
[**setAgentPool**](AgentApi.md#setAgentPool) | **PUT** /app/rest/agents/{agentLocator}/pool | 
[**setAllowedRunConfigurations**](AgentApi.md#setAllowedRunConfigurations) | **PUT** /app/rest/agents/{agentLocator}/compatibilityPolicy | 
[**setAuthorizedInfo**](AgentApi.md#setAuthorizedInfo) | **PUT** /app/rest/agents/{agentLocator}/authorizedInfo | 
[**setEnabledInfo**](AgentApi.md#setEnabledInfo) | **PUT** /app/rest/agents/{agentLocator}/enabledInfo | 


<a name="deleteAgent"></a>
# **deleteAgent**
> deleteAgent(agentLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
try {
    apiInstance.deleteAgent(agentLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#deleteAgent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="geIncompatibleBuildTypes"></a>
# **geIncompatibleBuildTypes**
> Compatibilities geIncompatibleBuildTypes(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Compatibilities result = apiInstance.geIncompatibleBuildTypes(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#geIncompatibleBuildTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Compatibilities**](Compatibilities.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAgentPool"></a>
# **getAgentPool**
> AgentPool getAgentPool(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AgentPool result = apiInstance.getAgentPool(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#getAgentPool");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AgentPool**](AgentPool.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAllowedRunConfigurations"></a>
# **getAllowedRunConfigurations**
> CompatibilityPolicy getAllowedRunConfigurations(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    CompatibilityPolicy result = apiInstance.getAllowedRunConfigurations(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#getAllowedRunConfigurations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**CompatibilityPolicy**](CompatibilityPolicy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getAuthorizedInfo"></a>
# **getAuthorizedInfo**
> AuthorizedInfo getAuthorizedInfo(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    AuthorizedInfo result = apiInstance.getAuthorizedInfo(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#getAuthorizedInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**AuthorizedInfo**](AuthorizedInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCompatibleBuildTypes"></a>
# **getCompatibleBuildTypes**
> BuildTypes getCompatibleBuildTypes(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.getCompatibleBuildTypes(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#getCompatibleBuildTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getEnabledInfo"></a>
# **getEnabledInfo**
> EnabledInfo getEnabledInfo(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    EnabledInfo result = apiInstance.getEnabledInfo(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#getEnabledInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**EnabledInfo**](EnabledInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAgent"></a>
# **serveAgent**
> Agent serveAgent(agentLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Agent result = apiInstance.serveAgent(agentLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#serveAgent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Agent**](Agent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAgentField"></a>
# **serveAgentField**
> String serveAgentField(agentLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveAgentField(agentLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#serveAgentField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveAgents"></a>
# **serveAgents**
> Agents serveAgents(includeDisconnected, includeUnauthorized, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
Boolean includeDisconnected = true; // Boolean | 
Boolean includeUnauthorized = true; // Boolean | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Agents result = apiInstance.serveAgents(includeDisconnected, includeUnauthorized, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#serveAgents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **includeDisconnected** | **Boolean**|  | [optional]
 **includeUnauthorized** | **Boolean**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Agents**](Agents.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setAgentField"></a>
# **setAgentField**
> String setAgentField(agentLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setAgentField(agentLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#setAgentField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setAgentPool"></a>
# **setAgentPool**
> AgentPool setAgentPool(agentLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
AgentPool body = new AgentPool(); // AgentPool | 
String fields = "fields_example"; // String | 
try {
    AgentPool result = apiInstance.setAgentPool(agentLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#setAgentPool");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **body** | [**AgentPool**](AgentPool.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AgentPool**](AgentPool.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setAllowedRunConfigurations"></a>
# **setAllowedRunConfigurations**
> CompatibilityPolicy setAllowedRunConfigurations(agentLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
CompatibilityPolicy body = new CompatibilityPolicy(); // CompatibilityPolicy | 
String fields = "fields_example"; // String | 
try {
    CompatibilityPolicy result = apiInstance.setAllowedRunConfigurations(agentLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#setAllowedRunConfigurations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **body** | [**CompatibilityPolicy**](CompatibilityPolicy.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**CompatibilityPolicy**](CompatibilityPolicy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setAuthorizedInfo"></a>
# **setAuthorizedInfo**
> AuthorizedInfo setAuthorizedInfo(agentLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
AuthorizedInfo body = new AuthorizedInfo(); // AuthorizedInfo | 
String fields = "fields_example"; // String | 
try {
    AuthorizedInfo result = apiInstance.setAuthorizedInfo(agentLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#setAuthorizedInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **body** | [**AuthorizedInfo**](AuthorizedInfo.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AuthorizedInfo**](AuthorizedInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setEnabledInfo"></a>
# **setEnabledInfo**
> EnabledInfo setEnabledInfo(agentLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.AgentApi;


AgentApi apiInstance = new AgentApi();
String agentLocator = "agentLocator_example"; // String | 
EnabledInfo body = new EnabledInfo(); // EnabledInfo | 
String fields = "fields_example"; // String | 
try {
    EnabledInfo result = apiInstance.setEnabledInfo(agentLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AgentApi#setEnabledInfo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentLocator** | **String**|  |
 **body** | [**EnabledInfo**](EnabledInfo.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**EnabledInfo**](EnabledInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

