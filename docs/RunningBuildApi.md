# RunningBuildApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addLogMessage**](RunningBuildApi.md#addLogMessage) | **POST** /app/rest/runningBuilds/{buildLocator}/log | Adds a message to the build log. Service messages are accepted.
[**markBuildAsRunning**](RunningBuildApi.md#markBuildAsRunning) | **PUT** /app/rest/runningBuilds/{buildLocator}/runningData | Marks the previously queued (agent-less) build which matches buildLocator as running and returns this build.
[**setFinishedTime**](RunningBuildApi.md#setFinishedTime) | **PUT** /app/rest/runningBuilds/{buildLocator}/finishDate | Marks the running agent-less build as finished. An empty finish date means \&quot;now\&quot;.


<a name="addLogMessage"></a>
# **addLogMessage**
> addLogMessage(buildLocator, body, fields)

Adds a message to the build log. Service messages are accepted.



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.RunningBuildApi;


RunningBuildApi apiInstance = new RunningBuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.addLogMessage(buildLocator, body, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling RunningBuildApi#addLogMessage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: Not defined

<a name="markBuildAsRunning"></a>
# **markBuildAsRunning**
> Build markBuildAsRunning(buildLocator, body, fields)

Marks the previously queued (agent-less) build which matches buildLocator as running and returns this build.



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.RunningBuildApi;


RunningBuildApi apiInstance = new RunningBuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Build result = apiInstance.markBuildAsRunning(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RunningBuildApi#markBuildAsRunning");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Build**](Build.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: application/xml, application/json

<a name="setFinishedTime"></a>
# **setFinishedTime**
> String setFinishedTime(buildLocator, body, fields)

Marks the running agent-less build as finished. An empty finish date means \&quot;now\&quot;.



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.RunningBuildApi;


RunningBuildApi apiInstance = new RunningBuildApi();
String buildLocator = "buildLocator_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    String result = apiInstance.setFinishedTime(buildLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RunningBuildApi#setFinishedTime");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **buildLocator** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: text/plain
 - **Accept**: text/plain

