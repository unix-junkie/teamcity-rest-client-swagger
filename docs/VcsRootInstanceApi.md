# VcsRootInstanceApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteInstanceField**](VcsRootInstanceApi.md#deleteInstanceField) | **DELETE** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field} | 
[**deleteRepositoryState**](VcsRootInstanceApi.md#deleteRepositoryState) | **DELETE** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState | 
[**getChildren**](VcsRootInstanceApi.md#getChildren) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/children{path} | 
[**getChildrenAlias**](VcsRootInstanceApi.md#getChildrenAlias) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/{path} | 
[**getContent**](VcsRootInstanceApi.md#getContent) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/content{path} | 
[**getContentAlias**](VcsRootInstanceApi.md#getContentAlias) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/files{path} | 
[**getMetadata**](VcsRootInstanceApi.md#getMetadata) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/metadata{path} | 
[**getRepositoryState**](VcsRootInstanceApi.md#getRepositoryState) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState | 
[**getRepositoryStateCreationDate**](VcsRootInstanceApi.md#getRepositoryStateCreationDate) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState/creationDate | 
[**getRoot**](VcsRootInstanceApi.md#getRoot) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest | 
[**getZipped**](VcsRootInstanceApi.md#getZipped) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/files/latest/archived{path} | 
[**scheduleCheckingForChanges**](VcsRootInstanceApi.md#scheduleCheckingForChanges) | **POST** /app/rest/vcs-root-instances/checkingForChangesQueue | 
[**scheduleCheckingForChanges_0**](VcsRootInstanceApi.md#scheduleCheckingForChanges_0) | **POST** /app/rest/vcs-root-instances/commitHookNotification | 
[**serveInstance**](VcsRootInstanceApi.md#serveInstance) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator} | 
[**serveInstanceField**](VcsRootInstanceApi.md#serveInstanceField) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field} | 
[**serveInstances**](VcsRootInstanceApi.md#serveInstances) | **GET** /app/rest/vcs-root-instances | 
[**serveRootInstanceProperties**](VcsRootInstanceApi.md#serveRootInstanceProperties) | **GET** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/properties | 
[**setInstanceField**](VcsRootInstanceApi.md#setInstanceField) | **PUT** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/{field} | 
[**setRepositoryState**](VcsRootInstanceApi.md#setRepositoryState) | **PUT** /app/rest/vcs-root-instances/{vcsRootInstanceLocator}/repositoryState | 


<a name="deleteInstanceField"></a>
# **deleteInstanceField**
> deleteInstanceField(vcsRootInstanceLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    apiInstance.deleteInstanceField(vcsRootInstanceLocator, field);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#deleteInstanceField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **field** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteRepositoryState"></a>
# **deleteRepositoryState**
> deleteRepositoryState(vcsRootInstanceLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
try {
    apiInstance.deleteRepositoryState(vcsRootInstanceLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#deleteRepositoryState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildren"></a>
# **getChildren**
> Files getChildren(path, vcsRootInstanceLocator, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getChildren(path, vcsRootInstanceLocator, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getChildren");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChildrenAlias"></a>
# **getChildrenAlias**
> Files getChildrenAlias(path, vcsRootInstanceLocator, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getChildrenAlias(path, vcsRootInstanceLocator, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getChildrenAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContent"></a>
# **getContent**
> getContent(path, vcsRootInstanceLocator, responseBuilder)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String responseBuilder = "responseBuilder_example"; // String | 
try {
    apiInstance.getContent(path, vcsRootInstanceLocator, responseBuilder);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getContent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **responseBuilder** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getContentAlias"></a>
# **getContentAlias**
> getContentAlias(path, vcsRootInstanceLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
try {
    apiInstance.getContentAlias(path, vcsRootInstanceLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getContentAlias");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getMetadata"></a>
# **getMetadata**
> File getMetadata(path, vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    File result = apiInstance.getMetadata(path, vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getMetadata");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRepositoryState"></a>
# **getRepositoryState**
> Entries getRepositoryState(vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Entries result = apiInstance.getRepositoryState(vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getRepositoryState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Entries**](Entries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRepositoryStateCreationDate"></a>
# **getRepositoryStateCreationDate**
> String getRepositoryStateCreationDate(vcsRootInstanceLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
try {
    String result = apiInstance.getRepositoryStateCreationDate(vcsRootInstanceLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getRepositoryStateCreationDate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRoot"></a>
# **getRoot**
> Files getRoot(vcsRootInstanceLocator, basePath, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Files result = apiInstance.getRoot(vcsRootInstanceLocator, basePath, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Files**](Files.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getZipped"></a>
# **getZipped**
> getZipped(path, vcsRootInstanceLocator, basePath, locator, name)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String path = "path_example"; // String | 
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String basePath = "basePath_example"; // String | 
String locator = "locator_example"; // String | 
String name = "name_example"; // String | 
try {
    apiInstance.getZipped(path, vcsRootInstanceLocator, basePath, locator, name);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#getZipped");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **path** | **String**|  |
 **vcsRootInstanceLocator** | **String**|  |
 **basePath** | **String**|  | [optional]
 **locator** | **String**|  | [optional]
 **name** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="scheduleCheckingForChanges"></a>
# **scheduleCheckingForChanges**
> VcsRootInstances scheduleCheckingForChanges(locator, requestor, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String locator = "locator_example"; // String | 
String requestor = "requestor_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.scheduleCheckingForChanges(locator, requestor, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#scheduleCheckingForChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **requestor** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="scheduleCheckingForChanges_0"></a>
# **scheduleCheckingForChanges_0**
> scheduleCheckingForChanges_0(locator, okOnNothingFound)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String locator = "locator_example"; // String | 
Boolean okOnNothingFound = true; // Boolean | 
try {
    apiInstance.scheduleCheckingForChanges_0(locator, okOnNothingFound);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#scheduleCheckingForChanges_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **okOnNothingFound** | **Boolean**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> VcsRootInstance serveInstance(vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstance result = apiInstance.serveInstance(vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstance**](VcsRootInstance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstanceField"></a>
# **serveInstanceField**
> String serveInstanceField(vcsRootInstanceLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.serveInstanceField(vcsRootInstanceLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#serveInstanceField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstances"></a>
# **serveInstances**
> VcsRootInstances serveInstances(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstances result = apiInstance.serveInstances(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#serveInstances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstances**](VcsRootInstances.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveRootInstanceProperties"></a>
# **serveRootInstanceProperties**
> Properties serveRootInstanceProperties(vcsRootInstanceLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Properties result = apiInstance.serveRootInstanceProperties(vcsRootInstanceLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#serveRootInstanceProperties");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setInstanceField"></a>
# **setInstanceField**
> String setInstanceField(vcsRootInstanceLocator, field, body)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
String field = "field_example"; // String | 
String body = "body_example"; // String | 
try {
    String result = apiInstance.setInstanceField(vcsRootInstanceLocator, field, body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#setInstanceField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **field** | **String**|  |
 **body** | **String**|  | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="setRepositoryState"></a>
# **setRepositoryState**
> Entries setRepositoryState(vcsRootInstanceLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.VcsRootInstanceApi;


VcsRootInstanceApi apiInstance = new VcsRootInstanceApi();
String vcsRootInstanceLocator = "vcsRootInstanceLocator_example"; // String | 
Entries body = new Entries(); // Entries | 
String fields = "fields_example"; // String | 
try {
    Entries result = apiInstance.setRepositoryState(vcsRootInstanceLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling VcsRootInstanceApi#setRepositoryState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vcsRootInstanceLocator** | **String**|  |
 **body** | [**Entries**](Entries.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Entries**](Entries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

