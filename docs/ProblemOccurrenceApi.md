# ProblemOccurrenceApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProblems**](ProblemOccurrenceApi.md#getProblems) | **GET** /app/rest/problemOccurrences | 
[**serveInstance**](ProblemOccurrenceApi.md#serveInstance) | **GET** /app/rest/problemOccurrences/{problemLocator} | 


<a name="getProblems"></a>
# **getProblems**
> ProblemOccurrences getProblems(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProblemOccurrenceApi;


ProblemOccurrenceApi apiInstance = new ProblemOccurrenceApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ProblemOccurrences result = apiInstance.getProblems(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProblemOccurrenceApi#getProblems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**ProblemOccurrences**](ProblemOccurrences.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> ProblemOccurrence serveInstance(problemLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ProblemOccurrenceApi;


ProblemOccurrenceApi apiInstance = new ProblemOccurrenceApi();
String problemLocator = "problemLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ProblemOccurrence result = apiInstance.serveInstance(problemLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProblemOccurrenceApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **problemLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**ProblemOccurrence**](ProblemOccurrence.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

