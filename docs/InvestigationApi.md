# InvestigationApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createInstance**](InvestigationApi.md#createInstance) | **POST** /app/rest/investigations | 
[**createInstances**](InvestigationApi.md#createInstances) | **POST** /app/rest/investigations/multiple | 
[**deleteInstance**](InvestigationApi.md#deleteInstance) | **DELETE** /app/rest/investigations/{investigationLocator} | 
[**getInvestigations**](InvestigationApi.md#getInvestigations) | **GET** /app/rest/investigations | 
[**replaceInstance**](InvestigationApi.md#replaceInstance) | **PUT** /app/rest/investigations/{investigationLocator} | 
[**serveInstance**](InvestigationApi.md#serveInstance) | **GET** /app/rest/investigations/{investigationLocator} | 


<a name="createInstance"></a>
# **createInstance**
> Investigation createInstance(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
Investigation body = new Investigation(); // Investigation | 
String fields = "fields_example"; // String | 
try {
    Investigation result = apiInstance.createInstance(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#createInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Investigation**](Investigation.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Investigation**](Investigation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="createInstances"></a>
# **createInstances**
> Investigations createInstances(body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
Investigations body = new Investigations(); // Investigations | 
String fields = "fields_example"; // String | 
try {
    Investigations result = apiInstance.createInstances(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#createInstances");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Investigations**](Investigations.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Investigations**](Investigations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="deleteInstance"></a>
# **deleteInstance**
> deleteInstance(investigationLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
String investigationLocator = "investigationLocator_example"; // String | 
try {
    apiInstance.deleteInstance(investigationLocator);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#deleteInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **investigationLocator** | **String**|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getInvestigations"></a>
# **getInvestigations**
> Investigations getInvestigations(locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Investigations result = apiInstance.getInvestigations(locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#getInvestigations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Investigations**](Investigations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="replaceInstance"></a>
# **replaceInstance**
> Investigation replaceInstance(investigationLocator, body, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
String investigationLocator = "investigationLocator_example"; // String | 
Investigation body = new Investigation(); // Investigation | 
String fields = "fields_example"; // String | 
try {
    Investigation result = apiInstance.replaceInstance(investigationLocator, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#replaceInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **investigationLocator** | **String**|  |
 **body** | [**Investigation**](Investigation.md)|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Investigation**](Investigation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveInstance"></a>
# **serveInstance**
> Investigation serveInstance(investigationLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.InvestigationApi;


InvestigationApi apiInstance = new InvestigationApi();
String investigationLocator = "investigationLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Investigation result = apiInstance.serveInstance(investigationLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling InvestigationApi#serveInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **investigationLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Investigation**](Investigation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

