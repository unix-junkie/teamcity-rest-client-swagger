# ChangeApi

All URIs are relative to *https://unit-725:8111/bs*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getChangeAttributes**](ChangeApi.md#getChangeAttributes) | **GET** /app/rest/changes/{changeLocator}/attributes | 
[**getChangeDuplicates**](ChangeApi.md#getChangeDuplicates) | **GET** /app/rest/changes/{changeLocator}/duplicates | 
[**getChangeField**](ChangeApi.md#getChangeField) | **GET** /app/rest/changes/{changeLocator}/{field} | 
[**getChangeFirstBuilds**](ChangeApi.md#getChangeFirstBuilds) | **GET** /app/rest/changes/{changeLocator}/firstBuilds | 
[**getChangeIssue**](ChangeApi.md#getChangeIssue) | **GET** /app/rest/changes/{changeLocator}/issues | 
[**getChangeParentRevisions**](ChangeApi.md#getChangeParentRevisions) | **GET** /app/rest/changes/{changeLocator}/parentRevisions | 
[**getChangeVCSRoot**](ChangeApi.md#getChangeVCSRoot) | **GET** /app/rest/changes/{changeLocator}/vcsRoot | 
[**getChangeVCSRootInstance**](ChangeApi.md#getChangeVCSRootInstance) | **GET** /app/rest/changes/{changeLocator}/vcsRootInstance | 
[**getParentChanges**](ChangeApi.md#getParentChanges) | **GET** /app/rest/changes/{changeLocator}/parentChanges | 
[**getRelatedBuildTypes**](ChangeApi.md#getRelatedBuildTypes) | **GET** /app/rest/changes/{changeLocator}/buildTypes | 
[**serveChange**](ChangeApi.md#serveChange) | **GET** /app/rest/changes/{changeLocator} | 
[**serveChanges**](ChangeApi.md#serveChanges) | **GET** /app/rest/changes | 


<a name="getChangeAttributes"></a>
# **getChangeAttributes**
> Entries getChangeAttributes(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Entries result = apiInstance.getChangeAttributes(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Entries**](Entries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeDuplicates"></a>
# **getChangeDuplicates**
> Changes getChangeDuplicates(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Changes result = apiInstance.getChangeDuplicates(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeDuplicates");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Changes**](Changes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeField"></a>
# **getChangeField**
> String getChangeField(changeLocator, field)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String field = "field_example"; // String | 
try {
    String result = apiInstance.getChangeField(changeLocator, field);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeField");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **field** | **String**|  |

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeFirstBuilds"></a>
# **getChangeFirstBuilds**
> Builds getChangeFirstBuilds(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Builds result = apiInstance.getChangeFirstBuilds(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeFirstBuilds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Builds**](Builds.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeIssue"></a>
# **getChangeIssue**
> Issues getChangeIssue(changeLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
try {
    Issues result = apiInstance.getChangeIssue(changeLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeIssue");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |

### Return type

[**Issues**](Issues.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeParentRevisions"></a>
# **getChangeParentRevisions**
> Items getChangeParentRevisions(changeLocator)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
try {
    Items result = apiInstance.getChangeParentRevisions(changeLocator);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeParentRevisions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |

### Return type

[**Items**](Items.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeVCSRoot"></a>
# **getChangeVCSRoot**
> VcsRootInstance getChangeVCSRoot(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstance result = apiInstance.getChangeVCSRoot(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeVCSRoot");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstance**](VcsRootInstance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getChangeVCSRootInstance"></a>
# **getChangeVCSRootInstance**
> VcsRootInstance getChangeVCSRootInstance(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    VcsRootInstance result = apiInstance.getChangeVCSRootInstance(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getChangeVCSRootInstance");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**VcsRootInstance**](VcsRootInstance.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getParentChanges"></a>
# **getParentChanges**
> Changes getParentChanges(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Changes result = apiInstance.getParentChanges(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getParentChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Changes**](Changes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getRelatedBuildTypes"></a>
# **getRelatedBuildTypes**
> BuildTypes getRelatedBuildTypes(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    BuildTypes result = apiInstance.getRelatedBuildTypes(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#getRelatedBuildTypes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**BuildTypes**](BuildTypes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveChange"></a>
# **serveChange**
> Change serveChange(changeLocator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String changeLocator = "changeLocator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Change result = apiInstance.serveChange(changeLocator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#serveChange");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **changeLocator** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Change**](Change.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="serveChanges"></a>
# **serveChanges**
> Changes serveChanges(project, buildType, build, vcsRoot, sinceChange, start, count, locator, fields)



### Example
```java
// Import classes:
//import org.jetbrains.teamcity.rest.client.swagger.ApiException;
//import org.jetbrains.teamcity.rest.client.api.ChangeApi;


ChangeApi apiInstance = new ChangeApi();
String project = "project_example"; // String | 
String buildType = "buildType_example"; // String | 
String build = "build_example"; // String | 
String vcsRoot = "vcsRoot_example"; // String | 
String sinceChange = "sinceChange_example"; // String | 
Long start = 789L; // Long | 
Integer count = 56; // Integer | 
String locator = "locator_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Changes result = apiInstance.serveChanges(project, buildType, build, vcsRoot, sinceChange, start, count, locator, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ChangeApi#serveChanges");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project** | **String**|  | [optional]
 **buildType** | **String**|  | [optional]
 **build** | **String**|  | [optional]
 **vcsRoot** | **String**|  | [optional]
 **sinceChange** | **String**|  | [optional]
 **start** | **Long**|  | [optional]
 **count** | **Integer**|  | [optional]
 **locator** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Changes**](Changes.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

